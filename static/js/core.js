var cm = cm || {};

cm.collapsible = function(){
    $$('.collapsible').each(function(el){

        var ev = function(event){
            if ( event ) event.stop();
            el.getElement('.inner').slide();
        };

        el.adopt(
            new Element('div').addClass('collapsible-toggle').set('html', '&mdash;').set('title', 'Toggle Visibility').addEvent('click', ev)
        );
    });
};

cm.spinner = {
    start: function(){},
    stop: function(){}
};

window.addEvent('domready', function(){
    cm.collapsible();
});
