var cm = cm || {};
cm.users = {};

var pi = Math.PI,
    angles = [
     0,
     45,
     90,
     135,
     180,
     225,
     270,
     315, // end first level
     0,
     45,
     90,
     135,
     180,
     225,
     270,
     315,
     22.5, // below here is 45*i - (45/2) [22.5]
     67.5,
     112.5,
     157.5,
     202.5,
     247.5,
     292.5,
     340
    ];

cm.setup_downline = function( info, page ) {

    $$('.dlimg').setStyle('display', 'none');
    $(cm.current_center).setStyle('display', 'block');

	page = page || 1;

	var pos = $(cm.current_center).getPosition( $('gen') ),
	    per_page = angles.length,
	    skip_amount = (page - 1) * per_page;

	$each(info.downline, function(dl, i){
		if ( i < skip_amount ) { return; }
		else { i = i - skip_amount; }

        if ( i >= per_page ) { // we stop here too
            return;
        }

		var a = angles[i] ? ( angles[i] / 57.2957795 ) : 0, // 57.2957795 = 180/pi
		    radius = i <= 7 ? 360/2 : 610/2,
		    x = radius * Math.cos(a),
		    y = radius * Math.sin(a),
            el = cm.downline_elements( dl );

		// Check to see if this is the max per page
		if ( i >= per_page ) return;

		el.setStyle('display', 'block').setStyles({
			'left': (pos.x + x) + 'px',
			'top' : (pos.y + y) + 'px'
		});
	});
}

/**
 * Creates the elements for the graphical view
 */
cm.downline_elements = function( info, current ){
    var src = cm.urls.static_url + ( current ? 'images/gen.png' : 'images/gen_fade.png'),
        e = new Element('div');

    e.addClass('dlimg');
    e.adopt(
		element = new Element('img', {
			'class': 'person small',
			'src': src,
			'title': info.name,
			'alt': info.name
		}),
		new Element('h3').set('text', info.name)
	).inject( $('gen') ); // inject into genealogy holder

    if ( current ) {
        if ( info.more ) {
            e.adopt(
                new Element('p').set('html', info.more).addClass('more')
            );
        }

        if ( info.user_info ) {
            $$('#user_info').set('html', info.user_info)
        }

        e.addClass('open');
        element.removeClass('small');

        e.position({ relativeTo: $('gen') });

        cm.current = info.id;
        cm.current_center = e;
        //console.log(e);
    }

    element.addEvent('click', function(event){
        if ( event ) event.stop();
        if ( cm.request && cm.request.running ) return false;

        if ( cm.current == info.id ) {
            if ( info.upline_id ) {
                cm.downline( info.upline_id );
	    		return false;
            }

            return false;
		}

        cm.downline( info.id );
	});

    return e;
};


/**
 * On the display of a requested user.
 */
cm.downline_load_success = function( info ){

    // Delete all current downline icons
    $$('.dlimg').destroy();

    // Update title of page
    $('gen_title').set('html', 'Downline of ' + info.name);

    // Update counter
    if ( info.downline && info.downline.length ) {
        $('count_and_title').set('html', 'There are <strong>' + info.downline.length + '</strong> frontline IBOs under ' + info.name);
    } else {
        $('count_and_title').set('html', 'You currently have no frontline IBOs under ' + info.name);
    }

    // Create the elements for this user
    cm.downline_elements( info, true );

    // Create downline icons around center man
    cm.setup_downline( info );

    // Reset paginator
    $('paginator_current').set('text', '1');
    if ( info.downline && info.downline.length ) {
        $('paginator_max').set('text', Math.ceil( info.downline.length / angles.length ));
    } else {
        $('paginator_max').set('text', 1);
    }
};

/**
 * Load the downline information for the given
 * user_id, if we do not have that information
 * cached.
 */
cm.downline = function( user_id ){

    // check cache
    if ( cm.users[ user_id ] ) {
        // process
        return cm.downline_load_success( cm.users[user_id] );
    }

    // request info if not in cache
    cm.request = new Request.JSON({
        'method': 'post',
        'headers': {
            'X-CSRFToken': cm.cookie.read()
        },
        'url': cm.urls.gen_ajax,
        'data': {
            'user_id': user_id
        },
        'onRequest': function(){
            cm.spinner.start();
        },
        'oComplete': function(){
            cm.spinner.end();
        },
        'onSuccess': function(info){
            // add to cache
            cm.users[ user_id ] = info;

            // process
            cm.downline_load_success(info);
        },
        'onFailure': function(){
            alert('An error occurred.');
        }
    }).send();
};

/**
 * Enables use of Prev and Next page buttons on Genealogy.
 */
cm.downline.paginator = function(){
	$('paginatorPrev').addEvent('click', function(event){
		event.stop();
		var page = $('paginator_current').get('text').toInt() - 1,
		max = $('paginator_max').get('text').toInt();

		if ( page > 0 && page <= max ) {
			$('paginator_current').set('text', page);
			cm.setup_downline( cm.users[ cm.current ], page );
		}
	});

	$('paginatorNext').addEvent('click', function(event){
		event.stop();
		var page = $('paginator_current').get('text').toInt() + 1,
		max = $('paginator_max').get('text').toInt();

		if ( page > 0 && page <= max ) {
			$('paginator_current').set('text', page);
			cm.setup_downline( cm.users[ cm.current ], page );
		}
	});
};

window.addEvent('domready', function(){
    cm.cookie = new Cookie('csrftoken');

    cm.downline( cm.user_id );
	cm.downline.paginator();
});
