
var Lightbox = new Class({

    initialize: function(){
        this.overlay = new Element('div').addClass('lb overlay');
        this.box = new Element('div').addClass('lb lightbox');
        this.box.set('html', 'Loading form&hellip;');
        document.body.adopt([this.overlay, this.box]);

        this.overlay.addEvent('click', this.close.bindWithEvent(this));
    },

    ajax: function(url, data){
        cm.ajax.get(url, data, function(html){
            this.box.set('html', html);
            this.box.getElements('form').each(function(el){
                el.addEvent('submit', function(event){
                    event.stop();

                    cm.ajax.post(url, $H(data).toQueryString() + '&' + el, function(json){
                        if(json.success){
                            this.close();
                            human_msg.display_msg('Successfully updated customer profile.');
                        } else {
                            console.log(json);
                            human_msg.display_msg('There appears to be an error in the data.');
                        }
                    });

                    return false;
                });
            }.bind(this));
        }.bind(this), false);
    },

    close: function(){
        this.overlay.destroy();
        this.box.destroy();
    }

});
