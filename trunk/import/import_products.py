import os, sys
import csv
import re
from decimal import *

path = os.path.join(os.path.dirname(__file__), '..')
if path not in sys.path:
        sys.path.append(path)

os.environ['PYTHON_EGG_CACHE'] = '/home/ben/.python-eggs'
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'


#from django.contrib.auth.models import User
from django.db import connection, transaction
from shop.models import Product


def import_products():
	cursor = connection.cursor()
	cursor.execute("SELECT products_id, products_quantity, products_model, products_price, products_price_w FROM products")

	for row in cursor.fetchall():
		desc_cursor = connection.cursor()
		desc_cursor.execute("SELECT products_name, products_description FROM products_description WHERE products_id = %s", [ row[0] ])
		product_description = desc_cursor.fetchone()

		print "Importing", product_description[0] #.product_name

		product = Product(
			name = product_description[0], #product_name,
			description = product_description[1], #.product_description,
			product_id = row[0], #products_id,
			allow_add_to_card = True,
			virtual = False,
			quantity = float( row[1] ),
			model = "",#row[2],
			weight = 0,
			price = Decimal( row[3] ),
			price_w = row[4],
		)

		product.save()

	return row


if __name__ == '__main__':
	import_products()
