import os, sys
import csv
import re
from decimal import *
import datetime
import time

path = os.path.join(os.path.dirname(__file__), '..')
if path not in sys.path:
        sys.path.append(path)

os.environ['PYTHON_EGG_CACHE'] = '/home/ben/.python-eggs'
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.contrib.auth.models import User
from django.db import connection, connections, transaction
from marvin.core.accounts.models import Profile

from marvin.core.contacts.models import Contact, PhoneNumber, AddressBook
#from shop.models import Product

address_book_sql = '''SELECT
    entry_firstname,
    entry_lastname,
    entry_company as company,
    entry_street_address as street_address,
    entry_suburb as suburb,
    entry_city as city,
    entry_postcode as postcode,
    entry_state as state,
    entry_zone_id as zone_id,
    entry_country_id as country_id
    FROM   address_book
    WHERE  customers_id = %s
    '''

def import_users():
    cursor = connection.cursor()
    cursor.execute("SELECT DISTINCT customers_id, customers_firstname, customers_lastname, customers_email_address, customers_dob, customers_telephone, customers_fax, customers_gender, customer_ibo FROM customers JOIN LTM_CustAddInfo USING(customers_id)")

    for row in cursor.fetchall():
        customer_id = int( row[0] )
        ibo_number = int( row[8] )

        print "Importing User IBO #", ibo_number

        try:
            dob = time.strptime(row[4], '%Y-%m-%d 00:00:00')
            dob = datetime.datetime(*dob[:6]).date()
        except:
            dob = None
       
        u = User(
            username = ibo_number,
            first_name = row[1],
            last_name = row[2],
            email = row[3],
        )

        u.set_password('password')
        try:
            u.save()
        except:
            print 'Continuing, already exists.'
            continue # already exists

        profile = Profile(
           user = u,
           ibo_number = int( ibo_number ),
           gender = row[7].upper()
        )
        
        profile.save()

        if len(row[5]):
            try:
                PhoneNumber(
                    user = u,
                    type = 'Home',
                    phone = row[5],
                    is_primary = True,
                ).save()
            except:
                pass

        if len(row[6]):
            try:
                PhoneNumber(
                    user = u,
                    type = 'Fax',
                    phone = row[6],
                ).save()
            except:
                pass

        cursor.execute(address_book_sql, [customer_id])
        for address in cursor.fetchall():
            Contact(
                user = u,
                first_name = address[0],
                last_name = address[1],
                dob = dob,
                email = row[3],
                is_primary = True,
                relationship = 'self',
            ).save()

            AddressBook(
                user = u,
                addressee = '%s %s' % (address[0], address[1]),
                street1 = address[3],
                street2 = address[4],
                state = address[7],
                city = address[5],
                postal_code = address[6],
                country_id = 1
            ).save()
            break

if __name__ == '__main__':
    import_users()
