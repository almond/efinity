"""
Base urls used by Satchmo.

Split out from urls.py to allow much easier
overriding and integration with larger apps.
"""
from django.conf.urls.defaults import *

from marvin.store.products.urls.base import adminpatterns as prodpatterns
from marvin.store.shipping.urls import adminpatterns as shippatterns
from marvin.core.dashboard.views import products_nav
from django.conf import settings


urlpatterns = patterns('',
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^products$', products_nav, {}, 'products_nav'),
    (r'^accounts/', include('marvin.core.accounts.urls')),
    (r'^settings/', include('marvin.utils.dbsettings.urls')),
    (r'^cache/', include('marvin.utils.keyedcache.urls')),
    (r'^i18n/', include('django.conf.urls.i18n')),
    (r'^trans/', include('marvin.trans.urls')),
    (r'^cms/', include('marvin.core.cms.urls')),
    (r'^user/dashboard/', include('marvin.core.dashboard.urls')),
    (r'^user/subscriptions/', include('marvin.store.products.modules.subscription.urls')),
    (r'^user/reports/', include('marvin.addons.reports.urls')),
    (r'^user/support/', include('marvin.addons.support.urls')),
    (r'^user/genealogy/', include('marvin.addons.genealogy.urls')),
    (r'^user/compensation/', include('marvin.addons.compensation.urls')),
    (r'^user/events/', include('marvin.addons.planning.urls')),
    (r'^user/notifications/', include('marvin.core.notifications.urls')),
    (r'^user/messages/', include('marvin.addons.messages.urls')),
    (r'^api/', include('marvin.api.urls')),
    (r'^r/', include('marvin.addons.replicate.urls')),
) + prodpatterns + shippatterns


if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^500/$', 'django.views.defaults.server_error'),
    )
