from django.db import models
from django.db.models import options
from django.db.models.query import QuerySet

options.DEFAULT_NAMES += ('db_name',)

class MultiDBManager(models.Manager):
	def get_query_set(self):
		db_name = getattr(self.model._meta, 'db_name', None)
		return QuerySet(self.model, using=db_name)

	@staticmethod
	def get_db_connection(model):
		db_name = getattr(model._meta, 'db_name', None)
		if not db_name is None:
			from django.db import connections
			return connections[db_name]
		else:
			from django.db import connection
			return connection


class Customer(models.Model):
    '''
    This Customer model handles the storage of Almond
    Development Customers and their primary contact
    information.
    '''
    objects = MultiDBManager()

    class Meta:
        db_name = 'almond'


