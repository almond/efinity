from django.db import transaction, DatabaseError
from django.utils.translation import ugettext_lazy as _
from marvin.utils.dbsettings import * 
from marvin.store.tax.config import TAX_MODULE

TAX_MODULE.add_choice(('marvin.store.tax.modules.area', _('By Country/Area')))
TAX_GROUP = config_get_group('TAX')
        
config_register(
     BooleanValue(TAX_GROUP,
         'TAX_SHIPPING',
         description=_("Tax Shipping?"),
         requires=TAX_MODULE,
         requiresvalue='marvin.store.tax.modules.area',
         default=False)
)

config_register(
     StringValue(TAX_GROUP,
         'TAX_CLASS',
         description=_("TaxClass for shipping"),
         help_text=_("Select a TaxClass that should be applied for shipments."),
         #TODO: [BJK] make this dynamic - doesn't work to have it be preloaded.
         default='Shipping'
     )
)

