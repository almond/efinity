import logging
from marvin.utils.templatetags import get_filter_args
from django import template
from marvin.store.shop.models import Cart, NullCart
from django.contrib.contenttypes.models import ContentType

log = logging.getLogger("add_based_on_category")
register = template.Library()

@register.tag
def add_based_on_category(parser, token):
    nodelist=parser.parse(('end_add_based_on_category',))
    parser.delete_first_token()
    try:
        tag_name, request, product=token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires exactly two arguments" % token.contents.split()[0])


    return AddBasedOnCategory(request, product, nodelist)

class AddBasedOnCategory(template.Node):
    def __init__(self, request, product, nodelist):
        self.request=template.Variable(request)
        self.product=template.Variable(product)
        self.nodelist=nodelist

    def render(self, context):
        request=self.request.resolve(context)
        product=self.product.resolve(context)
        
        output=self.nodelist.render(context)
        cart=Cart.objects.from_request(request=request, create=False)
        if cart.__class__==NullCart:
           return output
        elif cart.is_empty:
           return output
        else:
           cart_items=cart.cartitem_set.all()
           item=cart_items[0]
           if item.product.is_product == product.is_product:
               return output
           
        return '<span style="color:red">You cannot have both tickets and tools in the same cart. Please check out the current cart and purchase this item again.</span>'


@register.tag
def is_convention_cart(parser, token):
    '''
    Returns whether a cart is a convention cart or not
    Intended to be used in javascript
    {% is_convention_cart cart %}
    '''
    try:
        tag_name, cart=token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires exactly two arguments" % token.contents.split()[0])
    
    return IsConventionCart(cart)

class IsConventionCart(template.Node):
    def __init__(self, cart):
        self.cart=template.Variable(cart)
    
    def render(self, context):
        cart = self.cart.resolve(context)
        cart_items=cart.cartitem_set.all()
        item=cart_items[0]
        if item.product.is_product:
            return "false"
        else:
            return "true"