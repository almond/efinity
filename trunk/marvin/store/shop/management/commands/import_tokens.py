from marvin.store.shop.models import Order, OrderItem, OrderPayment
from marvin.store.products.models import Product
from marvin.core.accounts.models import UserCIM
from marvin.store.payments.models import CIMPaymentProfileToOrderPayment

from django.contrib.auth.models import User
from django.db import connection, transaction
from django.core.management.base import NoArgsCommand

class Command(NoArgsCommand):
    help = ("Invokes recurring billing system to do stuff like "
            "charge subscription customers each month.  You typically "
            "want to invoke this from a cron script.  For non-root "
            "users this is generally done with ``crontab -e``.")

    def handle_noargs(self, **options):
        cursor = connection.cursor()
        
        sql_text = 'Select * from temp_efinity_tokens'
        cursor.execute(sql_text)
        
        for row in cursor.fetchall():
            ibo_num = row[0]
            customer_profile_id = row[1]
            payment_profile_id = row[2]
            
            user = User.get_by_aid(ibo_num)
            if not user:
                # Could not find the user in our system
                continue
            
            # Set the users CIM info
            try:
                user_cim = user.cim
            except UserCIM.DoesNotExist:
                user_cim = UserCIM()
                user_cim.user = user            
                user_cim.customer_profile_id = customer_profile_id
                user_cim.save()
            
            # Associate any orders that were imported with the payment profile
            os = Order.objects.filter(notes='Automatic import of subscription from Database table', user=user)
            for order in os:
                payment = order.payments.all()[0]
                print 'OP:%s | PP:%s' % (payment, payment_profile_id)
                try:
                    pp_op = payment.cim_payment_profile
                except CIMPaymentProfileToOrderPayment.DoesNotExist:
                    pp_op = CIMPaymentProfileToOrderPayment()
                    pp_op.payment_profile_id = payment_profile_id
                    pp_op.orderpayment = payment
                    pp_op.save()
            
    
