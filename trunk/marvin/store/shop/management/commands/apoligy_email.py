# -*- coding: utf-8 -*-
from django.core.mail import send_mass_mail
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
	email_text_english = """
	Estimado y valioso Empresario Independiente,
	Por la presente eFinity le pide disculpas por la confusión creada con un mensaje incorrecto que muchos de ustedes 
	recibieron durante el fin de semana.  Estamos trabajando con nuestro equipo de desarrollo y le pedimos disculpas por el
	inconveniente que esto ha creado. Por favor vea el mensaje a continuación con la explicación sobre lo que ocurrió. 
	No es necesario comunicarse con la oficina, estamos conscientes del problema y trabajando para corregirlo.

	Sinceramente,
	Ken Woods
	VP eFinity


	Estimado Empresario de eFinity,
	Durante el fin de semana, nuestros programadores estuvieron trabajando para actualizar la base de datos y el 
	programa de eFinity con la intención de mejorar el formato y apariencia de los reportes. 
	Sin embargo, según el sistema absorbió los nuevos códigos de programación, se generó un mensaje electronico incorrecto a 
	todos los suscritos al paquete del mes. 
	En lugar de enviarle el recibo normal por el mes individual de su orden, el sistema generó un mensaje que le muestra el total de todas
	las suscripciones que han sido facturadas desde el comienzo de su suscripción hasta el mes actual.

	El mensaje que usted recibió no es una factura actual de lo que le fue cobrado; fue un detalle de la historia de datos solamente y 
	fue enviado por error como una factura con un total de toda la historia y parece una sola orden.
	Normalmente, usted recibe un mensaje del personal de eFinity, sin embargo, yo quería enviarles 
	personalmente una explicación y disculpa a nombre Almond Development, 
	ya que somos el equipo de desarrollo de programas que trabaja con eFinity. 
	Le pedimos la más sincera disculpa por la confusión que esto causó.
	Estamos trabajando para enviarle el recibo correcto por la transacción correspondiente a su suscripción de este mes.
	Gracias por su paciencia según el personal de Almond Development, trabaja a nombre de eFinity para asegurar que este error no se repita.

	Sinceramente,

	Shawn A. Pinkston
	CEO
	Almond Development LLC
	205 26th St. Suite 23
	Ogden, UT 84401
	801.781.0952
	"""

	subject_text_english = "Correo electrónico de eFinity acerca de facturación incorrecta"

	def handle(self, *args, **kwargs):
		# Prepare the email tuple
		english_users = User.objects.filter(profile_object__language__icontains="es").distinct()
		# messages = (("TESTING", "WHATEVER CONTENT AND STUFF", "IBOsupport@myefinity.com", ["mario.r.vallejo@gmail.com"]),)
		messages = tuple()
		for u in english_users:
			if u.subscriptions():
				print "Adding IBO: %s to the list" % u.username
				messages += ((self.subject_text_english, self.email_text_english, "IBOsupport@myefinity.com", [u.email]),)

		send_mass_mail(messages, fail_silently=True)