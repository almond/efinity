from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site
from django.contrib.auth.models import User

from marvin.store.shop.models import Order, OrderItem, OrderPayment
from marvin.store.products.models import Product
from marvin.core.contacts.models import AddressBook
from datetime import datetime
from marvin.utils import add_month

product_map = {
    'PEC Tradic': 103,
    'Registered': None,
    'Libro del ': 71,
    'Book of th': 104,
    'Digital CE': 40,
    'Traditiona': 39,
    'Island CEP': None,
}

class Command(BaseCommand):
    help = "Imports subscriptions"
    args = ['ibo_numbers...']

    def handle(self, *ibo_numbers, **options):
        from django.db import connection, transaction
        cursor = connection.cursor()
        verbosity = int(options.get('verbosity', 1))
        if len(ibo_numbers) == 0:
            sql_text = 'Select * from ltm_imported_subscriptions'
            cursor.execute(sql_text)
        else:
            ibo_nums = []
            for number in ibo_numbers:
                ibo_nums.append(number)
            
            sql_text = 'Select * from ltm_imported_subscriptions where customer_ibo in %s'
            cursor.execute(sql_text, [ibo_nums])
        
        
        for row in cursor.fetchall():
            ibo_num = row[2]
            item = row[3][:10]
            
            prod_id = product_map[item]
            if prod_id:
                user = User.get_by_aid(ibo_num)
                
                # Could not find the user in our system
                if user == None:
                    continue
                
                try:
                    # Try to get the product
                    product = Product.objects.get(id=prod_id)
                except:
                    continue

                try:
                    # If there is already a subscription in the DB, we do not need a new one
                    order = OrderItem.objects.get(order__user=user, product=product)
                    break
                except:
                    # No order found, we need to create one
                    temp = 'temp'
                
                # Fix the user address, if necessary
                if not user.shipping_address and not user.billing_address:
                    ab = AddressBook()
                    ab.user = user
                    ab.addressee = user.get_full_name
                    ab.street1 = '1 fake st'
                    ab.state = 'Utah'
                    ab.city = 'Fakeville'
                    ab.postal_code = '12345'
                    ab.country = 1
                    ab.is_default_billing = True
                    ab.is_default_shipping = True
                    ab.save()
                
                if not user.shipping_address:
                    if user.billing_address:
                        address = user.billing_address
                        address.is_default_shipping = True
                        address.save()
                if not user.billing_address:
                    if user.shipping_address:
                        address = user.shipping_address
                        address.is_default_billing = True
                        address.save()
                
                
                site = Site.objects.get_current()
                order = Order()
                order.site = site
                order.user = user
                order.notes = 'Automatic import of subscription from Database table'
                
                order.sub_total = product.unit_price
                if product.is_shippable:
                    order.total = order.sub_total + 4
                    order.shipping_description = "Per Item shipping"
                    order.shipping_method = "U.S. Mail"
                    order.shipping_model = "PerItem"
                    order.shipping_cost = 4
                else:
                    order.total = order.sub_total
                order.method = "Imported"
                order.save()
                
                order.add_status("Shipped")
                
                # ---Order Item ---#
                oi = OrderItem()
                
                oi.order = order
                oi.product = product
                oi.quantity = 1
                oi.unit_price = product.unit_price
                oi.line_item_price = product.unit_price
                oi.unit_tax = 0
                oi.completed = True
                oi.expire_date = add_month(datetime.now(), n=1)
                oi.expire_length = 1
                
                oi.save()
                
                # ---Order Payment ---#
                op = OrderPayment()
                op.order = order
                op.payment = 'AUTHORIZENET'
                op.amount = product.unit_price
                op.save()   
            