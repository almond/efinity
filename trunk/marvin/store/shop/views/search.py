from django.db.models import Q
from django.shortcuts import render_to_response
from django.template import RequestContext

from marvin.store.products.models import Product
from marvin.store.shop import signals
from marvin.store.products.util import paginate
from marvin.utils.signals_ahoy.signals import application_search

def search_view(request, template="shop/search.html"):
    """
    Perform a search based on keywords and
    categories in the form submission
    
    """
    page = int(request.GET.get('page', '1'))
    search = request.GET.get('q', '')
    category = request.GET.get('category', None)

    products = Product.objects.active_by_site().filter(
        Q(name__icontains=search) | Q(description__contains=search)
    )
    
    if category:
        products = products.filter(category__name=category)
      
    product_list = paginate(products, page, 6)

    context = RequestContext(request, {
            'product_list': product_list,
            'category' : category,
            'search' : search})
    return render_to_response(template, context_instance=context)
