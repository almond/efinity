from marvin.utils.view import *
from marvin.store.shop.models import Order
from marvin.utils.dbsettings import config_value 

@login_required
def order_history(request):
    orders = Order.objects.filter(user=request.user).order_by('-time_stamp').exclude(status="")

    return render(request, 'shop/order_history.html', {
        'default_view_tax': config_value('TAX', 'DEFAULT_VIEW_TAX'),
        'orders' : orders,
        })

@login_required
def order_tracking(request, order_id):
    order = get_object_or_404(Order, id__exact=order_id, user=request.user)

    return render(request, 'shop/order_tracking.html', {
        'default_view_tax': config_value('TAX', 'DEFAULT_VIEW_TAX'),
        'order' : order,
        })
