from django.core.paginator import Paginator, EmptyPage
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required


from marvin.utils.dbsettings import config_value
from marvin.store.products.views import display_featured
from marvin.store.products.util import paginate
from marvin.utils.views import bad_or_missing
from marvin.store.products.models import Product

@login_required
def home(request, template="shop/index.html"):
    # Display the category, its child categories, and its products.
    
    featured = display_featured()
    featured_list = paginate(featured, 1, 5)
    
    page = int(request.GET.get('page', '1'))
    products = Product.objects.active_by_site()
    product_list = paginate(products, page, 6)

    return render_to_response(template, {
        'featured_list' : featured_list,
        'product_list': product_list,
    }, context_instance=RequestContext(request))
    
