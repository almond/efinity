import shipper
from marvin.utils.dbsettings import config_choice_values

def get_methods():
    return [shipper.Shipper(service_type=value) for value in config_choice_values('marvin.store.shipping.modules.ups', 'UPS_SHIPPING_CHOICES')]
    