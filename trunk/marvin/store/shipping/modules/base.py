class BaseShipper(object):
    def __init__(self, cart=None, user=None):
        self._calculated = False
        self.cart = cart
        self.user = user 
        self._calculated = False
        
        if cart or user:
            self.calculate(cart, user)
        
    def calculate(self, cart, user):
        """
        Perform shipping calculations, separated from __init__ so that the object can be 
        used for keys and labels more easily.
        """
        self.cart = cart
        self.user = user
        self._calculated = True
