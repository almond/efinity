from django.template import Library, Node
from marvin.store.products.models import Product, Category

class CategoryCountOutput(Node):
    def render(self, context):
        count = Category.objects.all().count()
        return "%s" % (count)

class ProductCountOutput(Node):
    def render(self, context):
        count = Product.objects.all().count()
        return "%s" % (count)

def do_print_category_count(parser, token):
    return CategoryCountOutput()

def do_print_product_count(parser, token):
    return ProductCountOutput()

register = Library()     
register.tag('category_count', do_print_category_count)
register.tag('product_count', do_print_product_count)
