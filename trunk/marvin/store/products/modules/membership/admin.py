from django.contrib import admin
from marvin.store.products.modules.membership.models import MembershipProduct, Trial

class Trial_Inline(admin.StackedInline):
    model = Trial
    extra = 2


class MembershipProductOptions(admin.ModelAdmin):
    inlines = [Trial_Inline]

admin.site.register(MembershipProduct, MembershipProductOptions)

