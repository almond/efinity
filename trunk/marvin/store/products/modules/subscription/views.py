from marvin.utils.view import *
from marvin.store.products.modules.subscription.models import SubscriptionProduct
from marvin.store.shop.models import OrderItem
from django.http import Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse


import datetime

def manage(request):
    '''
    Allows a user to manage their subscriptions.
    '''
    
    available_subs = SubscriptionProduct.objects.filter(pk__in=(103, 39, 40, ))

    return render(request, 'subscription/manage.html', {"available_subs": available_subs})

def remove(request):
    '''
    Removes a subscription.
    '''
    if request.POST:
        oi_id = request.POST.get('orderitem', None)
        if oi_id:
            oi_id = int(oi_id)
            oi = OrderItem.objects.get(id=oi_id)
            oi.completed = False
            oi.expire_date = datetime.date.today()
            oi.save()
    return HttpResponseRedirect(reverse('manage_subscriptions'))
