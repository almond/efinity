from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.store.products.modules.subscription.views',

	(r'^manage/$', 'manage', {}, 'manage_subscriptions'),
    (r'^remove/$', 'remove', {}, 'remove_subscription'),
)
