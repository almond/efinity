from django.contrib import admin
from marvin.store.products.modules.downloadable.models import DownloadableProduct, DownloadLink

admin.site.register(DownloadableProduct)
admin.site.register(DownloadLink)

