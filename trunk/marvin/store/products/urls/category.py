from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.store.products.views',
    (r'^(?P<parent_slugs>([-\w]+/)*)?(?P<slug>[-\w]+)/$',
        'category_view', {}, 'shop_category'),
    (r'^$', 'category_index', {}, 'shop_category_index'),
)
