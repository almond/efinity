from category import urlpatterns as catpatterns
from django.conf.urls.defaults import *
from marvin.store.products.urls.products import urlpatterns as prodpatterns
from marvin.store.shop import get_satchmo_setting

catbase = r'^' + get_satchmo_setting('CATEGORY_SLUG') + '/'
prodbase = r'^' + get_satchmo_setting('PRODUCT_SLUG') + '/'

urlpatterns = patterns('',
    (prodbase, include('marvin.store.products.urls.products')),
    (catbase, include('marvin.store.products.urls.category')),
)
