# import csv
from marvin import unicodecsv
import ftplib
from datetime import date
from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order
from django.db.models import Q
from marvin.addons.planning.models import Convention
from django.conf import settings
from decimal import Decimal

import datetime

class Command(BaseCommand):
    help = 'Generates CSV order sheet to transmit to fulfillment'

    # Now set a backup server. This server should also be contacted upon file generation
    if settings.DEBUG:
        local_parent = '/home/axolote/workspace/efinity/'
    else:
        local_parent = '/home/mtp/'

    def handle(self, *args, **options):
        today = date.today()
        filename = today.strftime('%Y-%m-%d') + '.csv'

        orders = []
        f = open(self.local_parent + 'daily/' + filename, 'wb')
        writer = unicodecsv.writer(f, encoding='utf-8', quoting=unicodecsv.QUOTE_ALL)
        writer.writerow([
            'Order #',
            'Shipping Method',
            'First Name',
            'Last Name',
            'Address 1',
            'Address 2',
            'City',
            'State',
            'Postal Code',
            'Country',
            'Phone Number',
            'Gift Message or Order Notes',
            'Product SKU',
            'Quantity',
            'Unit Weight',
            'Product Description',
            'Unit Price',
            'Pick Location (bin location)',
            'Carrier Shipper',
            'Insured Value',
            'Delivery Confirmation',
            'Signature Confirmation',
            'Adult Signature Confirmation',
            'COD_Indicator',
            'COD_Amount',
            'Billed Shipping Cost',
            'Tax Billed to Customer',
            'Customer Number (IBO Number)',
            'Order Time',
            'Current Month',
            'Language',
            'EMAIL',
            'Team',
        ])

        # go through all order's that are shippable and
        # not shipped

        yesterday = datetime.date.today() - datetime.timedelta(days=1)

        for order in Order.objects.filter(time_stamp__gte=yesterday).distinct():

            if order.balance > Decimal("0.00"):
                continue

            for orderitem in order.orderitem_set.all():
                # Test for convetion since we do not check for shippable above
                # check if it points to no product at all
                if not orderitem.product:
                    continue

                if type(orderitem.product) == Convention:
                    continue

                # Here test to see if the orderitem is subscription. If so,
                # Make sure this orderitem was ordered within the past 25 days.
                # Usually it will be picked up the next day.
                if hasattr(orderitem.product, 'is_subscription') and orderitem.product.is_subscription:
                    if orderitem.order.time_stamp.day >= 25:
                        continue

                # Test for certain product sku's that should not be in the csv
                # Event though non-shippable orders should be excluded as per the
                # order.is_shippable above...
                if orderitem.product.sku == 'subscription-digital':
                    continue

                shipping_method = ''
                shipping_carrier = ''
                # if ups, set correct shipping carrier, if no method, default to USPS
                if order.shipping_method == 'UPS':
                    shipping_method = 'UPS Ground'
                    shipping_carrier = 'UPS Online'
                else:
                    shipping_method = 'Priority Mail'
                    shipping_carrier = 'Express 1'

                product_weight = ''
                if orderitem.product.weight:
                    product_weight = (orderitem.product.weight * orderitem.quantity)

                order_notes = ''
                if order.notes:
                    order_notes = order.notes

                language = ''
                if orderitem.product.product_language == 'en':
                    language = 'english'
                elif orderitem.product.product_language == 'es':
                    language = 'spanish'
                writer.writerow([
                    order.id,
                    shipping_method,
                    order.user.first_name,
                    order.user.last_name,
                    order.ship_street1,
                    order.ship_street2,
                    order.ship_city,
                    order.ship_state,
                    order.ship_postal_code,
                    order.ship_country_name,
                    '',
                    order_notes,
                    orderitem.product.sku,
                    orderitem.quantity,
                    product_weight,
                    orderitem.product.name,
                    orderitem.unit_price,
                    '', # Bin location
                    shipping_carrier, # Carrier Shipper
                    '', # Insured Value
                    '', # Delivery Confirmation
                    '', # Signature
                    '', # Adult Sig
                    '', # COD Indicator
                    '', # COD Amount
                    order.shipping_cost,
                    order.tax,
                    order.user.aid,
                    str(order.time_stamp),
                    order.time_stamp.strftime('%B'),
                    language,
                    'ibosupport@myefinity.com',
                    order.user.profile.team,
                ])

            order.status = "Shipped"
            order.notes = "CSV generation"
            order.save()

        # close csv file
        f.close()
