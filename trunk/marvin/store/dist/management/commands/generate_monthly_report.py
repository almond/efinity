import sys
# import csv
from marvin import unicodecsv
import ftplib
import codecs
import cStringIO
from datetime import date
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from marvin.store.shop.models import Order
from django.db.models import Q
from marvin.addons.planning.models import Convention
from django.conf import settings
from marvin.store.products.models import Product
from marvin.store.products.modules.subscription.models import SubscriptionProduct
from marvin.store.shop.models import OrderItem
import datetime
import re
from django.conf import settings
#from datetime import date


class Command(BaseCommand):
    help = 'Generates CSV order sheet to get monthly books and cep orders'

    ftp_hostname = '67.78.250.98'
    ftp_username = 'efinityorders'
    ftp_password = 'eFinord09'

    months=[ 'January', 'February', 'March', 'April', 'May',
             'June', 'July', 'August', 'September', 'October',
             'November', 'December']

    if settings.DEBUG:
        local_parent = settings.DIRNAME+'/marvin/store/dist/'
    else:
        local_parent = '/home/mtp/monthly/'

    def handle(self, *args, **options):
        months=[ 'January', 'February', 'March', 'April', 'May',
                 'June', 'July', 'August', 'September', 'October',
                 'November', 'December']

        states={'Mississippi': 'MS', 'Northern Mariana Islands': 'MP', 'Oklahoma': 'OK', 'Wyoming': 'WY', 'Minnesota': 'MN',
                'Alaska': 'AK', 'American Samoa': 'AS', 'Arkansas': 'AR', 'New Mexico': 'NM', 'Indiana': 'IN', 'Maryland': 'MD',
                'Louisiana': 'LA', 'Texas': 'TX', 'Tennessee': 'TN', 'Iowa': 'IA', 'Wisconsin': 'WI', 'Arizona': 'AZ', 'Michigan': 'MI',
                'Kansas': 'KS', 'Utah': 'UT', 'Virginia': 'VA', 'Oregon': 'OR', 'Connecticut': 'CT', 'District of Columbia': 'DC',
                'New Hampshire': 'NH', 'Idaho': 'ID', 'West Virginia': 'WV', 'South Carolina': 'SC', 'California': 'CA', 'Massachusetts': 'MA',
                'Vermont': 'VT', 'Georgia': 'GA', 'North Dakota': 'ND', 'Pennsylvania': 'PA', 'Puerto Rico': 'PR', 'Florida': 'FL',
                'Hawaii': 'HI', 'Kentucky': 'KY', 'Rhode Island': 'RI', 'Nebraska': 'NE', 'Missouri': 'MO', 'Ohio': 'OH', 'Alabama': 'AL',
                'Illinois': 'IL', 'Virgin Islands': 'VI', 'South Dakota': 'SD', 'Colorado': 'CO', 'New Jersey': 'NJ', 'National': 'NA',
                'Washington': 'WA', 'North Carolina': 'NC', 'Maine': 'ME', 'New York': 'NY', 'Montana': 'MT', 'Nevada': 'NV', 'Delaware': 'DE', 'Guam': 'GU'}

        date=datetime.datetime.now()
        """ Get details of months"""
        last_month=(date.month-1, 12)[date.month==1]
        last_months_year=(date.year, date.year-1)[date.month==1]
        first_day_of_next_month=datetime.date((date.year, date.year+1)[date.month==12],
                                              (date.month+1, 1)[date.month==12], 1)

        product_ids={
            'BOOK':{
                'SPANISH':SubscriptionProduct.objects.values_list('product__id', flat=True).filter(product__product_language='es').filter(Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional')),
                'ENGLISH':SubscriptionProduct.objects.values_list('product__id', flat=True).filter(product__product_language='en').filter(Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional'))
                },
            'CEP':{
                'SPANISH':SubscriptionProduct.objects.values_list('product__id', flat=True).filter(product__product_language='es').filter(~Q(product__slug__contains='book') & Q(product__slug__contains='traditional')),
                'ENGLISH':SubscriptionProduct.objects.values_list('product__id', flat=True).filter(product__product_language='en').filter(~Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))
                },
            'CEP + BOOK':{
                'SPANISH':SubscriptionProduct.objects.values_list('product__id', flat=True).filter(product__product_language='es').filter(Q(product__slug__contains='book') & Q(product__slug__contains='traditional')),
                'ENGLISH':SubscriptionProduct.objects.values_list('product__id', flat=True).filter(product__product_language='en').filter(Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))
                }
            }

        #print first_day_of_next_month, product_ids
        orders={}
        for order_type, lang_data in product_ids.items():
            if order_type not in orders:
                orders[order_type]={}
            for lang, pids in lang_data.items():
                orders[order_type][lang]=OrderItem.objects.select_related('order', 'order__user').extra(select={'weight':'select ifnull(weight, 0) from products_product where products_product.id=shop_orderitem.product_id'}).filter(product_id__in=list(pids), expire_date__gte=first_day_of_next_month)

        user_order={}
        for order_type, lang_data in orders.items():
            if order_type not in orders:
                user_order[order_type]={}
            for lang, order_items in lang_data.items():
                label="%s %s"%(order_type, lang)

                for o in order_items:
                    if o.order.user.username not in user_order:
                        user_order[o.order.user.username]={'order_ids':[], 'orders':{}, 'order_ids_by_label':{}, 'weight_by_label':{}}
                        if len(o.order.ship_state)==2:
                            state=o.order.ship_state
                        elif o.order.ship_state.title() in states:
                            state=states[o.order.ship_state.title()]
                        else:
                            state=o.order.ship_state.title()

                        user_order[o.order.user.username]['details']={'first_name':o.order.user.first_name, 'last_name':o.order.user.last_name,
                            'street1':o.order.ship_street1, 'street2':o.order.ship_street2,
                            'city':o.order.ship_city, 'state':state, 'zip':o.order.ship_postal_code,
                            'country':o.order.ship_country, 'email': 'ibosupport@myefinity.com', 'team': o.order.user.profile.team}

                        user_order[o.order.user.username]['weight']=0
                        user_order[o.order.user.username]['total_quantity']=0

                    if label not in user_order[o.order.user.username]['orders']:
                        user_order[o.order.user.username]['orders'][label]=0

                    if label not in user_order[o.order.user.username]['weight_by_label']:
                        user_order[o.order.user.username]['weight_by_label'][label]=0

                    if label not in user_order[o.order.user.username]['order_ids_by_label']:
                        user_order[o.order.user.username]['order_ids_by_label'][label]=[]


                    user_order[o.order.user.username]['total_quantity'] += o.quantity
                    user_order[o.order.user.username]['orders'][label] += o.quantity
                    user_order[o.order.user.username]['order_ids'].append(str(o.id))
                    user_order[o.order.user.username]['order_ids_by_label'][label].append(str(o.id))
                    user_order[o.order.user.username]['weight_by_label'][label] += (o.weight*o.quantity)
                    user_order[o.order.user.username]['weight'] += (o.weight*o.quantity)

        #test_ibo='5999551'
        report_data={'book_only':[], 'book_and_cep':[], 'multiple_cep':[], 'single_pack':[]}
        for ibo, d in user_order.items():
            #if ibo==test_ibo:
                #print user_order[ibo]

            if d['total_quantity']==1:
                purchases="%d %s"%(1, d['orders'].keys()[0])
                if 'BOOK SPANISH' in d['orders'] or 'BOOK ENGLISH' in d['orders']:
                    append_to='book_only'
                elif 'CEP SPANISH' in d['orders'] or 'CEP ENGLISH' in d['orders']:
                    append_to='single_pack'
                else:
                    append_to='book_and_cep'

                data=[", ".join(d['order_ids']), d['details']['first_name'],
                    d['details']['last_name'], ibo, d['details']['street1'], d['details']['street2'],
                    d['details']['city'], d['details']['state'], d['details']['country'], d['details']['zip'], purchases,
                    'Priority Mail', 'Express 1', d['weight'], d['details']['email'], d['details']['team']]

                report_data[append_to].append(data)

            else:
                multiple=False
                #if ibo==test_ibo:
                #    print d['orders']

                for key, value in d['orders'].items():
                    if value>1:
                        multiple=True

                if multiple:
                    purchases=''
                    for p, val in d['orders'].items():
                        if purchases != '':
                            purchases += ' / '
                        purchases += "%.0d %s"%(val, p)

                    append_to='multiple_cep'
                    data=[", ".join(d['order_ids']), d['details']['first_name'], d['details']['last_name'],
                        ibo, d['details']['street1'], d['details']['street2'], d['details']['city'],
                        d['details']['state'], d['details']['country'], d['details']['zip'],
                        purchases, 'Priority Mail', 'Express 1', d['weight'], d['details']['email'],d['details']['team']]
                    report_data[append_to].append(data)

                else:
                    english_book=False
                    english_single_pack=False
                    spanish_book=False
                    spanish_single_pack=False
                    english_book_and_single_pack=False
                    spanish_book_and_single_pack=False
                    for l in d['order_ids_by_label']:
                        #if ibo==test_ibo:
                        #    print l

                        if l == 'BOOK SPANISH':
                            spanish_book=True
                        elif l == 'BOOK ENGLISH':
                            english_book=True
                        elif l == 'CEP ENGLISH':
                            english_single_pack=True
                        elif l == 'CEP SPANISH':
                            spanish_single_pack=True
                        elif l == 'CEP + BOOK SPANISH':
                            spanish_book_and_single_pack=True
                        elif l == 'CEP + BOOK ENGLISH':
                            english_book_and_single_pack=True

                    """
                    if ibo==test_ibo:
                        print 'spanish_book=', str(spanish_book)
                        print 'english_book=', str(english_book)
                        print 'english_single_pack=', str(english_single_pack)
                        print 'spanish_single_pack=', str(spanish_single_pack)
                        print 'spanish_book_and_single_pack=', str(spanish_book_and_single_pack)
                        print 'english_book_and_single_pack=', str(english_book_and_single_pack)
                        print true_count
                    """


                    """ Finding Multiples """
                    if (spanish_book or spanish_single_pack or spanish_book_and_single_pack) and (english_book or english_single_pack or english_book_and_single_pack):
                        purchases=''
                        for p, val in d['orders'].items():
                            if purchases != '':
                                purchases += ' / '
                            purchases += "%.0d %s"%(val, p)

                        append_to='multiple_cep'
                        data=[", ".join(d['order_ids']), d['details']['first_name'], d['details']['last_name'], ibo, d['details']['street1'],
                            d['details']['street2'], d['details']['city'], d['details']['state'], d['details']['country'],
                            d['details']['zip'], purchases, 'Priority Mail', 'Express 1', d['weight'], d['details']['email'], d['details']['team']]

                        report_data[append_to].append(data)

                    else:
                        if not spanish_book or not spanish_single_pack:
                            if spanish_book:
                                append_to='book_only'
                                data=[", ".join(d['order_ids_by_label']['BOOK SPANISH']), d['details']['first_name'], d['details']['last_name'], ibo,
                                    d['details']['street1'], d['details']['street2'], d['details']['city'], d['details']['state'],
                                    d['details']['country'], d['details']['zip'], "%.0d %s"%(1, 'BOOK SPANISH'),
                                    'Priority Mail', 'Express 1', d['weight_by_label']['BOOK SPANISH'], d['details']['email'], d['details']['team']]

                                report_data[append_to].append(data)

                            if spanish_single_pack:
                                append_to='single_pack'
                                data=[", ".join(d['order_ids_by_label']['CEP SPANISH']), d['details']['first_name'], d['details']['last_name'],
                                    ibo, d['details']['street1'], d['details']['street2'], d['details']['city'], d['details']['state'],
                                    d['details']['country'], d['details']['zip'], "%.0d %s"%(1, 'CEP SPANISH'),
                                    'Priority Mail', 'Express 1', d['weight_by_label']['CEP SPANISH'], d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)

                            if spanish_book_and_single_pack:
                                append_to='book_and_cep'
                                data=[", ".join(d['order_ids_by_label']['CEP + BOOK SPANISH']), d['details']['first_name'], d['details']['last_name'],
                                    ibo, d['details']['street1'], d['details']['street2'], d['details']['city'], d['details']['state'],
                                    d['details']['country'], d['details']['zip'], "%.0d %s"%(1, 'CEP + BOOK SPANISH'), 'Priority Mail', 'Express 1',
                                    d['weight_by_label']['CEP + BOOK SPANISH'], d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)
                        else:
                            if spanish_book_and_single_pack:

                                append_to='multiple_cep'
                                data=['%s, %s, %s'%(", ".join(d['order_ids_by_label']['CEP SPANISH']),
                                    ", ".join(d['order_ids_by_label']['BOOK SPANISH']),
                                    ", ".join(d['order_ids_by_label']['CEP + BOOK SPANISH'])),
                                    d['details']['first_name'], d['details']['last_name'], ibo,
                                    d['details']['street1'], d['details']['street2'], d['details']['city'],
                                    d['details']['state'], d['details']['country'], d['details']['zip'], "%.0d %s"%(2, 'CEP + BOOK SPANISH'),
                                    'Priority Mail', 'Express 1',
                                    d['weight_by_label']['CEP SPANISH']+d['weight_by_label']['BOOK SPANISH']+d['weight_by_label']['CEP + BOOK SPANISH'],
                                    d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)

                            else:
                                append_to='book_and_cep'
                                data=['%s, %s'%(", ".join(d['order_ids_by_label']['CEP SPANISH']),
                                    ", ".join(d['order_ids_by_label']['BOOK SPANISH'])),
                                    d['details']['first_name'], d['details']['last_name'],
                                    ibo, d['details']['street1'], d['details']['street2'], d['details']['city'],
                                    d['details']['state'], d['details']['country'], d['details']['zip'],
                                    "%.0d %s"%(1, 'CEP + BOOK SPANISH'), 'Priority Mail', 'Express 1',
                                    d['weight_by_label']['CEP SPANISH']+d['weight_by_label']['BOOK SPANISH'], d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)


                        if not english_book or not english_single_pack:
                            if english_book:
                                append_to='book_only'
                                data=[", ".join(d['order_ids_by_label']['BOOK ENGLISH']), d['details']['first_name'],
                                    d['details']['last_name'], ibo, d['details']['street1'], d['details']['street2'],
                                    d['details']['city'], d['details']['state'], d['details']['country'], d['details']['zip'],
                                    "%.0d %s"%(1, 'BOOK ENGLISH'), 'Priority Mail', 'Express 1', d['weight_by_label']['BOOK ENGLISH'], d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)

                            if english_single_pack:
                                append_to='single_pack'
                                data=[", ".join(d['order_ids_by_label']['CEP ENGLISH']),
                                    d['details']['first_name'], d['details']['last_name'], ibo,
                                    d['details']['street1'], d['details']['street2'], d['details']['city'],
                                    d['details']['state'], d['details']['country'], d['details']['zip'],
                                    "%.0d %s"%(1, 'CEP ENGLISH'), 'Priority Mail', 'Express 1',
                                    d['weight_by_label']['CEP ENGLISH'], d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)

                            if english_book_and_single_pack:
                                append_to='book_and_cep'
                                data=[", ".join(d['order_ids_by_label']['CEP + BOOK ENGLISH']),
                                    d['details']['first_name'], d['details']['last_name'],
                                    ibo, d['details']['street1'], d['details']['street2'],
                                    d['details']['city'], d['details']['state'],
                                    d['details']['country'], d['details']['zip'],
                                    "%.0d %s"%(1, 'CEP + BOOK ENGLISH'), 'Priority Mail',
                                    'Express 1', d['weight_by_label']['CEP + BOOK ENGLISH'], d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)
                        else:
                            if english_book_and_single_pack:

                                append_to='multiple_cep'
                                data=['%s, %s, %s'%(", ".join(d['order_ids_by_label']['CEP ENGLISH']),
                                    ", ".join(d['order_ids_by_label']['BOOK ENGLISH']),
                                    ", ".join(d['order_ids_by_label']['CEP + BOOK ENGLISH'])),
                                    d['details']['first_name'], d['details']['last_name'], ibo,
                                    d['details']['street1'], d['details']['street2'], d['details']['city'],
                                    d['details']['state'], d['details']['country'], d['details']['zip'],
                                    "%.0d %s"%(2, 'CEP + BOOK ENGLISH'), 'Priority Mail', 'Express 1',
                                    d['weight_by_label']['CEP ENGLISH']+d['weight_by_label']['BOOK ENGLISH']+d['weight_by_label']['CEP + BOOK ENGLISH'],
                                    d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)

                            else:
                                append_to='book_and_cep'
                                data=['%s, %s'%(", ".join(d['order_ids_by_label']['CEP ENGLISH']),
                                    ", ".join(d['order_ids_by_label']['BOOK ENGLISH'])), d['details']['first_name'],
                                    d['details']['last_name'], ibo, d['details']['street1'], d['details']['street2'],
                                    d['details']['city'], d['details']['state'], d['details']['country'], d['details']['zip'],
                                    "%.0d %s"%(1, 'CEP + BOOK ENGLISH'), 'Priority Mail', 'Express 1',
                                    d['weight_by_label']['CEP ENGLISH']+d['weight_by_label']['BOOK ENGLISH'], d['details']['email'], d['details']['team']]
                                report_data[append_to].append(data)

        now_time=datetime.datetime.now()

        multiple_ceps = report_data.pop("multiple_cep")
        # generate multiple CEP's file
        self.write_multiples_file("multiple_cep", multiple_ceps)

        # Generate the rest of the list
        for key, value in report_data.items():
            # First we'll go through the whole list separating people into their respective teams
            i_commercio = []
            english_team = []
            ag_team = []

            for c in value:
                # omg this is so retarded... that's why they're doing multilevel instead of a real business
                # TODO: This is hideously hardcoded... Remember to change it whenever somebody thinks that
                # It needs to be different
                if c[-1] == "iComercio":
                    if "ENGLISH" in c[10]:
                        english_team.append(c)
                    else:
                        i_commercio.append(c)
                elif c[-1] == "English":
                    if "SPANISH" in c[10]:
                        i_commercio.append(c)
                    else:
                        english_team.append(c)
                elif c[-1] == "Alianza Global":
                    if "ENGLISH" in c[10]:
                        english_team.append(c)
                    else:
                        ag_team.append(c)

            self.write_to_file(key, i_commercio, "IC")
            self.write_to_file(key, english_team, "English")
            self.write_to_file(key, ag_team, "AG")


    def write_multiples_file(self, key, value):
        now_time = datetime.datetime.now()
        filename = "Multiple CEP Packs"
        filename = '%s %d %s.csv' % (self.months[now_time.month-1], now_time.year, filename)
        f = open(self.local_parent + 'orders/' + filename, 'wb')
        writer = unicodecsv.writer(f, encoding='utf-8', quoting=unicodecsv.QUOTE_ALL)
        writer.writerow(['Order ID', 'First Name',
                         'Last Name', 'IBO', 'Address1',
                         'Address2', 'City', 'State', 'Country',
                         'Zip', 'Subscriptions', 'Ship Method', 'Ship Model',
                         'Weight', 'Email', 'Team'])
        for v in value:
            writer.writerow(v)
        f.close()


    def write_to_file(self, key, value, team):
        now_time = datetime.datetime.now()
        if key == 'book_only':
            filename='Book Only'
        elif key == 'book_and_cep':
            filename='Book and CEP'
        elif key == 'multiple_cep':
            filename='Multiple CEP Packs'
        elif key == 'single_pack':
            filename='Single Packs'

        filename='%s-%s %d %s.csv' % (team, self.months[now_time.month-1], now_time.year, filename)
        f = open(self.local_parent + 'orders/' + filename, 'wb')
        writer = unicodecsv.writer(f, encoding='utf-8', quoting=unicodecsv.QUOTE_ALL)
        writer.writerow(['Order ID', 'First Name',
                         'Last Name', 'IBO', 'Address1',
                         'Address2', 'City', 'State', 'Country',
                         'Zip', 'Subscriptions', 'Ship Method', 'Ship Model',
                         'Weight', 'Email', 'Team'])
        for v in value:
            writer.writerow(v)
        f.close()
