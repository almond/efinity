# import csv
from marvin import unicodecsv
import ftplib
from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order
from django.db.models import Q
from marvin.addons.planning.models import Convention
from django.conf import settings

import datetime

class Command(BaseCommand):
    help = 'Generates CSV order sheet to transmit to fulfillment'

    # FTP connections
    ftp_connections = [
        {"hostname": "67.78.250.98", "username": 'efinityorders', "password": "eFinord09"},
        {"hostname": "ftp.cdduplicationorlando.com", "username": 'efinityorders@cdduplicationorlando.com',
         "password": "e.shipping"},
    ]


    # Now set a backup server. This server should also be contacted upon file generation
    if settings.DEBUG:
        local_parent = '/Users/axolote/Workspace/dbdumps/efinity/orders/'
    else:
        local_parent = "/home/mtp/"

    def handle(self, *args, **options):
        # Get the correct filename because this drives me up the walls
        date_checked = datetime.date(int(args[0]), int(args[1]), int(args[2]))
        time_diff = datetime.timedelta(days=1)
        tomorrow = date_checked + time_diff

        filename = "%d-%d-%d.csv" % (tomorrow.year, tomorrow.month, tomorrow.day)

        orders = []
        f = open(self.local_parent + 'daily/' + filename, 'wb')
        writer = unicodecsv.writer(f, encoding='utf-8', quoting=unicodecsv.QUOTE_ALL)
        writer.writerow([
            'Order #',
            'Shipping Method',
            'First Name',
            'Last Name',
            'Address 1',
            'Address 2',
            'City',
            'State',
            'Postal Code',
            'Country',
            'Phone Number',
            'Gift Message or Order Notes',
            'Product SKU',
            'Quantity',
            'Unit Weight',
            'Product Description',
            'Unit Price',
            'Pick Location (bin location)',
            'Carrier Shipper',
            'Insured Value',
            'Delivery Confirmation',
            'Signature Confirmation',
            'Adult Signature Confirmation',
            'COD_Indicator',
            'COD_Amount',
            'Billed Shipping Cost',
            'Tax Billed to Customer',
            'Customer Number (IBO Number)',
            'Order Time',
            'Current Month',
            'Language',
            'EMAIL',
            'Team',
        ])

        # go through all order's that are shippable and
        # not shipped

        date_to_check = datetime.date(int(args[0]), int(args[1]), int(args[2]))
        time_delta = datetime.timedelta(days=1)
        until_date = date_to_check + time_delta

        print date_to_check
        query = Q(status='Billed') | Q(status='In Process') | Q(status='Reshipped') | Q(status='New') | Q(
            status="Shipped")

        for order in Order.objects.filter(query).filter(time_stamp__gte=date_to_check, time_stamp__lt=until_date):
            if not order.is_shippable:
                continue

            orders.append(order)

            for orderitem in order.orderitem_set.all():
                # Test for convetion since we do not check for shippable above
                # check if it points to no product at all
                if not orderitem.product:
                    continue

                if type(orderitem.product) == Convention:
                    continue

                # Delayed billing order items should not be in the nightly CSV, only monthly
                if orderitem.order.time_stamp.day >= 15:
                    if orderitem.product.delay_billing:
                        continue

                # Here test to see if the orderitem is subscription. If so,
                # Make sure this orderitem was ordered within the past 27 days.
                # Usually it will be picked up the next day.
                if hasattr(orderitem.product, 'is_subscription') and orderitem.product.is_subscription:
                    if not orderitem.expire_date == None:
                        if not orderitem.expire_date > datetime.date.today():
                            continue

                # Test for certain product sku's that should not be in the csv
                # Event though non-shippable orders should be excluded as per the
                # order.is_shippable above...
                if orderitem.product.sku == 'subscription-digital':
                    continue

                # Add more exceptions here if needed
                # if orderitem.product.sku == 'subscription-digital':
                #     continue


                shipping_method = ''
                shipping_carrier = ''
                # if ups, set correct shipping carrier, if no method, default to USPS
                if order.shipping_method == 'UPS':
                    shipping_method = 'UPS Ground'
                    shipping_carrier = 'UPS Online'
                else:
                    shipping_method = 'Priority Mail'
                    shipping_carrier = 'Express 1'

                product_weight = ''
                if orderitem.product.weight:
                    product_weight = (orderitem.product.weight * orderitem.quantity)

                order_notes = ''
                if order.notes:
                    order_notes = order.notes

                language = ''
                if orderitem.product.product_language == 'en':
                    language = 'english'
                elif orderitem.product.product_language == 'es':
                    language = 'spanish'
                writer.writerow([
                    order.id,
                    shipping_method,
                    order.user.first_name,
                    order.user.last_name,
                    order.ship_street1,
                    order.ship_street2,
                    order.ship_city,
                    order.ship_state,
                    order.ship_postal_code,
                    order.ship_country_name,
                    '',
                    order_notes,
                    orderitem.product.sku,
                    orderitem.quantity,
                    product_weight,
                    orderitem.product.name,
                    orderitem.unit_price,
                    '', # Bin location
                    shipping_carrier, # Carrier Shipper
                    '', # Insured Value
                    '', # Delivery Confirmation
                    '', # Signature
                    '', # Adult Sig
                    '', # COD Indicator
                    '', # COD Amount
                    order.shipping_cost,
                    order.tax,
                    order.user.aid,
                    str(order.time_stamp),
                    order.time_stamp.strftime('%B'),
                    language,
                    'ibosupport@myefinity.com',
                    order.user.profile.team,
                ])

            order.status = "New"
            order.save()

        # close csv file
        f.close()

        # Upload to FTP
        if not settings.DEBUG:
            for conn in self.ftp_connections:
                f = open(self.local_parent + 'daily/' + filename, 'rb')
                s = ftplib.FTP(conn["hostname"], conn["username"], conn["password"])
                s.cwd('/')
                try:
                    s.mkd(tomorrow.strftime('%B'))
                except:
                    pass
                    #s.cwd(today.strftime('%Y-%m-%d') + '/')
                s.cwd(tomorrow.strftime('%B'))
                #s.cwd('/orders/')
                s.storbinary('STOR ' + filename, f)

                s.quit()
                f.close()

                # Change statuses to shipped. Remember, you're fixing


                #if not settings.DEBUG:
                ## upload csv to ftp
                ## MRVG Made this a list of dictionaries defined in global scope of the class
                ## so that an arbitrary number of FTP hosts could be added.

                #for conn in self.ftp_connections:
                #f = open(self.local_parent + 'orders/' + filename, 'rb')
                #s = ftplib.FTP(conn["hostname"], conn["username"], conn["password"])
                #s.cwd('/')
                #try:
                #s.mkd(today.strftime('%B'))
                #except:
                #pass
                ##s.cwd(today.strftime('%Y-%m-%d') + '/')
                #s.cwd(today.strftime('%B'))
                ##s.cwd('/orders/')
                #s.storbinary('STOR ' + filename, f)

                #s.quit()
                #f.close()

                ## Close the file

                ## now after we have successfully saved the file and upload,
                ## mark all orders as shipped
                #for order in orders:
                #order.status = "Shipped"
                #order.notes = "CSV generation"
                #order.save()
