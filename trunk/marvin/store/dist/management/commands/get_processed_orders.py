from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order

class Command(BaseCommand):
  """Messed up emails. Get what's been processed so far"""
  def handle(self, *args, **kwargs):
    orders = Order.objects.filter(status="renewed_april")
    for o in orders:
      print o.user.username + ", "
