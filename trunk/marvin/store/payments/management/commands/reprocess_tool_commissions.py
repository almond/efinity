from marvin.store.shop.models import OrderItem
from marvin.addons.compensation.models import Commission
from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
import datetime
from marvin.addons.compensation.utils import process_commission
from marvin.addons.compensation.views.payout import process_event_commissions_helper

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        ibo = args[0]
        year = args[1]
        month = args[2]

        # get necessary data
        user = User.objects.get(username=ibo)

        # Delete every commission object. We want no repeats
        commissions = Commission.objects.filter(user=user, created__month=month,
                                                created__year=year)

        # Just delete said month's commissions
        commissions.delete()

        # Now get everyone in downline
        # downline = user.downline.all()

        # users_in_downline = [x.user for x in downline]

        # Now get all order items
        ois = OrderItem.objects.filter(order__user=user,
                                       order__payments__time_stamp__month=month,
                                       order__payments__time_stamp__year=year)
        for o in ois:
            process_commission(o, commission_date=(year, month, 15))

        # # For each user process the pertinent commissions for each year and relevant month
        # convention_type = ContentType.objects.get(app_label="planning", model="convention")

        # from_date = datetime.datetime(2012, 11, 12, 0, 0, 0)
        # today = datetime.datetime(2012, 11, 30, 11, 59, 59)

        # ois = OrderItem.objects.filter(order__time_stamp__gte=from_date,
        #     order__time_stamp__lte=today).exclude(order__status="Cancelled").distinct()

        # tools = ois.exclude(content_type=convention_type)

        # print "Processing %d tools" % len(tools)

        # subscriptions = list()
        # for o in tools:
        #     if o.product.is_subscription:
        #         # Process a subscription separately
        #         subscriptions.append(o)
        #     else:
        #         try:
        #             self.process_tool_commissions(oi=o, year=o.order.payments.all()[0].time_stamp.year,
        #                                     month=o.order.payments.all()[0].time_stamp.month,
        #                                     day=o.order.payments.all()[0].time_stamp.day)
        #         except IndexError:
        #             print "Order %d has no payments under it" % o.order.pk


    # def process_tool_commissions(self, *args, **kwargs):
    #     """
    #     Process the info
    #     """
    #     # Check that this OI commission's haven't been processed yet
    #     Commission.objects.filter(order_item=kwargs.get("oi")).delete()

    #     process_commission(kwargs.get("oi"), 
    #         commission_date=(kwargs.get("year"), 
    #             kwargs.get("month"), kwargs.get("day")))

    # def process_event_commissions(self, *args, **kwargs):
    #     Commission.objects.filter(order_item=kwargs.get("oi")).delete()
    #     process_event_commissions_helper(event_id=kwargs.get("oi").product)
