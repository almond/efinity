from datetime import datetime, timedelta, date
from django.utils.translation import ugettext
from marvin.store.shop.models import OrderItem
import logging
import os
import csv
from marvin.addons.compensation.utils import process_commission
from django.conf import settings


log = logging.getLogger('payments.command.cron')

from django.core.management.base import NoArgsCommand

class Command(NoArgsCommand):
    help = ("Invokes recurring billing system to do stuff like "
            "charge subscription customers each month.  You typically "
            "want to invoke this from a cron script.  For non-root "
            "users this is generally done with ``crontab -e``.")

    def get_file_path(self, filename):
        return os.path.join(settings.DIRNAME, "..", "csv", filename)

    def write_to_file(self, oi, payment_status="Failed"):
        """
        Write to a CSV the just processed subscription file
        """
        # If empty write header, otherwise just append data
        csv_file = open(self.get_file_path("processed_orders_april.csv"), "a")
        writer = csv.writer(csv_file)
        if os.stat(csv_file).st_size == 0:
            columns = ["IBO", "Order", "Order Item", "Expiration Date", "Team", "Status"]
            writer.writerow(columns)
        else:
            writer.writerow([oi.order.user.username, oi.order.pk,
                oi.pk, "%s-%s-%s" % (oi.expire_date.year, oi.expire_date.month, oi.expire_date.day),
                oi.order.user.profile.team, payment_status])

    def handle_noargs(self, **options):
        """
        Rebill customers with expiring recurring subscription products
        """
        #TODO: support re-try billing for failed transactions

        # Anything that expires after the first of this month should be put in the rebill queue.
        # TODO one time mods
        first_of_month = date(2013, 10, 1)
        last_of_month = date(2013, 10, 30)

        # One time
        old_expiration_date = date(2013, 11, 1)
        next_expiration_date = date(2013, 12, 1)

        # When where delayed billings staged?
        delayed_billing_date = first_of_month - timedelta(days=6)

        # I did this in order not to have a freaken huge line of code. Looks nasty though
        expiring_subscriptions = OrderItem.objects.filter(expire_date__gte=first_of_month)
        expiring_subscriptions = expiring_subscriptions.filter(completed=True)
        # On next run remove the Renewed status from the query
        expiring_subscriptions = expiring_subscriptions.exclude(order__status__in=("Cancelled", )).distinct()

        for item in expiring_subscriptions.order_by("order", "id", "expire_date"):
            # We currently do not want to process an order that has multiple orderitems on it
            if item.product.is_subscription:#TODO - need to add support for products with trial but non-recurring
                if (item.product.subscriptionproduct.recurring_times
                    and item.product.subscriptionproduct.recurring_times
                    + item.product.subscriptionproduct.get_trial_terms().count()
                    == OrderItem.objects.filter(order=item.order, product_id=item.product.id).count()):
                    continue
                if item.completed:
                # if item.expire_date.month == (datetime.date(datetime.now()).month - 1) and item.completed:
                    if item.id == OrderItem.objects.filter(product_id=item.product_id, order=item.order).order_by('-id')[0].id:
                        #bill => add orderitem, recalculate total, process CIM
                        new_order_item = OrderItem(order=item.order, product_id=item.product.id, quantity=item.quantity, unit_price=item.product.get_qty_price(item.quantity), line_item_price=(item.product.get_qty_price(item.quantity) * item.quantity))
                        
                        #if product is recurring, set subscription end
                        if item.product.subscriptionproduct.recurring:
                            new_order_item.expire_date = next_expiration_date

                        #check if product has 2 or more trial periods and if the last one paid was a trial or a regular payment.
                        # ordercount = item.order.orderitem_set.all().count()
                        # if item.product.subscriptionproduct.get_trial_terms().count() > 1 and item.unit_price == item.product.subscriptionproduct.get_trial_terms(ordercount - 1).price:
                        #     new_order_item.unit_price = item.product.subscriptionproduct.get_trial.terms(ordercount).price
                        #     new_order_item.line_item_price = new_order_item.quantity * new_order_item.unit_price
                        #     new_order_item.expire_date = item.product.subscriptionproduct.get_trial_terms(ordercount).calc_expire_date()

                        # DEBUG
                        print 'Order:%s - OrderItem:%s' % (item.order.id, item.id)

                        # Just in case
                        new_order_item.expire_date = next_expiration_date
                        new_order_item.save()

                        item.order.recalculate_total()

                        # Update shipping address to the default in the user's settings
                        # TODO: Make sure this works on next subscription processing thing
                        item.order.save()

                        payments = item.order.payments.all()[0]
                        #list of ipn based payment modules.  Include processors that use 3rd party recurring billing.
                        ipn_based = ['PAYPAL']
                        if not payments.payment in ipn_based and item.order.balance > 0:
                            #run card
                            #Do the credit card processing here & if successful, execute the success_handler
                            from marvin.utils.dbsettings.functions import config_get_group
                            payment_module = config_get_group('PAYMENT_%s' % payments.payment)
                            credit_processor = payment_module.MODULE.load_module('processor')
                            processor = credit_processor.PaymentProcessor(payment_module)
                            processor.prepare_data(item.order)
                            # result = processor.process()

                            try:
                                payment_profile_id = payments.cim_payment_profile.payment_profile_id
                            except:
                                # this order does not have a CIM payment associated with it
                                print 'Order %s does not have a CIM payment associated with it.' % item.order
                                new_order_item.delete()
                                item.order.recalculate_total()
                                continue

                            result = processor.process_recurring_cim(amount=item.order.balance, payment_profile_id=payment_profile_id)

                            if result.payment:
                                reason_code = result.payment.reason_code
                            else:
                                reason_code = "unknown"
                            log.info("""Processing %s recurring transaction with %s
                                Order #%i
                                Results=%s
                                Response=%s
                                Reason=%s""",
                                payment_module.LABEL.value,
                                payment_module.KEY.value,
                                item.order.id,
                                result.success,
                                reason_code,
                                result.message)

                            print 'Results=%s *** Response=%s *** Reason=%s' % (result.success, reason_code, result.message)

                            if result.success:
                                #success handler
                                item.order.add_status(status='renewed_november', notes = ugettext("Subscription Renewal Order successfully submitted for July"))
                                new_order_item.completed = True
                                new_order_item.expire_date = next_expiration_date
                                new_order_item.save()
                                # orderpayment = OrderPayment(order=item.order, amount=item.order.balance, payment=unicode(payment_module.KEY.value))
                                # orderpayment.save()
                                process_commission(item)
                                # self.write_to_file(item, "OK")
                            else:
                                # self.write_to_file(item, "Failed")
                                new_order_item.delete()
                                item.order.recalculate_total()
        return ''
