from datetime import datetime, timedelta, date
from decimal import Decimal
from django.conf import settings, settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from django.db.models import Q
from django.http import HttpResponse
from django.utils.translation import ugettext, ugettext_lazy as _
from marvin import unicodecsv
from marvin.addons.compensation.utils import process_commission
from marvin.addons.planning.models import Convention
from marvin.store.products.models import Product
from marvin.store.shop.models import Order, Order, OrderItem, OrderPayment, \
    OrderItem
from marvin.utils.dbsettings import config_get_group, config_value
from marvin.utils.functions import add_month
from marvin.utils.views import bad_or_missing
import cStringIO
import codecs
import ftplib
import logging
import os
import re
import sys

log = logging.getLogger('payments.command.cron')


# import csv


class Command(NoArgsCommand):
    help = ("Generates the monthly decline list "
            "this can generally be done with ``crontab -e``.")

    def handle_noargs(self, **options):
        """
        Generate the monthly decline list
        """

        first_of_month = date(datetime.now().year, datetime.now().month, 1)
        last_of_month = add_month(first_of_month)

        ois = OrderItem.objects.filter(expire_date__gte=first_of_month).filter(expire_date__lte=last_of_month).order_by('order', 'id', 'expire_date')

        ois = ois.filter(order__paymentfailures__time_stamp__gte=first_of_month).filter(order__paymentfailures__time_stamp__lte=last_of_month)

        filename='Decline List %s %d.csv' % (first_of_month.month, first_of_month.year)
        path = '/home/mtp/declines/' + filename

        directory = os.path.dirname(path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        f = open(path, 'wb')
        writer = unicodecsv.writer(f, encoding='utf-8', quoting=unicodecsv.QUOTE_ALL)
        writer.writerow(['Order Info', 'Order ID', 'OrderItem ID', 'IBO', 'Amount', 'Reason Code', 'Details', 'Team'])

        for oi in ois:
            pf = oi.order.paymentfailures.all()[0]
            writer.writerow([oi.order, oi.order.id, oi.id, oi.order.user.aid, pf.amount, pf.reason_code, pf.details, oi.order.user.profile.team])

