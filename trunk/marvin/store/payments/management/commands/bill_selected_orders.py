from datetime import datetime, timedelta, date
from decimal import Decimal
from django.http import HttpResponse
from django.utils.translation import ugettext, ugettext_lazy as _
from marvin.utils.dbsettings import config_get_group, config_value
from marvin.store.shop.models import Order, OrderItem, OrderPayment
from marvin.utils.views import bad_or_missing
import logging
from marvin.addons.compensation.utils import process_commission


log = logging.getLogger('payments.command.cron')

from django.core.management.base import NoArgsCommand

class Command(NoArgsCommand):
    help = ("Invokes recurring billing system to do stuff like "
            "charge subscription customers each month.  You typically "
            "want to invoke this from a cron script.  For non-root "
            "users this is generally done with ``crontab -e``.")

    def handle_noargs(self, **options):
        """
        Rebill customers with expiring recurring subscription products
        """
        # IBO's to check this time
        ORDERS = (
            43834, 
            43863, 
            43902, 
            43912, 
            43939, 
            43950, 
            43967, 
            43969, 
            43971, 
            44089, 
            44108, 
            44156, 
            44152, 
            44162, 
            44163, 
            44318, 
            44398, 
            44431, 
            44434, 
            44441, 
            44448, 
            44452, 
            44453, 
            44468, 
            44470, 
            44472, 
            44474, 
            44479, 
            44482, 
            44484, 
            44485, 
            44493, 
            44499, 
            44500, 
            44501, 
            44502, 
            44503, 
            44506, 
            44508, 
            44509, 
            44513, 
            44515, 
            44516, 
            44517, 
            44518, 
            44526, 
            44528, 
            44566, 
            44572, 
            44577, 
            44593, 
            44595, 
            44609, 
            44612, 
            44613, 
            44614, 
            44616, 
            44617, 
            44618, 
            44619, 
            44620, 
            44621, 
            44623, 
            44629, 
            44644, 
        )

        next_expiration_date = date(datetime.now().year, 12, 1)

        expiring_subscriptions = OrderItem.objects.filter(order__in=ORDERS).filter(completed=True).order_by('order', 'id', 'expire_date')


        test_list = list()

        for item in expiring_subscriptions:
            # We currently do not want to process an order that has multiple orderitems on it
            if item.product.is_subscription:#TODO - need to add support for products with trial but non-recurring
                # if item.product.subscriptionproduct.recurring_times and item.product.subscriptionproduct.recurring_times + item.product.subscriptionproduct.get_trial_terms().count() == OrderItem.objects.filter(order=item.order, product_id=item.product.id).count():
                #     continue
                if item.completed:
                # if item.expire_date.month == (datetime.date(datetime.now()).month - 1) and item.completed:

                    if item.id == OrderItem.objects.filter(product_id=item.product_id, order=item.order).order_by('-id')[0].id:
                        #bill => add orderitem, recalculate total, process CIM
                        new_order_item = OrderItem(order=item.order, product_id=item.product.id, quantity=item.quantity, unit_price=item.unit_price, line_item_price=item.line_item_price)
                        #if product is recurring, set subscription end
                        # if item.product.subscriptionproduct.recurring:
                            # new_order_item.expire_date = item.product.subscriptionproduct.calc_expire_date()
                        new_order_item.expire_date = next_expiration_date

                        # check if product has 2 or more trial periods and if the last one paid was a trial or a regular payment.
                        ordercount = item.order.orderitem_set.all().count()
                        if item.product.subscriptionproduct.get_trial_terms().count() > 1 and item.unit_price == item.product.subscriptionproduct.get_trial_terms(ordercount - 1).price:
                            new_order_item.unit_price = item.product.subscriptionproduct.get_trial.terms(ordercount).price
                            new_order_item.line_item_price = new_order_item.quantity * new_order_item.unit_price
                            new_order_item.expire_date = item.product.subscriptionproduct.get_trial_terms(ordercount).calc_expire_date()
                        
                        # DEBUG
                        # print 'Order:%s - OrderItem:%s - IBO:%s' % (item.order.id, item.id, item.order.user.username)
                        if item.order.user.subscriptions():
                            continue

                        new_order_item.save()
                        
                        item.order.recalculate_total()

                        # print 'Order:%s - OrderItem:%s - IBO:%s' % (new_order_item.order.id, new_order_item.id, new_order_item.order.user.username) 
                        
                        payments = item.order.payments.all()[0]
                        #list of ipn based payment modules.  Include processors that use 3rd party recurring billing.
                        ipn_based = ['PAYPAL']
                        if not payments.payment in ipn_based and item.order.balance > 0:
                            #run card
                            #Do the credit card processing here & if successful, execute the success_handler
                            from marvin.utils.dbsettings.functions import config_get_group
                            payment_module = config_get_group('PAYMENT_%s' % payments.payment)
                            credit_processor = payment_module.MODULE.load_module('processor')
                            processor = credit_processor.PaymentProcessor(payment_module)
                            processor.prepare_data(item.order)
                            # result = processor.process()
                            
                            try:
                                payment_profile_id = payments.cim_payment_profile.payment_profile_id
                            except:
                                # this order does not have a CIM payment associated with it
                                print 'Order %s does not have a CIM payment associated with it.' % item.order
                                new_order_item.delete()
                                item.order.recalculate_total()
                                continue
                            
                            result = processor.process_recurring_cim(amount=item.order.balance, payment_profile_id=payment_profile_id)
    
                            if result.payment:
                                reason_code = result.payment.reason_code
                            else:
                                reason_code = "unknown"
                            log.info("""Processing %s recurring transaction with %s
                                Order #%i
                                Results=%s
                                Response=%s
                                Reason=%s""",
                                payment_module.LABEL.value,
                                payment_module.KEY.value,
                                item.order.id,
                                result.success,
                                reason_code,
                                result.message)
                            
                            print 'Results=%s *** Response=%s *** Reason=%s *** order=%s *** order_item=%s *** IBO=%s' % (result.success, reason_code, result.message, new_order_item.order.pk, new_order_item.pk, new_order_item.order.user.username)

                            if result.success:
                                #success handler
                                item.order.add_status(status='New', notes = ugettext("Subscription Renewal Order successfully submitted"))
                                new_order_item.completed = True
                                new_order_item.save()
                                # orderpayment = OrderPayment(order=item.order, amount=item.order.balance, payment=unicode(payment_module.KEY.value))
                                # orderpayment.save()
                                process_commission(item)
                            else:
                                new_order_item.delete()
                                item.order.recalculate_total()

        return ''
