from django.core.management.base import BaseCommand, CommandError
import datetime
from marvin.store.shop.models import OrderItem


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        # Get every order that has a registered payment the first of this month
        first_of_month = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
        expiration_date = datetime.date(datetime.date.today().year, datetime.date.today().month + 1, 1)
        order_items = OrderItem.objects.filter(order__payments__time_stamp__gte=first_of_month,
                                               order__payments__reason_code="I00001")
        for oi in order_items:
            # Now if they are subscriptions, make sure they have expiration dates. Else set them
            if oi.product.is_subscription:
                if not oi.expire_date:
                    oi.expire_date = expiration_date
                    oi.save()
                    print "OI %d for IBO %s has been updated" % (oi.pk, oi.order.user.username)

        print "A-OK"
