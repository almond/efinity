from marvin.addons.compensation.utils import process_commission
from marvin.store.shop.models import Order, OrderItem
from marvin.addons.compensation.models import Commission
import datetime

from django.core.management.base import BaseCommand, CommandError



class Command(BaseCommand):

	def handle(self, *args, **options):

		if args:
			# Just run the complete order for commissions
			order = Order.objects.get(pk=args[0])
			for oi in OrderItem.objects.filter(order=order):
				# First delete any commission this oi may have in order to recreate them
				if len(Commission.objects.filter(order_item=oi)):
					continue
					
				# Now process the commission
				process_commission(oi)

			# Now set to a proper date
			corrected_date = order.time_stamp
			commissions = Commission.objects.filter(order_item__in=order.orderitem_set.all())
			commissions.update(created=corrected_date, updated=corrected_date)
			return 

		date_to_check = datetime.date(2012, 9, 30)
		# First gather all subscriptions with a status of new
		new_subscriptions = OrderItem.objects.filter(expire_date__gt=date_to_check)

		# check if it has commissions
		for sub in new_subscriptions:
			if not len(sub.commission_set.all()):
				print "Processing commission for order %s" % (str(sub.order.pk))
				process_commission(sub)