from django.conf.urls.defaults import patterns
from  marvin.store.shop.satchmo_settings import get_satchmo_setting

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     (r'^$', 'marvin.store.payments.modules.cod.views.pay_ship_info', {'SSL':ssl}, 'COD_checkout-step2'),
     (r'^confirm/$', 'marvin.store.payments.modules.cod.views.confirm_info', {'SSL':ssl}, 'COD_checkout-step3'),
     (r'^success/$', 'marvin.store.payments.views.checkout.success', {'SSL':ssl}, 'COD_checkout-success'),
)
