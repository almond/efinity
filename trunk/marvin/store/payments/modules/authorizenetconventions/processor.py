from datetime import datetime
from decimal import Decimal
from django.template import loader, Context
from django.utils.http import urlencode
from django.utils.translation import ugettext_lazy as _
from marvin.store.payments.modules.base import BasePaymentProcessor, ProcessorResult
from marvin.store.shop.models import Config
from marvin.utils.numbers import trunc_decimal
from marvin.store.tax.util import get_tax_processor
from xml.dom import minidom
import random
import urllib2
import re

class PaymentProcessor(BasePaymentProcessor):
    """
    Authorize.NET payment processing module
    You must have an account with authorize.net in order to use this module.

    Additionally, you must have ARB enabled in your account to use recurring billing.
    """
    def __init__(self, settings):
        super(PaymentProcessor, self).__init__('authorizenetconventions', settings)
        self.arb_enabled = settings.ARB.value

    def authorize_payment(self, order=None, amount=None, testing=False):
        """Authorize a single payment.

        Returns: ProcessorResult
        """
        if order:
            self.prepare_data(order)
        else:
            order = self.order

        if order.paid_in_full:
            self.log_extra('%s is paid in full, no authorization attempted.', order)
            results = ProcessorResult(self.key, True, _("No charge needed, paid in full."))
        else:
            self.log_extra('Authorizing payment of %s for %s', amount, order)

            standard = self.get_standard_charge_data(authorize=True, amount=amount)
            results = self.send_post(standard, testing)

        return results

    def can_authorize(self):
        return True

    def can_recur_bill(self):
        return True

    def capture_authorized_payment(self, authorization, testing=False, order=None, amount=None):
        """Capture a single payment"""
        if order:
            self.prepare_data(order)
        else:
            order = self.order

        if order.authorized_remaining == Decimal('0.00'):
            self.log_extra('No remaining authorizations on %s', order)
            return ProcessorResult(self.key, True, _("Already complete"))

        self.log_extra('Capturing Authorization #%i for %s', authorization.id, order)
        data = self.get_prior_auth_data(authorization, amount=amount)
        results = None
        if data:
            results = self.send_post(data, testing)

        return results

    def capture_payment(self, testing=False, order=None, amount=None):
        """Process payments without an authorization step."""
        if order:
            self.prepare_data(order)
        else:
            order = self.order

        recurlist = self.get_recurring_charge_data()
        if recurlist:
            success, results = self.process_recurring_subscriptions(recurlist, testing)
            if not success:
                self.log_extra('recur payment failed, aborting the rest of the module')
                return results

        if order.paid_in_full:
            self.log_extra('%s is paid in full, no capture attempted.', order)
            results = ProcessorResult(self.key, True, _("No charge needed, paid in full."))
            self.record_payment()
        else:
            self.log_extra('Capturing payment for %s', order)

            standard = self.get_standard_charge_data(amount=amount)
            results = self.send_post(standard, testing)

        return results

    def get_prior_auth_data(self, authorization, amount=None):
        """Build the dictionary needed to process a prior auth capture."""
        settings = self.settings
        trans = {'authorization' : authorization}
        remaining = authorization.remaining()
        if amount is None or amount > remaining:
            amount = remaining

        balance = trunc_decimal(amount, 2)
        trans['amount'] = amount

        if self.is_live():
            conn = settings.CONNECTION.value
            self.log_extra('Using live connection.')
        else:
            testflag = 'TRUE'
            conn = settings.CONNECTION_TEST.value
            self.log_extra('Using test connection.')

        if self.settings.SIMULATE.value:
            testflag = 'TRUE'
        else:
            testflag = 'FALSE'

        trans['connection'] = conn

        trans['configuration'] = {
            'x_login' : settings.LOGIN.value,
            'x_tran_key' : settings.TRANKEY.value,
            'x_version' : '3.1',
            'x_relay_response' : 'FALSE',
            'x_test_request' : testflag,
            'x_delim_data' : 'TRUE',
            'x_delim_char' : '|',
            'x_type': 'PRIOR_AUTH_CAPTURE',
            'x_trans_id' : authorization.transaction_id
            
            }

        self.log_extra('prior auth configuration: %s', trans['configuration'])

        trans['transactionData'] = {
            'x_amount' : balance,
            }

        part1 = urlencode(trans['configuration'])
        postdata = part1 + "&" + urlencode(trans['transactionData'])
        trans['postString'] = postdata

        self.log_extra('prior auth poststring: %s', postdata)
        trans['logPostString'] = postdata

        return trans


    def get_void_auth_data(self, authorization):
        """Build the dictionary needed to process a prior auth release."""
        settings = self.settings
        trans = {
            'authorization' : authorization,
            'amount' : Decimal('0.00'),
        }

        if self.is_live():
            conn = settings.CONNECTION.value
            self.log_extra('Using live connection.')
        else:
            testflag = 'TRUE'
            conn = settings.CONNECTION_TEST.value
            self.log_extra('Using test connection.')

        if self.settings.SIMULATE.value:
            testflag = 'TRUE'
        else:
            testflag = 'FALSE'

        trans['connection'] = conn

        trans['configuration'] = {
            'x_login' : settings.LOGIN.value,
            'x_tran_key' : settings.TRANKEY.value,
            'x_version' : '3.1',
            'x_relay_response' : 'FALSE',
            'x_test_request' : testflag,
            'x_delim_data' : 'TRUE',
            'x_delim_char' : '|',
            'x_type': 'VOID',
            'x_trans_id' : authorization.transaction_id
            }

        self.log_extra('void auth configuration: %s', trans['configuration'])

        postdata = urlencode(trans['configuration'])
        trans['postString'] = postdata

        self.log_extra('void auth poststring: %s', postdata)
        trans['logPostString'] = postdata

        return trans

    def get_recurring_charge_data(self, testing=False):
        """Build the list of dictionaries needed to process a recurring charge.

        Because Authorize can only take one subscription at a time, we build a list
        of the transaction dictionaries, for later sequential posting.
        """
        if not self.arb_enabled:
            return []

        # get all subscriptions from the order
        subscriptions = self.get_recurring_orderitems()

        if len(subscriptions) == 0:
            self.log_extra('No subscription items')
            return []

        settings = self.settings
        # set up the base dictionary
        trans = {}

        if self.is_live():
            conn = settings.ARB_CONNECTION.value
            self.log_extra('Using live recurring charge connection.')
        else:
            conn = settings.ARB_CONNECTION_TEST.value
            self.log_extra('Using test recurring charge connection.')

        shop_config = Config.objects.get_current()

        trans['connection'] = conn
        trans['config'] = {
            'merchantID' : settings.LOGIN.value,
            'transactionKey' : settings.TRANKEY.value,
            'shop_name' : shop_config.store_name,
        }
        trans['order'] = self.order
        trans['card'] = self.order.credit_card
        trans['card_expiration'] =  "%4i-%02i" % (self.order.credit_card.expire_year, self.order.credit_card.expire_month)

        translist = []
        taxer = get_tax_processor(user = self.order.user)

        for subscription in subscriptions:
            product = subscription.product
            subtrans = trans.copy()
            subtrans['subscription'] = subscription
            subtrans['product'] = product

            sub = product.subscriptionproduct

            trial = sub.get_trial_terms(0)
            if trial:
                price = trunc_decimal(trial.price, 2)
                trial_amount = price
                if price and subscription.product.taxable:
                    trial_amount = taxer.by_price(subscription.product.taxClass, price)
                    #todo, maybe add shipping for trial?
                amount = sub.recurring_price()
                trial_occurrences = trial.occurrences
                if not trial_occurrences:
                    self.log.warn("Trial expiration period is less than one recurring billing cycle. " +
                        "Authorize does not allow this, so the trial period has been adjusted to be equal to one recurring cycle.")
                    trial_occurrences = 1
            else:
                trial_occurrences = 0
                trial_amount = Decimal('0.00')
                amount = subscription.total_with_tax

            occurrences = sub.recurring_times + trial_occurrences
            if occurrences > 9999:
                occurrences = 9999

            subtrans['occurrences'] = occurrences
            subtrans['trial_occurrences'] = trial_occurrences
            subtrans['trial'] = trial
            subtrans['trial_amount'] = trunc_decimal(trial_amount, 2)
            subtrans['amount'] = trunc_decimal(amount, 2)
            if trial:
                charged_today = trial_amount
            else:
                charged_today = amount

            charged_today = trunc_decimal(charged_today, 2)

            subtrans['charged_today'] = charged_today
            translist.append(subtrans)

        return translist

    def get_standard_charge_data(self, amount=None, authorize=False):
        """Build the dictionary needed to process a credit card charge"""

        order = self.order
        settings = self.settings
        trans = {}
        if amount is None:
            amount = order.balance

        balance = trunc_decimal(amount, 2)
        trans['amount'] = balance

        if self.is_live():
            conn = settings.CONNECTION.value
            self.log_extra('Using live connection.')
        else:
            testflag = 'TRUE'
            conn = settings.CONNECTION_TEST.value
            self.log_extra('Using test connection.')

        if self.settings.SIMULATE.value:
            testflag = 'TRUE'
        else:
            testflag = 'FALSE'

        trans['connection'] = conn

        trans['authorize_only'] = authorize

        if not authorize:
            transaction_type = 'AUTH_CAPTURE'
        else:
            transaction_type = 'AUTH_ONLY'

        trans['configuration'] = {
            'x_login' : settings.LOGIN.value,
            'x_tran_key' : settings.TRANKEY.value,
            'x_version' : '3.1',
            'x_relay_response' : 'FALSE',
            'x_test_request' : testflag,
            'x_delim_data' : 'TRUE',
            'x_delim_char' : '|',
            'x_type': transaction_type,
            'x_method': 'CC',
            }

        self.log_extra('standard charges configuration: %s', trans['configuration'])
        
        trans['custBillData'] = {
            'x_first_name' : order.user.first_name,
            'x_last_name' : order.user.last_name,
            'x_address': order.full_bill_street,
            'x_city': order.bill_city,
            'x_state' : order.bill_state,
            'x_zip' : order.bill_postal_code,
            'x_country': order.bill_country,
            'x_phone' : order.user.phone,
            'x_email' : order.user.email,
            }

        trans['custShipData'] = {
            'x_ship_to_first_name' : order.user.first_name,
            'x_ship_to_last_name' : order.user.last_name,
            'x_ship_to_address' : order.full_ship_street,
            'x_ship_to_city' : order.ship_city,
            'x_ship_to_state' : order.ship_state,
            'x_ship_to_zip' : order.ship_postal_code,
            'x_ship_to_country' : order.ship_country,
        }

        self.log_extra('standard charges configuration: %s', trans['custBillData'])

        invoice = "%s" % order.id
        failct = order.paymentfailures.count()
        if failct > 0:
            invoice = "%s_%i" % (invoice, failct)

        if not self.is_live():
            # add random test id to this, for testing repeatability
            invoice = "%s_test_%s_%i" % (invoice,  datetime.now().strftime('%m%d%y'), random.randint(1,1000000))

        cc = order.credit_card.decryptedCC
        ccv = order.credit_card.ccv
        if not self.is_live() and cc == '4222222222222':
            if ccv == '222':
                self.log_extra('Setting a bad ccv number to force an error')
                ccv = '1'
            else:
                self.log_extra('Setting a bad credit card number to force an error')
                cc = '1234'
        trans['transactionData'] = {
            'x_amount' : balance,
            'x_card_num' : cc,
            'x_exp_date' : order.credit_card.expirationDate,
            'x_card_code' : ccv,
            'x_invoice_num' : invoice
            }

        part1 = urlencode(trans['configuration']) + "&"
        part2 = "&" + urlencode(trans['custBillData'])
        part3 = "&" + urlencode(trans['custShipData'])
        trans['postString'] = part1 + urlencode(trans['transactionData']) + part2 + part3

        redactedData = {
            'x_amount' : balance,
            'x_card_num' : order.credit_card.display_cc,
            'x_exp_date' : order.credit_card.expirationDate,
            'x_card_code' : "REDACTED",
            'x_invoice_num' : invoice
        }
        self.log_extra('standard charges transactionData: %s', redactedData)
        trans['logPostString'] = part1 + urlencode(redactedData) + part2

        return trans

    def process_recurring_subscriptions(self, recurlist, testing=False):
        """Post all subscription requests."""

        results = []
        for recur in recurlist:
            success, reason, response, subscription_id = self.process_recurring_subscription(recur, testing=testing)
            if success:
                if not testing:
                    payment = self.record_payment(order=self.order, amount=recur['charged_today'], transaction_id=subscription_id, reason_code=reason)
                    results.append(ProcessorResult(self.key, success, response, payment=payment))
            else:
                self.log.info("Failed to process recurring subscription, %s: %s", reason, response)
                break

        return success, results

    def process_recurring_subscription(self, data, testing=False):
        """Post one subscription request."""
        self.log_extra('Processing subscription: %s', data['product'].slug)

        t = loader.get_template('shop/checkout/authorizenetconventions/arb_create_subscription.xml')
        ctx = Context(data)
        request = t.render(ctx)

        if self.settings.EXTRA_LOGGING.value:
            data['redact'] = True
            ctx = Context(data)
            redacted = t.render(ctx)
            self.log_extra('Posting data to: %s\n%s', data['connection'], redacted)

        headers = {'Content-type':'text/xml'}
        conn = urllib2.Request(data['connection'], request, headers)
        try:
            f = urllib2.urlopen(conn)
            all_results = f.read()
        except urllib2.URLError, ue:
            self.log.error("error opening %s\n%s", data['connection'], ue)
            return (False, 'ERROR', _('Could not talk to Authorize.net gateway'), None)

        self.log_extra('Authorize response: %s', all_results)

        subscriptionID = None
        try:
            response = minidom.parseString(all_results)
            doc = response.documentElement
            reason = doc.getElementsByTagName('code')[0].firstChild.nodeValue
            response_text = doc.getElementsByTagName('text')[0].firstChild.nodeValue
            result = doc.getElementsByTagName('resultCode')[0].firstChild.nodeValue
            success = result == "Ok"

            if success:
                #refID = doc.getElementsByTagName('refId')[0].firstChild.nodeValue
                subscriptionID = doc.getElementsByTagName('subscriptionId')[0].firstChild.nodeValue
        except Exception, e:
            self.log.error("Error %s\nCould not parse response: %s", e, all_results)
            success = False
            reason = "Parse Error"
            response_text = "Could not parse response"

        return success, reason, response_text, subscriptionID


    def release_authorized_payment(self, order=None, auth=None, testing=False):
        """Release a previously authorized payment."""
        if order:
            self.prepare_data(order)
        else:
            order = self.order

        self.log_extra('Releasing Authorization #%i for %s', auth.id, order)
        data = self.get_void_auth_data(auth)
        results = None
        if data:
            results = self.send_post(data, testing)

        if results.success:
            auth.complete = True
            auth.save()

        return results

    def send_post(self, data, testing=False, amount=None):
        """Execute the post to Authorize Net.

        Params:
        - data: dictionary as returned by get_standard_charge_data
        - testing: if true, then don't record the payment

        Returns:
        - ProcessorResult
        """
        self.log.info("About to send a request to authorize.net: %(connection)s\n%(logPostString)s", data)

        conn = urllib2.Request(url=data['connection'], data=data['postString'])
        try:
            f = urllib2.urlopen(conn)
            all_results = f.read()
            self.log_extra('Authorize response: %s', all_results)
        except urllib2.URLError, ue:
            self.log.error("error opening %s\n%s", data['connection'], ue)
            return ProcessorResult(self.key, False, _('Could not talk to Authorize.net gateway'))

        parsed_results = all_results.split(data['configuration']['x_delim_char'])
        response_code = parsed_results[0]
        reason_code = parsed_results[1]
        response_text = parsed_results[3]
        transaction_id = parsed_results[6]
        success = response_code == '1'
        if amount is None:
            amount = data['amount']

        payment = None
        if success and not testing:
            if data.get('authorize_only', False):
                self.log_extra('Success, recording authorization')
                payment = self.record_authorization(order=self.order, amount=amount,
                    transaction_id=transaction_id, reason_code=reason_code)
            else:
                if amount <= 0:
                    self.log_extra('Success, recording refund')
                else:
                    self.log_extra('Success, recording payment')
                authorization = data.get('authorization', None)
                payment = self.record_payment(order=self.order, amount=amount,
                    transaction_id=transaction_id, reason_code=reason_code, authorization=authorization)

        elif not testing:
            payment = self.record_failure(amount=amount, transaction_id=transaction_id,
                reason_code=reason_code, details=response_text)

        self.log_extra("Returning success=%s, reason=%s, response_text=%s", success, reason_code, response_text)
        return ProcessorResult(self.key, success, response_text, payment=payment)



    ####### CIM ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ######
    def authorize_cim_payment(self):
        """Authorize a CIM payment. Not yet implemented 11/01/2011"""
        self.log.warn('Module does not implement authorize_cim_payment: %s', self.key)
        return ProcessorResult(self.key, False, _("Not Implemented"))
    

    def process_recurring_cim(self, testing=False, amount=None, payment_profile_id=None):
        """This will process the payment."""
        if self.can_authorize() and not self.settings.CAPTURE.value:
            self.log_extra('Authorizing payment on order #%i', self.order.id)
            return self.authorize_cim_payment(testing=testing)
        else:
            self.log_extra('Capturing payment on order #%i', self.order.id)
            return self.capture_cim_recurring_payment(testing=testing, amount=amount, payment_profile_id=payment_profile_id)
    

    def process_cim(self, testing=False):
        """This will process the payment."""
        if self.can_authorize() and not self.settings.CAPTURE.value:
            self.log_extra('Authorizing payment on order #%i', self.order.id)
            return self.authorize_cim_payment(testing=testing)
        else:
            self.log_extra('Capturing payment on order #%i', self.order.id)
            return self.capture_cim_payment(testing=testing)
    

    def capture_cim_recurring_payment(self, testing=False, order=None, amount=None, payment_profile_id=None):
        """Process recurring payments."""
        if order:
            self.prepare_data(order)
        else:
            order = self.order
        
        customer_profile_id = order.user.cim_convention.customer_profile_id
        
        data = self.cim_get_config_data()
        data['customer_profile_id'] = customer_profile_id
        data['payment_profile_id'] = payment_profile_id
        data['order_id'] = order.id
        data['amount'] = amount
        
        success, reason, response_text, doc = self.cim_create_customer_profile_transaction_request(data)

        payment = None
        if success:
            direct_response = doc.getElementsByTagName('directResponse')[0].firstChild.nodeValue 
            drs = direct_response.split(',')
            transaction_id = drs[6]
            authorization = data.get('authorization', None)
            payment = self.record_payment(order=self.order, amount=data['amount'],
                transaction_id=transaction_id, reason_code=reason, authorization=authorization, 
                payment_profile_id=payment_profile_id)
        else:
            try:
                direct_response = doc.getElementsByTagName('directResponse')[0].firstChild.nodeValue 
                drs = direct_response.split(',')
                transaction_id = drs[6]
            except:
                transaction_id = None
            payment = self.record_failure(amount=amount, transaction_id=transaction_id,
                reason_code=reason, details=response_text)
            

        self.log_extra("Returning success=%s, reason=%s, response_text=%s", success, reason, response_text)
        return ProcessorResult(self.key, success, response_text, payment=payment)
    

    def capture_cim_payment(self, testing=False, order=None, amount=None):
        from marvin.core.accounts.models import UserCIMConvention
        """Process payments without an authorization step."""
        if order:
            self.prepare_data(order)
        else:
            order = self.order
            
        
        data = self.cim_get_config_data()
        charge_data = self.cim_get_charge_data()
        data.update(charge_data)
        if self.is_live():
            data["validation_mode"] = 'liveMode'
        else:
            data["validation_mode"] = 'testMode'
            
        # Grab the user's CIM and payment profile if they already have it.
        # Create a new CIM and payment profile if they don't.
        user = order.user
        try:
            user_cim = user.cim_convention
            customer_profile_id = user_cim.customer_profile_id
            ibo_number, payment_profiles = self.cim_get_customer_profile_request(customer_profile_id)
            
            # Now check to see if the card info we are submitting already has an ID
            payment_profile_id = None
            for pp_id, pp in payment_profiles.iteritems():
                pp_bill_to = pp['billTo']
                data_bill_to = data['cust_bill_data']
                if (pp_bill_to['firstName'].strip(' ') == data_bill_to['first_name'].strip(' ') and 
                        ('lastName' in pp_bill_to and 'last_name' in data_bill_to and pp_bill_to['lastName'].strip(' ') == data_bill_to['last_name'].strip(' ')) and
                        pp_bill_to['address'].split(' ')[0] == data_bill_to['address'].split(' ')[0] and
                        pp_bill_to['zip'] == data_bill_to['zip'] and
                        pp['payment']['creditCard']['cardNumber'][-4:] == data['transaction_data']['card_num'][-4:]):
                    # found the profile, set the id.
                    payment_profile_id = pp_id
            # If no payment profile was found, create one
            if not payment_profile_id:
                success, reason, response_text, doc = self.cim_create_customer_payment_profile_request(data)
                
                payment_profile_id = None
                if success:
                    payment_profile_id = doc.getElementsByTagName('customerPaymentProfileId')[0].firstChild.nodeValue
                else:
                    try:
                        direct_response = doc.getElementsByTagName('validationDirectResponse')[0].firstChild.nodeValue 
                    except:
                        raise Exception('Error returned from API: %s' % doc.toxml()) 
                    drs = direct_response.split(',')
                    transaction_id = drs[6]
                    payment = self.record_failure(amount=amount, transaction_id=transaction_id,
                        reason_code=reason, details=response_text)
                    self.log_extra("Returning success=%s, reason=%s, response_text=%s", success, reason, response_text)

                    return ProcessorResult(self.key, success, response_text, payment=payment)
                    # raise Exception('Creation of customer payment profile failed. XML: %s' % doc.toxml()) 
                
        except UserCIMConvention.DoesNotExist:
            # This is the first time the user has ordered, create their info.
            success, reason, response_text, doc = self.cim_create_customer_profile_request(data)
            
            customer_profile_id = None
            payment_profile_id = None
            if success:
                customer_profile_id = doc.getElementsByTagName('customerProfileId')[0].firstChild.nodeValue 
                payment_profile_id = doc.getElementsByTagName('customerPaymentProfileIdList')[0].firstChild.firstChild.nodeValue
            else:
                if reason == 'E00039':
                    # Lame that they don't provide the information back that we need in an easily digestible form.
                    re_search = re.search('A duplicate record with ID (\d+) already exists', response_text)
                    customer_profile_id = re_search.groups()[0]
                    merchant_customer_id, payment_profile_id = self.cim_get_customer_profile_request(customer_profile_id)
                else:
                    try:
                        direct_response = doc.getElementsByTagName('validationDirectResponseList')[0].firstChild.firstChild.nodeValue 
                    except:
                        raise Exception('Error returned from API: %s' % doc.toxml()) 
                    drs = direct_response.split(',')
                    transaction_id = drs[6]
                    payment = self.record_failure(amount=amount, transaction_id=transaction_id,
                        reason_code=reason, details=response_text)
                    self.log_extra("Returning success=%s, reason=%s, response_text=%s", success, reason, response_text)

                    return ProcessorResult(self.key, success, response_text, payment=payment)
                
            user_cim = UserCIMConvention()
            user_cim.user = user            
            user_cim.customer_profile_id = customer_profile_id
            user_cim.save()
        
        data['customer_profile_id'] = customer_profile_id
        data['payment_profile_id'] = payment_profile_id
        data['order_id'] = order.id
        
        # ----- Zenos added - Delayed Billing - 2012/01/15 -----
        # If a subscription product is ordered after the 15th, the customer is not charged
        if Decimal(data["delay_amount"]) > 0:
            # This is an order that has delayed order items.
            success = True
            reason = "Delayed Billing"
            response_text = "Order payment was delayed until next billing cycle"
            transaction_id = "DelayedBilling"
            authorization = data.get('authorization', None)
            payment = self.record_payment(order=self.order, amount=data['delay_amount'],
                transaction_id=transaction_id, reason_code=reason, authorization=authorization, 
                payment_profile_id=payment_profile_id)
            
        # Normal workflow
        if Decimal(data["amount"]) > 0:
            success, reason, response_text, doc = self.cim_create_customer_profile_transaction_request(data)
        
            payment = None
            if success:
                direct_response = doc.getElementsByTagName('directResponse')[0].firstChild.nodeValue 
                drs = direct_response.split(',')
                transaction_id = drs[6]
                authorization = data.get('authorization', None)
                payment = self.record_payment(order=self.order, amount=data['amount'],
                    transaction_id=transaction_id, reason_code=reason, authorization=authorization, 
                    payment_profile_id=payment_profile_id)
            else:
                try:
                    direct_response = doc.getElementsByTagName('directResponse')[0].firstChild.nodeValue 
                except:
                    raise Exception('Error returned from API: %s' % doc.toxml()) 
            
                drs = direct_response.split(',')
                transaction_id = drs[6]
                payment = self.record_failure(amount=amount, transaction_id=transaction_id,
                     reason_code=reason, details=response_text)
        
        self.log_extra("Returning success=%s, reason=%s, response_text=%s", success, reason, response_text)
        return ProcessorResult(self.key, success, response_text, payment=payment)
    
    
    def cim_create_customer_profile_transaction_request(self, data):
        '''
        createCustomerProfileTransactionRequest
        '''
        template = 'shop/checkout/authorizenetconventions/cim_customer_profile_transaction_request.xml'
        return self.cim_process_request(data, template)
    

    def cim_issue_refund(self, amount, order_item, order_payment):
        '''
        uses createCustomerProfileTransactionRequest
        This is being created one off for now, should be updated to be more generic in the future
        '''
        
        order = order_item.order
        self.order = order
        
        data = self.cim_get_config_data()
            
        # override the amount that was returned from cim_get_charge_data. It is for the order, and we are issuing a refund
        data['amount'] = amount
        
        # add the transaction key
        data['trans_id'] = order_payment.transaction_id
        
        user = order.user
        # assumed to have CIM, since an order was placed against it.
        user_cim = user.cim_convention
        customer_profile_id = user_cim.customer_profile_id
        payment_profile_id = order_payment.cim_convention_payment_profile.payment_profile_id
        
        data['customer_profile_id'] = customer_profile_id
        data['payment_profile_id'] = payment_profile_id
        data['order_id'] = order.id
        
        template = 'shop/checkout/authorizenetconventions/cim_issue_refund.xml'
        
        success, reason, response_text, doc = self.cim_process_request(data, template)
        
        if success:
            direct_response = doc.getElementsByTagName('directResponse')[0].firstChild.nodeValue 
            drs = direct_response.split(',')
            transaction_id = drs[6]
            authorization = data.get('authorization', None)
            payment = self.record_refund(order=self.order, amount=data['amount'],
                transaction_id=transaction_id, details='REFUND', reason_code='REFUND', authorization=authorization, 
                payment_profile_id=payment_profile_id)


    def cim_process_request(self, data, template):
        t = loader.get_template(template)
        ctx = Context(data)
        request = t.render(ctx)
        
        headers = {'Content-type':'text/xml'}
        conn = urllib2.Request(data['connection'], request, headers)
        try:
            f = urllib2.urlopen(conn)
            all_results = f.read()
        except urllib2.URLError, ue:
            self.log.error("error opening %s\n%s", data['connection'], ue)
            return (False, 'ERROR', _('Could not talk to Authorize.net gateway'), None)
        
        self.log_extra('Authorize response: %s', all_results)
        
        success = False
        try:
            response = minidom.parseString(all_results)
            doc = response.documentElement
            reason = doc.getElementsByTagName('code')[0].firstChild.nodeValue
            response_text = doc.getElementsByTagName('text')[0].firstChild.nodeValue
            result = doc.getElementsByTagName('resultCode')[0].firstChild.nodeValue
            success = result == "Ok"
            
        except Exception, e:
            self.log.error("Error %s\nCould not parse response: %s", e, all_results)
            success = False
            reason = "Parse Error"
            response_text = "Could not parse response"
        
        return success, reason, response_text, doc
    

    def cim_get_config_data(self):
        settings = self.settings
        # set up the base dictionary
        data = {}
        
        if self.is_live():
            conn = settings.CIM_CONNECTION.value
            self.log_extra('Using live recurring charge connection.')
        else:
            conn = settings.CIM_CONNECTION_TEST.value
            self.log_extra('Using test recurring charge connection.')

        shop_config = Config.objects.get_current()

        data['connection'] = conn
        data['config'] = {
            'merchantID' : settings.LOGIN.value,
            'transactionKey' : settings.TRANKEY.value,
            'shop_name' : shop_config.store_name,
        }
        
        return data
    

    def cim_get_charge_data(self):
        order = self.order
        
        data = {}
        
        amount = order.balance
        delay_amount = Decimal("0.00")
        
        # ----- Zenos added - Delayed Billing - 2012/01/15 -----
        # If a subscription product is ordered after the 15th, the customer is not charged
        if order.time_stamp.day >= 15:
            all_delayed = True
            for oi in order.orderitem_set.all():
                if oi.product.delay_billing:
                    delay_amount += oi.total_with_tax
                else:
                    all_delayed = False
            if all_delayed:
                # This mitigates some problems we are having with rounding errors 2012/01/31. This should eliminate the need for the fix below, but will leave in.
                delay_amount = order.balance
        
        amount = amount - delay_amount
        
        # Sometimes we have a rounding error. Fix the amount if that is the case.
        if amount < Decimal("0.01"):
            amount = Decimal("0")
        
        balance = trunc_decimal(amount, 2)
        balance = str(balance).replace(',','.')
        data['amount'] = balance
        
        delay_balance = trunc_decimal(delay_amount, 2)
        delay_balance = str(delay_balance).replace(',','.')
        data['delay_amount'] = delay_balance

        phone=order.user.phone
        if phone is not None:
            phone=order.user.phone.number
        else:
            phone=0

        data['cust_bill_data'] = {
            'first_name' : order.user.first_name,
            'last_name' : order.user.last_name,
            'address': order.full_bill_street,
            'city': order.bill_city,
            'state' : order.bill_state,
            'zip' : order.bill_postal_code,
            'country': order.bill_country,
            'email' : order.user.email,
            }
        if phone:
            data['cust_bill_data']['phone']=phone

        data['cust_ship_data'] = {
            'ship_to_first_name' : order.user.first_name,
            'ship_to_last_name' : order.user.last_name,
            'ship_to_address' : order.full_ship_street,
            'ship_to_city' : order.ship_city,
            'ship_to_state' : order.ship_state,
            'ship_to_zip' : order.ship_postal_code,
            'ship_to_country' : order.ship_country,
        }
        
        data["user"] = order.user
        
        # data['card'] = self.order.credit_card
        # data['card_expiration'] =  "%4i-%02i" % (self.order.credit_card.expire_year, self.order.credit_card.expire_month)
        
        self.log_extra('standard charges configuration: %s', data['cust_bill_data'])

        invoice = "%s" % order.id
        
        # failct = order.paymentfailures.count()
        # if failct > 0:
        #     invoice = "%s_%i" % (invoice, failct)
        # 
        # if not self.is_live():
        #     # add random test id to this, for testing repeatability
        #     invoice = "%s_test_%s_%i" % (invoice,  datetime.now().strftime('%m%d%y'), random.randint(1,1000000))
        
        cc = order.credit_card.decryptedCC
        ccv = order.credit_card.ccv
        if not self.is_live() and cc == '4222222222222':
            if ccv == '222':
                self.log_extra('Setting a bad ccv number to force an error')
                ccv = '1'
            else:
                self.log_extra('Setting a bad credit card number to force an error')
                cc = '1234'
        data['transaction_data'] = {
            'amount' : balance,
            'card_num' : cc,
            'exp_date' : order.credit_card.expirationDate,
            'formatted_exp' :  "%4i-%02i" % (self.order.credit_card.expire_year, self.order.credit_card.expire_month),
            'card_code' : ccv,
            'invoice_num' : invoice
            }
        
        
        
        return data
    

    def cim_create_customer_profile_request(self, data):
        '''
        createCustomerProfileRequest    
        '''
        import re
        
        template = 'shop/checkout/authorizenetconventions/cim_create_customer_profile.xml'
        success, reason, response_text, doc = self.cim_process_request(data, template)
                
        return success, reason, response_text, doc
    

    def cim_get_customer_profile_request(self, customer_profile_id=None):
        '''
        getCustomerProfileRequest
        '''
        # Get the standard data needed for all requests
        data = self.cim_get_config_data()
        
        # Add additional data for request
        data['customer_profile_id'] = customer_profile_id

        self.log_extra('Getting Customer CIM Profile: %s', data['customer_profile_id'])
        
        # Set the template
        template = 'shop/checkout/authorizenetconventions/cim_get_customer_profile.xml'
        
        # Process the request from Authorize.net
        success, reason, response_text, doc = self.cim_process_request(data, template)
        
        payment_profiles = {}
        if success:
            merchant_customer_id = doc.getElementsByTagName('merchantCustomerId')[0].firstChild.nodeValue
            payment_profile_nodes = doc.getElementsByTagName('paymentProfiles')
            for pp in payment_profile_nodes:
                payment_profile_id = pp.getElementsByTagName('customerPaymentProfileId')[0].firstChild.nodeValue
                payment_profiles[payment_profile_id] = nodeToDic(pp)
        else:
            raise Exception('Error returned from API: %s: %s' %(reason, response_text))
        
        return merchant_customer_id, payment_profiles
    

    def cim_create_customer_payment_profile_request(self, data):
        '''
        createCustomerPaymentProfileRequest
        '''
        template = 'shop/checkout/authorizenetconventions/cim_create_customer_payment_profile.xml'
        
        success, reason, response_text, doc = self.cim_process_request(data, template)
        
        return success, reason, response_text, doc
    

    def cim_get_customer_profile_ids_request(self):
        '''
        getCustomerProfileIdsRequest
        Gets all customer IDs
        '''
        data = self.cim_get_config_data()
        template = 'shop/checkout/authorizenetconventions/cim_get_customer_profile_ids.xml'
        success, reason, response_text, doc = self.cim_process_request(data, template)
        
        if success:
            return doc
        else:
            return None
        
    

# Modules that can be created
# createCustomerShippingAddressRequest
# createCustomerProfileTransactionRequest
# deleteCustomerProfileRequest
# deleteCustomerPaymentProfileRequest
# deleteCustomerShippingAddressRequest
### getCustomerProfileIdsRequest
### getCustomerProfileRequest
# getCustomerPaymentProfileRequest
# getCustomerShippingAddressRequest
# getHostedProfilePageRequest
# updateCustomerProfileRequest
# updateCustomerPaymentProfileRequest
# updateCustomerShippingAddressRequest
# updateSplitTenderGroupRequest
# validateCustomerPaymentProfileRequest

class NotTextNodeError:
    pass


def getTextFromNode(node):
    """
    scans through all children of node and gathers the
    text. if node has non-text child-nodes, then
    NotTextNodeError is raised.
    """
    t = ""
    for n in node.childNodes:
	if n.nodeType == n.TEXT_NODE:
	    t += n.nodeValue
	else:
	    raise NotTextNodeError
    return t


def nodeToDic(node):
    """
    nodeToDic() scans through the children of node and makes a
    dictionary from the content.
    three cases are differentiated:
	- if the node contains no other nodes, it is a text-node
    and {nodeName:text} is merged into the dictionary.
	- if the node has the attribute "method" set to "true",
    then it's children will be appended to a list and this
    list is merged to the dictionary in the form: {nodeName:list}.
	- else, nodeToDic() will call itself recursively on
    the nodes children (merging {nodeName:nodeToDic()} to
    the dictionary).
    """
    dic = {} 
    for n in node.childNodes:
	if n.nodeType != n.ELEMENT_NODE:
	    continue
	if n.getAttribute("multiple") == "true":
	    # node with multiple children:
	    # put them in a list
	    l = []
	    for c in n.childNodes:
	        if c.nodeType != n.ELEMENT_NODE:
		    continue
		l.append(nodeToDic(c))
	        dic.update({n.nodeName:l})
	    continue
		
	try:
	    text = getTextFromNode(n)
	except NotTextNodeError:
            # 'normal' node
            dic.update({n.nodeName:nodeToDic(n)})
            continue

        # text node
        dic.update({n.nodeName:text})
	continue
    return dic



if __name__ == "__main__":
    """
    This is for testing - enabling you to run from the command line and make
    sure everything is ok
    """
    import os
    from livesettings import config_get_group
    
    # Set up some dummy classes to mimic classes being passed through Satchmo
    class testContact(object):
        pass
    class testCC(object):
        pass
    class testOrder(object):
        def __init__(self):
            self.contact = testContact()
            self.credit_card = testCC()
        def order_success(self):
            pass

    if not os.environ.has_key("DJANGO_SETTINGS_MODULE"):
        os.environ["DJANGO_SETTINGS_MODULE"]="satchmo_store.settings"

    settings_module = os.environ['DJANGO_SETTINGS_MODULE']
    settingsl = settings_module.split('.')
    settings = __import__(settings_module, {}, {}, settingsl[-1])

    sampleOrder = testOrder()
    sampleOrder.contact.first_name = 'Chris'
    sampleOrder.contact.last_name = 'Smith'
    sampleOrder.contact.primary_phone = '801-555-9242'
    sampleOrder.full_bill_street = '123 Main Street'
    sampleOrder.bill_postal_code = '12345'
    sampleOrder.bill_state = 'TN'
    sampleOrder.bill_city = 'Some City'
    sampleOrder.bill_country = 'US'
    sampleOrder.total = "27.01"
    sampleOrder.balance = "27.01"
    sampleOrder.credit_card.decryptedCC = '6011000000000012'
    sampleOrder.credit_card.expirationDate = "10/11"
    sampleOrder.credit_card.ccv = "144"

    authorize_settings = config_get_group('PAYMENT_AUTHORIZENETCONVENTION')
    if authorize_settings.LIVE.value:
        print "Warning.  You are submitting a live order.  AUTHORIZE.NET system is set LIVE."

    processor = PaymentProcessor(authorize_settings)
    processor.prepare_data(sampleOrder)
    results = processor.process(testing=True)
    print results


