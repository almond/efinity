from django.conf.urls.defaults import patterns
from marvin.store.shop.satchmo_settings import get_satchmo_setting

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     (r'^$', 'marvin.store.payments.modules.authorizenet.views.pay_ship_info', {'SSL':ssl}, 'AUTHORIZENET_checkout-step2'),
     (r'^confirm/$', 'marvin.store.payments.modules.authorizenet.views.confirm_info', {'SSL':ssl}, 'AUTHORIZENET_checkout-step3'),
     (r'^success/$', 'marvin.store.payments.views.checkout.success', {'SSL':ssl}, 'AUTHORIZENET_checkout-success'),
)
