from django.conf.urls.defaults import patterns
from marvin.store.shop.satchmo_settings import get_satchmo_setting

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     (r'^$', 'marvin.store.payments.modules.dummy.views.pay_ship_info', {'SSL':ssl}, 'DUMMY_checkout-step2'),
     (r'^confirm/$', 'marvin.store.payments.modules.dummy.views.confirm_info', {'SSL':ssl}, 'DUMMY_checkout-step3'),
     (r'^success/$', 'marvin.store.payments.views.checkout.success', {'SSL':ssl}, 'DUMMY_checkout-success'),
)
