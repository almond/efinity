"""Simple wrapper for standard checkout as implemented in payment.views"""

from django.views.decorators.cache import never_cache
from marvin.utils.dbsettings import config_get_group
from marvin.store.payments.views import confirm, payship
    
dummy = config_get_group('PAYMENT_DUMMY')

@never_cache
def pay_ship_info(request):
    return payship.credit_pay_ship_info(request, dummy)

@never_cache
def confirm_info(request):
    return confirm.credit_confirm_info(request, dummy)
