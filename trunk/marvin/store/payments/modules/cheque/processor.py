"""                                                                                                                                                                       
Handle a cheque payments
"""
from django.utils.translation import ugettext as _
from marvin.store.payments.modules.base import BasePaymentProcessor, ProcessorResult

class PaymentProcessor(BasePaymentProcessor):
    """CHEQUE Payment Processor"""

    def __init__(self, settings):
        super(PaymentProcessor, self).__init__('cheque', settings)

    def capture_payment(self, testing=False, order=None, amount=None):
        """                                                                                                                                                                                 CHEQUE is always successful.                                                                                                                                                         """
        if not order:
            order = self.order

        if amount is None:
            amount = order.balance

        payment = self.record_payment(order=order, amount=amount,
            transaction_id="CHEQUE", reason_code='0')

        return ProcessorResult(self.key, True, _('Success'), payment)

    def can_authorize(self):
        return True

    def authorize_cim_payment(self):
        """Authorize a CIM payment. Not yet implemented 11/01/2011"""
        self.log.warn('Module does not implement authorize_cim_payment: %s', self.key)
        
        payment = self.record_payment(order=self.order, amount=self.order.balance, reason_code='Cheque Payment',)
            
        return ProcessorResult(self.key, True, _('Success'))

    def process_cim(self, testing=False):
        """This will process the payment."""
        return self.authorize_cim_payment()
