from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache

from marvin.store.shop.models import Order, OrderPayment
from marvin.utils.views import bad_or_missing

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
import logging

@never_cache
def success(request):
    """
    The order has been succesfully processed. 
    This can be used to generate a receipt or some other confirmation
    
    """
    try:
        order = Order.objects.from_request(request)
    except Order.DoesNotExist:
        return bad_or_missing(request, _('Your order has already been processed.'))

    del request.session['orderID']

    """
    Send an email to the store admin about the order.
    """
    # op=OrderPayment.objects.get(order=order)
    ops = OrderPayment.objects.filter(order=order)
    op = ops[len(ops)-1]
    log = logging.getLogger('marvin.mail')
    
    if not settings.DEBUG and ( 'send_email' not in request.session or ('send_email' in request.session and request.session['send_email'])):
        if 'send_email' in request.session:
            del request.session['send_email']

        from marvin.store.shop.models import Config
        shop_config = Config.objects.get_current()
        shop_email = 'mailto:efinity@mediatechplus.com'

        mailcontent = get_template('shop/checkout/success_main.html')
        subject, from_email, to = 'Order Notification', settings.DEFAULT_FROM_EMAIL, shop_email
        msg = EmailMultiAlternatives(subject, '', from_email, [to])
        msg.attach_alternative(mailcontent.render(RequestContext(request, {'order':order})), "text/html")
        msg.send()
    else:
        log.warn('Not sending order notification emails as Debug mode is set to True.')

    return render_to_response('shop/checkout/success.html', {
        'order': order
    }, context_instance=RequestContext(request))

def failure(request):
    return render_to_response('shop/checkout/failure.html',{},
        context_instance=RequestContext(request))
