####################################################################
# First step in the order process - capture all the demographic info
#####################################################################

from django import http
from django.core import urlresolvers

from marvin.utils.view import *

from django.shortcuts import render_to_response
from django.template import RequestContext
from marvin.utils.dbsettings import config_get_group, config_value
from marvin.core.contacts.forms import area_choices_for_country
from marvin.core.contacts.models import Contact
from marvin.store.payments.decorators import cart_has_minimum_order
from marvin.store.payments.forms import ContactInfoForm, PaymentContactInfoForm
from marvin.store.shop.models import Cart, Config, Order
from marvin.utils.dynamic import lookup_url
from marvin.utils.signals_ahoy.signals import form_initialdata

import logging

log = logging.getLogger('marvin.store.payments.views.contact')

def authentication_required(request, template='shop/checkout/authentication_required.html'):
    return render_to_response(
        template, {}, context_instance = RequestContext(request)
    )

@login_required
def contact_info(request, **kwargs):
    """View which collects demographic information from customer."""

    send_email=request.POST.get('send_email', None)
    if not send_email:
        request.session['send_email']=False
    else:
        request.session['send_email']=True

    #First verify that the cart exists and has items
    tempCart = Cart.objects.from_request(request)
    if tempCart.numItems == 0:
        return render_to_response('shop/checkout/empty_cart.html',
                                  context_instance=RequestContext(request))

    if not request.user.is_authenticated() and config_value('SHOP', 'AUTHENTICATION_REQUIRED'):
        url = urlresolvers.reverse('checkout_auth_required')
        thisurl = urlresolvers.reverse('checkout-step1')
        return http.HttpResponseRedirect(url + "?next=" + thisurl)

    init_data = {}
    shop = Config.objects.get_current()
    if request.user.is_authenticated():
        if request.user.email:
            init_data['email'] = request.user.email
        if request.user.first_name:
            init_data['first_name'] = request.user.first_name
        if request.user.last_name:
            init_data['last_name'] = request.user.last_name
    
    contact = request.user.contact

    try:
        order = Order.objects.from_request(request)
        if order.discount_code:
            init_data['discount'] = order.discount_code
    except Order.DoesNotExist:
        pass

    if request.method == "POST":
        new_data = request.POST.copy()
        
        if not tempCart.is_shippable:
            new_data['copy_address'] = True

        form = PaymentContactInfoForm(
            user=request.user,
            data=new_data,
            shop=shop,
            contact=contact,
            shippable=tempCart.is_shippable,
            initial=init_data,
            cart=tempCart)

        if form.is_valid():
            form.save(request, cart=tempCart, user=request.user, contact=contact)

            modulename = new_data['paymentmethod']
            if not modulename.startswith('PAYMENT_'):
                modulename = 'PAYMENT_' + modulename
            paymentmodule = config_get_group(modulename)
            url = lookup_url(paymentmodule, 'checkout-step2')
            return http.HttpResponseRedirect(url)
        else:
            log.debug("Form errors: %s", form.errors)
    else:
        for item in contact.__dict__.keys():
            init_data[item] = getattr(contact,item)
        
        if request.user.shipping_address:
            for item in request.user.shipping_address.__dict__.keys():
                init_data["ship_"+item] = getattr(request.user.shipping_address,item)
        
        if request.user.billing_address:
            for item in request.user.billing_address.__dict__.keys():
                init_data[item] = getattr(request.user.billing_address,item)
        
        if request.user.phone:
            init_data['phone'] = request.user.phone.number

        form = PaymentContactInfoForm(
            shop=shop,
            contact=contact,
            shippable=tempCart.is_shippable,
            initial=init_data,
            cart=tempCart)

    only_country = None
    if shop.in_country_only:
        only_country = shop.sales_country
    else:
        only_country = None


    from l10n.models import Country
    countries=Country.objects.filter(active=True)
    country_states={}
    for country in countries:
        areas=country.adminarea_set.filter(active=True)
        choices=[]
        if areas.count()>0:
            choices = [('',"---Please Select---")] 
            choices.extend([(area.abbrev or area.name, area.name) for area in areas])
        else:
            choices = [('',"Not Applicable")]
        country_states[country.id]=choices

    context = RequestContext(request, {
        'form': form,
        'country': only_country,
        'paymentmethod_ct': len(form.fields['paymentmethod'].choices),
        'cart': tempCart,
        'country_states':country_states
        })
    return render_to_response('shop/checkout/form.html',
                              context_instance=context)

contact_info_view = cart_has_minimum_order()(contact_info)
