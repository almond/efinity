import logging
from marvin.utils.templatetags import get_filter_args
from django import template
from marvin.store.shop.models import Cart, NullCart
from django.contrib.contenttypes.models import ContentType

log = logging.getLogger("select_payment_type")
register = template.Library()

@register.tag
def select_payment_type(parser, token):
    try:
        tag_name, request = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires exactly one argument" % token.contents.split()[0])

    return SelectPaymentType(request)

class SelectPaymentType(template.Node):
    def __init__(self, request):
        self.request=template.Variable(request)

    def render(self, context):
        request=self.request.resolve(context)
        cart=Cart.objects.from_request(request=request, create=False)
        cart_items=cart.cartitem_set.all()
        item=cart_items[0]
        #if item.product.is_product:
        return '<script>document.getElementById("id_paymentmethod_0").checked=true</script>'
        #else:
        #return '<script>document.getElementById("id_paymentmethod_1").checked=true</script>'
