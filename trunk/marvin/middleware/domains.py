import re
from django.conf import settings
from django.contrib.sites.models import Site

class DomainMiddleware(object):
    """
    Domain Middleware handles processing for subdomain parsing,
    and dynamically changes Django settings based off the
    requested domain name. This is the core to allowing us to
    to offer Cross Market as a SaaS.

    Gets the correct instance of an application-specific model
    by matching the sub-domain of the request.

    Based off code obtained from:
        http://www.redrobotstudios.com/blog/2008/12/12/handling-subdomains-in-django/
    """

    def __init__(self):
        self.site_domain = Site.objects.get_current().domain
        if self.site_domain.startswith('www.'):
            self.site_domain = self.site_domain[4:]
        self.SUBDOMAIN_RE = re.compile(r'^(?:www\.)?(?P<slug>[\w-]+)\.%s' % re.escape(self.site_domain))
        try:
            app_name, model_name = settings.DOMAIN_MIDDLEWARE_MODEL.split('.', 2)
            self.model = get_model(app_name, model_name)
            self.instance_name = settings.DOMAIN_MIDDLEWARE_INSTANCE_NAME
            assert self.instance_name
        except (AttributeError, AssertionError):
            raise ImproperlyConfigured('DomainMiddleware requires DOMAIN_MIDDLEWARE_MODEL and DOMAIN_MIDDLEWARE_INSTANCE_NAME settings')

