#
# This enables global access to the request object.
#

from marvin.middleware.threaded.threadlocals import set_thread_variable, set_current_user

class ThreadLocalMiddleware(object):
    """Middleware that gets various objects from the
    request object and saves them in thread local storage."""

    def process_request(self, request):
        set_thread_variable('request', request)
        set_current_user(request.user)
        
