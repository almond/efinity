from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from marvin.addons.replicate.models import PromoInvite
from marvin.core.cms import mail

class Section(models.Model):
	def __unicode__(self):
		return self.name

	slug = models.CharField( max_length = 50 )
	name = models.CharField( max_length = 255 )
	max_length = models.IntegerField( blank = False, null = False, default = 0,
			help_text = 'Maximum number of characters this section can be. If set to 0 will be unlimited.' )
	default = models.TextField( blank = True, help_text = 'Default text for this content section, recommended you always have some default content.' )


class ContentSection(models.Model):
	editable = False

	def __unicode__(self):
		if self.content:
			return self.content
		else:
			return self.section.default

	def render(self):
		if self.content:
			content = self.content
		else:
			content = self.section.default

		edit = ''
		if self.editable:
			edit = '<a href="#" class="edit" rel="%s">Edit %s</a>' % ( self.section.slug, self.section.name )

		return '<div class="editable">%s<div id="%s">%s</div></div>' % ( edit, self.section.slug, content )


	section = models.ForeignKey( Section )
	user = models.ForeignKey( User, related_name = 'content_sections' )

	content = models.TextField()

	created = models.DateTimeField( auto_now_add = True )
	updated = models.DateTimeField( auto_now = True )


def get_content_section(user, slug, can_edit = False):
	section = Section.objects.filter(slug = slug).get()
	try:
		cs = ContentSection.objects.filter(user = user, section = section).get()
	except ContentSection.DoesNotExist:
		cs = ContentSection(
			user = user,
			section = section
		)
		#cs.save()

	cs.editable = can_edit

	return cs

User.add_to_class('content_section', get_content_section)


class PromoEmailLog(models.Model):
    user = models.ForeignKey( User, help_text = _('The user who sent the email.') )
    name = models.CharField(max_length=100)
    email = models.EmailField()

    invite = models.ForeignKey(PromoInvite, related_name='emaillog')
    
    attempt = models.DateTimeField()
    created = models.DateTimeField( auto_now_add = True )
    updated = models.DateTimeField( auto_now = True )

    def send(self):
        invite = PromoInvite(
            user = self.user,
            name = self.name,
            email = self.email,
            replicated_site = None,
	    created = datetime.now()
        )
	
        invite.save()
        mail.send_promo_email(
            user = self.user,
            email = self.email,
            name = self.name,
            subject = self.user.get_addon_value('promo_email_subject'),
            body = self.user.get_addon_value('promo_email_body'),
            invite = invite,
        )

        self.invite = invite
        self.attempt = datetime.now()
        self.save()
