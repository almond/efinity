from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseNotAllowed
from django.shortcuts import redirect
from django.utils import simplejson
from django.utils.translation import ugettext_lazy as _
from marvin.utils.app import *
from models import Section, ContentSection, PromoEmailLog
import datetime


@login_required
def update(request):
    if request.method == 'POST':
        slug = request.POST.get('content_section')
        content = request.POST.get('content_text')

        section = Section.objects.filter(slug=slug).get()

        if section.max_length > 0 and len(content) > section.max_length:
            #messages.error(request, 'Content is too long for section')
            return HttpResponse(simplejson.dumps({
                'status': 'error',
                'message': 'Content is too long for section',
            }))

            badwords = ['.com', '.net', '.org', '@', 'http', 'www', 'dot-com', 'dot com']
            for x in badwords:
                    if x in content:
                            return HttpResponse(simplejson.dumps({
                                    'status': 'error',
                                    'message': 'Please refrain from referencing email addresses or websites in text',
                            }))

        try:
            cs = ContentSection.objects.filter(user=request.user, section=section).get()
            cs.content = content
        except ContentSection.DoesNotExist:
            cs = ContentSection(
                section=section,
                content=content,
                user=request.user
            )

        cs.save()

        #messages.success(request, 'Successfully updated your replicated pages.')
        return HttpResponse(simplejson.dumps({
            'status': 'ok',
            'message': 'Successfully updated content section.',
        }))

    return HttpResponseNotAllowed(['POST', ])

@login_required
def splash(request):
    return render(request, 'cms/splash.html')

@login_required
def promo(request):
    return render(request, 'cms/promo.html')

@login_required
def email(request):

    params = {}
    if request.method == 'POST':
        if request.POST.get('save_email'): # Pressed the 'Save Promo Email text' button
            email_text = request.POST.get('email_text')
            subject = request.POST.get('subject')
            request.user.set_addon_value('promo_email_body', email_text)
            request.user.set_addon_value('promo_email_subject', subject)
            messages.success(request, _('Successfully saved promo email.'))
            return redirect(reverse('cms_email'))

        elif request.POST.get('send_messages'): # Pressed the 'Send Out Emails' button
            mails = request.POST.getlist('mails')
            names = request.POST.getlist('names')
            
            # Set these to the values in the text boxes if they are not set
            if request.user.get_addon_value('promo_email_subject') == None:
                email_text = request.POST.get('email_text')
                request.user.set_addon_value('promo_email_body', email_text)
            if request.user.get_addon_value('promo_email_body'):
                subject = request.POST.get('subject')
                request.user.set_addon_value('promo_email_subject', subject)
            
        if len(mails) < 6:
            for num in range(0, len(mails)):
                email = mails[num]
                name = names[num]

                if email and name:
                    # print "Derp"
                    mailllog = PromoEmailLog(
                        user=request.user,
                        email=email,
                        name=name,
                        created=datetime.datetime.now()
                        )

                                    #mailllog.save()
                    mailllog.send()

                    messages.success(request, _('Sent an email'))
        else:
            messages.error(request, _('You can sent only 5 promo emails at one go.'))

    elif request.method == 'GET':
        if request.GET.get('reset'):
            request.user.set_addon_value('promo_email_body', None)
            request.user.set_addon_value('promo_email_subject', None)
            messages.success(request, _('Reset promo email text.'))
            return redirect(reverse('cms_email'))

    params.update({
        'email_text': request.user.get_addon_value('promo_email_body'),
        'email_subject': request.user.get_addon_value('promo_email_subject'),
        'maillog': PromoEmailLog.objects.filter(user=request.user),
        })

    return render(request, 'cms/promo_email.html', params)
