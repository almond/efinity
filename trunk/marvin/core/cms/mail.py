"""Sends mail related to accounts."""

from django.conf import settings
from django.utils.translation import ugettext
from django.template import Context, Template
from django.core.mail import EmailMessage
from marvin.mail import send_store_mail

# TODO add html email template
def send_promo_email(user, email, name, body, subject, invite):
    """Send a store new account welcome mail to `email`."""

    body = body.replace('{', '{{').replace('}', '}}')
    t = Template(body)

    c = Context({
        'email': email,
        'name': name,
        'promo_url': invite.url,
    })

    message = t.render(c)

    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [email,],
            headers = {'Reply-To': user.email})
    msg.content_subtype = "html"
    msg.send()
