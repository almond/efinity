from django.contrib import admin
from models import *


class SectionAdmin(admin.ModelAdmin):
	list_display = ('__unicode__', 'max_length', )

admin.site.register( Section, SectionAdmin )
admin.site.register( PromoEmailLog )
