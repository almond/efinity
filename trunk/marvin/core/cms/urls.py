from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.core.cms.views',

    (r'^update/$', 'update', {}, 'cms_update'),
	(r'^splash/$', 'splash', {}, 'cms_splash'),
	(r'^promo/$', 'promo', {}, 'cms_promo'),
	(r'^email/$', 'email', {}, 'cms_email'),

)

