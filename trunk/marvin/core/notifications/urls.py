from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.core.notifications.views',
    url(r"^settings/$", 'notice_settings', name="notification_notice_settings"),
)
