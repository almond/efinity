from django.conf.urls.defaults import *


urlpatterns = patterns('marvin.core.dashboard.views',
    (r'^$', 'index', {}, 'dash_home'),
    (r'^v2/$', 'index_v2', {}, 'dash_home_v2'),
    (r'^videos/$', 'videos', {}, 'dash_videos'),
    (r'^etv/$', 'etv', {}, 'dash_etv'),
    (r'^files/$', 'files', {}, 'dash_files'),
    (r'^cep/$', 'cep', {}, 'dash_cep'),
    (r'^cep/dl', 'cep_dl', {}, 'dash_cep_dl'),
    (r'^etv-detail/(?P<etv_id>[-\w]+)$', 'etv_detail', {}, 'etv_detail'),
	# url(r'^email/$', 'email', name='email_downline'),
	# url(r'^email/([^/]+)/$', 'email', name='email_direct'),
)
