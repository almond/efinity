from django.db import models


HOSTS = (
	('youtube', 'YouTube'),
	('vimeo', 'Vimeo'),
	('other', 'Other, will provide Embed code'),
)


class ExternalVideo(models.Model):
	host = models.CharField(max_length=30, choices=HOSTS)
	video_id = models.CharField(max_length=255)
	title = models.CharField(max_length=255)
	description = models.TextField(blank=True)
	embed_code = models.TextField(blank=True)

	def __unicode__(self):
		return self.title

	def embed(self):
		if self.host == 'youtube':
			return '<iframe title="YouTube video player" width="480" height="390" src="http://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>' % self.video_id
		elif self.host == 'vimeo':
			return ''
		else:
			return self.embed_code


class Document(models.Model):
	title = models.CharField(max_length=255)
	description = models.TextField(blank=True)
	doc = models.FileField(upload_to='documents/')

	def __unicode__(self):
		return self.title
