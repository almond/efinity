from django.core.management.base import BaseCommand, CommandError
from marvin.store.products.models import Product


class Command(BaseCommand):
    def handle(self, *args, **options):
        ceps = Product.objects.filter(slug__contains='cep').order_by("date_added")
        for c in ceps:
            print c.name
            for i in c.downloads.all():
                print "\t" + i.file.name
