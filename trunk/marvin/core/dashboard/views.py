from django.contrib.auth.decorators import login_required
from django.core.servers.basehttp import FileWrapper
from django.db.models import Q
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from marvin.addons.mediaupload.models import Training, ETV
from marvin.addons.planning.models import Convention
from marvin.core.dashboard.models import *  # @UnusedWildImport
from marvin.store.products.models import Product
from django.contrib.auth.models import User


from marvin.addons.genealogy.models import Position, Tree
from marvin.addons.genealogy.utils import get_upline_ibo


from urlparse import urlparse
import datetime
import urllib2


@login_required
def index(request, template='dash/index.html'):
    events = Convention.objects.filter(active=True)
    subscriptions = request.user.subscriptions()

    show_downloads = False
    digi_subs = False
    book_subs = False
    trad_subs = False
    for key, values in subscriptions.items():
        print values
        if "traditional" in values['product_slug']:
            trad_subs = True
        elif "digital" in values['product_slug']:
            digi_subs = True
        elif "book" in values['product_slug']:
            book_subs = True

    if not digi_subs and not book_subs and not trad_subs:
        show_downloads = False
    elif not digi_subs and book_subs and not trad_subs:
        show_downloads = False
    elif digi_subs or trad_subs:
        show_downloads = True

    # MRVG: MASTER LIVESWITCH CODE.
    if request.user.profile.download_expiration_date:  # Check that user has permission. If the date is set it means he has
        if request.user.profile.download_expiration_date > datetime.date.today():  # Check that this permission hasn't expired
            show_downloads = True
        else:
            request.user.profile.download_expiration_date = None
            request.user.profile.save()

    # Check upline upon login and make relevant changes if needed
    # TODO this code is repeated in the run_tree management command. Make a module out of it
    # Get complete modified user object
    #try:
        #user_position = request.user.position.get(user=request.user)
    #except Position.DoesNotExist:
        #user_position = Position(user=request.user, auto_update=True, auto_update_team=True)

    #if user_position.auto_update:
        #upline_ibo = get_upline_ibo(int(request.user.username))

        #if upline_ibo:
            ## Change to the new upline
            #user = User.objects.get(username=request.user.username)

            #user.upline_ibo = upline_ibo

            ## Now check team allegiance
            #if not request.user.is_platinum_or_above():
                #try:
                    #if user_position.auto_update_team:
                        #user.profile.team = User.objects.get(username=upline_ibo).profile.team
                        #user.profile.save()
                #except User.DoesNotExist:
                    #pass

            #position = user_position

            #position.tree = Tree.objects.get(id=1)
            ## position.upline = request.user.get_by_aid(user.upline_ibo)
            #try:
                #position.upline = User.objects.get(username=user.upline_ibo)
                #position.save()
                #user.save()
            #except (User.DoesNotExist, ValueError):
                #pass

    return render_to_response(template, {
        'events': events, 'show_downloads': show_downloads
    }, context_instance=RequestContext(request))


@login_required
def products_nav(request):
    return render_to_response('dash/prodnav.html', {'title':'Products'}, context_instance=RequestContext(request))

@login_required
def index_v2(request):
    return render_to_response('dash/index_v2.html', {'title':'Dashboard'}, context_instance=RequestContext(request))


@login_required
def cep(request, template='dash/cep.html'):
    ordered_items = None
    if request.user.orders.all() or request.user.profile.download_expiration_date > datetime.date.today():
        ordered_items = True
    sel_language = None

    if 'language' in request.GET:
        if request.GET['language'] == 'Spanish':
            sel_language = "Spanish"
            if request.user.profile.team == "English":
                query = Q(team="iComercio") | Q(team__isnull=True, slug__contains="spanish") | Q(slug__contains="espanol")
            else:
                query = Q(team=request.user.profile.team) | Q(team__isnull=True, slug__contains='spanish') | Q(slug__contains="espanol")
            all_items = Product.objects.filter(slug__contains='cep').filter(query)
        else:  # request.get must be english
            sel_language = "English"
            query = Q(team="English") | Q(team__isnull=True)
            all_items = Product.objects.filter(slug__contains='cep').filter(query).exclude(Q(slug__contains="spanish") | Q(slug__contains="espanol"))
    else:
        # Default to the users selected language
        # TODO: This solution sucks... but I'm pretty much bored to death with this codebase
        if request.user.profile.language == "es":
            sel_language = "Spanish"
            if request.user.profile.team == "English":
                query = Q(team="iComercio") | Q(team__isnull=True, slug__contains="spanish") | Q(slug__contains="espanol")
            else:
                query = Q(team=request.user.profile.team) | Q(team__isnull=True, slug__contains="spanish") | Q(slug__contains="espanol")
            all_items = Product.objects.filter(slug__contains='cep').filter(query)
        else:
            sel_language = "English"
            query = Q(team="English") | Q(team__isnull=True)
            all_items = Product.objects.filter(slug__contains='cep').filter(query).exclude(Q(slug__contains="espanol") | Q(slug__contains="espanol"))

    item_cache = {}
    for ai in all_items:
        if sel_language == 'Spanish':
            months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        else:
            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        details = get_year_month(ai.name)

        try:
            if details[0] in months:
                if int(details[1]) not in item_cache:
                    item_cache[int(details[1])] = {}
        except IndexError:
            continue

        try:
            item_cache[int(details[1])][int(months.index(details[0]))] = ai
        except (ValueError, KeyError):
            continue

    # If a year GET parameter is sent only display the CEP related to that year
    if request.GET.get('year', None):
        if len(item_cache):
            item_cache = {int(request.GET.get('year', 2013)): item_cache[int(request.GET.get('year', 2013))]}

    all_items = []
    if len(item_cache):
        for year in item_cache:
            for item in item_cache[year]:
                all_items.append(item_cache[year][item])

    all_items.reverse()
    # Paginate the crap out of it
    paginator = Paginator(all_items, 3)
    page = request.GET.get('page', 1)

    try:
        all_items = paginator.page(page)
    except (EmptyPage, InvalidPage):
        all_items = paginator.page(paginator.num_pages)

    return render_to_response(template, {
        'ordered_items': ordered_items,
        'all_items': all_items,
        'year': request.GET.get('year', '2012'),
        'lang': sel_language,
    }, context_instance=RequestContext(request))


def get_year_month(name):
    name=name.split('-')
    name=name[0].split(' ')
    return name

@login_required
def cep_dl(request):
    f = urllib2.unquote(request.GET['f'])
    o = urllib2.urlopen(f)
    u = urlparse(f)
    path = u[2]
    if path == "" or "/" not in path:
        response = HttpResponseNotFound("No File Found.")
    else:
        response = HttpResponse(FileWrapper(o))
        response['Content-Type'] = 'audio/mpeg3'
        response['Content-Disposition'] = 'attachment; filename="%s"' % path.split("/")[-1]

    return response


@login_required
def files(request, template='dash/files.html'):
    return render_to_response(template, {
        'documents': Document.objects.all(),
    }, context_instance=RequestContext(request))


@login_required
def videos(request, template='dash/videos.html'):
    medias = Training.objects.filter(is_published=True)
    return render_to_response(template, {
        'medias': medias,
    }, context_instance=RequestContext(request))


@login_required
def etv(request, template='dash/etv.html'):
    medias = ETV.objects.filter(is_published=True)
    featured_medias = medias.filter(is_featured=True)
    return render_to_response(template, {
        'featured_medias':featured_medias,
        'medias': medias,
    }, context_instance=RequestContext(request))

@login_required
def etv_detail(request, etv_id=None, template='dash/etv_detail.html'):
    etv = ETV.objects.get(id=etv_id)
    return render_to_response(template, {
        'etv': etv,
    }, context_instance=RequestContext(request))
