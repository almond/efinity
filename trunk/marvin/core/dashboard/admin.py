from django.contrib import admin

from marvin.core.dashboard.models import Document, ExternalVideo


admin.site.register(ExternalVideo)
admin.site.register(Document)
