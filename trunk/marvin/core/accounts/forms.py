from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.localflavor.us.forms import USPhoneNumberField
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.template import loader
from django.template.context import Context
from django.utils.http import int_to_base36
from django.utils.translation import ugettext_lazy as _  #@UnusedImport
from marvin.addons.genealogy.models import Position, Tree, AccessLevel
from marvin.addons.genealogy.utils import get_upline_ibo
from marvin.core.accounts.models import Profile
from marvin.core.contacts.forms import ContactInfoForm
from marvin.core.contacts.models import AddressBook, PhoneNumber,\
    PHONE_CHOICES, PRIVACY_SETTINGS
from marvin.store.products.modules.subscription.models import\
    SubscriptionProduct
from marvin.store.shop.models import Config, NullConfig

import logging

log = logging.getLogger('marvin.core.accounts.forms')


class EmailAuthenticationForm(AuthenticationForm):
    """Authentication form with a longer username field."""

    def __init__(self, *args, **kwargs):
        super(EmailAuthenticationForm, self).__init__(*args, **kwargs)
        username = self.fields['username']
        username.max_length = 75
        username.widget.attrs['maxlength'] = 75


class RegisterStep1Form(forms.Form):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    email = forms.EmailField()

    if settings.USE_AID:
        aid = forms.IntegerField(
            label=_(settings.AID_TITLE),
        )

    def clean_aid(self):
        number = self.cleaned_data['aid']

        # Check unique
        # MRVG: Couldn't this be made in the model level? As in class Profile: ibo_number = models.CharFieldOrWhatever(unique=True)?
        try:
            Profile.objects.get(ibo_number=number)
            raise forms.ValidationError(_(
                '''%s is already in use within our system. If you have forgotten your password you can have it reset here, or if you forgot your account's login entirely please contact customer support here.''' % (
                settings.AID_TITLE, )))
        except Profile.DoesNotExist:
            pass

        try:
            User.objects.get(username=number)
            raise forms.ValidationError(_(
                '%s is already in use within our system. If you have forgotten your password you can have it reset here, or if you forgot your account\'s login entirely please contact customer support here.' % (
                settings.AID_TITLE, )))
        except User.DoesNotExist:
            pass

        if number == 31415:
            return number

        # Check third-party
        upline_ibo = get_upline_ibo(int(number))

        #if upline_ibo is False: # this allows people without sponsors, shouldn't ever happen(?) # TODO
        if not upline_ibo:
            raise forms.ValidationError(_(
                'It appears there is a problem with your IBO Number, please contact customer support to complete your registration.'))

        # Store the upline IBO.  We use it to get the user's sponsor
        self.upline_ibo = upline_ibo

        return number

    def save(self, send_email=True):
        user = User(
            username=self.cleaned_data['aid'],
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
            email=self.cleaned_data['email'],
            is_active=False,
        )

        upline = User.objects.get(username=self.upline_ibo)

        user.is_active = False
        user.save()

        # Not sure if this is using django-notification... nevertheless I'd rather not send an email on backend registration
        if send_email:
            subject = _('You\'re almost done!')
            email_text = 'Thank you for registering with eFinity... you\'re almost done! To complete your registration and login to your account, please click here: '
            email_text = '%s%s%s' % (
            email_text, Site.objects.get_current().domain, reverse('register.step2', args=[user.pk]))
            user.email_user(
                subject,
                email_text,
            )

        # Add this user to their upline's genealogy
        # TODO make this nicer... but if username is
        # testing_do_not_store just return the user
        if user.username == 31415:
            Profile(
                user=user,
                ibo_number=self.cleaned_data['aid'],
                pinlevel=AccessLevel.objects.get(group__name='Regular')
            ).save()

            return user

        Position(
            tree=Tree.objects.get(id=1),
            user=user,
            upline=User.get_by_aid(self.upline_ibo)  # @UndefinedVariable
        ).save()

        Profile(
            user=user,
            ibo_number=self.cleaned_data['aid'],
            pinlevel=AccessLevel.objects.get(group__name='Regular'),
            team=upline.profile.team
        ).save()

        return user

shop = Config.objects.get_current()

class RegisterStep2Form(forms.Form):
    phone = USPhoneNumberField()
    phone_type = forms.ChoiceField(choices=PHONE_CHOICES)
    phone_privacy = forms.ChoiceField(choices=PRIVACY_SETTINGS)
    street1 = forms.CharField(max_length=30, label=_('Street'), required=False)
    street2 = forms.CharField(max_length=30, label=_('Street 2'), required=False)
    city = forms.CharField(max_length=30, label=_('City'), required=False)
    state = forms.CharField(max_length=30, label=_('State'), required=False)
    postal_code = forms.CharField(max_length=10, label=_('ZIP code/Postcode'), required=False)
    if shop.sales_country:
        pk = shop.sales_country.pk
    else:
        pk = None
        #~ country = forms.ModelChoiceField(shop.countries(), required=False, label=_('Country'), empty_label=None, initial=shop.sales_country.pk)
    #~ country = forms.ModelChoiceField(shop.countries(), required=False, label=_('Country'), empty_label=None, initial=shop.sales_country.pk if shop.sales_country else None) #SMA python2.6
    if type(shop) == NullConfig:
        country = forms.ChoiceField(required=False, label=_('Country'), choices=[])
    else:
        country = forms.ModelChoiceField(shop.countries(), required=False, label=_('Country'), empty_label=None,
            initial=pk)

    def save(self, user):
        AddressBook(
            user=user,
            addressee=user.get_full_name(),
            street1=self.cleaned_data['street1'],
            street2=self.cleaned_data['street2'],
            state=self.cleaned_data['state'],
            city=self.cleaned_data['city'],
            postal_code=self.cleaned_data['postal_code'],
            country=self.cleaned_data['country'],
            is_default_billing=True,
        ).save()

        PhoneNumber(
            user=user,
            phone=self.cleaned_data['phone'],
            type=self.cleaned_data['phone_type'],
            privacy_setting=self.cleaned_data['phone_privacy']
        ).save()

        return user


class RegisterStep3Form(forms.Form):
    password1 = forms.CharField(max_length=30, widget=forms.PasswordInput, label=_('Password'), required=True)
    password2 = forms.CharField(max_length=30, widget=forms.PasswordInput, label=_('Confirm Password'), required=True)
    # subscription = forms.ModelChoiceField(SubscriptionProduct.objects.all(), required=False, label=_('Subscription'))
    subscription = forms.ModelChoiceField(SubscriptionProduct.objects.filter(pk__in=(103, 39, 40,)), required=False, label=_('Subscription'))


    def clean_password1(self):
        password = self.cleaned_data.get('password1')

        if len(password) < 8:
            raise forms.ValidationError(_('Your password must be at least 8 characters long.'))

        return password


    def clean(self):
        pass1 = self.cleaned_data.get('password1')
        pass2 = self.cleaned_data.get('password2')

        if pass1 != pass2:
            raise forms.ValidationError(_('Your passwords do not match.'))

        return self.cleaned_data


    def save(self, user):
        user.set_password(self.cleaned_data.get('password1'))
        user.save()

        return user


class RegistrationForm(forms.Form):
    class Meta:
        model = Profile
        exclude = ('user', )
        widgets = {
            'address1': forms.TextInput(),
            'address2': forms.TextInput(),
        }

    first_name = forms.CharField(label='First Name')
    last_name = forms.CharField(label='Last Name')
    username = forms.RegexField(
        label='Username',
        max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text="Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.",
        error_messages={'invalid': "This value may contain only letters, numbers and @/./+/-/_ characters."})

    password1 = forms.CharField(label="Password", widget=forms.PasswordInput)
    password2 = forms.CharField(label="Password confirmation", widget=forms.PasswordInput,
        help_text="Enter the same password as above, for verification.")

    email = forms.EmailField(label="Email Address", max_length=75)

    terms = forms.BooleanField(
        error_messages={'required': 'You are required to agree to the terms, if you do not comply, go away.'}
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError("The two password fields didn't match.")
        return password2

    def clean_email(self):
        email = self.cleaned_data["email"]
        users_found = User.objects.filter(email__iexact=email)
        if len(users_found) >= 1:
            raise forms.ValidationError("A user with that email already exist.")
        return email

    def clean_username(self):
        username = self.cleaned_data["username"]
        users_found = User.objects.filter(username__iexact=username)
        if len(users_found) >= 1:
            raise forms.ValidationError("A user with that username already exist.")
        return username

    def save(self, *args, **kwargs):
        token_generator = default_token_generator
        domain_override = None
        email_template_name = 'registration/signup_email.html'
        use_https = False

        user = User(
            username=self.cleaned_data['username'],
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
            email=self.cleaned_data['email'],
        )

        user.set_password(self.cleaned_data['password1'])
        user.is_active = False
        user.save()

        profile = super(UserCreationForm, self).save(commit=False, *args, **kwargs)
        profile.user = user
        user.profile = profile
        user.save()

        if not domain_override:
            current_site = Site.objects.get_current()
            site_name = current_site.name
            domain = current_site.domain
        else:
            site_name = domain = domain_override

        t = loader.get_template(email_template_name)
        c = {
            'email': user.email,
            'domain': domain,
            'site_name': site_name,
            'uid': int_to_base36(user.id),
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': use_https and 'https' or 'http',
        }

        user.email_user('Confirmation link sent on %s' % site_name, t.render(Context(c)))

        return (user, profile)


class RegistrationAddressForm(RegistrationForm, ContactInfoForm):
    """Registration form which also requires address information."""

    def __init__(self, *args, **kwargs):
        super(RegistrationAddressForm, self).__init__(*args, **kwargs)

    def save(self, request=None, **kwargs):
        contact = self.save_contact(request)
        kwargs['contact'] = contact

        log.debug('Saving address for %s', contact)
        self.save_info(**kwargs)

        return contact


class CompleteRegistrationForm(RegisterStep1Form, RegistrationAddressForm):
    pass
