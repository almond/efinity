# Copyright (c) 2011 Almond Development, LLC. All rights reserved.
#
# COPYING PROHIBITED This software product, including but not limited to all
# software files, compiled code, source code, documents, materials, files,
# assets and media are copyrighted. All rights are reserved by Almond
# Development, LLC. Promito Platform is protected by the copyright laws that
# pertain to computer software. You may not copy the software except that you
# may make one copy of the software solely for backup or archival purposes.
# You may not loan, sell, rent, lease, give, sub license, or otherwise
# transfer the software (or any copy). You may not modify, adapt, translate,
# create derivative works, decompile, disassemble, or otherwise reverse
# engineer or derive source code from, all or any portion of the software or
# anything incorporated therein or permit or encourage any third party to do so.
#
# This software was created by Almond Development.
#  http://www.almondev.com/
#
# Licensed under License and Development Agreement, (the "License");
# you may not use this file except in compliance with the License.
# A copy of the License is provided with a copy of this file.
# You may obtain a copy of the License at
#
#     http://www.almondev.com/licenses/dev
#
#
# DISCLAIMER Unless required by applicable law or agreed to in writing,
# software and developer content distributed under the License is provided
# strictly on an "AS IS" BASIS AND DEVELOPER MAKES NO OTHER WARRANTIES,
# either express or implied.
#

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from marvin.addons.genealogy.models import Position
from marvin.addons.support.models import CustomerNote
from marvin.core.accounts.models import *
from marvin.core.contacts.models import *


class UserCIM_Inline(admin.TabularInline):
    model = UserCIM

class UserCIMConvention_Inline(admin.TabularInline):
    model = UserCIMConvention

class PhoneNumber_Inline(admin.TabularInline):
    model = PhoneNumber
    extra = 1

class AddressBook_Inline(admin.StackedInline):
    model = AddressBook
    extra = 1

class CustomKeyAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('__unicode__', 'content_type', 'required')

class ProfileInline(admin.StackedInline):
    model = Profile
    fk_name = 'user' # see http://stackoverflow.com/questions/2408989/more-than-1-foreign-key
    can_delete = False

class CustomFieldInline(admin.TabularInline):
    model = CustomField
    extra = 1

class CustomerNotes_Inline(admin.StackedInline):
    model = CustomerNote
    fk_name = 'user'
    extra = 1

class Contact_Inline(admin.StackedInline):
    model = Contact
    extra = 1

class Position_Inline(admin.StackedInline):
    model = Position
    fk_name = 'user'
    extra=1

setattr(UserAdmin, 'inlines', [
    ProfileInline,
    PhoneNumber_Inline,
    AddressBook_Inline,
    Contact_Inline,
    CustomerNotes_Inline,
    CustomFieldInline,
    Position_Inline,
    UserCIM_Inline,
    UserCIMConvention_Inline,
    ])

admin.site.unregister( User )
admin.site.register( User, UserAdmin )

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name')
    #inlines = [ UserInline, ]

    def get_urls(self):
        from django.conf.urls.defaults import patterns
        return patterns('',
            (r'^(\d+)/user/$', self.admin_site.admin_view(self.view_user))
        ) + super(ProfileAdmin, self).get_urls()


    def view_user(self, request, id):
        profile = get_object_or_404(self.model, pk=id)
        return HttpResponseRedirect('/admin/auth/user/%i' % profile.user.pk)


class CustomFieldAdmin(admin.ModelAdmin):
    list_display = ('user', 'key', 'value', )


admin.site.register( CustomKey, CustomKeyAdmin )
admin.site.register( Activity )
admin.site.register( UserCIM )
admin.site.register( UserCIMConvention )
#admin.site.register( CustomField, CustomFieldAdmin )
#admin.site.register( Profile, ProfileAdmin )
