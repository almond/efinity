from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from marvin.addons.genealogy.models import get_upline_ibo


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        """ Get downline and upline of lost user. """
        user = User.objects.get(username=args[0])
        upline_ibo = get_upline_ibo(int(user.username))
        if not upline_ibo:
            # If no upline can be found, deactivate user
            user.is_active = False
            user.save()
            return 
        else:
            # Store new upline in user table
            upline = User.objects.get(username=upline_ibo)
        user.upline_ibo = upline.username
