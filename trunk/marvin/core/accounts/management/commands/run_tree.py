from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from marvin.core.accounts.models import Profile
from marvin.addons.genealogy.models import Position, Tree, AccessLevel

from marvin.addons.genealogy.utils import get_upline_ibo

class Command(BaseCommand):

	def handle(self, *args, **kwargs):
		""" This should be done automatically on every login... nevertheless
			it doesn't. Although I used to blame programmer incompetence I think now 
			that these issues creep up because of bad management """
		# Get all users
		user = User.objects.get(username=args[0])
		upline_ibo = get_upline_ibo(int(user.username))
		if not upline_ibo:
			user.is_active = False
			user.save()
		else:
			user.upline_ibo = upline_ibo
			user.save()

		# Now change her downline and stuff
		position = Position.objects.get(user=user)
		position.tree = Tree.objects.get(id=1)
		position.upline = user.get_by_aid(user.upline_ibo)
		position.save()

		profile = Profile.objects.get(user=user)
		profile.pinlevel = AccessLevel.objects.get(group__name='Platinum')

		profile.save()

		# Now get uplines downline
		upline = user.upline

		for u in upline.downline.all():
			new_upline = get_upline_ibo(int(u.user.username))
			if not new_upline:
				u.user.is_active = False
				u.user.save()
				print "%s has no upline" % u.user.username
			else:
				u.user.upline_ibo = new_upline
				u.user.save()
				print "%s changed upline" % u.user.username

				position = Position.objects.get(user=u.user)
				position.tree = Tree.objects.get(id=1)
				position.upline = u.user.get_by_aid(u.user.upline_ibo)
				position.save()


		# Check their respective IBO's
		# for u in users:
		# 	# Print the current IBO being processed
		# 	print "%s is being checked against the Amway API" % u.username

		# 	try:
		# 		upline_ibo = get_upline_ibo(int(u.username))
		# 	except ValueError:
		# 		continue

		# 	if not upline_ibo:
		# 		u.is_active = False
		# 		u.save()
		# 		print "%s is not active anymore" % u.username
		# 	else:
		# 		u.upline_ibo = upline_ibo
		# 		u.save()
		# 		print "%s has been updated" % u.username
