from django.conf import settings # import the settings file


def settings_variables(context):
    return {
        'AID_TITLE': settings.AID_TITLE,
        'AID_TITLE_SHORT': 'IBO',
    }

