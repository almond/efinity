"""
URLConf for Django user registration.

Recommended usage is to use a call to ``include()`` in your project's
root URLConf to include this URLConf for any URL beginning with
'/accounts/'.

"""
from django.conf.urls.defaults import patterns
from marvin.utils.dbsettings import config_value
from marvin.core.contacts.urls import urlpatterns


# Activation keys get matched by \w+ instead of the more specific
# [a-fA-F0-9]+ because a bad activation key should still get to the view;
# that way it can return a sensible "invalid key" message instead of a
# confusing 404.
urlpatterns += patterns('marvin.core.accounts.views',
    (r'^activate/(?P<activation_key>\w+)/$', 'activate', {}, 'registration_activate'),
    (r'^login/$', 'emaillogin', {'template_name': 'accounts/login.html'}, 'auth_login'),
    (r'^secure/login/$', 'emaillogin', {'SSL' : True, 'template_name': 'accounts/login.html'}, 'auth_secure_login'),

    (r'^register/$', 'register.begin', {}, 'registration_register'),
    (r'^register/step1/$', 'register.step1', {}, 'register.step1'),
    (r'^register/step2/(\d+)/$', 'register.step2', {}, 'register.step2'),
    (r'^register/step3/(\d+)/$', 'register.step3', {}, 'register.step3'),
    (r'^register/finish/(\d+)/$', 'register.finish', {}, 'register.finish'),

    (r'^profile/$', 'profile.view_profile', {}, 'my_profile'),
    (r'^profile/(?P<user>\w+)/$', 'profile.view_profile', {}, 'user_profile'),
)

urlpatterns += patterns('',
    ('^logout/$','django.contrib.auth.views.logout', {'template_name': 'accounts/logout.html'}, 'auth_logout'),
    )

verify = (config_value('SHOP', 'ACCOUNT_VERIFICATION') == 'EMAIL')

urlpatterns += patterns('django.views.generic',
    (r'^register/complete/$', 'simple.direct_to_template',
        {'template': 'accounts/registration_complete.html',
        'extra_context': {'verify': verify }},
        'registration_complete'),
)

#Dictionary for authentication views
password_reset_dict = {
    'template_name': 'accounts/password_reset_form.html',
    'email_template_name': 'accounts/password_reset.txt',
}

# the "from email" in password reset is problematic... it is hard coded as None
urlpatterns += patterns('django.contrib.auth.views',
    (r'^password_reset/$', 'password_reset', password_reset_dict, 'auth_password_reset'),
    (r'^password_reset/done/$', 'password_reset_done', {'template_name':'accounts/password_reset_done.html'}, 'auth_password_reset_done'),
    (r'^password_change/$', 'password_change', {'template_name':'accounts/password_change_form.html'}, 'auth_password_change'),
    (r'^password_change/done/$', 'password_change_done', {'template_name':'accounts/password_change_done.html'}, 'auth_change_done'),
    (r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'password_reset_confirm'),
    (r'^reset/done/$', 'password_reset_complete'),
)
