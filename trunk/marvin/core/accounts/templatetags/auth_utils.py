from django.template import Library, Node
from django.contrib.auth.models import User

class UserCountOutput(Node):
    def render(self, context):
        count = User.objects.all().count()
        return "%s" % (count)

def do_print_user_count(parser, token):
    return UserCountOutput()

register = Library()     
register.tag('user_count', do_print_user_count)
