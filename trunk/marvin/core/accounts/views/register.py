from django.conf import settings
from django.contrib.auth import authenticate, login
from marvin.core.accounts.forms import RegisterStep1Form, RegisterStep2Form, \
    RegisterStep3Form
from marvin.store.shop.models import Cart
from marvin.utils.view import *  # @UnusedWildImport


def begin(request):
    '''
    Redirect the user to the first step to register.
    '''
    return redirect(reverse('register.step1'))


def step1(request, template_name='accounts/registration_step1.html'):
    '''
    Step 1 of Registration.
    Here we do pre-checks to see if the user can register, if
    they automatically pass them all, redirect them to next step.
    '''

    # If we do not use AIDs then do not check against them
    if settings.USE_AID is False:
        return redirect(reverse('register.step2'))

    if request.method == 'POST':
        form = RegisterStep1Form(request.POST)

        if form.is_valid():
            user = form.save()
            return redirect(reverse('register.step2', args=[user.pk]))

    else:
        form = RegisterStep1Form()

    return render(request, template_name, {'form': form})


def step2(request, user_id, template_name='accounts/registration_step2.html', form=RegisterStep2Form):
    user = get_object_or_404(User, pk=user_id)
    if user.is_active:
        raise Http404()

    if user.addresses.count():
        return redirect(reverse('register.step3', args=[user.pk]))

    if request.method == 'POST':
        form = form(request.POST)

        if form.is_valid():
            form.save(user)
            return redirect(reverse('register.step3', args=[user.pk]))

    else:
        form = form()
    return render(request, template_name, {
        'form': form,
        'member': user,
    })


def step3(request, user_id, template_name='accounts/registration_step3.html', form=RegisterStep3Form):
    user = get_object_or_404(User, pk=user_id)
    if user.is_active:
        raise Http404()

    if request.method == 'POST':
        form = form(request.POST)

        if form.is_valid():
            form.save(user)

            user = authenticate(username=user.username, password=form.cleaned_data['password1'])
            login(request, user)
            
            # if subscription, add to cart and proceed to checkout
            if form.cleaned_data.get('subscription'):
                cart = Cart.objects.from_request(request, create=True)
                product = form.cleaned_data.get('subscription')
                added_item = cart.add_item('products', 'product', product.pk, number_added=1)

                # Trevor copied these from marvin.store.shop.views.cart.add because I wasn't sure if we'd break without them                
                # got to here with no error, now send a signal so that listeners can also operate on this form.
                from marvin.store.shop.signals import satchmo_cart_changed, satchmo_cart_add_complete
                satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item, product=product, request=request, form=form.cleaned_data)
                satchmo_cart_changed.send(cart, cart=cart, request=request)
                return redirect(reverse('shop_cart'))
            
            else:
                return redirect(reverse('register.finish', args=[user.pk]))

    else:
        form = form()
    
    return render(request, template_name, {
        'form': form,
        'member': user,
    })
    

def finish(request, user_id, template_name='accounts/registration_finish.html'):
    user = get_object_or_404(User, pk=user_id)
    if user.is_active:
        raise Http404()

    return render(request, template_name, {'member': user})
