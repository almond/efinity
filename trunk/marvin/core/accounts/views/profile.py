from marvin.utils.view import * #@UnusedWildImport
from marvin.store.shop.models import Order

# For now this is private,
# later we should make this configurable

def err(request):
    return 1

@login_required
def view_profile(request, user = None, template_name = 'accounts/profile.html'):

    ctx = {}

    if not user:
        ctx['member'] = request.user
        ctx['is_self'] = True
    else:
        ctx['member'] = get_object_or_404(User, username = user)
    orders = Order.objects.filter(user=request.user).order_by('-time_stamp').exclude(status="Cancelled")

    ctx['orders'] = orders
    if not template_name:
        if request.user.is_staff:
            template_name = 'support/profile.html'
        else:
            template_name = 'accounts/profile.html'

    return render(request, template_name, ctx)
