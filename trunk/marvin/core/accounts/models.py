from django.conf import settings
from django.contrib.auth.models import Group, User
from django.db import models
from django.db.models.signals import pre_save
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from marvin.addons.compensation.models import CompensationLevel
from marvin.addons.genealogy.models import AccessLevel
from marvin.core.contacts.models import SocialNetwork
from marvin.utils import consts
import datetime



CONTENT_TYPES = (
    ('CharField', 'String or Text'),
    ('Textarea', 'Large Text Area'),
    ('IntegerField', 'Integer (Number)'),
    ('currency', 'Money Value'),
    ('email', 'Email Address'),
    ('phone', 'Phone Number'),
    ('ssn', 'Social Security Number (US)'),
    ('ein', 'Employer ID Numbers (EIN) (US)'),
    ('vat', 'VAT Number'),
    ('driver', 'Driver\'s License Number'),
    ('date', 'Date'),
    ('select', 'Create Select (e.g. Gender)'),
    ('bool', 'Checkbox / Boolean'),
    ('URLField', 'URL or Webste Address'),
)


class CustomKey(models.Model):
    """
    Stores custom fields that the client
    defines within their admin panel which
    we can use to generate forms from.

    """
    name = models.CharField(max_length=100,
        help_text=_('Name or label of field. Something like "Home Phone Number" works best.'))
    slug = models.SlugField(blank=True)
    help_text = models.TextField(verbose_name=_('Help Text'), blank=True, null=True,
        help_text=_('Short description of the field, provided to users to explain to them what this field means. If obvious, leave blank.'))
    content_type = models.CharField(max_length=30, choices=CONTENT_TYPES,
        help_text=_('Select the data storage format and verification that should be performed on this data type. Below you can set other requirements.'))
    options = models.TextField(blank=True,
        help_text=_('Enter the options you would like to provide within the Select, each on its own line.a'))
    required = models.BooleanField(default=False,
        help_text=_('Will include this field on registration if required.'))
    min_length = models.IntegerField(null=True, blank=True, verbose_name=_('Minimum Length'),
        help_text=_('If you select String or Text (or Integer), here you can set a minimum length'))
    max_length = models.IntegerField(null=True, blank=True, verbose_name=_('Maximum Length'),
        help_text=_('If you select String or Text (or Integer), here you can set a maximum length'))


    class Meta:
        verbose_name = 'Custom Field'
        verbose_name_plural = 'Custom Fields'

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id and len(self.slug) == 0:
            self.slug = slugify( self.name )

        return super( CustomKey, self ).save(*args, **kwargs)


class CustomField(models.Model):
    class Meta:
        verbose_name = 'User Custom Field'
        verbose_name_plural ='User Custom Fields'
        unique_together = [('user', 'key')]

    def __unicode__(self):
        return '%s %s' % (str(self.user), str(self.key))

    user = models.ForeignKey(User, related_name='custom')
    key = models.ForeignKey(CustomKey)
    value = models.TextField()


class AddonField(models.Model):
    class Meta:
        unique_together = [('user', 'key')]

    user = models.ForeignKey(User)
    key = models.CharField(max_length=50)
    value = models.TextField()


class Activity(models.Model):
    CHANGE = 1

    user = models.ForeignKey(User, related_name='activities')
    performed_by = models.ForeignKey(User, related_name='performed_activities',
        blank=True, null=True)
    generated_comment = models.TextField()
    comment = models.TextField(blank=True, null=True)
    flag = models.PositiveSmallIntegerField()
    created = models.DateTimeField(_('action time'), auto_now=True)

    class Meta:
        verbose_name = _("User Activity")
        verbose_name_plural = _("User Activities")
        ordering = ['-created', ]

class UserCIM(models.Model):
    '''
    This model stores the link between a user and the CIM customer_profile_id stored in authorize.net
    '''
    user = models.OneToOneField(User, related_name='cim')
    customer_profile_id = models.IntegerField(unique=True)

    def __unicode__(self):
        return 'IBO:%s > AUTH:%s' % (self.user.aid, self.customer_profile_id)

class UserCIMConvention(models.Model):
    '''
    This model stores the link between a user and the CIM customer_profile_id stored in authorize.net
    '''
    user = models.OneToOneField(User, related_name='cim_convention')
    customer_profile_id = models.IntegerField(unique=True)

    def __unicode__(self):
        return 'IBO:%s > AUTH:%s' % (self.user.aid, self.customer_profile_id)

# TODO: This is hardcoded... perhaps you should look for a way to add new stuff and what not
EFINITY_TEAMS = (
    ('iComercio', 'iComercio'),
    ('English', 'English'),
    ('Alianza Global', 'Alianza Global'),
)

class Profile(models.Model):
    """
    User Profile Class

    Stores standard data every user,
    on every client, will need to
    store. Also provides easy access
    to custom fields.

    """
    team = models.CharField(_("Team"), max_length=40, choices=EFINITY_TEAMS,
                            blank=True, null=True, default=None)
    user = models.OneToOneField(User, related_name='profile_object',
        help_text='Also see user <a href="user">login information</a>.')
    language = models.CharField(_('language'), max_length=10,
        choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE)
    ibo_number = models.IntegerField(verbose_name=settings.AID_TITLE,
        blank=True, null=True)
    gender = models.CharField(max_length=1, choices=consts.GENDERS,
        blank=True, null=True)
    received_w9 = models.BooleanField(verbose_name=_('Received W-9'),
        default=False)
    w9_date = models.DateTimeField(verbose_name='W-9 Modified Date',
        editable=True, blank=True, null=True)
    received_efinity_agreement = models.BooleanField(default=False,
        verbose_name=_('Received eFinity Agreement'))
    agreement_date = models.DateTimeField(editable=True, blank=True,
        verbose_name=_('Agreement Modified Date'), null=True)
    personal_photo = models.ImageField(upload_to='profiles/',
        blank=True, null=True)
    pinlevel = models.ForeignKey(AccessLevel, related_name='profiles')
    pinlevel_vanity = models.ForeignKey(AccessLevel, related_name='vanity_profiles', blank=True, null=True)
    can_download = models.BooleanField(blank=False, null=False, default=False, verbose_name=_(u"Allow downloads for a month"))

    # This is for the new users thing that's happening. Just for the mobile guys. Not used elsewhere
    subscription_type = models.CharField(blank=True, null=True, max_length=140)

    download_expiration_date = models.DateField(blank=True, null=True, verbose_name=_(u"Download expiration date"))

    def save(self, *args, **kwargs):
        """
        Mods for the team stuff and what not
        Arguments:
        - `self`: Object instance
        - `*args`: bunch of stuff
        - `**kwargs`: Even more stuff
        """
        super(Profile, self).save(*args, **kwargs)

        # Now I think I'll have to get the object itself from DB...
        if self.pk:
            try:
                profile = Profile.objects.get(pk=self.pk)
                if profile.user.is_platinum_or_above():
                    for p in profile.user.downline.all():
                        if not p.user.is_platinum_or_above():
                            if p.auto_update_team:
                                p.user.profile.team = profile.team
                                p.user.profile.save()
                        else:
                            if self.user.position.all()[0].auto_update_team:
                                self.team = self.user.upline.profile.team
            except Profile.DoesNotExist:
                pass

    class Meta:
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'

    def __unicode__(self):
        return self.get_ambiguous_title()

    def get_ambiguous_title(self):
        """ Utils function to return the user's name and their AID """
        return '%s (%s)' % (self.user.get_full_name(), str(self.aid))

    @property
    def aid(self):
        """
        Alternative Identifier (aid)
        This is a quick ID used for when we migrate an existing
        system into Cross Market. This makes it easy to continue
        to reference a user by thier old IDs while still maintaining
        thier user ids within Cross Maret.
        """
        return self.ibo_number


def get_custom(user, slug, default = None):
    try:
        key = CustomKey.objects.filter( slug = slug ).get()
    except CustomKey.DoesNotExist:
        raise AttributeError

    field = CustomField.objects.filter(user = user, key = key)
    if len(field) == 1:
        field = field.get()
        return field.value
    else:
        return default


def set_custom(user, slug, value):
    try:
        key = CustomKey.objects.filter( slug = slug ).get()
    except CustomKey.DoesNotExist:
        raise AttributeError

    field, created = CustomField.objects.get_or_create(user=user, key=key)
    field.value = value
    field.save()
    return True


@property
def get_profile(user):
    if not hasattr(user, 'profile_cache'):
        try:
            user.profile_cache = Profile.objects.filter( user = user ).get()
        except:
            user.profile_cache = Profile( user = user )

    return user.profile_cache


def get_addon_value(user, key, default = None):
    try:
        return AddonField.objects.get(user=user, key=key).value
    except AddonField.DoesNotExist:
        return default


def set_addon_value(user, key, value):
    info, created = AddonField.objects.get_or_create(user=user, key=key)
    if value is None:
        info.delete()
    else:
        info.value = value
        info.save()
    return True


def set_addons(user, **kwargs):
    """
    update the other service info for the given
    user using the given keyword args.

    e.g. update_other_services(user, twitter_user=..., twitter_password=...)
    """
    for key, value in kwargs.items():
        info, created = AddonField.objects.get_or_create(user=user, key=key)
        info.value = value
        info.save()


def get_social_network(network):
    def get_network(user):
        social = SocialNetwork.objects.filter(service=network, user=user)
        try:
            account = social.get()
            return account
        except:
            return None

    return get_network


@property
def get_aid(user):
    '''Utils function, returns the AID'''
    return user.profile.aid


@classmethod
def get_by_aid(cls, aid):
    try:
        profile = Profile.objects.get(ibo_number=aid)
    except Profile.DoesNotExist:
        return None
    return profile.user


def get_user_json(user):
    return {
        'pk': user.pk,
        'id': user.pk,
        'name': user.get_full_name(),
        'email': user.email,
    }


@property
def get_user_photo(user):
    if user.profile.personal_photo:
        return user.profile.personal_photo
    else:
        return {
            'url': '%s/images/noimage.png' % (settings.STATIC_URL),
        }


def get_pinlevel(user):
    return user.profile.pinlevel


def create_activity(user, generated_comment, comment, by):
    activity = Activity(
        user = user,
        performed_by = by,
        flag = Activity.CHANGE,
        generated_comment = generated_comment,
        comment = comment,
    ).save()


def is_platinum_or_above(user):
    platinum=Group.objects.get(name='Platinum').id
    try:
        user_level=user.pinlevel().id
        if user_level >= platinum:
            return True
        return False
    except:
        return False

User.add_to_class('is_platinum_or_above', is_platinum_or_above)
User.add_to_class('photo', get_user_photo)
User.add_to_class('getJSON', get_user_json)
User.add_to_class('aid', get_aid)
User.add_to_class('get_by_aid', get_by_aid)
User.add_to_class('profile', get_profile)
User.add_to_class('get_custom', get_custom)
User.add_to_class('set_custom', set_custom)
User.add_to_class('get_addon_value', get_addon_value)
User.add_to_class('set_addon_value', set_addon_value)
User.add_to_class('set_addons', set_addons)
User.add_to_class('twitter', get_social_network('Twitter'))
User.add_to_class('facebook', get_social_network('Facebook'))
User.add_to_class('linkedin', get_social_network('LinkedIn'))
User.add_to_class('activity_change', create_activity)
User.add_to_class('pinlevel',get_pinlevel)
