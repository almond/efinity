from django.contrib import admin
from marvin.utils.admin import AutocompleteAdmin
from marvin.core.contacts.models import *

class PhoneNumber_Inline(admin.TabularInline):
    model = PhoneNumber
    extra = 1

class AddressBook_Inline(admin.StackedInline):
    model = AddressBook
    extra = 1


class OrganizationOptions(admin.ModelAdmin):
    list_filter = ['type', 'role']
    list_display = ['name', 'type', 'role']

class ContactOptions(AutocompleteAdmin):
    list_display = ('last_name', 'first_name',)
    list_filter = ['created',]
    ordering = ['last_name']
    search_fields = ('first_name', 'last_name', 'email')
    related_search_fields = {'user': ('username', 'first_name', 'last_name', 'email')}
    related_string_functions = {'user': lambda u: u"%s (%s)" % (u.username, u.get_full_name())}
    inlines = [PhoneNumber_Inline, AddressBook_Inline]

admin.site.register(Contact, ContactOptions)
