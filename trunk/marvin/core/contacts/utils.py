from models import *

@property
def get_primary_contact(user, cache='primary_contact_cache'):
    if hasattr(user, cache):
        return getattr(user, cache)

    try:
        contact = Contact.objects.filter(user=user, is_primary=True).get()
    except Contact.DoesNotExist:
        contact = Contact(
            user = user,
            first_name = user.first_name,
            last_name = user.last_name,
            email = user.email,
            relationship = 'self',
            is_primary = True,
        )
        contact.save()
    
    setattr(user, cache, contact)
    return contact


@property
def get_primary_phone_number(user):
    try:
        return PhoneNumber.objects.filter(user=user)[0]
        return PhoneNumber.objects.filter(user=user, is_primary=True).get()
    except PhoneNumber.DoesNotExist:
        return None
    except IndexError:
        return None


@property
def get_shipping_address(user):
    """Return the default shipping address or None."""
    try:
        return user.addresses.filter(is_default_shipping=True)[0]
    except:# AddressBook.DoesNotExist:
        return None


@property
def get_billing_address(self):
    """Return the default billing address or None."""
    try:
        return self.addresses.filter(is_default_billing=True)[0]
    except:# AddressBook.DoesNotExist:
        return None
