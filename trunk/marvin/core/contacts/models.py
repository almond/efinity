import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.localflavor.us.models import PhoneNumberField

from l10n.models import Country


class Contact(models.Model):
    """
    Contact is a record of details on any individual that a customer
    may add to his/her account overtime. This could be details on his
    wife, or parents, or anyone. This allows him to store these details
    to mail out things directly to there contacts and other seemingly
    useful things.
    """
    user = models.ForeignKey(User, related_name='contacts')
    relationship = models.CharField(_("Relationship"), max_length=50, blank=True, null=True)
    title = models.CharField(_("Title"), max_length=30, blank=True, null=True)
    first_name = models.CharField(_("First name"), max_length=30, )
    last_name = models.CharField(_("Last name"), max_length=30, )
    dob = models.DateField(_("Date of birth"), blank=True, null=True)
    email = models.EmailField(_("Email"), blank=True, max_length=75)
    notes = models.TextField(_("Notes"), max_length=500, blank=True)
    is_primary = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_full_name(self):
        """Return the person's full name."""
        return u'%s %s' % (self.first_name, self.last_name)

    @property
    def full_name(self):
        return self.get_full_name()

    def __unicode__(self):
        return self.get_full_name()

    class Meta:
        verbose_name = _("Contact")
        verbose_name_plural = _("Contacts")


PHONE_CHOICES = (
    ('Work', _('Work')),
    ('Home', _('Home')),
    ('Fax', _('Fax')),
    ('Mobile', _('Mobile')),
)

SOCIAL_SERVICES = (
    ('Facebook', _('Facebook')),
    ('Twitter', _('Twitter')),
    ('LinkedIn', _('LinkedIn')),
    ('MySpace', _('MySpace')),
    ('Skype', _('Skype')),
)

SOCIAL_SERVICES_BASE = {
    'Facebook': 'http://www.facebook.com/%s',
    'Twitter': 'http://www.twitter.com/%s',
    'LinkedIn': 'http://www.linkedin.com/in/%s',
    'MySpace': 'http://myspace.com/%s',
}

PRIVACY_SETTINGS = (
    ('public', _('Visible to your downline and potential leads')),
    ('private', _('Visible only to you')),
    ('downline', _('Visible only to your downline')),
)


class SocialNetwork(models.Model):
    """
    Social network account
    """
    user = models.ForeignKey(User, related_name='social')
    service = models.CharField(max_length=30, choices=SOCIAL_SERVICES)
    account = models.CharField(max_length=255)
    privacy_settings = models.CharField(max_length=30, choices=PRIVACY_SETTINGS)
    
    created = models.DateTimeField(_('Created Date and Time'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated Date and Time'), auto_now=True)
    
    def __unicode__(self):
        return u'%s - %s' % (self.get_service_display, self.account)

    def url(self):
        if 'http' in self.account:
            return self.account
        else:
            base = SOCIAL_SERVICES_BASE.get(self.service, '')
            return base % ( self.account )

    class Meta:
        verbose_name = _('Social Network Account')
        verbose_name_plural = _('Social Network Accounts')


class PhoneNumber(models.Model):
    """
    Phone number associated with a contact.
    """
    user = models.ForeignKey(User, related_name='phones')
    type = models.CharField(_("Description"), choices=PHONE_CHOICES, max_length=20)
    phone = PhoneNumberField(_("Phone Number")) 
    privacy_setting = models.CharField(max_length=30, choices=PRIVACY_SETTINGS)
    is_primary = models.BooleanField(_("Primary"), default=False)

    @property
    def number(self):
        return self.phone

    def __unicode__(self):
        return u'%s Phone - %s' % (self.type, self.phone)

    def save(self, **kwargs):
        """
        If this number is the default, then make sure that it is the only
        primary phone number. If there is no existing default, then make
        this number the default.
        """
        existing_number = False # self.user.primary_phone
        if existing_number:
            if self.primary:
                existing_number.primary = False
                #existing_number.save() # this would rerun the check and break
                super(PhoneNumber, existing_number).save()
        else:
            self.primary = True
        super(PhoneNumber, self).save(**kwargs)

    class Meta:
        ordering = ['-is_primary']
        verbose_name = _("Phone Number")
        verbose_name_plural = _("Phone Numbers")


class AddressBook(models.Model):
    """
    Address information associated with a contact.
    """
    user = models.ForeignKey(User, related_name='addresses')
    description = models.CharField(_("Description"), max_length=20, blank=True, help_text=_('Description of address - Home, Office, Warehouse, etc.',))
    addressee = models.CharField(_("Addressee"), max_length=80)
    street1 = models.CharField(_("Street"), max_length=80)
    street2 = models.CharField(_("Street"), max_length=80, blank=True, null=True)
    state = models.CharField(_("State"), max_length=50, blank=True)
    city = models.CharField(_("City"), max_length=50)
    postal_code = models.CharField(_("Zip Code"), max_length=30)
    country = models.ForeignKey(Country, verbose_name=_("Country"))

    is_default_shipping = models.BooleanField(default=False)
    is_default_billing = models.BooleanField(default=False)

    def __unicode__(self):
       return u'%s - %s address' % (self.user.get_full_name(), self.description)

    class Meta:
        verbose_name = _("Address Book")
        verbose_name_plural = _("Address Books")

import utils
User.add_to_class('contact', utils.get_primary_contact)
User.add_to_class('phone', utils.get_primary_phone_number)
User.add_to_class('shipping_address', utils.get_shipping_address)
User.add_to_class('billing_address', utils.get_billing_address)

import config
