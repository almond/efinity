from django import http
from django.core import urlresolvers
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext
from django.forms.models import inlineformset_factory
from marvin.core.contacts import signals
from marvin.core.contacts.forms import ExtendedContactInfoForm, ContactInfoForm, area_choices_for_country
from marvin.core.contacts.models import Contact, PhoneNumber, SocialNetwork
from marvin.core.accounts.models import Profile
from marvin.store.shop.models import Config
import logging

log = logging.getLogger('marvin.core.contacts.views')

from marvin.utils.view import *

@login_required
def view(request):
    """View contact info."""
    return redirect(reverse('my_profile'))

@login_required
def update(request):
    """Update contact info"""

    init_data = {}
    shop = Config.objects.get_current()
    contact = request.user.contact

    social_formset = inlineformset_factory(User, SocialNetwork)
    phones_formset = inlineformset_factory(User, PhoneNumber, exclude=('is_primary',))
    profile_formset = inlineformset_factory(User, Profile, exclude=(
        'ibo_number','received_w9','w9_date',
        'received_efinity_agreement','agreement_date','compensation_level',
        'pinlevel','pinlevel_vanity',
    ), can_delete=False)

    if request.method == "POST":
        form = ExtendedContactInfoForm(
            user=request.user,
            data=request.POST.copy(),
            shop=shop,
            contact=contact,
            shippable=True
        )

        social_form = social_formset(request.POST, request.FILES, instance=request.user)
        phones_form = phones_formset(request.POST, request.FILES, instance=request.user)
        profile_form = profile_formset(request.POST, request.FILES, instance=request.user)


        updated=True
        if form.is_valid():
            form.save()
        else:
            messages.success(request, t('Couldnot update basic information.'))
            updated=False

        if social_form.is_valid():
            social_form.save()
        else:
            messages.success(request, t('Couldnot update social information.'))
            updated=False

        if phones_form.is_valid():
            phones_form.save()
        else:
            messages.success(request, t('Couldnot update phone numbers.'))            
            updated=False

        if profile_form.is_valid():
            profile_form.save()
        else:
            messages.success(request, t('Couldnot update profile information.'))            
            updated=False

        if updated:
            messages.success(request, t('Profile information updated.'))            
            

        return http.HttpResponseRedirect(urlresolvers.reverse('my_profile'))
    else:
        #If a person has their contact info, make sure we populate it in the form
        for item in contact.__dict__.keys():
            init_data[item] = getattr(contact,item)

        if request.user.shipping_address:
            for item in request.user.shipping_address.__dict__.keys():
                init_data["ship_"+item] = getattr(request.user.shipping_address,item)

        if request.user.billing_address:
            for item in request.user.billing_address.__dict__.keys():
                init_data[item] = getattr(request.user.billing_address,item)

        if request.user.phone:
            init_data['phone'] = request.user.phone.number

        #signals.satchmo_contact_view.send(contact, contact=contact, contact_dict=init_data)
        form = ExtendedContactInfoForm(user=request.user, shop=shop, contact=contact, shippable=True, initial=init_data)
        social_form = social_formset(instance=request.user)
        phones_form = phones_formset(instance=request.user)
        profile_form = profile_formset(instance=request.user)

    init_data['social_form'] = social_form
    init_data['phones_form'] = phones_form
    init_data['profile_form'] = profile_form
    init_data['form'] = form
    if shop.in_country_only:
        init_data['country'] = shop.sales_country
    else:
        countries = shop.countries()
        if countries and countries.count() == 1:
            init_data['country'] = countries[0]

    return render(request, 'contact/update_form.html', init_data)


class AjaxGetStateException(Exception):
    """Used to barf."""
    def __init__(self, message):
        self.message = message

def ajax_get_state(request, **kwargs):
    formdata = request.REQUEST.copy()

    try:
        if formdata.has_key("country"):
            country_field = 'country'
        elif formdata.has_key("ship_country"):
            country_field = 'ship_country'
        else:
            raise AjaxGetStateException("No country specified")

        form = ContactInfoForm(data=formdata)
        country_data = formdata.get(country_field)
        try:
            country_obj = form.fields[country_field].clean(country_data)
        except:
            raise AjaxGetStateException("Invalid country specified")

        areas = area_choices_for_country(country_obj, ugettext)

        context = RequestContext(request, {
            'areas': areas,
        })
        return render_to_response('contact/_state_choices.html',
                                  context_instance=context)
    except AjaxGetStateException, e:
        log.error("ajax_get_state aborting: %s" % e.message)

    return http.HttpResponseServerError()
