from django.conf.urls.defaults import patterns

urlpatterns = patterns('marvin.core.contacts.views',
    (r'^$', 'view', {}, 'profile_info'),
    (r'^update/$', 'update', {}, 'profile_update'),
    (r'^ajax_state/$', 'ajax_get_state', {}, 'satchmo_contact_ajax_state'),
)
