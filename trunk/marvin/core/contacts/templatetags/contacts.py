from django import template
from marvin.core.contacts.models import AddressBook

register = template.Library()

def addressblock(address):
    """Output an address as a HTML formatted text block"""
    return {"address" : address}

register.inclusion_tag('contact/_addressblock.html')(addressblock)

def addressblock_by_id(address_id):
    """Output an address as a HTML formatted text block"""
    try:
        address = AddressBook.objects.get(pk=address_id)
    except AddressBook.DoesNotExist:
        address = None
    return {"address" : address}

register.inclusion_tag('contact/_addressblock.html')(addressblock_by_id)

def contact_for_user(user):
    if not user.is_anonymous() and user.contact_set.count() > 0:
        return user.contact_set.all()[0]
    return None

register.filter('contact_for_user')(contact_for_user)
