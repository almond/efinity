from django.utils.translation import ugettext_lazy as _
from django.db import models
from marvin.base.typeable import Typeable

dimension_units = (('cm','cm'), ('in','in'))
weight_units = (('kg','kg'), ('lb','lb'))

SHIP_CLASS_CHOICES = (
    ('DEFAULT', _('Default')),
    ('YES', _('Shippable')),
    ('NO', _('Not Shippable'))
)

class Shippable(models.Model, Typeable):
    '''
    The Shippable Base allows for computing the shipping costs
    of a potentially-purchasable model.
    '''

    class Meta:
        abstract = True

    weight = models.DecimalField(_("Weight"), max_digits=8, decimal_places=2, null=True, blank=True)
    weight_units = models.CharField(_("Weight units"), max_length=3, choices=weight_units, null=True, blank=True)
    
    length = models.DecimalField(_("Length"), max_digits=6, decimal_places=2, null=True, blank=True)
    length_units = models.CharField(_("Length units"), max_length=3, choices=dimension_units, null=True, blank=True)
    
    width = models.DecimalField(_("Width"), max_digits=6, decimal_places=2, null=True, blank=True)
    width_units = models.CharField(_("Width units"), max_length=3, choices=dimension_units, null=True, blank=True)
    
    height = models.DecimalField(_("Height"), max_digits=6, decimal_places=2, null=True, blank=True)
    height_units = models.CharField(_("Height units"), max_length=3, choices=dimension_units, null=True, blank=True)

    shipclass = models.CharField(_('Shipping'), choices=SHIP_CLASS_CHOICES, default="DEFAULT", max_length=10, help_text=_("If this is 'Default', then we'll use the product type to determine if it is shippable."))

    @property
    def has_full_dimensions(self):
        """Return true if the dimensions all have units and values. Used in shipping calcs. """
        for att in ('length', 'length_units', 'width', 'width_units', 'height', 'height_units'):
            if self.smart_attr(att) is None:
                return False
        return True

    @property
    def has_full_weight(self):
        """Return True if we have weight and weight units"""
        for att in ('weight', 'weight_units'):
            if self.smart_attr(att) is None:
                return False
        return True

    @property
    def is_shippable(self):
        """
        If this Product has any subtypes associated with it that are not
        shippable, then consider the product not shippable.
        If it is downloadable, then we don't ship it either.
        """
        if self.shipclass=="DEFAULT":
            subtype = self.get_subtype_with_attr('is_shippable')
            if subtype and subtype is not self and not subtype.is_shippable:
                return False
            return True
        elif self.shipclass=="YES":
            return True
        else:
            return False
