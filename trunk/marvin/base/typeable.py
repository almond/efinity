from django.utils.translation import get_language, ugettext, ugettext_lazy as _
from django.db import models
from marvin.utils.dbsettings import SettingNotSet
from marvin.store.products import active_product_types

class Typeable(object):
    '''
    A Typeable object enables the use of extending a model
    by external addons, rather than always adding to the same
    model it allows for intermediate models to take control.
    '''

    def get_subtypes(self):
        # If we've already computed it once, let's not do it again.
        # This is a performance speedup.
        if hasattr(self,"_sub_types"):
            return self._sub_types

        types = []
        try:
            for module, subtype in active_product_types():
                try:
                    subclass = getattr(self, subtype.lower())
                    gettype = getattr(subclass, '_get_subtype')
                    subtype = gettype()
                    if not subtype in types:
                        types.append(subtype)
                except models.ObjectDoesNotExist:
                    pass
        except SettingNotSet:
            log.warn("Error getting subtypes, OK if in SyncDB")

        self._sub_types = tuple(types)
        return self._sub_types

    get_subtypes.short_description = _("Subtypes")

    def get_subtype_with_attr(self, *args):
        """Get a subtype with the specified attributes.  Note that this can be chained
        so that you can ensure that the attribute then must have the specified attributes itself.

        example:  get_subtype_with_attr('parent') = any parent
        example:  get_subtype_with_attr('parent', 'product') = any parent which has a product attribute
        """
        for subtype_name in self.get_subtypes():
            subtype = getattr(self, subtype_name.lower())
            if hasattr(subtype, args[0]):
                if len(args) == 1:
                    return subtype
                else:
                    found = True
                    for attr in args[1:-1]:
                        if hasattr(subtype, attr):
                            subtype = getattr(self, attr)
                        else:
                            found = False
                            break
                    if found and hasattr(subtype, args[-1]):
                        return subtype

        return None

    def smart_attr(self, attr):
        """Retrieve an attribute, or its parent's attribute if it is null or blank.
        Ex: to get a weight.  obj.smart_attr('weight')"""

        val = getattr(self, attr)
        if val is None or val == "":
            for subtype_name in self.get_subtypes():
                subtype = getattr(self, subtype_name.lower())

                if hasattr(subtype, 'parent'):
                    subtype = subtype.parent.product

                if hasattr(subtype, attr):
                    val = getattr(subtype, attr)
                    if val is not None:
                        break
        return val

    def smart_relation(self, relation):
        """Retrieve a relation, or its parent's relation if the relation count is 0"""
        q = getattr(self, relation)
        if q.count() > 0:
            return q

        for subtype_name in self.get_subtypes():
            subtype = getattr(self, subtype_name.lower())

            if hasattr(subtype, 'parent'):
                subtype = subtype.parent.product

                return getattr(subtype, relation)

    @property
    def has_variants(self):
        subtype = self.get_subtype_with_attr('has_variants')
        return subtype and subtype.has_variants
