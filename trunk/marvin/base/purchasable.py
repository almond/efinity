import datetime
from decimal import Context, Decimal, ROUND_FLOOR
from django.utils.translation import get_language, ugettext_lazy as _
from django.core import urlresolvers
from django.conf import settings
from django.db import models
from django.contrib.sites.models import Site
from marvin.utils.fields import CurrencyField
from marvin.utils.unique_id import slugify
#from marvin.trans import TransMeta
from marvin.base.typeable import Typeable
#from marvin.addons.compensation.models import Plan
from marvin.store.products.prices import get_product_quantity_price, get_product_quantity_adjustments, PriceAdjustmentCalc

class PurchasableManager(models.Manager):

    def active(self, variations=True, **kwargs):
        if not variations:
            kwargs['productvariation__parent__isnull'] = True
        return self.filter(active=True, **kwargs)

    def active_by_site(self, variations=True, **kwargs):
        return self.by_site(active=True, variations=variations, **kwargs)

    def by_site(self, site=None, variations=True, **kwargs):
        if not site:
            site = Site.objects.get_current()

        site = site.id

        #log.debug("by_site: site=%s", site)
        if not variations:
            kwargs['productvariation__parent__isnull'] = True
        return self.filter(site__id__exact=site, **kwargs)

    def featured_by_site(self, site=None, **kwargs):
        return self.by_site(site=site, active=True, featured=True, **kwargs)

    def get_by_site(self, site=None, **kwargs):
        if not site:
            site = Site.objects.get_current()
        return self.get(site = site, **kwargs)


    def recent_by_site(self, **kwargs):
        query = self.active_by_site(**kwargs)
        if query.count() == 0:
            query = self.active_by_site()

        query = query.order_by('-date_added', '-id')
        return query

class Purchasable(models.Model, Typeable):
    '''
    Root class for all models that can be purchased through Checkout
    '''
    site = models.ForeignKey(Site, verbose_name=_('Site'))

    name = models.CharField(
        _("Name"),
        max_length=255,
        blank=False,
        help_text=_("This is what the product will be called in the default site language.  To add non-default translations, use the Product Translation section below."))

    #display_name = models.CharField(_("Display name"), max_length=255, editable=False,)
    
    slug = models.SlugField(_("Slug Name"), blank=True,
        help_text=_("Used for URLs, auto-generated from name if blank"), max_length=255)
    
    sku = models.CharField(_("SKU"), max_length=255, blank=True, null=True,
        help_text=_("Defaults to slug if left blank"))
    
    short_description = models.CharField(_("Short description of product"),
        help_text=_("This should be a 1 or 2 line description in the default site language for use in product listing screens"), max_length=200, default='', blank=True)
    
    description = models.TextField(_("Description of product"),
        help_text=_("This field can contain HTML and should be a few paragraphs in the default site language explaining the background of the product, and anything that would help the potential customer make their purchase."), default='', blank=True)
    
    
    items_in_stock = models.IntegerField(_("Number in stock"),  default='0')
    
    meta = models.TextField(_("Meta Description"), max_length=200, blank=True, null=True, help_text=_("Meta description for this product"))
    date_added = models.DateField(_("Date added"), null=True, blank=True)
    active = models.BooleanField(_("Active"), default=True, help_text=_("This will determine whether or not this product will appear on the site"))
    featured = models.BooleanField(_("Featured"), default=False, help_text=_("Featured items will show on the front page"))
    ordering = models.IntegerField(_("Ordering"), default=0, help_text=_("Override alphabetical order in category display"))
    
    total_sold = models.IntegerField(_("Total sold"),  default='0')

    objects = PurchasableManager()

    ##################################################
    # Check to see if we need to add in Compensation
    ##################################################
    if 'marvin.addons.compensation' in settings.INSTALLED_APPS:
        compensation_plan = models.ForeignKey('compensation.Plan', blank=True, null=True)
        distributable_profit = models.DecimalField(decimal_places=2, max_digits=15, null=True, blank=True, help_text=_("Amount of distributable profit for this product. Will be dispersed according to the selected Compensation Plan."))

    #__metaclass__ = TransMeta

    class Meta:
        ordering = ('site', 'ordering', 'name')
        verbose_name = _("Product")
        verbose_name_plural = _("Products")
        unique_together = (('site', 'sku'),('site','slug'))
        #translate = ('display_name', 'short_description', 'description', 'meta', )
        abstract = True

    def _is_discountable(self):
        p = self.get_subtype_with_attr('discountable')
        if p:
            return p.discountable
        else:
            return True

    is_discountable = property(_is_discountable)

    def translated_attributes(self, language_code=None):
        if not language_code:
            language_code = get_language()
        q = self.productattribute_set.filter(languagecode__exact = language_code)
        if q.count() == 0:
            q = self.productattribute_set.filter(models.Q(languagecode__isnull = True) | models.Q(languagecode__exact = ""))
        return q

    @property
    def unit_price(self):
        """
        Here we check againt the Prices,
        which by default only really considers quantity,
        if none exists we return a decimal of 0.00 instead of None
        """

        subtype = self.get_subtype_with_attr('unit_price')

        if subtype and subtype is not self:
            price = subtype.unit_price
        else:
            price = get_product_quantity_price(self, Decimal('1'))

        if not price:
            price = Decimal("0.00")

        return price

    @property
    def unit_price_display(self):
        return '$' + str(self.unit_price.quantize(Decimal("0.01")))

    def get_qty_price(self, qty, include_discount=True):
        """
        If QTY_DISCOUNT prices are specified, then return the appropriate discount price for
        the specified qty.  Otherwise, return the unit_price
        returns price as a Decimal
        """
        subtype = self.get_subtype_with_attr('get_qty_price')
        if subtype and subtype is not self:
            price = subtype.get_qty_price(qty, include_discount=include_discount)

        else:
            if include_discount:
                price = get_product_quantity_price(self, qty)
            else:
                adjustment = get_product_quantity_adjustments(self, qty)
                if adjustment.price is not None:
                    price = adjustment.price.price
                else:
                    price = None
            if not price:
                price = self.unit_price

        return price

    def get_qty_price_list(self):
        """Return a list of tuples (qty, price)"""
        prices = self.price_set.all().exclude(
            expires__isnull=False,
            expires__lt=datetime.date.today()
        ).select_related()
        return [(price.quantity, price.dynamic_price) for price in prices]

    def in_stock(self):
        subtype = self.get_subtype_with_attr('in_stock')
        if subtype and subtype is not self:
            return subtype.in_stock

        return self.items_in_stock > 0


    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return urlresolvers.reverse('satchmo_product', kwargs={'product_slug': self.slug})

    def save(self, **kwargs):
        if not self.pk:
            self.date_added = datetime.date.today()

        #self.display_name = self.name

        if self.name and not self.slug:
            self.slug = slugify(self.name, instance=self)

        if not self.sku:
            self.sku = self.slug

        super(Purchasable, self).save(**kwargs)

    def add_template_context(self, context, *args, **kwargs):
        """
        Add context for the product template.
        Call the add_template_context method of each subtype and return the
        combined context.
        """
        subtypes = self.get_subtypes()
        for subtype_name in subtypes:
            subtype = getattr(self, subtype_name.lower())
            if subtype == self:
                continue
            if hasattr(subtype, 'add_template_context'):
                context = subtype.add_template_context(context, *args, **kwargs)

        return context



class PriceBase(models.Model):
    """
    A Price!
    Separating it out lets us have different prices for the same product for different purposes.
    For example for quantity discounts.
    The current price should be the one with the earliest expires date, and the highest quantity
    that's still below the user specified (IE: ordered) quantity, that matches a given product.
    """
    price = CurrencyField(_("Price"), max_digits=14, decimal_places=6, )
    quantity = models.DecimalField(_("Discount Quantity"), max_digits=18,
        decimal_places=6, default='1.0',
        help_text=_("Use this price only for this quantity or higher"))
    expires = models.DateField(_("Expires"), null=True, blank=True)
    #TODO: add fields here for locale/currency specific pricing

    def __unicode__(self):
        return unicode(self.price)

    def adjustments(self, product=None):
        """Get a list of price adjustments, in the form of a PriceAdjustmentCalc
        object. Optionally, provide a pre-fetched product to avoid the foreign
        key lookup of the `product' attribute.
        """
        if product is None:
            product = self.product
        adjust = PriceAdjustmentCalc(self, product)
        #signals.satchmo_price_query.send(self, adjustment=adjust,
        #    slug=product.slug, discountable=product.is_discountable)
        return adjust

    def _dynamic_price(self):
        """Get the current price as modified by all listeners."""

        adjustment = self.adjustments()
        return adjustment.final_price()

    dynamic_price = property(fget=_dynamic_price)

    class Meta:
        abstract = True
        ordering = ['expires', '-quantity']
        verbose_name = _("Price")
        verbose_name_plural = _("Prices")
        unique_together = (("product", "quantity", "expires"),)
