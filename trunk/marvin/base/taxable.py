from django.utils.translation import ugettext_lazy as _
from django.db import models
from marvin.utils.dbsettings import config_value
from marvin.store.tax.models import TaxClass

adminfieldset = (_('Tax'), {'fields':('taxable', 'taxClass'), 'classes': ('collapse',)})

class Taxable(models.Model):
    class Meta:
        abstract = True

    taxable = models.BooleanField(_("Taxable"), default=lambda: config_value('TAX', 'PRODUCTS_TAXABLE_BY_DEFAULT'))
    taxClass = models.ForeignKey(TaxClass, verbose_name=_('Tax Class'), blank=True, null=True, help_text=_("If it is taxable, what kind of tax?"))
