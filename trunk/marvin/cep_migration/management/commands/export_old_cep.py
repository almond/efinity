from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order


class Command(BaseCommand):
    """
    Something neater
    """
    help = "Export the csv with the orders"

    def handle(self, *args, **kwargs):
        """@todo: Docstring for handle.

        :*args: @todo
        :**kwargs: @todo
        :returns: @todo

        """
        print "id,status"
        for order in Order.objects.all():
            print "%d,%s" % (order.pk, order.status)
