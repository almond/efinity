from datetime import datetime, date, timedelta

from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType

from marvin.store.shop.models import OrderItem

class Command(BaseCommand):
    help = "Just test what the cim billing script will fetch"
    def handle(self, *args, **kwargs):
        ct = ContentType.objects.get(app_label="products", model="product")

        # Anything that expires after the first of this month should be put in the rebill queue.
        # TODO one time mods
        first_of_month = date(2013, 8, 1)
        last_of_month = date(2013, 8, 31)

        # One time
        old_expiration_date = date(2013, 9, 1)
        next_expiration_date = date(2013, 10, 1)

        # When where delayed billings staged?
        delayed_billing_date = first_of_month - timedelta(days=6)

        # I did this in order not to have a freaken huge line of code. Looks nasty though
        expiring_subscriptions = OrderItem.objects.filter(expire_date__gte=first_of_month)
        expiring_subscriptions = expiring_subscriptions.filter(completed=True)
        # On next run remove the Renewed status from the query
        expiring_subscriptions = expiring_subscriptions.exclude(order__status__in=("Cancelled", )).distinct()

        alianza_global = []

        for item in expiring_subscriptions:
            if item.order.user.profile_object.team == "Alianza Global":
                alianza_global.append(item)

            print "User %s, Team %s, Order %d, Subscription %s" % (
                item.order.user.username,
                item.order.user.profile_object.team,
                item.order.pk,
                item.product.name,
            )

        print len(expiring_subscriptions)
        print len(alianza_global)

