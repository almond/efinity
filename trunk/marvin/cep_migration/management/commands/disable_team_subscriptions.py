from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

from marvin.store.shop.models import OrderItem


class Command(BaseCommand):
    help = "Disable autobilling from alianza global users"
    
    def handle(self, *args, **kwargs):
        content_type = ContentType.objects.get(app_label="products", model="product")
        order_items = OrderItem.objects.filter(
            order__user__profile_object__team=args[0],
            content_type=content_type,
            product_id__in= (39, 103, 102, 42, 40, 43, 101, 71, 104)
        )

        print len(order_items)
        raw_input("Press any key to continue...")

        for order_item in order_items:
            print "fixing for %s, team %s" % (order_item.order.user.username, 
                    order_item.order.user.profile_object.team)

            order_item.completed = False
            order_item.save()
            print "%s of team %s has been fixed" % (
                order_item.order.user.username,
                order_item.order.user.profile_object.team,
            )
