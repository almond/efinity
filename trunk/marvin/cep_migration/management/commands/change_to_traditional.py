from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType

from marvin.store.shop.models import OrderItem, Order


class Command(BaseCommand):
    help = "Change everyone from traditional + book to traditional"

    def handle(self, *args, **kwargs):
        content_type = ContentType.objects.get(app_label="products", model="product")
        items = OrderItem.objects.filter(
            content_type=content_type, 
            product_id=args[0],
            completed=True
        )

        for item in items:
            try:
                print "Fixing order item %d, for user %s, subscription %s" % (
                    item.pk,
                    item.order.user.username,
                    item.product.name,
                )
            except Order.DoesNotExist:
                print "Order not there. Fixing order item %d, subscription %s" % (
                    item.pk,
                    item.product.name,
                )


            item.product_id = args[1]

            try:
                item.save()
            except Order.DoesNotExist:
                print "Nothing to save"
                continue
            
            print "******** Item %s saved succesfully" % (item.pk, )

            item = OrderItem.objects.get(pk=item.pk)

            try:
                print "Results are id: %d, user: %s, subscription: %s" % (
                    item.pk,
                    item.order.user.username,
                    item.product.name
                )
            except Order.DoesNotExist:
                print "Order not there. Results are id: %d, subscription: %s" % (item.pk, item.product.name)
            
