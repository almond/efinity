from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType

from marvin.store.shop.models import OrderItem, Order


class Command(BaseCommand):
    help = "Disable subscriptions that should not be anymore"
    def handle(self, *args, **kwargs):
        content_type = ContentType.objects.get(app_label="products", model="product")
        items = OrderItem.objects.filter(product_id__in=args, completed=True)

        # Now disable them
        for item in items:
            try:
                print "fixing %d for user %s and subscription %s" % (
                    item.pk, item.order.user.username, item.product.name
                )
                item.completed = False
                item.save()
            except Order.DoesNotExist:
                print "No order exists for item %d" % item.pk
                continue
