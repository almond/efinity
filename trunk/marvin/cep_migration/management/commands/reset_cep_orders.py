import os
import csv

from django.core.management.base import BaseCommand
from django.conf import settings

from marvin.store.shop.models import Order


class Command(BaseCommand):
    """
    Something neat
    """
    help = "Return every order status to their previous one"

    def get_csv_path(self, filename):
        return os.path.join(settings.DIRNAME, "csv", filename)

    def open_file(self, filename):
        return csv.DictReader(open(self.get_csv_path(filename)))

    def handle(self, *args, **kwargs):
        reader = self.open_file(args[0])

        for row in reader:
            print "Processing order %s" % row["id"]
            order = Order.objects.get(pk=row["id"])
            order.status = row["status"]
            order.save()
