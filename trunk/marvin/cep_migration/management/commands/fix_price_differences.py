import os
import decimal
import datetime

from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType
from django.conf import settings

from marvin.store.shop.models import Order, OrderItem, Product

class Command(BaseCommand):
    """ 
    Ok... so get all orderitems that have a traditional subscription
    Then, if the price listed there is > than 10 of the product price, correct accordingly
    Print the changes to a CSV
    """
    def handle(self, *args, **kwargs):
        ct = ContentType.objects.get(app_label="products", model="product")
        expiry_date = datetime.date(2013, 10, 1)
        items = OrderItem.objects.filter(content_type=ct, product_id__in=(103, 39), 
                completed=True, expire_date=expiry_date, order__status="renewed_september")
        print "order_id,orderitem_id,product_price,ibo,charged,corrected,difference"
        for item in items:
            if item.unit_price > item.product.get_qty_price(item.quantity):
                new_unit_price = item.product.get_qty_price(item.quantity)
                new_line_item_price = item.product.get_qty_price(item.quantity) * item.quantity
                print "%d,%d,%s,%s,%s,%s,%s" % (item.order.pk, item.pk, new_unit_price, 
                        item.order.user.username, 
                        item.line_item_price, new_line_item_price, 
                        item.line_item_price - new_line_item_price)

                item.unit_price = new_unit_price
                item.line_item_price = new_line_item_price
                item.save()
