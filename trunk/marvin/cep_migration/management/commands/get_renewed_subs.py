import csv
import os

from django.core.management.base import BaseCommand
from django.conf import settings

from marvin.store.shop.models import Order

class Command(BaseCommand):
    """ There were a lot of billing annomalies. This 
    command will read the Auth.net csv file, then fetch 
    the order ID defined there and look for discrepancies on it."""

    def get_csv_path(self, filename):
        return os.path.join(settings.DIRNAME, "csv", filename)

    def open_file(self, filename):
        return csv.DictReader(open(self.get_csv_path(filename)))

    def handle(self, *args, **kwargs):
        # Open the file from Auth.net
        reader = self.open_file(args[0])
        print "Order ID,IBO,Price,Payment"
        for row in reader:
            order = Order.objects.get(pk=row["Invoice Number"])
            print "%d,%s,%s,%s" % (order.pk, order.user.username, order.total, row["Settlement Amount"])
