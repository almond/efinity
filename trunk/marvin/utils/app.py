from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.sites.models import Site
from django.contrib.auth.models import User

# Redirect user
from django.http import HttpResponseNotFound
#from marvin.core.accounts.models import CustomField, CustomKey


# Template wrapper
def render(request, template, context = {}, *args, **kwargs):
    kwargs['context_instance'] = RequestContext(request)

    if request.user and not request.user.is_anonymous():
        context.update({
            'user': request.user,
            'username': request.user.username,
            #'profile': request.user.get_profile(),
            'logged_in': True,
        })

    context.update({
        'site': Site.objects.get_current(),
        })

    if request.session.has_key('sponsor') and request.session['sponsor'] is not None:
        '''
        customfields = {}
        for key in CustomKey.objects.all():
            try:
                entry = CustomField.objects.filter(user = request.session['sponsor'], key = key).get()
            except CustomField.DoesNotExist:
                entry = None

            if entry:
                customfields[key.slug] = entry.value
        '''

        context.update({
            'sponsor': request.session['sponsor'],
            #'sponsor_profile': request.session['sponsor'].get_profile(),
            #'sponsor_custom': customfields,
            'username': request.session['sponsor'].username,
            })

    return render_to_response(template, context, *args, **kwargs)


def verify_username( func ):
    def temp(request, sponsorUsername, *args, **kwargs):
        try:
            sponsor = User.objects.get(username__iexact = sponsorUsername)
        except:# DoesNotExist:
            return HttpResponseNotFound()

        if sponsor and sponsor.is_active:
            request.session['sponsor'] = sponsor
            return func(request, sponsor, *args, **kwargs)
        else:
            return HttpResponseNotFound()

    return temp
