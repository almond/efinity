from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.utils.dbsettings.views',
    (r'^$', 'site_settings', {}, 'site_settings'),
    (r'^export/$', 'export_as_python', {}, 'settings_export'),
    (r'^(?P<group>[^/]+)/$', 'group_settings'),
)
