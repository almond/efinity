import string
import datetime
import random

from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.contrib.sites.models import Site
from django.contrib.auth.decorators import login_required, permission_required
from marvin.utils.decorators import staff_only
from django.contrib.auth.models import User, Group
from django.contrib import messages

# Redirect user
from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.http import HttpResponseRedirect as redirect
from django.core.urlresolvers import resolve, reverse

# Translation
from django.utils.translation import ugettext_lazy as t


# Template wrapper
def render(request, template, context = {}, *args, **kwargs):
    kwargs['context_instance'] = RequestContext(request)
    return render_to_response(template, context, *args, **kwargs)
