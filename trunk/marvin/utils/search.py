from django.db import models
import operator

def construct_search(field_name):
    if field_name.startswith('^'):
        return "%s__istartswith" % field_name[1:]
    elif field_name.startswith('='):
        return "%s__iexact" % field_name[1:]
    elif field_name.startswith('@'):
        return "%s__search" % field_name[1:]
    else:
        return "%s__icontains" % field_name


def perform_search(qs, search_fields, query, use_distinct=True):
    '''
    qs              Pass a QuerySet
    Search Fields   ['list', 'of', 'fields'] to search
    query           String to search for
    '''
    orm_lookups = [construct_search(str(search_field))
        for search_field in search_fields]

    for bit in query.split():
        or_queries = [models.Q(**{orm_lookup: bit})
                for orm_lookup in orm_lookups]

        qs = qs.filter(reduce(operator.or_, or_queries))
    
    if not use_distinct:
        for search_spec in orm_lookups:
            field_name = search_spec.split('__', 1)[0]
            f = self.lookup_opts.get_field_by_name(field_name)[0]
            if hasattr(f, 'rel') and isinstance(f.rel, models.ManyToManyRel):
                use_distinct = True
                break

    return qs