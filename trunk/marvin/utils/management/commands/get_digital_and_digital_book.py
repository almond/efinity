'''
Created on Feb 28, 2013

@author: axolote

Get subscribed users per month. Pass the month as a number
'''
from django.core.management.base import BaseCommand
import datetime
from dateutil.relativedelta import relativedelta 
from marvin import unicodecsv
from django.db.models.query_utils import Q
from marvin.store.products.modules.subscription.models import SubscriptionProduct
from marvin.store.shop.models import OrderItem


class Command(BaseCommand):
    
    product_ids = list(SubscriptionProduct.objects.values_list('product__id', flat=True).filter(~Q(product__slug__contains='book') & Q(product__slug__contains='traditional')))
    product_ids.extend(SubscriptionProduct.objects.values_list('product__id', flat=True).filter(Q(product__slug__contains='book') & Q(product__slug__contains='traditional')))
    
    def write_csv_file(self, order_list, filename):
        writer = unicodecsv.writer(open("/home/axolote/workspace/efinity/csv/user_lists/%s" % filename, "w"))
        writer.writerow(["email", "name", "ibo_number"])
        for order in order_list:
            print "processing %s" % order.user.username
            writer.writerow([order.user.email, "%s %s" % (order.user.first_name, order.user.last_name), order.user.username])
    
    def handle(self, *args, **kwargs):
        """
        Generate a nice CSV file with the following fields
        email, name, ibo_number
        of people that bought a subscription a given month
        """
        try:
            month = int(args[0])
            year = int(args[1])
            
        except IndexError:
            month = 1
            year= datetime.date.today().year
            
        date_to_compare = datetime.date(year, month, 01) + relativedelta(months=1)
        
        print date_to_compare
        
        order_items = OrderItem.objects.filter(product_id__in=self.product_ids, expire_date__gte=date_to_compare)
        orders = list()
        
        for oi in order_items:
            if oi.order not in orders:
                orders.append(oi.order)
            else:
                continue
            
        self.write_csv_file(orders, "subscriptions_%s_%s.csv" % (year, month))