from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    """ Print to console a CSV of people that are active and
        subscribed to products that are about to be removed """
        
    help = "Well.. some subscriptions will be removed. Make a csv of affected users"

    def handle (self, *args, **kwargs):
        """ Make stuff happen!! """
        users_to_print = dict()
        for user in User.objects.all().exclude(profile_object__team="Alianza Global"):
            for key, value in user.subscriptions().items():
                # Check if user needs be printed
                if value["product"].pk in [71, 43, 101, 104]:
                    users_to_print[user] = value["product"]

        # Now print the hell out of everything
        print "ibo_number,name,phone,email,subscription_name,team"
        for k, v in users_to_print.items():
            print "%s,%s,%s,%s,%s,%s" % (k.username, 
                    k.first_name + " " + k.last_name, k.phone.number, k.email, 
                    v.name, k.profile_object.team)
