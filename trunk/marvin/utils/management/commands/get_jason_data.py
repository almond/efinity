from django.core.management.base import BaseCommand
from marvin.store.shop.models import OrderItem


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        cep_ids = (39, 103,)

        ois_icommercio = OrderItem.objects.filter(order__user__profile_object__team="iCommercio",
                                        expire_date__year=2013, expire_date__month=5, product_id__in=cep_ids)
        ois_teamenglish = OrderItem.objects.filter(order__user__profile_object__team="English", expire_date__year=2013,
                                        expire_date__month=5, product_id__in=cep_ids)

        print "ICOMMERCIO: %d, ENGLISH: %d" % (len(ois_icommercio), len(ois_teamenglish))


