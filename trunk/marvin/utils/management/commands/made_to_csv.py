from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order, OrderItem
from marvin.addons.planning.models import Convention
from marvin import unicodecsv
import csv
import datetime


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        """What orders are present in the system that are not present in the csv file"""
        reader = csv.DictReader(open("/home/axolote/workspace/efinity/csv/daily/bigfile.csv"))
        writer = unicodecsv.writer(open("/home/axolote/workspace/efinity/csv/daily/emergency_mediatech.csv", "wb"),
                encoding='utf-8', quoting=unicodecsv.QUOTE_ALL)
        from_date = datetime.date(2013, 2, 1)
        to_date = datetime.date(2013, 2, 13)
        results = []
        missing = []
        orderitems = OrderItem.objects.filter(order__time_stamp__gte=from_date, order__time_stamp__lte=to_date)

        writer.writerow([
            'Order #',
            'Shipping Method',
            'First Name',
            'Last Name',
            'Address 1',
            'Address 2',
            'City',
            'State',
            'Postal Code',
            'Country',
            'Phone Number',
            'Gift Message or Order Notes',
            'Product SKU',
            'Quantity',
            'Unit Weight',
            'Product Description',
            'Unit Price',
            'Pick Location (bin location)',
            'Carrier Shipper',
            'Insured Value',
            'Delivery Confirmation',
            'Signature Confirmation',
            'Adult Signature Confirmation',
            'COD_Indicator',
            'COD_Amount',
            'Billed Shipping Cost',
            'Tax Billed to Customer',
            'Customer Number (IBO Number)',
            'Order Time',
            'Current Month',
            'Language',
            'EMAIL',
            'Team',
        ])


        for oi in orderitems:
            if not oi.product:
                continue
            if type(oi.product) == Convention:
                continue
            if hasattr(oi.product, 'is_subscription') and oi.product.is_subscription:
                if oi.order.time_stamp.day >=25:
                    continue
            if oi.product.sku == 'subscription-digital':
                continue
            if oi.order not in results:
                results.append(oi.order)
        # Now check the files that there 
        for row in reader:
            print row["Order #"]
            try:
                order = Order.objects.get(pk=row["Order #"])
                if order not in results:
                    if order not in missing:
                        missing.append(order)
            except Order.DoesNotExist:
                print "something wrong"
        
        print len(missing)
        # Now write the hell out of those orders
        for order in missing:
            for orderitem in order.orderitem_set.all():
                shipping_method = ''
                shipping_carrier = ''
                # if ups, set correct shipping carrier, if no method, default to USPS
                if order.shipping_method == 'UPS':
                    shipping_method = 'UPS Ground'
                    shipping_carrier = 'UPS Online'
                else:
                    shipping_method = 'Priority Mail'
                    shipping_carrier = 'Express 1'

                product_weight = ''
                if orderitem.product.weight:
                    product_weight = (orderitem.product.weight * orderitem.quantity)

                order_notes = ''
                if order.notes:
                    order_notes = order.notes

                language = ''
                if orderitem.product.product_language == 'en':
                    language = 'english'
                elif orderitem.product.product_language == 'es':
                    language = 'spanish'
                writer.writerow([
                    order.id,
                    shipping_method,
                    order.user.first_name,
                    order.user.last_name,
                    order.ship_street1,
                    order.ship_street2,
                    order.ship_city,
                    order.ship_state,
                    order.ship_postal_code,
                    order.ship_country_name,
                    '',
                    order_notes,
                    orderitem.product.sku,
                    orderitem.quantity,
                    product_weight,
                    orderitem.product.name,
                    orderitem.unit_price,
                    '', # Bin location
                    shipping_carrier, # Carrier Shipper
                    '', # Insured Value
                    '', # Delivery Confirmation
                    '', # Signature
                    '', # Adult Sig
                    '', # COD Indicator
                    '', # COD Amount
                    order.shipping_cost,
                    order.tax,
                    order.user.aid,
                    str(order.time_stamp),
                    order.time_stamp.strftime('%B'),
                    language,
                    'ibosupport@myefinity.com',
                    order.user.profile.team,
                ])

            #order.status = "Shipped"
            #order.notes = "CSV generation"
            #order.save()

        # close csv file
