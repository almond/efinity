from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    """ You know.. they kind of left... """
    def handle(self, *args, **kwargs):
        """@todo: I actually have no idea what goes here

        :*args: Anything from command line
        :**kwargs: Anything trailed by words on the command line
        :returns: Nothing really

        """
        users = User.objects.filter(profile_object__team="Alianza Global")
        users_len = len(users)

        # print a count so at least we know what's up
        print u"%d users will be affected by this" % users_len
        
        for u in users:
            # Print which one you are working with
            print u"User %s is being processed from team %s" % (u.username, u.profile_object.team)

            # Mark every order done by the user as Cancelled
            for o in u.orders.filter(orderitem__expire_date__isnull=True).distinct():
                print u"Order %d from user %s is being cancelled" % (o.pk, u.username)
                o.status = "Cancelled"
                o.save()

            print u"User %s is being inactivated" % u.username
            print "\n"
            u.is_active = False
            u.save()
            users_len -= 1
