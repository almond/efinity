from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        """ List of every user platinum or above in team iComercio """
        print "Name, Email, Qualified Pin, Team"
        for u in User.objects.filter(profile_object__team="Alianza Global"):
            if u.is_platinum_or_above():
                print "%s, %s, %s, %s" % (u.first_name.encode('ascii', 'ignore') + u.last_name.encode("ascii", "ignore"), u.email, u.profile_object.pinlevel, u.profile_object.team)
