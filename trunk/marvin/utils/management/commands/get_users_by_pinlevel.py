'''
Created on Feb 27, 2013

@author: axolote
'''
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from marvin import unicodecsv
from django.db.models.query_utils import Q
from marvin.store.products.modules.subscription.models import SubscriptionProduct


class Command(BaseCommand):
    product_ids = SubscriptionProduct.objects.values_list('product__id', flat=True).filter(Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional'))
    
    def write_csv_file(self, queryset, filename):
        writer = unicodecsv.writer(open("/home/axolote/workspace/efinity/csv/user_lists/%s" % filename, "w"))
        writer.writerow(["email", "name", "ibo_number", "pin_level"])
        for q in queryset:
            print "processing %s" % q.username
            writer.writerow([q.email, "%s %s" % (q.first_name, q.last_name), q.username, q.pinlevel().name])
    
    def handle(self, *args, **kwargs):
        # Generate 3 different CSV list of every user in the DB according to their pinlevel
        # Platinums or higher but not including Emerald
        # Emerald or higher but not including Diamond
        # Diamond or higher.
        platinums = User.objects.filter(profile_object__pinlevel__group__id__gte=2, profile_object__pinlevel__group__id__lte=5)
        emeralds = User.objects.filter(profile_object__pinlevel__group__id__gte=6, profile_object__pinlevel__group__id__lte=8)
        diamonds = User.objects.filter(profile_object__pinlevel__group__id__gte=9)
        
        print len(platinums)
        print len(emeralds)
        print len(diamonds)
        
        # Now generate the lists
        self.write_csv_file(platinums, "platinums.csv")
        self.write_csv_file(emeralds, "emeralds.csv")
        self.write_csv_file(diamonds, "diamonds.csv")
        