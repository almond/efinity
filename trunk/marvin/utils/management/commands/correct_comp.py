from marvin.store.shop.models import OrderItem
from marvin.addons.compensation.models import Commission
from marvin.store.products.modules.subscription.models import SubscriptionProduct


from django.contrib.auth.models import User
from django.db.models import Sum, Q
from django.core.management.base import BaseCommand
from django.conf import settings
import csv
import os
from decimal import Decimal

class Command(BaseCommand):
  """ How did this get so fucked up? """
  def get_subscription_ids(self):
    product_ids = list(SubscriptionProduct.objects.values_list('product__id', flat=True).filter(Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional')))
    product_ids += list(SubscriptionProduct.objects.values_list('product__id', flat=True).filter(~Q(product__slug__contains='book') & Q(product__slug__contains='traditional')))
    product_ids += list(SubscriptionProduct.objects.values_list('product__id', flat=True).filter(Q(product__slug__contains='book') & Q(product__slug__contains='traditional')))

    return product_ids

  def get_file_name(self, filename):
    return os.path.join(settings.DIRNAME, "csv", filename)

  def handle(self, *args, **kwargs):
    comms = Commission.objects.filter(order_item__content_type__id=55, created__month=3, created__year=2013)

    ois = list()
    for c in comms:
      if c.order_item.pk not in ois:
        ois.append(c.order_item.pk)

    total_revenue = OrderItem.objects.filter(pk__in=ois).aggregate(total=Sum("line_item_price"))["total"]
    print total_revenue

    order_items = OrderItem.objects.filter(order__payments__time_stamp__month=3, order__payments__time_stamp__year=2013).distinct().aggregate(total=Sum("line_item_price"))["total"]
    print order_items
  # def handle(self, *args, **kwargs):
  #   # Read in the CSV file and just kind
  #   # of skim it and stuff... get a number of subs
  #   # For each user and match is with the CSV you've got going on
  #   try:
  #     reader = csv.DictReader(open(self.get_file_name(args[0]), "rU"))
  #   except IOError:
  #     reader = csv.DictReader(open("/srv/www/myefinity.com/app/trunk/csv/%s" % args[0], "rU"))
  #   # You got the reader thing. Now surf through it and
  #   # print how many subs each user has

  #   ois_list = list()
  #   for r in reader:
  #     print "IBO %s" % r["IBO"]
  #     # order_items = OrderItem.objects.filter(product_id__in=self.get_subscription_ids, order__user__username=r["IBO"], expire_date__month=4, expire_date__year=2013)

  #     order_items = OrderItem.objects.filter(expire_date__month=5, expire_date__year=2013)

  #     try:
  #       print "Total Commissions: %f" % Commission.objects.filter(order_item__in=order_items).aggregate(total=Sum("amount"))["total"]
  #     except TypeError:
  #       print "Total Commissions: 0.00"
  #     for o in order_items:
  #       if o.order.balance == 0.00:
  #         print "\tIBO: %s, Subscription Type: %s, Expiration Date: %s" % (o.order.user.username, o.product, o.expire_date)
