from marvin.store.shop.models import OrderItem
from marvin.addons.compensation.models import Commission

from django.contrib.auth.models import User
from django.db.models import Sum
from django.core.management.base import BaseCommand
from django.conf import settings
import csv
import os
from decimal import Decimal

class Command(BaseCommand):
  """ How did this get so fucked up? """
  def get_file_name(self, filename):
    return os.path.join(settings.DIRNAME, "csv", filename)

  def handle(self, *args, **kwargs):
    # Read in the CSV file and just kind
    # of skim it and stuff... get a number of subs
    # For each user and match is with the CSV you've got going on
    try:
      reader = csv.DictReader(open(self.get_file_name(args[0]), "rU"))
    except IOError:
      reader = csv.DictReader(open("/srv/www/myefinity.com/app/trunk/csv/%s" % args[0], "rU"))
    # You got the reader thing. Now surf through it and
    # print how many subs each user has

    corrected_total = Decimal("0.00")

    for r in reader:
      # Get only the order items that were not defined in the
      # CSV file and remove the compensation for those
      # Turn the orderitem string into an array
      ois_ids = r["Order ID"].split(",")

      ois = OrderItem.objects.filter(order__user__username=r["IBO"], pk__in=ois_ids)

      list_ois = list(ois)
      if list_ois:
        # saved_one = list_ois.pop()
        comms = Commission.objects.filter(order_item__in=list_ois)
        for c in comms:
          print "\t%s" % c.created
        total_amount = comms.aggregate(total=Sum("amount"))["total"]
        if not total_amount:
          total_amount = Decimal("0.00")

        corrected_total += total_amount

        # Debug info
        for o in list_ois:
          order_items = ''
          product_names = ''

          for o in list_ois:
            order_items += "%s, " % o.pk
            product_names += "%s, " % o.product.name

          print "Before: %d, After: %d, OIS: %s, Products: %s, Amount: %f" % (len(ois), len(list_ois), order_items, product_names, total_amount)
    print "Corrected total: %f" % corrected_total
