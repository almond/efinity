'''
Created on Feb 27, 2013

Not sure why Jason asked for this... well we might as well get it

@author: axolote
'''
from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order
from dateutil.relativedelta import *
from django.db.models.query_utils import Q
from marvin.store.products.modules.subscription.models import SubscriptionProduct

class Command(BaseCommand):
    help = "Weird data requested by Jason even though he already has it"
    
    product_ids = SubscriptionProduct.objects.values_list('product__id', flat=True).filter(Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional'))
    
    def process_orders(self, qs):
        # Store only the ones that are different
        data_to_print = []
        for order in qs:
            order_items = order.orderitem_set.filter(expire_date__isnull=False).exclude(product_id__in=self.product_ids).order_by("expire_date")
            # Go through every orderitem and check if it is a subscription
            for order_item in order_items:
                if order_item.expire_date:
                    # Add one to counter
                    expire_date = order_item.expire_date - relativedelta(months=1)
                    date_string = "%d-%d" % (expire_date.year, expire_date.month)
                    if date_string not in data_to_print:
                        data_to_print.append(date_string)
                        
        return data_to_print
    
    def handle(self, *args, **kwargs):
        pedro_orders = Order.objects.filter(user__username=6575).order_by("time_stamp")
        ana_orders = Order.objects.filter(user__username=6101341).order_by("time_stamp")
        
        # Now we need to find out the dates when CEP was paid
        pedro_data = self.process_orders(pedro_orders)
        ana_data = self.process_orders(ana_orders)
        
        print 6575
        for p in pedro_data:
            print p

        print 6101341
        for a in ana_data:
            print a
