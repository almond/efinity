from django.core.management.base import BaseCommand
from django.core.mail import send_mail

class Command(BaseCommand):
    """
    Whatever just testing stuff I guess
    """
    def handle(self, *args, **kwargs):
        """Testting email so that I can test cronjobs
        """
        send_mail(" efinity cronjob stuff", "Wow... this actually worked",
                  "support@myefinity.com", ["mario.r.vallejo@gmail.com"],
                  fail_silently=True)
