import csv
import os

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.conf import settings

from marvin.store.shop.models import Order



class Command(BaseCommand):
    """ Load that csv to fix your fuckup """
    help = "Load that csv file you generated"

    def get_csv_path(self, filename):
        return os.path.join(settings.DIRNAME, "csv", filename)

    def open_file(self, filename):
        return csv.DictReader(open(self.get_csv_path(filename)))


    def handle(self, *args, **kwargs):
        reader = self.open_file("fuckup.csv")

        # Plain counter
        counter = 0

        for row in reader:
            print "processing order %s" % row["order_id"]
            order = Order.objects.get(pk=row["order_id"])
            if order.status == row["status"]:
                counter += 1

            order.status = row["status"]
            order.save()

        print "%d registries matched" % counter
