'''
Created on Feb 4, 2013

@author: axolote
'''
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import csv

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        '''
        Export into a CSV the current user's ibo's and teams so that they can be imported
        at a later date
        '''
        users = User.objects.all().values("username", "profile_object__team")
        # Now create a csv writer
        writer = csv.writer(open("ibos_and_users_to_import.csv", "wb"))
        # Write header
        writer.writerow(["ibo", "team"])
        for u in users:
            print u["username"]
            if u["username"] and u["profile_object__team"]:
                writer.writerow([u["username"], u["profile_object__team"]])
            else:
                print "Data was blank" 
            
        print "Finished, check file"
