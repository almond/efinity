from django.core.management.base import BaseCommand

from marvin.store.shop.models import Order

import datetime

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        """Export all of the orders which last payment has a transaction_id of 'linked'"""
        orders = Order.objects.filter(time_stamp__gte=datetime.date(2013, 1, 1))
        # Get every order that has a LINKED payment status into a list. We'll drill down from there
        list_to_print = []
        for o in orders:
            payments = o.payments.filter(transaction_id="LINKED").order_by("-id")
            if payments:
                if payments[0].transaction_id == "LINKED":
                    list_to_print.append(o)
                else:
                    continue

        # Now print your results
        for l in list_to_print:
            print l.pk

