'''
Created on June 18, 2013

Something something sidevolumes

@author: axolote
'''
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        """ Generate a sidevolume csv to be opened in excel """
        print "IBO NAME, UPLINE PLATINUM NAME, BOOK ONLY, DIGITAL, DIGITAL AND BOOK, TRADITIONAL, TRADITIONAL AND BOOK"
        for u in User.objects.filter(profile_object__pinlevel__group__pk__gte=2, profile_object__team="iComercio"):
            # Now look into each of these users downlines and only fetch the ones that are regulars.
            downline = u.downline.filter(user__profile_object__pinlevel__group__pk__lte=1)

            # initiate array to store count of how many of each subscription they have
            subs = {
                "book_only": 0,
                "digital": 0,
                "digital_book": 0,
                "traditional": 0,
                "traditional_book": 0,
            }

            # How many?
            for d in downline:
                for sub in d.user.subscriptions().items():
                    if sub[1]["product"].pk in (71, 104):
                        subs["book_only"] += 1
                    elif sub[1]["product"].pk in (40,):
                        subs["digital"] += 1
                    elif sub[1]["product"].pk in (43, 101):
                        subs["digital_book"] += 1
                    elif sub[1]["product"].pk in (39, 103):
                        subs["traditional"] += 1
                    elif sub[1]["product"].pk in (42, 102):
                        subs["traditional_book"] += 1

            # Now print the results
            print "%s, %s, %d, %d, %d, %d, %d" % (u.first_name + " " + u.last_name, u.upline.first_name + " " + u.upline.last_name, 
                    subs["book_only"], subs["digital"], subs["digital_book"], subs["traditional"], subs["traditional_book"])

