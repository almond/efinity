from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    """ Fix IBO's that have the same uplines... actually just identify them """
    def handle(self, *args, **kwargs):
        users = User.objects.all()
        for u in users:
            try:
                if u.upline.username == u.username:
                    print u.username
            except AttributeError:
                continue
