import csv
import os
import datetime
from django.conf import settings
from django.contrib.auth.models import User

from django.core.management import BaseCommand
from marvin.addons.genealogy.models import Position, Tree, AccessLevel
from marvin.core.accounts.models import Profile


class Command(BaseCommand):
    help = "Preliminary csv import code thing"

    def get_csv_path(self, filename):
        return os.path.join(settings.DIRNAME, "csv", filename)

    def open_file(self, filename):
        return csv.DictReader(open(self.get_csv_path(filename)))

    def handle(self, *args, **kwargs):
        # Get reader
        reader = self.open_file(args[0])

        # Go through every row gathering data
        for row in reader:
            try:
                upline = User.objects.get(username=row["IBO # Upline Platinum"])
            except User.DoesNotExist:
                continue

            try:
                new_user = User.objects.get(username=row["IBO Number"])
                profile = Profile.objects.get(user=new_user)
            except User.DoesNotExist:
                new_user = User.objects.create_user(row["IBO Number"], row["email"], row["Password"])
                new_user.first_name = row["First Name"]
                new_user.last_name = row["Last Name"]

                # What else...? Well assign teams and stuff
                new_user.is_active = True

                Position(
                    tree=Tree.objects.get(id=1),
                    user=new_user,
                    upline=upline
                ).save()

                profile = Profile(
                    user=new_user,
                    ibo_number=new_user.username,
                    pinlevel=AccessLevel.objects.get(group__name='Regular'),
                    team=upline.profile.team,
                    subscription_type=row["Subscription Type"]
                )

            except Profile.DoesNotExist:
                continue

            profile.subscription_type = row["Subscription Type"]
            profile.can_download = True
            profile.download_expiration_date = datetime.date(2013, 10, 1)
            profile.save()