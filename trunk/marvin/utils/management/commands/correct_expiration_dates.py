from django.core.management.base import BaseCommand
from django.conf import settings

from marvin.store.shop.models import OrderItem

import datetime
from dateutil.relativedelta import *
import decimal

class Command(BaseCommand):
    help = "Correct expiration dates for valid subscriptions"

    first_of_month = datetime.date(datetime.date.today().year,
                                   datetime.date.today().month, 1)
    correct_expiration_date = first_of_month + relativedelta(months=1)
    
    log_file = None

    def get_file(self):
        if settings.LOCAL_DEV:
            if self.log_file:
                return self.log_file
            else:
                self.log_file = open(
                    "/home/axolote/Workspace/efinity/csv/corrected_subs/%d-%d-%d.log" % (self.first_of_month.year,
                                                                                         self.first_of_month.month,
                                                                                         self.first_of_month.day), 'w')
                self.log_file.write("order_id, orderitem_id\n")
                return self.log_file
        else:
            if self.log_file:
                return self.log_file
            else:
                self.log_file = open(
                    "/srv/www/myefinity.com/logs/corrected_subs/%d-%d-%d.log" % (self.first_of_month.year,
                                                                                 self.first_of_month.month,
                                                                                 self.first_of_month.day), 'w')
                self.log_file.write("order_id, orderitem_id\n")
                return self.log_file
    
    def write_to_file(self, line):
        log_file = self.get_file()
        log_file.write(line)

    def handle(self, *args, **kwargs):
        # Find every subscription that expires next month and find
        # out if it expires the first.
        ois = OrderItem.objects.filter(expire_date__gt=self.correct_expiration_date)
        for oi in ois:
            # Check that the order's payed
            if oi.order.balance == decimal.Decimal("0.00"):
                # If the order was payed
                oi.expire_date = self.correct_expiration_date
                oi.save()
                self.write_to_file("%d, %d\n" % (oi.order.pk, oi.pk))
