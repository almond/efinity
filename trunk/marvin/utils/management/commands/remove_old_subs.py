from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order


class Command(BaseCommand):
    """ Remove subs that aren't anymore """
    help = "Some subs should not be renewable... they won't be there"

    def handle(self, *args, **kwargs):
        """@todo: Docstring for handle.

        :*args: @todo
        :**kwargs: @todo
        :returns: @todo
        """
        orders = Order.objects.filter(orderitem__expire_date__isnull=False, 
                orderitem__product_id__in=(71, 43, 101, 104)).exclude(status="Cancelled").distinct()
        for order in orders:
            print u"Cancelling order %d from user %s and team %s" % (order.pk, order.user.username,
                    order.user.profile_object.team)

            for ois in order.orderitem_set.all():
                print u"%s" % ois.product.name

            order.status = "Cancelled"
            order.save()
