# Copyright (c) 2009 Vitja Makarov <vitja.makarov@gmail.com>
# Licensed under the terms of the MIT License (see LICENSE.txt)

from django.db.models import signals
from django.conf import settings
from marvin.trans.models import Language

def update_languages(sender, verbosity=2, **kwargs):
    active_pks = []
    for weight, (code, name) in enumerate(settings.LANGUAGES):
        try:
            lang = Language.all_objects.get(code=code)
        except Language.DoesNotExist:
            lang = Language()
            lang.code = code
            if verbosity >= 2:
                print 'Installing translation language %s' % code
        lang.name = name
        lang.weight = weight
        lang.active = True
        lang.save()
        active_pks.append(lang.pk)

    for lang in Language.all_objects.exclude(pk__in=active_pks):
        lang.active = False
        lang.save()

import marvin.trans.models
signals.post_syncdb.connect(update_languages, sender=marvin.trans.models)
