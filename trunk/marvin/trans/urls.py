# Copyright (c) 2009 Vitja Makarov <vitja.makarov@gmail.com>
# Licensed under the terms of the MIT License (see LICENSE.txt)

from django.conf.urls.defaults import *

from marvin.trans import views

urlpatterns = patterns(
    '',
    url(r'^$', views.browse_models, name='translate.browse_models'),
    url(r'^(?P<app_label>[_\w]+)/(?P<obj_name>[_\w]+)/$', views.browse_objects, name='translate.browse_objects'),
    url(r'^(?P<app_label>[_\w]+)/(?P<obj_name>[_\w]+)/(?P<pk>\d*)/$', views.translate_obj, name='translate.translate_obj')
)
