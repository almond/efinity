# Copyright (c) 2009 Vitja Makarov <vitja.makarov@gmail.com>
# Licensed under the terms of the MIT License (see LICENSE.txt)

from marvin.trans.utils import update_translations

def force_translation_update(func):
    """Update translations cache before calling view"""
    def decorator(*args, **kwargs):
        update_translations()
        return func(*args, **kwargs)
    return decorator
