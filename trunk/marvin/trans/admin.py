# Copyright (c) 2009 Vitja Makarov <vitja.makarov@gmail.com>
# Licensed under the terms of the MIT License (see LICENSE.txt)

## TODO: add translations to admin site
from django.contrib import admin
from marvin.trans.models import SimpleTranslation, Language

class TranslationAdmin(admin.ModelAdmin):
    list_display = ('lang', 'original', 'translation')

admin.site.register(SimpleTranslation, TranslationAdmin)
admin.site.register(Language)
