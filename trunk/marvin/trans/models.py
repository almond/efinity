# Copyright (c) 2009 Vitja Makarov <vitja.makarov@gmail.com>
# Licensed under the terms of the MIT License (see LICENSE.txt)

from django.db import models
from django.utils.translation import ugettext_lazy as _

import marvin.trans.utils as trans

class ActiveLanguageManager(models.Manager):
    def get_query_set(self):
        return super(ActiveLanguageManager, self).get_query_set().filter(active=True)

class Language(models.Model):
    code = models.CharField(_('Code'), max_length=5, unique=True)
    name = models.CharField(_('Name'), max_length=20)
    weight = models.IntegerField(_('Weight'), default=0, help_text=_("Weight, use it to order items"))
    active = models.BooleanField(_('Active'), default=True)

    all_objects = models.Manager()
    objects = ActiveLanguageManager()

    @property
    def native_name(self):
        return trans.translate(self.name, lang=self.code)

    def __unicode__(self):
        return self.localized_name

    class Meta:
        ordering = ('weight',)

    class Translate:
        fields = ('name',)

class SimpleTranslation(models.Model):
    lang = models.ForeignKey(Language, related_name="simple_translations")
    update_time = models.DateTimeField(auto_now=True)
    original = models.TextField()
    translation = models.TextField()

    class Meta:
        ordering = ('-update_time',)

    def __unicode__(self):
        return self.translation
