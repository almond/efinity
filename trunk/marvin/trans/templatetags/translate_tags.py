from django.template import Library
from django.conf import settings
from django.utils.encoding import force_unicode
from marvin.trans.utils import translate as _translate

register = Library()

@register.filter
def translate(string, lang=None):
    return _translate(unicode(string), lang)
