"""
This is pretentiosly called an API. It's really quite crude and disgusting but it'll get fixed
"""
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from django.http import HttpResponse, Http404
from django.utils import simplejson as json
from django.views.decorators.csrf import csrf_exempt
from marvin.store.shop.models import OrderPayment

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404

from suds.client import Client
from suds.wsse import *


@csrf_exempt
def login(request):
    """Does the user and password combination exist in the server"""
    ibo = request.POST.get("ibo", None)
    password = request.POST.get("password", None)
    data = dict()

    # Some testing changes that John wanted made
    if ibo == "cepguy":
        ibo = "6575"
    elif ibo == "guest":
        ibo = "718679"

    # Authenticate and return relevant user information. Otherwise explode and take humanity with you
    user = authenticate(username=ibo, password=password)
    if user:
        # Now get the subscriptions
        subs = user.subscriptions()
        slugs = list()
        for k, v in subs.items():
            slugs.append(v["product_slug"])
        data["status"] = True
        data["data"] = {"subscriptions": slugs, "firstName": user.first_name,
                        "lastName": user.last_name,
                        "team": user.profile.team}
        data["message"] = "User authenticated successfully"
    else:
        # Create an error message
        data["status"] = False
        data["data"] = list()
        data["message"] = "IBO number or password do not match"

    return HttpResponse(json.dumps(data), mimetype="application/json")


def get_subscriptions(request):
    return HttpResponse("YOur mom")


def get_subs_payment_dates(request):
    subscription_ids = [39, 103, 102, 42, 40, 43, 101]

    # After getting every subscription ID get the IBO from the request and return necessary data.
    ibo = request.GET.get("ibo")

    if ibo == "cepguy":
        ibo = "6575"
    elif ibo == "guest":
        ibo = "718679"

    try:
        user = User.objects.get(username=request.GET.get("ibo"))
    except User.DoesNotExist:
        raise Http404

    # Now return the requested information
    order_payments = OrderPayment.objects.filter(order__orderitem__product_id__in=subscription_ids,
                                                 order__user=user,
                                                 transaction_id__isnull=False).exclude(transaction_id="").values("time_stamp").order_by("time_stamp")
    dates = []
    for op in order_payments:
        date = "%d-%d" % (op["time_stamp"].year, op["time_stamp"].month)
        if date not in dates:
            dates.append(date)

    # Now Json it and return it
    data = {"ibo": user.username, "dates": dates}
    return HttpResponse(json.dumps(data), mimetype="application/json")


def get_current_cep(request):
    ibo = request.GET.get("ibo", None)
    try:
        user = User.objects.get(username=ibo)
    except User.DoesNotExist:
        return HttpResponse(json.dumps({"status": False, "data": False}))

    subs = user.subscriptions()

    cep = False
    for k, v in subs.items():
        if v["product"].pk in [39, 103, 102, 42, 40, 43, 101]:
            cep = True

    return HttpResponse(json.dumps({"status": True, "data": cep}))


def _get_amway_request_parameter(client, service_name, ibo):
    """ Response object ot call and stuff """
    request_parameter = client.factory.create(service_name)
    request_parameter.IBOID = ibo
    request_parameter.TransactionID = "3F2504ED-4F89-11D3-9A0C-0305E82C3321"
    return client, request_parameter


def _prepare_security_token(client, user_id, password):
    """ Retarded API security crap """
    security = Security()
    token = UsernameToken(user_id, password)
    token.setnonce()
    token.setcreated()
    security.tokens.append(token)
    client.set_options(wsse=security)

    return client


def _prepare_amway_client(url, user_id, password, ibo):
    """ Return a SOAP client for the amway api """
    client = Client(url)
    client = _prepare_security_token(client, settings.API_USER_ID, settings.API_PASSWORD)
    client, request_param = _get_amway_request_parameter(client, "ns2:GetIBOLOADetailsRequest", ibo)

    return client, request_param


def _does_user_exist(ibo):
    try:
        return User.objects.get(username=ibo)
    except User.DoesNotExist:
        return False


def _call_amway_api(ibo):
    """ Get the amway soap client and call it with the predefined ibo """
    client, request_parameter = _prepare_amway_client(settings.API_URL, settings.API_USER_ID, settings.API_PASSWORD, ibo)
    return client.service.GetIBOLOADetails(request_parameter)


def _check_for_existing_sponsor(ibo, response=None):
    """ Check if sponsor is in DB. Otherwise look for one """
    if not response:
        response = _call_amway_api(ibo)

    if _does_user_exist(response.UplineInformation.Sponsor.ID):
        return response.UplineInformation.Sponsor.ID
    else:
        return _check_for_existing_sponsor(response.UplineInformation.Sponsor.ID)


def _check_for_existing_platinum(ibo, response=None):
    if not response:
        response = _call_amway_api(ibo)

    if _does_user_exist(response.UplineInformation.Platinum.ID):
        return response.UplineInformation.Platinum.ID
    else:
        return _check_for_existing_platinum(response.UplineInformation.Platinum.ID)


def _check_for_existing_diamond(ibo, response=None):
    if not response:
        response = _call_amway_api(ibo)

    if _does_user_exist(response.UplineInformation.Diamond.ID):
        return response.UplineInformation.Diamond.ID
    else:
        return _check_for_existing_diamond(response.UplineInformation.Diamond.ID)


def amway_api(request):
    """ Replace the PHP api ugly thing and mantain access for diagnostics """
    ibo = request.GET.get("ibo", None)
    if not ibo:
        raise Http404

    #response = _check_for_existing_sponsor(ibo)

    response = _call_amway_api(ibo)

    # Make sure the IBO exists in the system
    #while True:
        #try:
            #tempo = User.objects.get(username=response.UplineInformation.Sponsor.ID)
            #break
        #except User.DoesNotExist:
            #request_parameter.IBOID = response.UplineInformation.Sponsor.ID
            #response = client.service.GetIBOLOADetails(request_parameter)

    # Dictionary to hold the data to be serialized
    data = {}

    # Make sure the IBO exists
    if not response.RequestedIBO:
        data["status"] = False
        data["info"] = 0
    else:
        data["status"] = True

        # Return available data
        try:
            platinum = response.UplineInformation.Platinum.ID
            platinum = _check_for_existing_platinum(platinum, response)
        except AttributeError:
            platinum = False

        try:
            diamond = response.UplineInformation.Diamond.ID
            diamond = _check_for_existing_diamond(diamond, response)
        except AttributeError:
            diamond = False

        try:
            sponsor = response.UplineInformation.Sponsor.ID
            sponsor = _check_for_existing_sponsor(sponsor, response)

        except AttributeError:
            sponsor = False

        data["info"] = {
            "platinum": platinum,
            "diamond": diamond,
            "sponsor": sponsor
        }

    return HttpResponse(json.dumps(data), mimetype="application/json")
