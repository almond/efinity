from django.conf.urls.defaults import patterns, url


urlpatterns = patterns("",
    url(r'^get-user-data/', "marvin.api.views.login", {}, 'api_login'),
    url(r'^get-subscriptions/', "marvin.api.views.get_subscriptions", {}, 'api_subscriptions'),
    url(r'^get-payment-dates/', "marvin.api.views.get_subs_payment_dates", {}, 'api_payment_dates'),
    url(r'^get-current-cep/', "marvin.api.views.get_current_cep", {}, 'api_current_cep'),
    url(r'^amway/', "marvin.api.views.amway_api", {}, 'api_amway_api'),
)
