class Router(object):
    
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'efinity':
            return 'efinity'

        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'efinity':
            return 'efinity'

        return None

    '''
    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'myapp' or obj2._meta.app_label == 'myapp':
            return True

        return None

    def allow_syncdb(self, db, model):
        if db == 'other':
            return model._meta.app_label == 'myapp'

        elif model._meta.app_label == 'myapp':
            return False

        return None
    '''
