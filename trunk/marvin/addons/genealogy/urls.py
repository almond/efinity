from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.addons.genealogy.views',

	(r'^$', 'index', {}, 'gen_home'),
    (r'^import/$', 'import_upline'),
	(r'^graphical/$', 'graphical', {}, 'gen_graphical'),
	(r'^ajax/$', 'ajax', {}, 'gen_ajax'),

)

