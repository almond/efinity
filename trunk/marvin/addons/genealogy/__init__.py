#
# Here we connect to the collect_urls signal to add ourselves.
# Luckily this only happens if this addon is enabled.
#

'''
from utils.signals_ahoy.signals import collect_urls
from django.conf.urls.defaults import patterns, include

def add_urls(sender, urlpatterns):
    urlpatterns += patterns('',
        (r'^genealogy/', include('marvin.addons.genealogy.urls'))
    )

    return urlpatterns

collect_urls.connect(add_urls, dispatch_uid="genealogy_urls")
'''
