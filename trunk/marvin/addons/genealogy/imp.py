# this file helps import users from Amway
from django.utils import simplejson
from django.contrib.auth.models import User
from marvin.utils.app import *
from marvin.utils.request import fetch

from marvin.core.accounts.models import Profile
from marvin.addons.genealogy.models import Position, Tree

'''
result = fetch('http://efinity_files.almondev.org/marvin/addons/genealogy/test.php?ibo=' + str( request.user.profile.ibo_number))

    result = result['data']
    result = simplejson.loads( result )

    LOSResult = result['LOSVerificationResponse']['LOSVerificationResult']

    is_valid_ibo = LOSResult['LOSVerificationFlag']['value']
    if is_valid_ibo == 'false':
        return HttpResponse('Your account\'s registered IBO Number is invalid.')
    
    UplinePlatinum = { # this is what is important
        'ibo_number': LOSResult['UplinePlatinumIMCNumber']['value'],
        'name': LOSResult['UplinePlatinumNameText']['value'],
    }

    UplineDiamond = {
        'ibo_number': LOSResult['UplineDiamondIMCNumber']['value'],
        'name': LOSResult['UplineDiamondNameText']['value'],
    }

    UplineSponsor = {
        'ibo_number': LOSResult['UplineSponsorIMCNumber']['value'],
        'name': LOSResult['UplineSponsorNameText']['value'],
    }
'''

def import_upline(user):
    TREE = Tree.objects.get(pk=1) # hard coded, for now

    result = fetch('http://amway.crossmarketsoftware.com/?ibo=' + str(user.profile.ibo_number))

    result = result['data']
    result = simplejson.loads( result )

    LOSResult = result['LOSVerificationResponse']['LOSVerificationResult']

    is_valid_ibo = LOSResult['LOSVerificationFlag']['value']
    if is_valid_ibo == 'false':
        return HttpResponse('Your account\'s registered IBO Number is invalid.')

    if not LOSResult.get('UplinePlatinumIMCNumber'):
        return HttpResponse('No sponsor')

    UplinePlatinum = { # this is what is important
        'ibo_number': LOSResult['UplinePlatinumIMCNumber']['value'],
        'name': LOSResult['UplinePlatinumNameText']['value'],
    }

    # find this guy's upline IBO in our system, if he is there, which we hope
    try:
        upline_profile = Profile.objects.get(ibo_number = UplinePlatinum['ibo_number'])
    except Profile.DoesNotExist:
        upline_profile = None # SHIT WHAT TO DO?
        return HttpResponse('Upline no exist, so for now, break until we handle this')

    # ok, we have the upline, now position this guy in the system
    try:
        Position(
            user = user,
            upline = upline_profile.user,
            tree = TREE
        ).save()
    except:
        pass

    return HttpResponse('Imported your upline')
