from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils import simplejson
from marvin.addons.genealogy.models import Position
from marvin.utils.app import *
from marvin.utils.request import fetch



@login_required
def import_upline(request):
    from marvin.addons.genealogy.imp import import_upline
    return import_upline(request.user)


@login_required
def index(request):
    return render(request, 'genealogy/index.html', {
        'upline': Position.objects.filter(user=request.user),
        'downline': Position.objects.filter(upline=request.user),#.downline.all(),#get_downline( request.user ),
    })


@login_required
def graphical(request):
    return render(request, 'genealogy/graphical.html')


@login_required
def ajax(request):

    user_id = request.POST.get('user_id', False)
    user = get_object_or_404(User, pk=user_id)

    downline = []
    for position in Position.objects.filter(upline=user):
        downline.append(position.user.getJSON())

    upline_id = None
    if request.user != user:
        if user.upline:
            upline_id = user.upline.pk

    phone = ''
    if user.phone:
        phone = user.phone.number

    out = {
        'id': user.pk,
        'name': user.get_full_name(),
        'upline_id': upline_id,
        'more': '%s %s' % (settings.AID_TITLE, user.aid),
        'downline': downline,
        'user_info': '''

            <strong>%(name)s</strong>
            <br>
            %(aid_title)s: %(aid)s
            <br>
            Email: <a href="mailto:%(email)s">%(email)s</a>
            <br>
            Phone: %(phone)s

        ''' % {
            'name': user.get_full_name(),
            'aid_title': settings.AID_TITLE,
            'aid': user.aid,
            'email': user.email,
            'phone': phone,
            }
    }

    return HttpResponse( simplejson.dumps( out ) )
