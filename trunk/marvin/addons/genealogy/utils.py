# this file helps import users from Amway
from django.utils import simplejson
from django.contrib.auth.models import User
from marvin.utils.request import fetch

from marvin.addons.genealogy.models import Position, Tree

from django.conf import settings


def PositionWithDefaultTree(user, upline):
    tree = Tree.objects.get(pk=1)
    return Position(
        tree=tree,
        user=user,
        upline=upline
    )


def get_wsdl_result(ibo):
    """
    Pretty much like get_upline_ibo but returning the whole dataset. 
    """
    url = 'http://amway.crossmarketsoftware.com/'
    full_url = '%s?ibo=%i' % ( url, ibo )
    result = fetch(full_url)

    py_result = simplejson.loads(result["data"])

    # Verify that you got a nice response
    if py_result["LOSVerificationResponse"]["LOSVerificationResult"].get("LOSVerificationFlag", None):
        return py_result["LOSVerificationResponse"]["LOSVerificationResult"]
    else:
        return {}


def get_upline_ibo(ibo):
    full_url = "http://%s/api/amway/?ibo=%d" % (settings.WEBSITE, int(ibo))
    result = fetch(full_url)

    try:
        result = simplejson.loads(result['data'])
    except simplejson.JSONDecodeError:
        return False

    if not result["status"]:
        return False

    if result["info"]["platinum"]:
        return result["info"]["platinum"]
    elif result["info"]["diamond"]:
        return result["info"]["diamond"]
    elif result["info"]["sponsor"]:
        return result["info"]["sponsor"]
    else:
        return 0