import sys
from marvin import unicodecsv
from datetime import date
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User, Group
from marvin.addons.genealogy.models import AccessLevel
import datetime
import re
from django.conf import settings
from marvin.core.accounts.models import Profile 
class Command(BaseCommand):
    help = 'Generates CSV with names, pinlevels, and shipping addresses of all the Emeralds'

    def handle(self, *args, **options):
        emerald_id=Group.objects.get(name='Emerald').id
        profiles=Profile.objects.filter(pinlevel__group__id__gte=emerald_id)
        path=settings.DIRNAME

        now_time=datetime.datetime.now()
        filename='emerald_and_above_%d_%d_%d.csv' % (now_time.day, now_time.month, now_time.year)
        f=open(path + '/' + filename, 'wb')
        writer = unicodecsv.writer(f, encoding='utf-8', quoting=unicodecsv.QUOTE_ALL)
        writer.writerow(['IBO', 'First Name', 'Last Name', 'Pin Level', 'Pin Level(Verbose)', 'Shipping Address'])
        for p in profiles:
            u=p.user
            address=u.shipping_address
            if address:
                add=''
                if address.street1:
                    add="%s"%(address.street1)
                if address.street2:
                    add="%s\n%s"%(add, address.street2)
                if address.city:
                    add="%s\nCity: %s"%(add, address.city)
                if address.state:
                    add="%s\nState: %s"%(add, address.state)
                if address.postal_code:
                    add="%s\nZip: %s"%(add, address.postal_code)
                if address.country:
                    add="%s\nCountry: %s"%(add, address.country)
                address=add
            else:
                address='Not Available'
            writer.writerow([u.username, u.first_name, u.last_name, p.pinlevel.group.id, p.pinlevel, address])
        f.close()
