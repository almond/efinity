import sys
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from marvin.core.accounts.models import Profile
from marvin.addons.genealogy.models import Position, Tree

from marvin.addons.genealogy.utils import get_upline_ibo
from marvin.addons.genealogy.models import TempUserConfirm

#raise CommandError('Poll "%s" does not exist' % poll_id)



class Command(BaseCommand):
    help = 'Runs upline import for all users in the system'

    def handle(self, *args, **options):
        filename = '%s%s' % (settings.DIRNAME, '/amway_import.txt')
        f = open(filename, 'w')
        f.write('Beginning the amway import. \n')

        TREE = Tree.objects.get(pk=1)

        
        # for user in User.objects.all().exclude(username='root').exclude(username='0').exclude(username='1'):
        for tuc in TempUserConfirm.objects.filter(processed=False):
            user = tuc.user
            
            # print 'Running for IBO', user.aid

            # first see if we have a position for this user
            try:
                pos = Position.objects.get(user=user, tree=TREE)
            except Position.DoesNotExist:
                pos = Position()

            # Set position, no matter what
            if not user.aid:
                print 'User ID %s has no IBO number' % user.id
                f.write('- User ID %s has no IBO number\n' % user.id)
                tuc.message = 'User ID %s has no IBO number' % user.id
                tuc.processed = True
                tuc.failed = True
                tuc.fail_code = 1
                tuc.save()
                continue
            
            try:
                upline_ibo = get_upline_ibo(user.aid)
            except :
                tuc.message = 'External error on %s. %s' % (user.aid, sys.exc_info()[0])
                tuc.processed = True
                tuc.failed = True
                tuc.fail_code = 2
                tuc.save()
            
            if upline_ibo is False:
                print '- No IBO found for', user.aid
                f.write('-- No IBO found for %s. Deleting \n' % user.aid)
                tuc.message = 'No IBO found for %s. Deleting' % user.aid
                tuc.processed = True
                tuc.failed = True
                tuc.fail_code = 3
                tuc.save()
                # here we delete the user
                user.profile.delete() # delete profile data
                user.contacts.all().delete() # delete all contact data
                user.delete() # delete the primary user
            elif upline_ibo is None:
                print 'No Upline found for', user.profile.aid
                f.write('--- No Upline found in Amway for valid IBO %s.\n' % user.aid)
                tuc.message = 'No Upline found for %s' % user.profile.aid
                tuc.processed = True
                tuc.failed = True
                tuc.fail_code = 4
                tuc.save()
            else:#elif upline is not None:
                try:
                    upline = User.get_by_aid(upline_ibo)
                    if upline == None:
                        raise User.DoesNotExist
                except:
                    f.write('---- Upline IBO %s not found in efinity db for IBO %s.\n' % (upline_ibo, user.aid))
                    tuc.message = 'Upline IBO %s not found in efinity db for IBO %s.' % (upline_ibo, user.aid)
                    tuc.processed = True
                    tuc.failed = True
                    tuc.fail_code = 5
                    tuc.save()
                    continue
                
                print 'Setting IBO %s upline to IBO %s.' % (user.aid, upline_ibo)
                # f.write('Setting IBO %s upline to IBO %s.\n' % (user.aid, upline_ibo))
                
                tuc.message = 'Setting IBO %s upline to IBO %s.' % (user.aid, upline_ibo)
                tuc.processed = True
                tuc.save()
                
                pos.user = user
                pos.upline = upline
                pos.tree = TREE
                pos.save()
        
        f.close()