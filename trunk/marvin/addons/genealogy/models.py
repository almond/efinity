from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext_lazy as _

from marvin.addons.replicate.models import ReplicatedSite
from marvin.utils.consts import COLOR_CODES
# from marvin.addons.compensation.models import CompensationLevel

TYPES = (
    ('network', _('Network, unlimited width')),
    ('binary', _('Binary')),
    ('matrix', _('Matrix')),
)


FAIL_CODES = (
    (1,'User ID has no IBO number'),
    (2,'External error '),
    (3,'No valid matching IBO number found in Amway'),
    (4,'No Upline found'),
    (5,'Upline IBO not found in efinity db'),
)
class TempUserConfirm(models.Model):
    """
    This table is for the user import

    Must run the following to populate:

    from django.contrib.auth.models import User
    from marvin.addons.genealogy.models import TempUserConfirm
    us = User.objects.all().exclude(username='root').exclude(username='0').exclude(username='1').exclude(is_staff=True)
    for u in us:
      tuc = TempUserConfirm()
      tuc.user = u
      tuc.save()
    """
    user = models.ForeignKey(User)
    processed = models.BooleanField(blank=True)
    failed = models.BooleanField(blank=True)
    fail_code = models.IntegerField(blank=True, null=True, choices=FAIL_CODES)
    message = models.CharField(max_length=300, blank=True, null=True)


class Tree(models.Model):
    """
    Handles the setup of different type of trees.

    """
    name = models.CharField(max_length=50, help_text=_('Name to call the tree'))
    type = models.CharField(max_length=10, choices=TYPES, help_text=_('Type of tree this is'))
    replicated_site = models.ForeignKey(ReplicatedSite, blank=True, null=True,
        help_text=_('To enable replicated websites directly attached to a  '
        'program, you can select what replicated website you want to use   '
        'here. You can configure the site separately allowing how you want '
        'new members to come in.'))
    auto_fix_sponsors = models.BooleanField(default=True)
    max_width = models.IntegerField(default=0, help_text=_('Zero is unlimited.'))
    max_depth = models.IntegerField(default=0, help_text=_('Zero is unlimited.'))

    class Meta:
        verbose_name = _('Genealogy Program')
        verbose_name_plural = _('Genealogy Programs')

    def __unicode__(self):
        return self.name


class AccessLevel(models.Model):
    """
    Genealogy levels are user levels within
    the system, extended from the Group

    """
    def __unicode__(self):
        return self.group.name

    group = models.OneToOneField(Group, related_name = 'gen_level',
        help_text=_('Here you can relate the genealogy level to a group '
        'to be able to setup permissions easily.'))
    tree = models.ForeignKey(Tree, related_name='levels')
    compensation_level = models.ForeignKey('compensation.CompensationLevel', related_name='gen_level', blank=True, null=True)
    color = models.CharField(max_length=20, choices=COLOR_CODES, blank=True,
        null=True, default='', help_text=_('Color code to use for this '
        'genealogy with generated icons and reports. Must be unique.'))
    icon = models.ImageField(upload_to='genealogy_icons', blank=True, null=True)
    note = models.TextField(blank=True, null=True,
        help_text=_('Optional. This note is used for company members to '
        'better understand how this genealogy level should work.'))
    is_active = models.BooleanField(default=True,
        help_text=_('If this genealogy level is active then users will be  '
        'placed into the level as directed by the genealogy plan. If not '
        'then this level is used for internal purposes most likely and   '
        'will not be displayed most of the time.'))

    @property
    def name(self):
        return self.group.name


class Position(models.Model):
    tree = models.ForeignKey(Tree, related_name='pins')
    user = models.ForeignKey(User, related_name='position')
    upline = models.ForeignKey(User, related_name='downline')
    auto_update = models.BooleanField(default=True)
    auto_update_team = models.BooleanField(default=True)


    class Meta:
        verbose_name = _('Genealogy Program Position')
        verbose_name_plural = _('Genealogy Program Positions')
        unique_together = (
            ('tree', 'user'),
        )

@property
def get_upline(user):
    if not hasattr(user, 'upline_cache'):
        try:
            upline = Position.objects.get(user=user).upline
        except:
            upline = None

        setattr(user, 'upline_cache', upline)

    return getattr(user, 'upline_cache')

User.add_to_class('upline', get_upline)
