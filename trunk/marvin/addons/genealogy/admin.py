from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import GroupAdmin
from django.utils.translation import ugettext_lazy as _

from models import *


class AccessLevelInline(admin.StackedInline):
    model = AccessLevel


class GroupAdmin(GroupAdmin):
    fieldsets = (
        (None, {'fields': ('name', )}),
        (_('Permissions'), {'fields': ('permissions',), 'classes': ('collapse',)}),
    )
    inlines = [AccessLevelInline, ]


class PositionAdmin(admin.ModelAdmin):
    list_display = ('tree', 'user', 'upline', 'auto_update', 'auto_update_team')


class TempUserConfirmAdmin(admin.ModelAdmin):
    list_display = ('user', 'processed', 'failed', 'fail_code', 'message')
    list_filter = ('processed', 'failed')

admin.site.register(TempUserConfirm, TempUserConfirmAdmin)

admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)

admin.site.register(Tree)
admin.site.register(Position, PositionAdmin)
