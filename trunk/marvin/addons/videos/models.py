from django.db import models
from django.utils.translation import ugettext_lazy as _

from marvin.utils.thumbnail.field import ImageWithThumbnailField

class ImageBase(models.Model):
    """
    A picture that can be attached to other models.
    Thumbnails are automatically created.
    """
    class Meta:
        abstract = True
        ordering = ['sort']
        verbose_name = _("Image")
        verbose_name_plural = _("Images")

    picture = ImageWithThumbnailField(verbose_name=_('Picture'),
        upload_to="__DYNAMIC__",
        name_field="_filename",
        max_length=200) #Media root is automatically prepended

    caption = models.CharField(_("Optional caption"), max_length=100,
        null=True, blank=True)
    
    sort = models.IntegerField(_("Sort Order"), default=0)

    def _filename(self):
        return 'default-%s' % (self.id)

    def __unicode__(self):
        return self.picture


class MediaBase(models.Model):
    class Meta:
        abstract = True
        ordering = ['sort']
        verbose_name = _("Media")
        verbose_name_plural = _("Medias")

    def __unicode__(self):
        return self.name

    TYPES = (
        (1, _('Video')),
        (2, _('Audio')),
        (3, _('Gallery Photo')),
    )

    name = models.CharField(max_length=50)
    type = models.IntegerField(choices=TYPES)
    description = models.CharField(_("Optional caption"), max_length=100, null=True, blank=True)
    original_file = models.FileField(upload_to='media_uploads')
    converted_file = models.FileField(editable=False, blank=True, null=True, upload_to='media_uploads_converted')
    thumbnail = models.ImageField(upload_to='media_thumbnails', blank=True, null=True, help_text=_('You can optionally select a thumbnal to attack to this video, if none is uploaded then one will be generated (if possible).'))

    is_published = models.BooleanField(_('Published'), help_text=_('If published the media will be transferred to the Content Delivery Network and made available to customers.'))

    sort = models.IntegerField(_("Sort Order"), default=0)
    viewed_count = models.IntegerField(_('View Count'), default=0)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
