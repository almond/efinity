from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Media(models.Manager):
    '''
    Storage for media data.
    '''
    def __unicode__(self):
        return self.name

    media_name = models.CharField(
            max_length = 255,
            help_text = _('The name of the file being uploaded.'),
    )

    description = models.TextField(
            blank = True,
            null = True,
            help_text = _('Description of the file being uploaded.'),
    )

    # This is where the files will be uploaded:
    # MEDIA_ROOT = os.path.join(DIRNAME, 'media')
    # MEDIA_ROOT/upload_temp
    file_upload_temp = models.FileField(
            upload_to = 'upload_temp'
    )

    file_converted = models.CharField(
            max_length = 250,
            blank = True,
            null = True,
    )

    filename = models.CharField(
            max_length = 250,
    )

    FILE_TYPES = (
            ('a', _('MP3')),
            ('v', _('MP4')),
    )

    file_type = models.CharField(
            max_length = 1,
            choices = FILE_TYPES,
            help_text = _('This will be the type of file that is being uploaded.'),
    )

    is_published = models.BooleanField(
            _('published'),
            help_text = _('The current status of the media, published or unpublished.'),
    )
