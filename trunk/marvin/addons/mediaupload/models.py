from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from django.conf import settings
import os
class Media(models.Model):
    '''
    Storage for media data.
    '''
    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name

    name = models.CharField(
            _('Name'),
            max_length = 255,
            help_text = _('The name of the file being uploaded.'),
    )

    description = models.TextField(
            blank = True,
            null = True,
            help_text = _('Description of the file being uploaded.'),
    )

    # This is where the files will be uploaded:
    # MEDIA_ROOT = os.path.join(DIRNAME, 'media')
    # MEDIA_ROOT/upload_temp
    file_upload_temp = models.FilePathField(
            _('Selected File'),
            path = os.path.join(settings.MEDIA_ROOT, 'upload_temp'),
            max_length=1024
    )

    thumbnail = models.ImageField(
            _('Thumbnail'),
            upload_to = 'upload_thumbnail'
    )

    file_converted = models.CharField(
            max_length = 250,
            blank = True,
            null = True,
            editable = False,
    )

    FILE_TYPES = (
            ('a', _('Audio')),
            ('v', _('Video')),
    )

    file_type = models.CharField(
            max_length = 1,
            choices = FILE_TYPES,
            default = 'v',
            help_text = _('This will be the type of file that is being uploaded.'),
    )

    is_published = models.BooleanField(
            _('Publish'),
            help_text = _('The current status of the media, published or unpublished.'),
    )

    def url(self):
        path = self._meta.get_field('file_upload_temp').path
        return "%supload_temp%s" % (settings.MEDIA_URL, self.file_upload_temp.replace(path, '', 1))

class Promo(Media):
    '''
    Promo Media
    '''
    class Meta:
        verbose_name = _('Promotional Video')
        verbose_name_plural = _('Promotional Videos')

    is_featured = models.BooleanField(
            _('Feature'),
            help_text = _('These will be the featured videos that will default for the user.'),
    )

class Training(Media):
    '''
    Training Media
    '''
    class Meta:
        verbose_name = _('Training Video')
        verbose_name_plural = _('Training Videos')
    pass

class BBK(Media):
    '''
    BBK Media
    '''
    class Meta:
        verbose_name = _('Business Builder Kit Video')
        verbose_name_plural = _('Business Builder Kit Videos')
    pass

class ETV(Media):
    start_datetime = models.DateTimeField(blank=True, null=True, verbose_name="Date and Time Event Will Start", help_text="Date and Time the video will start")
    end_datetime = models.DateTimeField(blank=True, null=True, verbose_name="Date and Time Event Will End", help_text="Date and Time the video will end")
    is_featured = models.BooleanField(_('Featured'), help_text="Featured videos show up in the header of the eTV page.")
    '''
    eTV Media
    '''
    class Meta:
        verbose_name = _('eTV Video')
        verbose_name_plural = _('eTV Videos')
    pass

class PromoUser(models.Model):

    user = models.ForeignKey(User)
    media = models.ForeignKey(Promo)
    
    order = models.IntegerField ()
