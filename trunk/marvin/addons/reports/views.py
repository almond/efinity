# -*- coding: utf-8 -*-
#from django.contrib.auth.models import User
#from django.core.mail import send_mail
from decimal import Decimal
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.db.models import Min, Max, Count, Q, Sum
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response
from django.template import defaultfilters

from django.utils.datastructures import SortedDict

from marvin.addons.compensation.models import Commission
from marvin.addons.genealogy.models import Position
from marvin.addons.planning.models import Ticket, Convention
from marvin.addons.reports.models import Report, ReportFilter
from marvin.store.products.models import Product
from marvin.store.products.modules.subscription.models import\
    SubscriptionProduct
from marvin.store.shop.models import Order, OrderItem, OrderPayment
from marvin.utils.app import *
from marvin.utils.decorators import staff_only
import csv
import datetime
import time
from dateutil.relativedelta import *


def get_pin_level(ibo):
    """
    Depending on IBO return either one of 2 pinlevels
    Diamond
    Emerald
    """
    if ibo.pinlevel().pk >= 6 and ibo.pinlevel().pk <= 8:
        return "Emerald"
    elif ibo.pinlevel().pk > 8:
        return "Diamond"
    else:
        return "Diamond"


@staff_only
def list(request):
    return render(request, 'reports/list.html', {'title': 'Browse Report'})


@staff_only
def checkin(request):
    eid = request.GET.get('event', None)
    if eid:
        return HttpResponseRedirect('/admin/planning/ticket/checkin/%s' % (eid))

    events = Convention.objects.filter()
    return render(request, 'support/checkin.html',
        {'title': 'Checkin', 'events': events})


@staff_only
def all_reports(request):
    return render(request, 'reports/all_reports.html',
        {'title': 'Select Report'})


# Blank space to leave code at
# invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

@staff_only
def tax(request):
    invalid_date = False
    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)
    if sd and td:
        try:
            sd = datetime.datetime.strptime("%s 00:00:01" % sd, "%Y-%m-%d %H:%M:%S")
            td = datetime.datetime.strptime("%s 23:59:59" % td, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

        if not invalid_date:
            convention = ContentType.objects.get(app_label='planning', model='convention')
            # TODO: There has to be a nicer way for this too
            details = {'subscription': 0, 'others': 0, 'subscription_total': 0,
                       'others_total': 0, 'subscription_tax': 0, 'others_tax': 0}

            # Only get orders that have been payed forn
            query = Q(payments__reason_code="I00001") | Q(payments__reason_code="Cash Payment")

            # I'll try to get the orders better. They should be a lot fewer than the rest
            orders = Order.objects.filter(payments__time_stamp__gte=sd, payments__time_stamp__lte=td).filter(
                query).distinct()

            all_items = OrderItem.objects.filter(order__in=orders).exclude(content_type=convention)

            for o in all_items:
                if o.product:
                    if o.product.is_subscription:
                        details['subscription_total'] = details['subscription_total'] + o.line_item_price
                        details['subscription_tax'] = details['subscription_tax'] + o.tax
                        if o.order.ship_state == 'FL' or o.order.ship_state == 'Florida':
                            details['subscription'] += o.line_item_price
                    else:
                        details['others_total'] = details['others_total'] + o.line_item_price
                        details['others_tax'] = details['others_tax'] + o.tax
                        #if o.order.ship_state=='FL' or  o.order.ship_state=='Florida' or  o.order.bill_state=='FL' or  o.order.bill_state=='Florida' :
                        if o.order.ship_state == 'FL' or o.order.ship_state == 'Florida':
                            details['others'] += o.line_item_price

            if request.GET.get('export', None) == 'csv':
                response = HttpResponse(mimetype='text/csv')
                response['Content-Disposition'] = 'attachment; filename=tax-report-%s-to-%s.csv' % (
                    defaultfilters.slugify(sd), defaultfilters.slugify(td))
                writer = csv.writer(response)
                writer.writerow(['Start Date', sd])
                writer.writerow(['End Date', td])
                writer.writerow(['', 'Total Sales', 'Taxable Sales', 'Tax Collected'])
                writer.writerow(['Subscription', details['subscription_total'], details['subscription'],
                                 details['subscription_tax']])
                writer.writerow(['Sales', details['others_total'], details['others'], details['others_tax']])
                return response

            return render(request, 'reports/tax.html',
                {'title': 'Tax Report', 'start_date': sd, 'to_date': td, 'sd': sd, 'td': td, 'details': details})
    else:
        return render(request, 'reports/tax.html', {'title': 'Tax Report', })


@staff_only
def all_florida_sales(request):
    invalid_date = False
    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)

    if sd and td:
        try:
            start_date = time.strptime("%s 00:00:00" % (sd), "%Y-%m-%d %H:%M:%S")
            to_date = time.strptime("%s 23:59:59" % (td), "%Y-%m-%d %H:%M:%S")
        except:
            invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

        if not invalid_date:
            if to_date < start_date:
                invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

        if not invalid_date:
            convention = ContentType.objects.get(app_label='planning', model='convention')
            convention = convention.id
            sale_details = {'subs': [], 'tools': []}
            convention = ContentType.objects.get(app_label='planning', model='convention')
            convention = convention.id

            order_ids = Order.objects.values_list('id', flat=True).filter(
                status__in=['Shipped', 'Billed', 'In Process', 'Temp'], time_stamp__gte="%s 00:00:00" % (sd),
                time_stamp__lte="%s 23:59:59" % (td))
            #order_ids=Order.objects.values_list('id', flat=True).filter(status='Shipped', time_stamp__gte="%s 00:00:00"%(sd), time_stamp__lte="%s 23:59:59"%(td))
            all_items = OrderItem.objects.filter(~Q(content_type=convention), order__id__in=order_ids)
            for o in all_items:
                if o.product and 'digital' not in o.product.slug:
                    if o.product.is_subscription:
                        #details['subscription_total'] = details['subscription_total'] + o.line_item_price
                        #details['subscription_tax'] = details['subscription_tax'] + o.tax
                        #if o.order.bill_state=='FL' or  o.order.bill_state=='Florida' or  o.order.ship_state=='FL' or  o.order.ship_state=='Florida':
                        if o.order.ship_state == 'FL' or  o.order.ship_state == 'Florida':
                            sale_details['subs'].append(o)
                    else:
                        #details['others_total'] = details['others_total'] + o.line_item_price
                        #details['others_tax'] = details['others_tax'] + o.tax
                        #if o.order.ship_state=='FL' or  o.order.ship_state=='Florida' or  o.order.bill_state=='FL' or  o.order.bill_state=='Florida' :
                        if o.order.ship_state == 'FL' or  o.order.ship_state == 'Florida':
                            sale_details['tools'].append(o)

            return render(request, 'reports/all-florida-sales.html',
                {'title': 'All Florida Sales Report', 'start_date': start_date, 'to_date': to_date, 'sd': sd, 'td': td,
                 'sale_details': sale_details})
        else:
            return render(request, 'reports/all-florida-sales.html',
                {'title': 'All Florida Sales Report', 'msg': [{'tags': 'error', 'text': invalid_date}], 'sd': sd,
                 'td': td})
    else:
        return render(request, 'reports/all-florida-sales.html', {'title': 'All Florida Sales Report', })


@staff_only
def all_sales(request):
    invalid_date = False
    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)
    details = {}
    if sd and td:
        try:
            sd = datetime.datetime.strptime("%s 00:00:01" % sd, "%Y-%m-%d %H:%M:%S")
            td = datetime.datetime.strptime("%s 23:59:59" % td, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'
            return render(request, 'reports/all-sales.html', {'invalid_date': invalid_date})

        if not invalid_date:
            query = Q(reason_code__icontains="I00001") | Q(reason_code__icontains="Cash Payment")
            convention = ContentType.objects.get(app_label='planning', model='convention')
            items = OrderPayment.objects.filter(time_stamp__gte=sd, time_stamp__lte=td).filter(query)

            details['tools'] = items.exclude(order__orderitem__content_type=convention).aggregate(total=Sum("amount"))[
                               "total"]
            details['total_transactions_tools'] = len(items.exclude(order__orderitem__content_type=convention))
            items.filter(order__orderitem__content_type=convention).aggregate(total=Sum('order__payments__amount'))[
            "total"]
            details['total_transactions_events'] = len(items.filter(order__orderitem__content_type=convention))

            if request.GET.get('export', None) == 'csv':
                response = HttpResponse(mimetype='text/csv')
                response['Content-Disposition'] = 'attachment; filename=sales-report-%s-to-%s.csv' % (
                    defaultfilters.slugify(sd), defaultfilters.slugify(td))
                writer = csv.writer(response)
                writer.writerow(['Start Date', sd])
                writer.writerow(['End Date', td])
                writer.writerow(['', 'Total Sales'])
                writer.writerow(['Conventions', details['conventions']])
                writer.writerow(['Tools', details['tools']])
                return response

            return render(request, 'reports/all-sales.html', {'title': 'All Sales Report',
                                                              'start_date': sd, 'to_date': td, 'sd': sd, 'td': td,
                                                              'details': details})
    else:
        return render(request, 'reports/all-sales.html', {})


def is_in_upline(who, whose_upline):
    if who == whose_upline:
        return True, None

    pos = Position.objects.filter(user=whose_upline)
    while len(pos) == 1:
        if pos[0].upline == who or pos[0].upline == pos[0].user:
            if (who == whose_upline) or (pos[0].upline == pos[0].user):
                up = None
            else:
                up = Position.objects.filter(user=whose_upline)[0].upline
            return True, up
        else:
            pos = Position.objects.filter(user=pos[0].upline)

    return False, None


def get_downline(user, dl=[]):
    downline = Position.objects.filter(upline=user)
    for d in downline:
        if d.user_id not in dl:
            try:
                u = User.objects.get(pk=d.user_id)
                dl.append(d.user_id)
                dl = get_downline(u, dl)
            except:
                pass
    return dl


def get_frontline(user):
    front_line = Position.objects.values_list('user_id', flat=True).filter(upline=user)
    front_line = [id for id in front_line]
    return front_line


@login_required
def event_attendance(request):
    event = request.GET.get('event', None)
    if event:
        try:
            event = int(event)
        except:
            event = None

    ibo = None
    up = None
    attendance = {}
    total_count = 0
    if event:
        ibo = request.GET.get('ibo', request.user.username)
        try:
            ibo = User.objects.get(username=ibo)
        except User.DoesNotExist:
            raise Http404

        proceed, up = is_in_upline(request.user, ibo)
        if not proceed: #if the ibo doesnot has request.user in his upline then request.user cannot see his details
            return HttpResponse('IBO should be in your downline, if you want to see his/her report.')

        attendance = {}
        self_tickets = Ticket.objects.filter(user=ibo, event=event)
        total_count = self_tickets.count()
        attendance['SELF'] = {'ibo': ibo.username, 'name': ibo.get_full_name(), 'count': self_tickets.count()}
        front_line = get_frontline(ibo)
        for f in front_line:
            f = User.objects.get(pk=f)
            all_down = get_downline(f, []) + [f.id, ]
            all_tickets = Ticket.objects.filter(user__in=all_down, event=event)
            count = all_tickets.count()
            if count > 0:
                total_count = total_count + count
                ibo_link = '<a href="?event=' + str(event) + '&ibo=' + (f.username) + '">' + (f.username) + '</a>'
                attendance[f.id] = {'ibo': f.username, 'name': f.get_full_name(), 'count': count, 'ibo_link': ibo_link}

    events = Convention.objects.filter(~Q(name__startswith='Open')).order_by('name')
    return render(request, 'reports/event_attendance.html',
        {'up': up, 'title': 'Event Attendance', 'events': events, 'eid': event, 'attendance': attendance, 'ibo': ibo,
         'total_count': total_count})


@login_required
def group(request):
    event = request.GET.get('event', None)
    total_event_earnings = 0.00
    total_compensation = Decimal("0.00")
    event_ticket_total = 0
    pin_level_pools = dict()
    ibo = User.objects.get(username=request.GET.get('ibo', request.user.username))
    front_line = get_frontline(ibo)
    down_line = get_downline(ibo)
    all_users = [ibo.id, ] + front_line
    really_all_users = all_users + down_line
    logged_user = dict()
    expenses = None
    generation = request.GET.get("gen", 0)
    convention_type = ContentType.objects.get(app_label='planning', model='convention')
    total_tickets_sold_by_user = 0

    multiplier_constant = 0.33557047

    if event:
        event_obj = Convention.objects.get(pk=event)
        # Calculate total compensation
        user_compensation = Commission.objects.filter(order_item__product_id=event_obj.pk,
            order_item__content_type=convention_type,
            user=request.user).distinct()

        # Just as an experiment go through each and add up the ones that have no order balance
        for t in user_compensation:
            if t.order_item.order.balance == 0:
                total_compensation += t.amount
            else:
                print "Unpayed order pk: %d" % t.order_item.order.pk

        print "Result of experiment: %f" % total_compensation

        try:
            ibo = User.objects.get(username=ibo)
        except User.DoesNotExist:
            raise Http404

        min_level = Group.objects.get(name='Emerald')
        if request.user.pinlevel().id < min_level.id:
            return HttpResponse('This area is only for Emerald and above members.')

        proceed, up = is_in_upline(request.user, ibo)
        if not proceed: #if the ibo doesnot has request.user in his upline then request.user cannot see his details
            return HttpResponse('IBO should be in your downline, if you want to see his/her report.')

        event_obj = None
        try:
            event = int(event)
            if event <= 0:
                event = None
            try:
                event_obj = Convention.objects.get(pk=event)
            except:
                event = None
                event_obj = None
        except:
            event = None

        tickets = {}

        convention = ContentType.objects.get(app_label='planning', model='convention')
        total_count = 0
        pre_comms = Commission.objects.filter(order_item__content_type=convention, order_item__product_id=event).distinct()

        users = pre_comms.values_list("order_item__order__user__pk", flat=True)

        total_event_earnings = 0
        ois = OrderItem.objects.filter(content_type=convention, product_id=event_obj.pk)
        orders = Order.objects.filter(orderitem__in=ois).exclude(status="Cancelled")
        pools = {"Diamond": 0, "Emerald": 0}

        # Hideous way of getting totals... nevertheless I don't have time for anything else
        for o in orders:
            if o.balance == 0:
                total_event_earnings += o.total
                for oi in o.orderitem_set.all():
                    event_ticket_total += oi.quantity
                    if oi.order.user.pk in users:
                        total_tickets_sold_by_user += oi.quantity

        total_giveaway = pre_comms.aggregate(total=Sum("amount"))["total"]

        all_comms = pre_comms.filter(user=ibo).distinct()

        # Now the rest
        #total_event_users = all_comms.exclude(order_item__order__user=ibo).values('gen_level', "comp_level__name",
                #"order_item__order__user__username").annotate(total=Sum("amount")).annotate(points=Sum("points"))

        total_event_users = all_comms.values('gen_level', "comp_level__name",
                "order_item__order__user__username").annotate(total=Sum("amount")).annotate(points=Sum("points"))

        tickets["Diamond"] = dict()
        tickets["Emerald"] = dict()
        gen_totals = {"Diamond": dict(), "Emerald": dict()}

        for data in total_event_users:
            try:
                downline_points_total = data["total"] / Decimal("%f" % multiplier_constant)
            except TypeError:
                downline_points_total = 0

            u = User.objects.get(username=data["order_item__order__user__username"])

            tmp_tickets = {
                'ibo': u,
                'ibo_number': u.username,
                'ibo_name': u.get_full_name,
                'pin_level': u.pinlevel(),
                'quantity': len(event_obj.tickets.filter(user=u)),
                'earned': data["total"],
                "points": downline_points_total,
                "pool_level": get_pin_level(u),
            }

            gen_level = int(data["gen_level"])
            if gen_level in tickets[data["comp_level__name"]]:
                tickets[data["comp_level__name"]][gen_level].append(tmp_tickets)
            else:
                tickets[data["comp_level__name"]][gen_level] = [tmp_tickets, ]

            pools[data["comp_level__name"]] += tmp_tickets["earned"]

            if gen_level in gen_totals[data["comp_level__name"]]:
                gen_totals[data["comp_level__name"]][gen_level] += tmp_tickets["earned"]
            else:
                gen_totals[data["comp_level__name"]][gen_level] = tmp_tickets["earned"]

            total_count += tmp_tickets["quantity"]
        total_amt = 0

        # Expenses madafacka! Do you speak them?
        expenses = event_obj.eventexpensescategory_set.all()
        summaries_to_display = dict()
        expenses_total = 0
        for e in expenses:
            summaries_to_display[e.label] = Decimal("0.00")
            for x in e.eventexpenses_set.all():
                summaries_to_display[e.label] += x.amount
                expenses_total += x.amount

        # Now get relevant points count
        pools["Diamond"] = pools["Diamond"] if pools["Diamond"] else Decimal("0.00")
        pools["Emerald"] = pools["Emerald"] if pools["Emerald"] else Decimal("0.00")

        try:
            diamond_0_points = pools["Diamond"] / Decimal("%f" % multiplier_constant)
        except ZeroDivisionError:
            diamond_0_points = 0.00
        try:
            emerald_0_points = pools["Emerald"] / Decimal("%f" % multiplier_constant)
        except ZeroDivisionError:
            emerald_0_points = 0.00

        logged_user["SELF"] = dict()
        logged_user["SELF"]["Diamond"] = {"ibo": ibo, 'ibo_number': "SELF",
                               'ibo_name': ibo.get_full_name,
                               'pin_level': ibo.pinlevel(),
                               'quantity': len(event_obj.tickets.filter(user=ibo)),
                               'earned': pools["Diamond"],
                               'points': diamond_0_points,
                               "pool_level": get_pin_level(ibo),
                               }

        logged_user["SELF"]["Emerald"] = {"ibo": ibo, 'ibo_number': "SELF",
                               'ibo_name': ibo.get_full_name,
                               'pin_level': ibo.pinlevel(),
                               'quantity': len(event_obj.tickets.filter(user=ibo)),
                               'earned': pools["Emerald"],
                               'points': emerald_0_points,
                               "pool_level": get_pin_level(ibo),
                               }

        # TODO: You do realize this is totally retarded? Ok... moving on!
        total_count += logged_user["SELF"]["Diamond"]["quantity"] + logged_user["SELF"]["Emerald"]["quantity"]

        # Now calculate the DP per ticket
        try:
            dp_per_ticket = total_giveaway / event_ticket_total
        except TypeError:
            dp_per_ticket = 0

    else:
        event_obj = None
        expenses_total = 0
        tickets = {}
        total_count = 0
        total_amt = 0
        dp_per_ticket = 0
        ibo = request.user
        up = None
        pin_level_pools = dict()
        summaries_to_display = dict()
        total_giveaway = 0
        pools = {"Diamond": 0, "Emerald": 0}
        gen_totals = dict()

    # Just display events where anyone in the downline went
    events = Convention.objects.filter(compensation_paid=True,
        tickets__user__in=really_all_users).distinct().order_by('name')

    # Divide according to pinlevel
    #    for key, value in tickets.items():
    #        if key == "SELF":
    #            continue
    #
    #        pin_level = get_pin_level(value["ibo"])
    #        if pin_level in pin_level_pools:
    #            pin_level_pools[pin_level] += value["earned"]
    #        else:
    #            pin_level_pools[pin_level] = value["earned"]

    return render(request, 'reports/group_template.html',
        {'title': 'Group Report', 'events': events, 'eid': event, 'generation': generation,
         'event': event_obj, 'ibo': ibo, 'tickets': tickets, 'total_event_earnings': total_event_earnings,
         'total_compensation': total_compensation, "pin_level_pools": pin_level_pools, 'logged_user': logged_user,
         'total_count': total_count, 'up': up,
         'total_amt': total_amt, "expenses": summaries_to_display,
         "expenses_total": expenses_total,
         "total_giveaway": total_giveaway, "pools": pools,
         "dp_per_ticket": dp_per_ticket, "gen_totals": gen_totals,
         "event_ticket_total": event_ticket_total, "total_tickets_sold_by_user": total_tickets_sold_by_user})


@login_required
def group_tools(request):
    if not request.user.is_platinum_or_above():
        raise Http404('You are not allowed to view this report.')

    products = {
        '0-bbk-all': {'label': 'Business Kit – All',
                      'prod-slug': ['business-builder-kit', 'spanish-business-builder-kit']},
        '1-bbk-eng-bb': {'label': 'Business Kit – Eng USA', 'prod-slug': ['business-builder-kit', ]},
        '2-bbk-eng-bb': {'label': 'Business Kit – Eng BB'},
        '3-bbk-spn-usa': {'label': 'Business Kit – Spn USA', 'prod-slug': ['spanish-business-builder-kit', ]}}

    categories = {'0-category-all': {'label': 'Category – All', 'prod-cat-slug': 'all'},
                  '1-category-prospecting': {'label': 'Category – Prospecting', 'prod-cat-slug': ['prospecting', ]},
                  '2-category-followup': {'label': 'Category – Follow up', 'prod-cat-slug': ['follow-up', ]},
                  '3-category-productsupport': {'label': 'Category – Product Support',
                                                'prod-cat-slug': ['product-support', ]}}

    up = None
    total_amount = 0
    total_count = 0
    type = None
    slug = None
    item = None
    rs = request.GET.get('rs', None)
    report = {}
    ibo = request.GET.get('ibo', request.user.username)
    start_date = request.GET.get('start_date', None)
    to_date = request.GET.get('to_date', None)
    sd = None
    td = None
    invalid_date = None

    try:
        if start_date:
            sd = time.strptime("%s 00:00:00" % (start_date), "%Y-%m-%d %H:%M:%S")
        if to_date:
            td = time.strptime("%s 23:59:59" % (to_date), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not invalid_date and sd and td:
        if td < sd:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

    if not invalid_date:
        if rs:
            if rs in products:
                type = 'products'
                slug = rs
                item = products[rs]
            elif rs in categories:
                type = 'categories'
                slug = rs
                item = categories[rs]

        if type and slug:
            try:
                ibo = User.objects.get(username=ibo)
            except User.DoesNotExist:
                raise Http404('The IBO\'s report you want to access does not exists.')

            min_level = Group.objects.get(name='Platinum')
            if request.user.pinlevel().id < min_level.id:
                raise Http404('This area is only for Platinum and above members.')

            proceed, up = is_in_upline(request.user, ibo)
            if not proceed: #if the ibo doesnot has request.user in his upline then request.user cannot see his details
                return Http404('IBO should be in your downline, if you want to see his/her report.')

            p_id = None
            if 'prod-slug' in item:
                p_id = Product.objects.values_list('id', flat=True).filter(slug__in=item['prod-slug'])
            elif 'prod-cat-slug' in item:
                if item['prod-cat-slug'] == 'all':
                    p_id = Product.objects.values_list('id', flat=True).all()
                else:
                    p_id = Product.objects.values_list('id', flat=True).filter(category__slug__in=item['prod-cat-slug'])

            if p_id:
                prod = ContentType.objects.get(app_label='products', model='product')
                all_comms = Commission.objects.filter(order_item__content_type=prod.id, order_item__product_id__in=p_id,
                    user=request.user, order_item__order__user=ibo)
                if start_date:
                    all_comms = all_comms.filter(created__gte="%s 00:00:00" % (start_date))
                if to_date:
                    all_comms = all_comms.filter(created__lte="%s 23:59:59" % (to_date))

                ibo_number = 'SELF'
                ibo_link = 'SELF'
                report[ibo_number] = {'ibo_number': ibo_link, 'ibo_name': ibo.get_full_name, 'pin_level': ibo.pinlevel,
                                      'self_quantity': 0, 'quantity': 0, 'earned': 0}

                oi_added = []
                for com in all_comms:
                    if com.order_item.order.balance != 0:
                        continue

                    if not com.order_item in oi_added:
                        oi_added.append(com.order_item)
                        if com.order_item.order.user == com.user:
                            report[ibo_number]['self_quantity'] = report[ibo_number][
                                                                  'self_quantity'] + com.order_item.quantity

                        # total_count=total_count+com.order_item.quantity
                        report[ibo_number]['quantity'] = report[ibo_number]['quantity'] + com.order_item.quantity

                    total_amount += com.amount
                    report[ibo_number]['earned'] = report[ibo_number]['earned'] + com.amount

                front_line = get_frontline(ibo)
                all_users = [ibo.id, ] + front_line

                for f in front_line:
                    f = User.objects.get(pk=f)
                    all_down = get_downline(f, []) + [f.id, ]
                    all_comms = Commission.objects.filter(order_item__content_type=prod.id,
                        order_item__product_id__in=p_id, user=request.user, order_item__order__user__in=all_down)
                    if start_date:
                        all_comms = all_comms.filter(created__gte="%s 00:00:00" % (start_date))
                    if to_date:
                        all_comms = all_comms.filter(created__lte="%s 23:59:59" % (to_date))

                    ibo_number = f.username
                    # ibo_link='<a href="?rs='+slug+'&ibo='+ibo_number+'">'+ibo_number+'</a>'
                    ibo_link = '<a href="?rs=' + slug + '&ibo=' + ibo_number + '&start_date=' + start_date + '&to_date=' + to_date + '">' + ibo_number + '</a>'

                    regular_oi_added = []
                    upper_oi_added = []
                    for com in all_comms:
                        if com.order_item.order.balance != 0:
                            continue

                        if str(f.pinlevel()) != 'Regular':
                            if f.username not in report:
                                report[f.username] = {'ibo_number': ibo_link, 'ibo_name': f.get_full_name,
                                                      'pin_level': f.pinlevel, 'self_quantity': 0, 'quantity': 0,
                                                      'earned': 0}

                            if not com.order_item in regular_oi_added:
                                regular_oi_added.append(com.order_item)
                                report[f.username]['quantity'] = report[f.username][
                                                                 'quantity'] + com.order_item.quantity
                            report[f.username]['earned'] = report[f.username]['earned'] + com.amount

                        else:
                            if not com.order_item in upper_oi_added:
                                upper_oi_added.append(com.order_item)
                                report['SELF']['quantity'] = report['SELF']['quantity'] + com.order_item.quantity
                            report['SELF']['earned'] = report['SELF']['earned'] + com.amount

                        total_amount += com.amount
                        # total_count=total_count+com.order_item.quantity

        for k, v in report.iteritems():
            total_count += report[k]['quantity']
    return render(request, 'reports/group_tools_template.html',
        {'title': 'Group Tools Report', 'products': products, 'categories': categories, 'type': type, 'slug': slug,
         'item': item, 'report': report, 'ibo': ibo, 'total_count': total_count, 'total_amount': total_amount, 'up': up,
         'start_date': start_date, 'to_date': to_date, 'invalid_date': invalid_date})


@login_required
def prod_group_tools(request):
    if not request.user.is_platinum_or_above():
        raise Http404('You are not allowed to view this report.')

    products = {
        '0-bbk-all': {'label': 'Business Kit – All',
                      'prod-slug': ['business-builder-kit', 'spanish-business-builder-kit']},
        '1-bbk-eng-bb': {'label': 'Business Kit – Eng USA', 'prod-slug': ['business-builder-kit', ]},
        '2-bbk-eng-bb': {'label': 'Business Kit – Eng BB'},
        '3-bbk-spn-usa': {'label': 'Business Kit – Spn USA', 'prod-slug': ['spanish-business-builder-kit', ]}}

    categories = {'0-category-all': {'label': 'Category – All', 'prod-cat-slug': 'all'},
                  '1-category-prospecting': {'label': 'Category – Prospecting', 'prod-cat-slug': ['prospecting', ]},
                  '2-category-followup': {'label': 'Category – Follow up', 'prod-cat-slug': ['follow-up', ]},
                  '3-category-productsupport': {'label': 'Category – Product Support',
                                                'prod-cat-slug': ['product-support', ]}}

    up = None
    total_count = 0
    type = None
    slug = None
    item = None
    rs = request.GET.get('rs', None)
    report = {}
    ibo = request.GET.get('ibo', request.user.username)
    start_date = request.GET.get('start_date', None)
    to_date = request.GET.get('to_date', None)
    sd = None
    td = None
    invalid_date = None

    try:
        if start_date:
            sd = time.strptime("%s 00:00:00" % (start_date), "%Y-%m-%d %H:%M:%S")
        if to_date:
            td = time.strptime("%s 23:59:59" % (to_date), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not invalid_date and sd and td:
        if td < sd:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

    if not invalid_date:
        if rs:
            if rs in products:
                type = 'products'
                slug = rs
                item = products[rs]
            elif rs in categories:
                type = 'categories'
                slug = rs
                item = categories[rs]

        if type and slug:
            try:
                ibo = User.objects.get(username=ibo)
            except User.DoesNotExist:
                raise Http404('The IBO\'s report you want to access does not exists.')

            min_level = Group.objects.get(name='Platinum')
            if request.user.pinlevel().id < min_level.id:
                raise Http404('This area is only for Platinum and above members.')

            proceed, up = is_in_upline(request.user, ibo)
            if not proceed: #if the ibo doesnot has request.user in his upline then request.user cannot see his details
                return Http404('IBO should be in your downline, if you want to see his/her report.')

            p_id = None
            if 'prod-slug' in item:
                p_id = Product.objects.values_list('id', flat=True).filter(slug__in=item['prod-slug'])
            elif 'prod-cat-slug' in item:
                if item['prod-cat-slug'] == 'all':
                    p_id = Product.objects.values_list('id', flat=True).all()
                else:
                    p_id = Product.objects.values_list('id', flat=True).filter(category__slug__in=item['prod-cat-slug'])

            if p_id:
                prod = ContentType.objects.get(app_label='products', model='product')
                all_ois = OrderItem.objects.filter(content_type=prod.id, product_id__in=p_id, order__user=ibo)
                # all_comms=Commission.objects.filter(order_item__content_type=prod.id, order_item__product_id__in=p_id, user=request.user, order_item__order__user=ibo)
                if start_date:
                    all_ois = all_ois.filter(order__time_stamp__gte="%s 00:00:00" % (start_date))
                if to_date:
                    all_ois = all_ois.filter(order__time_stamp__lte="%s 23:59:59" % (to_date))

                ibo_number = 'SELF'
                ibo_link = 'SELF'
                report[ibo_number] = {'ibo_number': ibo_link, 'ibo_name': ibo.get_full_name, 'pin_level': ibo.pinlevel,
                                      'self_quantity': 0, 'quantity': 0, }

                sum_quantity = all_ois.aggregate(Sum('quantity'))['quantity__sum']
                if not sum_quantity:
                    sum_quantity = 0
                report[ibo_number]['self_quantity'] = sum_quantity
                report[ibo_number]['quantity'] = sum_quantity

                front_line = get_frontline(ibo)
                all_users = [ibo.id, ] + front_line

                for f in front_line:
                    f = User.objects.get(pk=f)

                    all_down = get_downline(f, []) + [f.id, ]
                    all_ois = OrderItem.objects.filter(content_type=prod.id, product_id__in=p_id,
                        order__user__in=all_down)
                    if start_date:
                        all_ois = all_ois.filter(order__time_stamp__gte="%s 00:00:00" % (start_date))
                    if to_date:
                        all_ois = all_ois.filter(order__time_stamp__lte="%s 23:59:59" % (to_date))

                    ibo_number = f.username
                    ibo_link = '<a href="?rs=' + slug + '&ibo=' + ibo_number + '&start_date=' + start_date + '&to_date=' + to_date + '">' + ibo_number + '</a>'

                    report[f.username] = {'ibo_number': ibo_link, 'ibo_name': f.get_full_name, 'pin_level': f.pinlevel,
                                          'self_quantity': 0, 'quantity': 0, }
                    for oi in all_ois:
                        if str(f.pinlevel()) != 'Regular':
                            report[f.username]['quantity'] = report[f.username]['quantity'] + oi.quantity

                        else:
                            report['SELF']['quantity'] = report['SELF']['quantity'] + oi.quantity

        for k, v in report.iteritems():
            total_count += report[k]['quantity']

    return render(request, 'reports/productivity_group_tools.html',
        {'title': 'Productivity - Group Tools Report', 'products': products, 'categories': categories, 'type': type,
         'slug': slug, 'item': item, 'report': report, 'ibo': ibo, 'total_count': total_count, 'up': up,
         'start_date': start_date, 'to_date': to_date, 'invalid_date': invalid_date})


@login_required
def prod_group_cep(request):
    products = {
        '0-cep-all': {
            'label': 'CEP – All',
            'prod-ids': []},
        '1-cep-digital': {
            'label': 'CEP - Digital',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                ~Q(product__slug__contains='book') & Q(product__slug__contains='digital'))},
        '2-cep-digital-book': {
            'label': 'CEP - Digital + Book',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & Q(product__slug__contains='digital'))},
        '3-cep-traditional': {
            'label': 'CEP - Traditional',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                ~Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))},
        '4-cep-traditional-book': {
            'label': 'CEP - Traditional + Book',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))},
        '5-cep-book-only': {
            'label': 'CEP - Book Only',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional'))},
    }

    all_ids = []
    for key, value in products.iteritems():
        all_ids.extend(value['prod-ids'])
    products['0-cep-all']['prod-ids'] = all_ids

    up = None
    total_amount = 0
    total_count = 0
    type = None
    selected = None
    item = None
    rs = request.GET.get('rs', None)
    start_date = request.GET.get('start_date', None)
    to_date = request.GET.get('to_date', None)
    sd = None
    td = None
    invalid_date = None
    report = None
    ibo = None

    try:
        if start_date:
            sd = time.strptime("%s 00:00:00" % (start_date), "%Y-%m-%d %H:%M:%S")
        if to_date:
            td = time.strptime("%s 23:59:59" % (to_date), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not invalid_date and sd and td:
        if td < sd:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

    if not invalid_date:
        report = {}
        ibo = request.GET.get('ibo', request.user.username)

        if rs:
            if rs in products:
                selected = rs
                item = products[rs]

        if selected:
            try:
                ibo = User.objects.get(username=ibo)
            except User.DoesNotExist:
                raise Http404('The IBO\'s report you want to access does not exists.')

            min_level = Group.objects.get(name='Platinum')
            if request.user.pinlevel().id < min_level.id:
                raise Http404('This area is only for Platinum and above members.')

            proceed, up = is_in_upline(request.user, ibo)
            if not proceed: #if the ibo doesnot has request.user in his upline then request.user cannot see his details
                return Http404('IBO should be in your downline, if you want to see his/her report.')

            p_id = item['prod-ids']

            if p_id:
                prod = ContentType.objects.get(app_label='products', model='product')
                all_ois = OrderItem.objects.filter(content_type=prod.id, product_id__in=p_id, order__user=ibo)

                # all_comms=Commission.objects.filter(order_item__content_type=prod.id, order_item__product_id__in=p_id, user=request.user, order_item__order__user=ibo)


                if start_date:
                    all_ois = all_ois.filter(order__time_stamp__gte="%s 00:00:00" % (start_date))
                if to_date:
                    all_ois = all_ois.filter(order__time_stamp__lte="%s 23:59:59" % (to_date))

                ibo_number = 'SELF'
                ibo_link = 'SELF'
                report[ibo_number] = {'ibo_number': ibo_link, 'ibo_name': ibo.get_full_name, 'pin_level': ibo.pinlevel,
                                      'self_quantity': 0, 'quantity': 0, }

                sum_quantity = all_ois.aggregate(Sum('quantity'))['quantity__sum']

                if not sum_quantity:
                    sum_quantity = 0
                report[ibo_number]['self_quantity'] = sum_quantity
                report[ibo_number]['quantity'] = sum_quantity

                front_line = get_frontline(ibo)
                all_users = [ibo.id, ] + front_line

                for f in front_line:
                    f = User.objects.get(pk=f)

                    all_down = get_downline(f, []) + [f.id, ]
                    all_ois = OrderItem.objects.filter(content_type=prod.id, product_id__in=p_id,
                        order__user__in=all_down)
                    if start_date:
                        all_ois = all_ois.filter(order__time_stamp__gte="%s 00:00:00" % (start_date))
                    if to_date:
                        all_ois = all_ois.filter(order__time_stamp__lte="%s 23:59:59" % (to_date))

                    ibo_number = f.username
                    ibo_link = '<a href="?rs=' + selected + '&ibo=' + ibo_number + '&start_date=' + start_date + '&to_date=' + to_date + '">' + ibo_number + '</a>'

                    report[f.username] = {'ibo_number': ibo_link, 'ibo_name': f.get_full_name, 'pin_level': f.pinlevel,
                                          'self_quantity': 0, 'quantity': 0, }

                    for oi in all_ois:
                        if str(f.pinlevel()) != 'Regular':
                            report[f.username]['quantity'] = report[f.username]['quantity'] + oi.quantity

                        else:
                            report['SELF']['quantity'] = report['SELF']['quantity'] + oi.quantity

        for k, v in report.iteritems():
            total_count += report[k]['quantity']

    return render(request, 'reports/productivity_group_cep_template.html',
        {'title': 'Productivity - Group CEP Report', 'products': products, 'selected': selected, 'item': item,
         'report': report, 'ibo': ibo, 'total_count': total_count, 'total_amount': total_amount, 'up': up,
         'start_date': start_date, 'to_date': to_date, 'invalid_date': invalid_date})



@login_required
def group_cep_drilldown(request):
    """ return a nice table with the user's underlings hohohoho """
    products = {
        '0-cep-all': {
            'label': 'CEP – All',
            'prod-ids': []},
        '1-cep-digital': {
            'label': 'CEP - Digital',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                ~Q(product__slug__contains='book') & Q(product__slug__contains='digital'))},
        '2-cep-digital-book': {
            'label': 'CEP - Digital + Book',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & Q(product__slug__contains='digital'))},
        '3-cep-traditional': {
            'label': 'CEP - Traditional',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                ~Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))},
        '4-cep-traditional-book': {
            'label': 'CEP - Traditional + Book',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))},
        '5-cep-book-only': {
            'label': 'CEP - Book Only',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional'))},
    }

    all_ids = []
    for key, value in products.iteritems():
        all_ids.extend(value['prod-ids'])
    products['0-cep-all']['prod-ids'] = all_ids

    user = User.objects.get(username=request.GET.get("ibo", None))
    start_date = datetime.datetime.strptime(request.GET.get("start_date"), "%Y-%m-%d")
    to_date = datetime.datetime.strptime(request.GET.get("to_date"), "%Y-%m-%d")
    products_selected = products[request.GET.get("rs")]["prod-ids"]

    # Who bought??
    content_type = ContentType.objects.get(app_label="products", model="product")

    comms = Commission.objects.filter(user=user, order_item__content_type=content_type, 
                    order_item__product_id__in=products_selected, created__gte=start_date, created__lte=to_date)

    print products_selected
    print request.GET.get("rs")
    print type(to_date)
    print type(start_date)
    print len(comms)

    # Now get the orders for this
    orders = Order.objects.filter(orderitem__commission__in=comms)

    print len(orders)

    ibos = User.objects.filter(orders__in=orders)

    #ibos = User.objects.filter(commissions__order_item__content_type=content_type,
            #commissions__order_item__product_id__in=products_selected, commissions__created__gte=start_date,
            #commissions__created__lte=to_date)

    print len(ibos)

    return render_to_response("reports/group_cep_drilldown.html", {'ibos': ibos})

@login_required
def group_cep(request):
    products = {
        '0-cep-all': {
            'label': 'CEP – All',
            'prod-ids': []},
        '1-cep-digital': {
            'label': 'CEP - Digital',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                ~Q(product__slug__contains='book') & Q(product__slug__contains='digital'))},
        '2-cep-digital-book': {
            'label': 'CEP - Digital + Book',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & Q(product__slug__contains='digital'))},
        '3-cep-traditional': {
            'label': 'CEP - Traditional',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                ~Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))},
        '4-cep-traditional-book': {
            'label': 'CEP - Traditional + Book',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & Q(product__slug__contains='traditional'))},
        '5-cep-book-only': {
            'label': 'CEP - Book Only',
            'prod-ids': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(
                Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional'))},
    }

    all_ids = []
    for key, value in products.iteritems():
        all_ids.extend(value['prod-ids'])
    products['0-cep-all']['prod-ids'] = all_ids

    up = None
    total_amount = 0
    total_count = 0
    type = None
    selected = None
    item = None
    rs = request.GET.get('rs', None)
    start_date = request.GET.get('start_date', None)
    to_date = request.GET.get('to_date', None)
    sd = None
    td = None
    invalid_date = None
    report = None
    ibo = None

    try:
        if start_date:
            sd = time.strptime("%s 00:00:00" % (start_date), "%Y-%m-%d %H:%M:%S")
        if to_date:
            td = time.strptime("%s 23:59:59" % (to_date), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not invalid_date and sd and td:
        if td < sd:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

    if not invalid_date:
        report = {}
        ibo = request.GET.get('ibo', request.user.username)

        if rs:
            if rs in products:
                selected = rs
                item = products[rs]

        if selected:
            try:
                ibo = User.objects.get(username=ibo)
            except User.DoesNotExist:
                raise Http404('The IBO\'s report you want to access does not exists.')

            min_level = Group.objects.get(name='Platinum')
            if request.user.pinlevel().id < min_level.id:
                raise Http404('This area is only for Platinum and above members.')

            proceed, up = is_in_upline(request.user, ibo)
            if not proceed: #if the ibo doesnot has request.user in his upline then request.user cannot see his details
                return Http404('IBO should be in your downline, if you want to see his/her report.')

            p_id = item['prod-ids']

            if p_id:
                prod = ContentType.objects.get(app_label='products', model='product')
                all_comms = Commission.objects.filter(order_item__content_type=prod.id, order_item__product_id__in=p_id,
                    user=request.user, order_item__order__user=ibo)
                if start_date:
                    all_comms = all_comms.filter(created__gte="%s 00:00:00" % (start_date))
                if to_date:
                    all_comms = all_comms.filter(created__lte="%s 23:59:59" % (to_date))

                ibo_number = 'SELF'
                ibo_link = '<a id="self-link" href="/user/reports/group_cep_drilldown?rs=' + selected + '&ibo=' + request.user.username + '&start_date=' + start_date + '&to_date=' + to_date + '">' + ibo_number + '</a>'

                report[ibo_number] = {'ibo_number': ibo_link, 'ibo_name': ibo.get_full_name, 'pin_level': ibo.pinlevel,
                                      'self_quantity': 0, 'quantity': 0, 'earned': 0}

                oi_added = []
                for com in all_comms:
                    if com.order_item.order.balance != 0:
                        continue

                    if not com.order_item in oi_added:
                        oi_added.append(com.order_item)
                        if com.order_item.order.user == com.user:
                            report[ibo_number]['self_quantity'] = report[ibo_number][
                                                                  'self_quantity'] + com.order_item.quantity

                        # total_count=total_count+com.order_item.quantity
                        report[ibo_number]['quantity'] = report[ibo_number]['quantity'] + com.order_item.quantity

                    total_amount += com.amount
                    report[ibo_number]['earned'] = report[ibo_number]['earned'] + com.amount

                front_line = get_frontline(ibo)
                all_users = [ibo.id, ] + front_line

                for f in front_line:
                    f = User.objects.get(pk=f)
                    all_down = get_downline(f, []) + [f.id, ]
                    all_comms = Commission.objects.filter(order_item__content_type=prod.id,
                        order_item__product_id__in=p_id, user=request.user, order_item__order__user__in=all_down)
                    if start_date:
                        all_comms = all_comms.filter(created__gte="%s 00:00:00" % (start_date))
                    if to_date:
                        all_comms = all_comms.filter(created__lte="%s 23:59:59" % (to_date))

                    ibo_number = f.username
                    ibo_link = '<a href="?rs=' + selected + '&ibo=' + ibo_number + '&start_date=' + start_date + '&to_date=' + to_date + '">' + ibo_number + '</a>'

                    regular_oi_added = []
                    upper_oi_added = []
                    for com in all_comms:
                        if com.order_item.order.balance != 0:
                            continue

                        if str(f.pinlevel()) != 'Regular':
                            if f.username not in report:
                                report[f.username] = {'ibo_number': ibo_link, 'ibo_name': f.get_full_name,
                                                      'pin_level': f.pinlevel, 'self_quantity': 0, 'quantity': 0,
                                                      'earned': 0}

                            if not com.order_item in regular_oi_added:
                                regular_oi_added.append(com.order_item)
                                report[f.username]['quantity'] = report[f.username][
                                                                 'quantity'] + com.order_item.quantity

                            report[f.username]['earned'] = report[f.username]['earned'] + com.amount

                        else:
                            if not com.order_item in upper_oi_added:
                                upper_oi_added.append(com.order_item)
                                report['SELF']['quantity'] = report['SELF']['quantity'] + com.order_item.quantity
                            report['SELF']['earned'] = report['SELF']['earned'] + com.amount

                        total_amount += com.amount
                        # total_count=total_count+com.order_item.quantity

        for k, v in report.iteritems():
            total_count += report[k]['quantity']

    return render(request, 'reports/group_cep_template.html',
        {'title': 'Group CEP Report', 'products': products, 'selected': selected, 'item': item, 'report': report,
         'ibo': ibo, 'total_count': total_count, 'total_amount': total_amount, 'up': up, 'start_date': start_date,
         'to_date': to_date, 'invalid_date': invalid_date})


@staff_only
def commissions(request):
    invalid_date = False
    today = datetime.date.today()
    yesterday = today - datetime.timedelta(1)
    last_7_day = today - datetime.timedelta(7)
    last_30_day = today - datetime.timedelta(30)

    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)
    only_tools = request.GET.get('only_tools', None)

    if only_tools == 'true':
        only_tools = True
    else:
        only_tools = False

    if sd is None or sd == '':
        sd = yesterday.strftime("%Y-%m-%d")

    if td is None or td == '':
        td = yesterday.strftime("%Y-%m-%d")

    try:
        start_date = time.strptime("%s 00:00:00" % (sd), "%Y-%m-%d %H:%M:%S")
        to_date = time.strptime("%s 23:59:59" % (td), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not invalid_date:
        if to_date < start_date:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

    if not invalid_date:
        details = {}

        from django.db import connection

        cursor = connection.cursor()  # @UndefinedVariable
        """ RAW SQL Used as give in http://www.almondev.org:8080/browse/EFIN-67 """

        query = """
        SELECT
          username as IBO_number,
          last_name,
          first_name,
          IF(received_w9 AND received_efinity_agreement,'yes','no') as eligible,
          IF(NOT received_w9 AND NOT received_efinity_agreement,'need BOTH',IF(NOT received_w9 OR received_w9 IS NULL,'need W9',IF(NOT received_efinity_agreement OR received_efinity_agreement IS NULL,'need contract',''))) as reason,
          SUM(amount)
        FROM
          """ + (('', 'shop_orderitem soi,')[only_tools]) + """
          auth_user as u,
          compensation_commission as c,
          accounts_profile as p
        WHERE
          """ + (('', 'soi.id=c.order_item_id AND soi.content_type_id=55 AND')[only_tools]) + """
          u.id=c.user_id AND
          u.id=p.user_id AND
          created>'%s 00:00:00' AND
          created<'%s 23:59:59'
        GROUP BY
          u.id; """ % (sd, td)
        #print query
        #print query

        cursor.execute(query)
        details = cursor.fetchall()
        tmp_details = []
        for data in details:
            u = User.objects.get(username=data[0])

            if len(u.subscriptions_in(datetime.date(start_date.tm_year, start_date.tm_mon, start_date.tm_mday),
                datetime.date(to_date.tm_year, to_date.tm_mon, to_date.tm_mday))) == 0:
            #if len(u.subscriptions_in(start_date, to_date))==0:
                reason = ''
                if data[4] != '':
                    reason = data[4] + ', '
                reason += 'no subscription'

                tmp_details.append([data[0], data[1], data[2], 'no', reason, data[5]])
            else:
                tmp_details.append([data[0], data[1], data[2], data[3], data[4], data[5]])
        details = tmp_details

        if request.GET.get('export', None) == 'csv':
            response = HttpResponse(mimetype='text/csv')
            response['Content-Disposition'] = 'attachment; filename=commission-report-%s-to-%s.csv' % (
                defaultfilters.slugify(sd), defaultfilters.slugify(td))
            writer = csv.writer(response)
            writer.writerow(['IBO Number', 'First Name', 'Last Name', 'Eligible', 'Reason', 'Amount'])
            for data in details:
                writer.writerow([data[0], data[2], data[1], data[3], data[4], data[5]])
            return response

        else:
            return render(request, 'reports/commissions.html',
                {'title': 'Commissions Report', 'only_tools': only_tools, 'start_date': start_date, 'to_date': to_date,
                 'yesterday': yesterday, 'last_7_day': last_7_day, 'last_30_day': last_30_day, 'sd': sd, 'td': td,
                 'details': details})
    else:
        return render(request, 'reports/commissions.html',
            {'title': 'Commissions Report', 'only_tools': only_tools, 'msg': [{'tags': 'error', 'text': invalid_date}],
             'yesterday': yesterday, 'last_7_day': last_7_day, 'last_30_day': last_30_day, 'sd': sd, 'td': td})


@staff_only
def browse(request):
    events = Convention.objects.all().order_by('name')

    invalid_date = False

    today = datetime.date.today()
    yesterday = today - datetime.timedelta(1)
    last_7_day = today - datetime.timedelta(7)
    last_30_day = today - datetime.timedelta(30)

    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)
    eid = request.GET.get('eid', -1)
    try:
        eid = int(eid)
    except:
        eid = -1

    if sd is None or sd == '':
        #sd=datetime.datetime.datex().strftime(yesterday, "%Y-%m-%d")
        #sd = yesterday.strftime("%Y-%m-%d")
        sd = yesterday.strftime("%Y-%m-%d")

    if td is None or td == '':
        #td=datetime.datetime.date().strftime(yesterday, "%Y-%m-%d")
        td = yesterday.strftime("%Y-%m-%d")

    try:
        start_date = time.strptime("%s 00:00:00" % (sd), "%Y-%m-%d %H:%M:%S")
        to_date = time.strptime("%s 23:59:59" % (td), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not invalid_date:
        if to_date < start_date:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

    if not invalid_date:
        ct = ContentType.objects.get(model='convention', app_label='planning')
        if eid == -1:
            oi = OrderItem.objects.filter(order__time_stamp__gte="%s 00:00:00" % (sd),
                order__time_stamp__lte="%s 23:59:59" % (td), content_type=ct, completed=False,
                order__payments__isnull=False).extra(select={
                'payment': 'select payment from shop_orderpayment where order_id=shop_orderitem.order_id limit 1'}).distinct().select_related(
                'product')
        else:
            oi = OrderItem.objects.filter(order__time_stamp__gte="%s 00:00:00" % (sd),
                order__time_stamp__lte="%s 23:59:59" % (td), content_type=ct, product_id=eid, completed=False,
                order__payments__isnull=False).extra(select={
                'payment': 'select payment from shop_orderpayment where order_id=shop_orderitem.order_id limit 1'}).distinct().select_related(
                'product')

        details = {}
        total = {'adn_sale': 0, 'adn_cost': 0, 'cod_sale': 0, 'cod_cost': 0, 'cheque_sale': 0, 'cheque_cost': 0,
                 't_sale': 0, 't_cost': 0}

        # Really really ugly hack
        processed_orders = []
        print len(oi)
        for o in oi:
            if o.order.balance == 0:
                try:
                    print o.product.name
                except AttributeError:
                    continue
                if o.product.name not in details:
                    details[o.product.name] = {'t_sale': 0, 't_cost': 0, 'adn_sale': 0, 'adn_cost': 0, 'cod_sale': 0,
                                               'cod_cost': 0, 'cheque_sale': 0, 'cheque_cost': 0}

                if not o.order.pk in processed_orders:
                    details[o.product.name]['t_cost'] += o.order.total
                    total['t_cost'] += o.order.total
                    processed_orders.append(o.order.pk)

                details[o.product.name]['t_sale'] += o.quantity
                total['t_sale'] += o.quantity

                #                if o.discount and o.discount > 0:
                #                    total['t_cost'] = total['t_cost'] - o.discount
                #                    details[o.product.name]['t_cost'] = details[o.product.name]['t_cost'] - o.discount

                if o.payment == 'AUTHORIZENET' or o.payment == 'AUTHORIZENETCONVENTIONS':
                    details[o.product.name]['adn_sale'] += o.quantity
                    details[o.product.name]['adn_cost'] += (o.quantity * o.unit_price)
                    total['adn_sale'] += o.quantity
                    total['adn_cost'] += (o.quantity * o.unit_price)
                    if o.discount and o.discount > 0:
                        total['adn_cost'] = total['adn_cost'] - o.discount
                        details[o.product.name]['adn_cost'] = details[o.product.name]['adn_cost'] - o.discount

                elif o.payment == 'COD':
                    details[o.product.name]['cod_sale'] += o.quantity
                    details[o.product.name]['cod_cost'] += (o.quantity * o.unit_price)
                    total['cod_sale'] += o.quantity
                    total['cod_cost'] += (o.quantity * o.unit_price)
                    if o.discount and o.discount > 0:
                        total['cod_cost'] = total['cod_cost'] - o.discount
                        details[o.product.name]['cod_cost'] = details[o.product.name]['cod_cost'] - o.discount

                elif o.payment == 'CHEQUE':
                    details[o.product.name]['cheque_sale'] += o.quantity
                    details[o.product.name]['cheque_cost'] += (o.quantity * o.unit_price)
                    total['cheque_sale'] += o.quantity
                    total['cheque_cost'] += (o.quantity * o.unit_price)
                #                    if o.discount and o.discount > 0:
                #                        total['cheque_cost'] = total['cheque_cost'] - o.discount
                #                        details[o.product.name]['cheque_cost'] = details[o.product.name]['cheque_cost'] - o.discount
                #
                details[o.product.name]["pk"] = o.product.pk

        if request.GET.get('export', None) == 'csv':
            response = HttpResponse(mimetype='text/csv')
            response['Content-Disposition'] = 'attachment; filename=event-sale-report-%s-to-%s.csv' % (
                defaultfilters.slugify(sd), defaultfilters.slugify(td))
            writer = csv.writer(response)
            writer.writerow(
                ['EVENT', 'VIA CASH', 'CASH AMT.', 'VIA CHEQUE', 'CHEQUE AMT.', 'VIA CREDIT CARD', 'CREDIT CARD AMT.',
                 'TOTAL SOLD', 'TOTAL AMT.'])
            for key, values in details.items():
                writer.writerow(
                    [key, values['cod_sale'], values['cod_cost'], values['cheque_sale'], values['cheque_cost'],
                     values['adn_sale'], values['adn_cost'], values['t_sale'], values['t_cost']])
            writer.writerow(['Total', total['cod_sale'], total['cod_cost'], total['cheque_sale'], total['cheque_cost'],
                             total['adn_sale'], total['adn_cost'], total['t_sale'], total['t_cost']])

            return response

        else:
            return render(request, 'reports/custom.html',
                {'title': 'Event Ticket Sales Report', 'details': details, 'start_date': start_date, 'to_date': to_date,
                 'yesterday': yesterday, 'last_7_day': last_7_day, 'last_30_day': last_30_day, 'sd': sd, 'td': td,
                 'events': events, 'eid': eid, 'total': total})

    else:
        return render(request, 'reports/custom.html',
            {'title': 'Event Ticket Sales Report', 'msg': [{'tags': 'error', 'text': invalid_date}],
             'yesterday': yesterday, 'last_7_day': last_7_day, 'last_30_day': last_30_day, 'sd': sd, 'td': td,
             'events': events, 'eid': eid})


@staff_only
def subscription_report(request):
    """ For now display a line graph with sales on subscription on a monthly basis """
    # First get the fucking subscriptions
    if request.GET.get("filter", None):
        team_selected = request.GET.getlist("filter")
        if "All" in team_selected:
            team_selected = None
    else:
        team_selected = None

    product_ids={
            'BOOK':SubscriptionProduct.objects.values_list('product__id',
                    flat=True).filter(Q(product__slug__contains='book') & ~Q(product__slug__contains='traditional')).exclude(product__pk=43),
            'CEP': SubscriptionProduct.objects.values_list('product__id',
                    flat=True).filter(~Q(product__slug__contains='book') & Q(product__slug__contains='traditional')),
            'CEP AND BOOK':SubscriptionProduct.objects.values_list('product__id',
                    flat=True).filter(Q(product__slug__contains='book') & Q(product__slug__contains='traditional')),
            'DIGITAL': SubscriptionProduct.objects.values_list('product__id', flat=True).filter(product__slug="subscription-digital"),
            'DIGITAL AND BOOK': SubscriptionProduct.objects.values_list('product__id', 
                flat=True).filter(product__slug="subscription-digital-book"),
            }

    print product_ids

    six_months_ago = datetime.date.today() - relativedelta(months=6)
    # Now get the correct range
    #subscription_payments = {"BOOK": {}, "CEP": {}, "CEP AND BOOK": {}}
    subscription_payments = SortedDict()
    #render_graph = [["Month", "BOOK", "CEP", "CEP AND BOOK"],]
    for subs_type in product_ids:
        order_payments = OrderPayment.objects.filter(order__orderitem__product_id__in=product_ids[subs_type],
                transaction_id__isnull=False, time_stamp__gte=six_months_ago).exclude(transaction_id="").distinct().order_by("time_stamp")

        if team_selected:
            order_payments = order_payments.filter(order__user__profile_object__team__in=team_selected)

        for o in order_payments:
            if o.time_stamp.strftime("%B") not in subscription_payments:
                print o.time_stamp.strftime("%B")
                subscription_payments[o.time_stamp.strftime("%B")] = {}
            if subs_type not in subscription_payments[o.time_stamp.strftime("%B")]:
                subscription_payments[o.time_stamp.strftime("%B")][subs_type] = 0
            subscription_payments[o.time_stamp.strftime("%B")][subs_type] += 1

    # Now we need a nice array for google charts
    # TODO: LAZIEST THING EVER FIX
    subscriptions = '[["Month", "BOOK", "CEP", "CEP AND BOOK", "DIGITAL", "DIGITAL AND BOOK"],\n'
    for month, values in subscription_payments.items():
        subscriptions += '["%s", %d, %d, %d, %d, %d],\n' % (month, values.get("BOOK", 0), values.get("CEP", 0), 
                values.get("CEP AND BOOK", 0), values.get("DIGITAL", 0), values.get("DIGITAL AND BOOK", 0))
    subscriptions += ']'

    title = "Subscriptions report"
    if team_selected:
        title += " for %s" % ", ".join(team_selected)
    else:
        title += " for ALL"

    return render(request, 'reports/subscription_report.html',
            {'title': title, 'subscriptions': subscriptions, 'team_selected': team_selected})


@staff_only
def browse_detailed(request, report_type=None):
    """
    Most of this can be refactored to different functions in another package.
    The thing here is that most of this functionality is repeated throught this and
    the function above it. Not nice.
    """
    # TODO: When back to Eclipse refector the hell out of this and the one above.
    events = Convention.objects.all().order_by('name')

    invalid_date = False

    today = datetime.date.today()
    yesterday = today - datetime.timedelta(1)

    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)
    eid = request.GET.get('eid', -1)
    try:
        eid = int(eid)
    except:
        eid = -1

    if sd is None or sd == '':
        #sd=datetime.datetime.datex().strftime(yesterday, "%Y-%m-%d")
        #sd = yesterday.strftime("%Y-%m-%d")
        sd = yesterday.strftime("%Y-%m-%d")

    if td is None or td == '':
        #td=datetime.datetime.date().strftime(yesterday, "%Y-%m-%d")
        td = yesterday.strftime("%Y-%m-%d")

    try:
        start_date = time.strptime("%s 00:00:00" % (sd), "%Y-%m-%d %H:%M:%S")
        to_date = time.strptime("%s 23:59:59" % (td), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not report_type:
        raise Http404
    elif report_type == "credit":
        check_string = "AUTHORIZE"
    else:
        check_string = "COD"

    if not invalid_date:
        if to_date < start_date:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'
    if not invalid_date:
        ct = ContentType.objects.get(model='convention', app_label='planning')
        if eid == -1:
            oi = OrderItem.objects.filter(order__time_stamp__gte="%s 00:00:00" % (sd),
                order__time_stamp__lte="%s 23:59:59" % (td), content_type=ct, completed=False,
                order__payments__isnull=False).distinct()
        else:
            oi = OrderItem.objects.filter(order__time_stamp__gte="%s 00:00:00" % (sd),
                order__time_stamp__lte="%s 23:59:59" % (td), content_type=ct, product_id=eid, completed=False,
                order__payments__isnull=False).distinct()
    # Filter according to report_type
    oi = oi.filter(order__payments__payment__icontains=check_string).exclude(order__payments__transaction_id="LINKED")
    orders = Order.objects.filter(orderitem__in=oi).distinct().annotate(quantity=Sum("orderitem__quantity"))

    return render(request, 'reports/custom_detailed.html',
        {'title': 'Event Sales Report Drilldown', 'sd': sd, 'td': td,
         'events': events, 'eid': eid, 'orders': orders})


@staff_only
def anti_browse(request):
    events = Convention.objects.all().order_by('name')

    invalid_date = False

    today = datetime.date.today()
    yesterday = today - datetime.timedelta(1)
    last_7_day = today - datetime.timedelta(7)
    last_30_day = today - datetime.timedelta(30)

    sd = request.GET.get('sd', None)
    td = request.GET.get('td', None)
    eid = request.GET.get('eid', -1)
    try:
        eid = int(eid)
    except:
        eid = -1

    if sd is None or sd == '':
        #sd=datetime.datetime.datex().strftime(yesterday, "%Y-%m-%d")
        #sd = yesterday.strftime("%Y-%m-%d")
        sd = yesterday.strftime("%Y-%m-%d")

    if td is None or td == '':
        #td=datetime.datetime.date().strftime(yesterday, "%Y-%m-%d")
        td = yesterday.strftime("%Y-%m-%d")

    try:
        start_date = time.strptime("%s 00:00:00" % (sd), "%Y-%m-%d %H:%M:%S")
        to_date = time.strptime("%s 23:59:59" % (td), "%Y-%m-%d %H:%M:%S")
    except:
        invalid_date = '<p>Please enter valid dates in yyyy-mm-dd format..</p>'

    if not invalid_date:
        if to_date < start_date:
            invalid_date = '<p><strong>To Date</strong> should be greater than equal to <strong>Start Date</strong>.</p>'

    if not invalid_date:
        ct = ContentType.objects.get(name='convention', app_label='planning')
        if eid == -1:
            oi = OrderItem.objects.filter(order__time_stamp__gte="%s 00:00:00" % (sd),
                order__time_stamp__lte="%s 23:59:59" % (td), content_type=ct, completed=False,
                order__payments__isnull=False, order__status="Cancelled").extra(select={
                'payment': 'select payment from shop_orderpayment where order_id=shop_orderitem.order_id limit 1'}).distinct().select_related(
                'product')
        else:
            oi = OrderItem.objects.filter(order__time_stamp__gte="%s 00:00:00" % (sd),
                order__time_stamp__lte="%s 23:59:59" % (td), content_type=ct, product_id=eid, completed=False,
                order__payments__isnull=False, order__status="Cancelled").extra(select={
                'payment': 'select payment from shop_orderpayment where order_id=shop_orderitem.order_id limit 1'}).distinct().select_related(
                'product')

        # Get relevant orders
        orders = Order.objects.filter(orderitem__in=oi).distinct().order_by("-time_stamp")
        tmp = []
        tickets_cancelled = Decimal("0.00")
        total_amount = Decimal("0.00")
        for o in orders:
            tmp_order = dict()
            tmp_order["order_id"] = o.pk
            tmp_order["date"] = o.time_stamp
            tmp_order["transaction_type"] = o.payments_completed()[0].payment
            tmp_order["tickets_sold"] = Decimal('0')
            for p in o.orderitem_set.all():
                tmp_order["tickets_sold"] += p.quantity
                tickets_cancelled += p.quantity

            tmp_order["total_amount"] = o.total
            total_amount += o.total

            tmp.append(tmp_order)

        orders = tmp

        return render(request, 'reports/anti-custom.html',
            {'title': 'Event Cancelled Tickets Report', 'orders': orders, 'start_date': start_date, 'to_date': to_date,
             'yesterday': yesterday, 'last_7_day': last_7_day, 'last_30_day': last_30_day, 'sd': sd, 'td': td,
             'events': events, 'eid': eid, "tickets_cancelled": tickets_cancelled, "total_amount": total_amount})

    else:
        return render(request, 'reports/anti-custom.html',
            {'title': 'Event Cancelled Tickets Report', 'msg': [{'tags': 'error', 'text': invalid_date}],
             'yesterday': yesterday, 'last_7_day': last_7_day, 'last_30_day': last_30_day, 'sd': sd, 'td': td,
             'events': events, 'eid': eid})


#@login_required
@staff_only
def tickets(request):
    eid = request.GET.get('event', None)
    try:
        eid = int(eid)
    except:
        eid = False

    #if eid != -1:
    #    tickets=Ticket.objects.filter(event=eid)
    if eid:
        tickets = Ticket.objects.filter(event=eid)
    else:
        tickets = {}

    t = {}
    total_count = 0
    payment_methods = {'UNKNOWN': 0}
    for ticket in tickets:
        if ticket.orderitem:
            order = ticket.orderitem.order
            payments = order.payments.all()
            if len(payments) > 0:
                p = payments[0].payment
                if p not in payment_methods:
                    payment_methods[p] = 0
                payment_methods[p] = payment_methods[p] + 1
        else:
            payment_methods['UNKNOWN'] = payment_methods['UNKNOWN'] + 1
        if "%d-%d" % (ticket.user_id, ticket.event_id) not in t:
            key = "%d-%d" % (ticket.user.id, ticket.event.id)
            t["%d-%d" % (ticket.user_id, ticket.event_id)] = {}
            t["%d-%d" % (ticket.user_id, ticket.event_id)]['event_name'] = ticket.event.name
            t["%d-%d" % (ticket.user_id, ticket.event_id)]['user_id'] = ticket.user_id
            t["%d-%d" % (ticket.user_id, ticket.event_id)]['count'] = 0
            t["%d-%d" % (ticket.user_id, ticket.event_id)]['ticket_id'] = []
            t["%d-%d" % (ticket.user_id, ticket.event_id)]['user_name'] = ticket.user.get_full_name()
        t["%d-%d" % (ticket.user_id, ticket.event_id)]['ticket_id'].append(str(ticket.id))
        t["%d-%d" % (ticket.user_id, ticket.event_id)]['count'] = t["%d-%d" % (ticket.user_id, ticket.event_id)][
                                                                  'count'] + 1
        total_count = total_count + 1

    if request.GET.get('export', None) == 'csv':
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=tickets%s.csv' % (
            ( '-eid-' + defaultfilters.slugify(eid), ''  )[eid == -1])
        writer = csv.writer(response)
        writer.writerow(['EVENT', 'USER ID', 'NO. OF TICKET', 'TICKET ID(s)'])
        for key, values in t.items():
            writer.writerow([values['event_name'], values['user_id'], values['count'], ', '.join(values['ticket_id'])])
        return response

    else:
        events = Convention.objects.all()
        return render(request, 'reports/tickets.html',
            {'title': 'Tickets', 'events': events, 'eid': eid, 'tickets': t, 'total_count': total_count,
             'payment_methods': payment_methods})

# What's this supposed to be doing?
def get_month_list(start, end):
    months = []
    current = start.year * 12 + start.month - 1
    end = end.year * 12 + end.month - 1
    while current <= end:
        months.append(datetime.date(current // 12, current % 12 + 1, 1))
        current += 1
    return months


@login_required
def new_home(request):
    #user = User.objects.get(username="4596")

    #removed hard-coded user request, pull current user instead -echung10/27/11
    user = request.user
    ois = OrderItem.objects.filter(commission__user=user).distinct()

    ois_online = ois.filter(order__method='Online')
    ois_event = ois.filter(order__method='Show')

    half_year = datetime.timedelta(183)
    today = datetime.datetime.today()
    half_year_ago = today - half_year
    months = get_month_list(half_year_ago, today)

    #platinum_level=Group.objects.get(name='Platinum').id
    platinum_founder_level = Group.objects.get(name='Platinum Founders').id
    #emerald_level=Group.objects.get(name='Emerald').id
    user_level = request.user.pinlevel().id
    access_extended = False

    if user_level > platinum_founder_level:
        access_extended = True

    return render(request, 'reports/new_home.html', {
        #'tool_sale_rows':tool_sale_rows,
        'access_extended': access_extended,
    })


@login_required
def tool_sales_by_ibo(request, ibo_number, year=None, month=None):
    report_user = User.objects.get(username=ibo_number)
    ois = OrderItem.objects.filter(order__user=report_user).distinct()
    report_months = None

    # No date passed in, let's default to the latest month
    if not year:
        today = datetime.datetime.today()
        year = today.year
        month = today.month

    if ois:
        # Get the max and min dates available for all objects returned, then create a list of valid months
        min_date = ois.aggregate(Min('order__time_stamp'))
        max_date = ois.aggregate(Max('order__time_stamp'))

        min_date = min_date['order__time_stamp__min']
        max_date = max_date['order__time_stamp__max']

        report_months = get_month_list(min_date, max_date)

        if year:
            ois = ois.filter(order__time_stamp__year=year)
        if month:
            ois = ois.filter(order__time_stamp__month=month)

    report_dt = datetime.datetime.date(int(year), int(month), 1)
    return render(request, 'reports/tool_sales_by_ibo.html', {
        'ois': ois,
        'report_months': report_months,
        'report_dt': report_dt,
        'report_user': report_user,
    })


@login_required
def tool_sales(request, report_type=None, year=None, month=None):
    user = request.user
    #removed hard-coded test case -echung10/27/11
    #user = User.objects.get(username="4596")

    ois = OrderItem.objects.filter(commission__user=user).distinct()

    report_months = None

    if report_type == 'all':
        placeholder = ''
    elif report_type == 'online':
        ois = ois.filter(order__method='Online')
    elif report_type == 'events':
        ois = ois.filter(order__method='Show')
    else:
        raise Http404('The report URL you requested was not found.')

    # No date passed in, let's default to the latest month
    if not year:
        today = datetime.datetime.today()
        year = today.year
        month = today.month

    if ois:
        # Get the max and min dates available for all objects returned, then create a list of valid months
        min_date = ois.aggregate(Min('order__time_stamp'))
        max_date = ois.aggregate(Max('order__time_stamp'))

        min_date = min_date['order__time_stamp__min']
        max_date = max_date['order__time_stamp__max']

        report_months = get_month_list(min_date, max_date)

        if year:
            ois = ois.filter(order__time_stamp__year=year)
        if month:
            ois = ois.filter(order__time_stamp__month=month)

    report_dt = datetime.datetime.date(int(year), int(month), 1)
    return render(request, 'reports/tool_sales.html', {
        'ois': ois,
        'report_type': report_type,
        'report_months': report_months,
        'report_dt': report_dt,
    })


@login_required
def reports_top(request, number_to_view, report_type, year=None, month=None):
    report_months = None
    #ois = OrderItem.objects.all()

    #modified to only pull current user instead of all orders -echung10/27/11
    user = request.user
    ois = OrderItem.objects.filter(commission__user=user).distinct()

    if report_type == 'all':
        placeholder = ''
    elif report_type == 'online':
        ois = ois.filter(order__method='Online')
    elif report_type == 'events':
        ois = ois.filter(order__method='Show')
    else:
        raise Http404('The report URL you requested was not found.')

    # No date passed in, let's default to the latest month
    if not year:
        today = datetime.datetime.today()
        year = today.year
        month = today.month

    if ois:
        # Get the max and min dates available for all objects returned, then create a list of valid months
        min_date = ois.aggregate(Min('order__time_stamp'))
        max_date = ois.aggregate(Max('order__time_stamp'))

        min_date = min_date['order__time_stamp__min']
        max_date = max_date['order__time_stamp__max']

        report_months = get_month_list(min_date, max_date)

        # limit the query based on date
        if year:
            ois = ois.filter(order__time_stamp__year=year)
        if month:
            ois = ois.filter(order__time_stamp__month=month)

    ois = ois.values('product_id').annotate(Count('product_id')).order_by('-product_id__count')[:number_to_view]
    for oi in ois:
        product_id = oi["product_id"]
        product = Product.objects.get(id=product_id)
        oi["product"] = product

    report_dt = datetime.datetime.date(int(year), int(month), 1)
    return render(request, 'reports/reports_top.html', {
        'ois': ois,
        'report_type': report_type,
        'report_months': report_months,
        'report_dt': report_dt,
    })


@login_required
def home(request):
    return render(request, 'reports/home.html', {
        'reports': Report.objects.all(),
    })


@login_required
def generate(request, slug):
    report = get_object_or_404(Report, slug=slug)
    results = report.generate_results()
    return render(request, 'reports/tabular.html', {
        'report': report,
        'reports': Report.objects.all(),
        'results': results,
    })


@login_required
def generate_with_filter(request, slug, filter):
    report = get_object_or_404(Report, slug=slug)
    filter = get_object_or_404(ReportFilter, report=report, pk=filter)
    try:
        results = report.generate_results(filter=filter)
    except:
        results = None
        #return HttpResponse(results)
    return render(request, 'reports/tabular.html', {
        'report': report,
        'filter': filter,
        'reports': Report.objects.all(),
        'results': results,
    })
