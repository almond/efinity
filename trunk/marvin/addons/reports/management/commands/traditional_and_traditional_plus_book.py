# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from marvin.store.products.modules.subscription.models import \
    SubscriptionProduct

from marvin.store.shop.models import Order, OrderItem

from django.db.models import Q


import csv
from marvin import unicodecsv
import datetime

class Command(BaseCommand):
    help='Get really weird stuff'


    def handle(self, *args, **options):
        # First get the IBO's from the CSV file
        reader = csv.DictReader(open("csv/source.csv", "rU"))
        ibos = tuple()
        results = []
        from_date = datetime.date(year=2012, month=6, day=30)
        to_date = datetime.date(year=2012, month=7, day=1)

        products = (103, 39, 102, 42)

        for row in reader:
            ibos += (int(row["IBO Number"]),)

        # Now get the necessary data from them
        users = User.objects.filter(username__in=ibos)
        for u in users:
            # Init dictionary
            print "Processing IBO: " + u.username

            current_user = {"IBO": u.username, "Amount": 0, "Name": u.first_name + " " + u.last_name}
            # Get platinums and below from their downline
            plats_and_below = u.downline.all()
            for v in plats_and_below:
                if v.user.pinlevel().pk <= 2:
                    print v.user.pinlevel().name
                    # Now check if they have those subscriptions active in July
                    orders = Order.objects.filter(user=v.user)
                    # Now filter further
                    orders = orders.filter(orderitem__product_id__in=products)

                    if len(orders):
                        print "Add to amount"
                        current_user["Amount"] += 1

            results.append(current_user)

        # Now write a CSV with it.
        writer = csv.writer(open("csv/report.csv", "wb"))
        print "IBO, Name, Amount \n"
        for x in results:
            print "%d, %s, %d" % (int(x["IBO"]), x["Name"], int(x["Amount"]))