# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.contrib.admin.models import LogEntry
from django.contrib.contenttypes.models import ContentType


class Command(BaseCommand):
    """
    Just small command that prints to terminal. Intended for pipe use
    """
    def handle(self, *args, **kwargs):
        order = ContentType.objects.filter(pk=36)

        # First print a nice comma separated header
        print "ID,DATE,USER_ID,USER,ORDER_ID,DETAIL,ACTION"
        # Now get the Logentries and print them
        entries = LogEntry.objects.filter(content_type=order, action_flag=3).order_by("-action_time")
        for e in entries:
            print u"%d,%s,%d,%s,%s,%s,DELETE" % (e.pk, str(e.action_time).encode("utf-8"), e.user.pk, e.user.first_name.encode("utf-8") +" "+ e.user.last_name.encode("utf-8"), e.object_id.encode("utf-8"), e.object_repr.encode("ascii", "replace"))
