from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order
import csv


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        order_pk = [
            40947,
            41127,
            40990,
            39477,
            40951,
            40971,
            39476,
            40958,
            40929,
            39468,
            39470,
            39474,
            39478,
            39479,
            39480,
            39481,
            39482,
            39485,
            39489,
            39490,
            39491,
            39493,
            39494,
            39495,
            39496,
            39497,
            39498,
            40874,
            39516,
            39559,
            39561,
            40564,
            40894,
            40869,
            40871,
            40873,
            40877,
            40880,
            40882,
            40889,
            40892,
            40900,
            40918,
            40920,
            40925,
            40930,
            40950,
            40952,
            40964,
            40965,
            40970,
            40981,
            40983,
            40984,
            40986,
            40993,
            40994,
            40997,
            40999,
            41008,
            41010,
            41012,
            41014,
            41015,
            41027,
            41032,
            41034,
            41123,
            41129,
            41134,
            41136,
            41137,
            41141,
            41142,
            41143,
            41149,
            41152,
            41153,
            41154,
            41155,
            41156,
            41157,
            41164,
            41166,
            41168,
            41171,
            41173,
            41194,
            41195,
            41203,
            41209,
            41212,
            41214,
            41217,
            41220,    
        ]

        # Now find them. Print the ones not found
        not_found = list()
        for pk in order_pk:
            try:
                order = Order.objects.get(pk=pk)
            except Order.DoesNotExist:
                not_found.append(pk)

        print "%d orders were not found on the system" % len(not_found)
        print
        print "They'll be printed below"
        for i in not_found:
            print i
