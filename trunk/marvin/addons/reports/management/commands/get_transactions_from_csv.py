from django.core.management.base import BaseCommand
from marvin.store.shop.models import OrderPayment
import csv


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        # File path
        path = "/Users/axolote/Workspace/efinity/csv/in/transactions-oct-2.csv"

        # Reader
        reader = csv.DictReader(open(path))

        # Now get into a tuple every transaction ID from the DB
        # that matches the CSV's defined ones
        transaction_ids = tuple()
        for l in reader:
            if l["Transaction Status"] == "Settled Successfully":
                transaction_ids += (l["Transaction ID"],)

        # How many transaction id's were recovered from file?
        print "%d Transaction ID's were recovered from file that were successful" % len(transaction_ids)

        # How many of those are present in the system?
        print "%d are present in the system" % len(
                OrderPayment.objects.filter(transaction_id__in=transaction_ids)
            )
        
        missing = list()
        for t in transaction_ids:
            try:
                tmp = OrderPayment.objects.get(transaction_id=t)
            except OrderPayment.DoesNotExist:
                missing.append(t)

        # How many are missing from the system? And print them
        print "%d are not" % len(missing)
        print
        print "They are printed in the following lines"
        for x in missing:
            print x        
