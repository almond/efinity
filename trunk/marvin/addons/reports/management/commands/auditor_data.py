from django.core.management.base import BaseCommand
from marvin.store.shop.models import Order, OrderItem
from marvin.addons.compensation.models import Commission, CompensationLevel
import csv
import os
from django.db.models.aggregates import Sum
from django.contrib.auth.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        From the tuple defined at the top get every order there and find out 
        how everyone was compensated and what generation they belong to
        """
        
        if args:
            event_number = "%s" % args[0]
        else:
            event_number = "3"
        
#        orders = ( # Evemt Three
#            21004,
#            32031,
#            32019,
#            32009,
#            31788,
#            31774,
#            31414,
#            30193,
#         )  
        
#        orders = ( # Event Two
#            23459,
#            19455,
#            23250,
#            23244,
#            18995,
#            11505,
#            21350,
#            11574,
#        ) 
        
        orders = ( # Event One
            30792,
            30740,
            17322,
            17294,
            30759,
            17448,
            17424,
            17422,
            17417,
        )
        
        orders = Order.objects.filter(pk__in=orders)
        ois = OrderItem.objects.filter(order__in=orders)
        
        # Now get the commissions. Order by generation
        comms = Commission.objects.filter(order_item__in=ois).distinct().values("gen_level", "comp_level",
                                                                     "user", 
                                                                     "order_item__order__pk").annotate(amount=Sum("amount")).order_by("order_item__order__pk", 
                                                                                                                                              "comp_level", "gen_level")
                                                                     
                                                                     
        # Name of the file we are writting to
        file_name = "csv/event-%s.csv" % event_number
        
        # Now write the CSV headers
        writer = csv.writer(open(file_name, "a"))
        
        order_tracker = list()
        comp_level_tracker = list()
        print len(comms)
        for c in comms:
            print c
            user = User.objects.get(pk=c["user"])
            order = Order.objects.get(pk=c["order_item__order__pk"])
            comp_level = CompensationLevel.objects.get(pk=c["comp_level"])
            if not order in order_tracker:
                writer.writerow(["Order number: %d" % c["order_item__order__pk"]])
                writer.writerow(["%s pool" % comp_level.name])
                columns = ["generation", "name", "pin_level", "amount"]
                writer.writerow(columns)
                order_tracker.append(order)
                del comp_level_tracker[:]
                comp_level_tracker.append(comp_level)
            
            if not comp_level in comp_level_tracker:
                writer.writerow(["%s pool" % comp_level.name])
                columns = ["generation", "name", "pin_level", "amount", "commission IDs"]
                writer.writerow(columns)
                comp_level_tracker.append(comp_level)
            
            writer.writerow([c["gen_level"], 
                             user.get_full_name(), 
                             user.pinlevel(),
                             c["amount"]])
