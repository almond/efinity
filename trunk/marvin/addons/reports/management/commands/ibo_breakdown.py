from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from marvin.store.products.modules.subscription.models import SubscriptionProduct
from marvin.store.shop.models import OrderItem
from marvin.addons.compensation.models import Commission


class Command(BaseCommand):
    help='Get a rundown based on a certain IBO number of people subscribed to certain stuff and normalize'
    
    uid_list ='6369297, 5944419, 6371225, 6424105, 4397217, 4104940, 6306290, 5945120, 6419863, 628402, 6329364, 4344442, 5204623, 5408290, 6371170, 6354037, 6359353, 6381005, 5402104, 5825319, 6381123, 6343373, 4665488, 2594786, 6279839, 6278395, 4815872, 5889217, 6086731, 6069680, 6404857, 5278927, 6329587, 6014952, 6363324, 6291925, 6081006, 6403657, 6019559, 5889440, 5312411, 4171563, 5964797, 4665915, 6271172, 6130093, 6412150, 6369952, 6139163, 5316494, 4512627, 5910873, 6369272, 5212059, 6178429, 4091841, 6058838, 6327295, 6108333, 5801442, 6348441, 6411516, 4970717, 1748891, 6409637, 5619537, 6335684, 5282319, 6179978, 6393187' 

    def handle(self, *args, **options):
        ibo=args[0] # IBO to gather info for
        user = User.objects.get(username=ibo)

        # Now get subscriptions
        subscriptions = SubscriptionProduct.objects.all()
        
        #split into a list or something the string above
        ibo_list = self.uid_list.split(", ")

        # Now sort them 
        product_ids = []
        for subscription in subscriptions:
            #users = get_users_for_subscription(subscription.product.pk)
            product_ids.append(subscription.product.pk)
            #sorting_dict[subscription.pk] = []

            #for u in users:
                #if User.objects.get(pk=u).username in ibo_list:
                    #sorting_dict[subscription.pk].append(u)

        # Now you've got the necessary info. Sort and present in a nice table or something
        all_ois = OrderItem.objects.filter(product_id__in=product_ids, order__user__username__in=ibo_list)

        
        all_comms=Commission.objects.filter(order_item__product_id__in=product_ids, order_item__order__user__in=ibo_list)


        import pdb; pdb.set_trace()


        # Now go through all of the order items and add quantities
        result_dict = {}
        for oi in all_ois:
            if result_dict.get(oi.product.pk, None):
                result_dict[oi.product.pk] += oi.quantity
            else:
                result_dict[oi.product.pk] = oi.quantity


        for key, val in result_dict.items():
            #result[key] = {
                    #"subscription_name": SubscriptionProduct.objects.get(pk=key).product.name,
                    #"user_total": len(val)
                    #}
            print "Subscription Name: %s, # of Users: %d, " % (SubscriptionProduct.objects.get(pk=key).product.name, val)
