from django.contrib import admin
from models import *

class ReportFilterInline(admin.TabularInline):
    model = ReportFilter

class ReportAdmin(admin.ModelAdmin):
    inlines = [
        ReportFilterInline,
    ]

admin.site.register( Report, ReportAdmin )
