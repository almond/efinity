from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from marvin.utils import uuid

class Report(models.Model):
    GRAPH_TYPES = (
        ('line', 'Line Chart'),
        ('column', 'Bar/Column Chart'),
        ('pie', 'Pie Chart'),
        ('gauge', 'Gauge'),
    )

    def __unicode__(self):
        return self.name

    def column_count(self):
        return len(self.columns)

    @property
    def columns(self):
        return self.display_columns.split('\n')

    def generate_results(self, filter=None):
        if filter is None:
            filter = '1 = 1'
        else:
            filter = unicode( filter )
            
        query = self.query + ' ' + filter

        from django.db import connection, transaction
        cursor = connection.cursor()        
        cursor.execute(query)
        results = cursor.fetchall()

        return results

    name = models.CharField(max_length=255)
    slug = models.SlugField(blank=True, help_text='Enter a slug if you are using the same domain for multiple replicated sites.')

    query = models.TextField(help_text='Enter the query you would like to have reported on.')
    display_columns = models.TextField(help_text='Enter each column name on a new line.')

    graph_type = models.CharField(max_length=50, choices=GRAPH_TYPES)

    description = models.TextField(blank=True, help_text='Description of what this report displays.')

    is_public = models.BooleanField(_('Available to Users'), default=True, help_text='If true then any registered user can access this report.' )
    is_active = models.BooleanField(_('active'), default=True, help_text='If active then this report will appear for users and admins.')


class ReportFilter(models.Model):
    def __unicode__(self):
        return self.query

    report = models.ForeignKey(Report, related_name='filters')
    name = models.CharField(max_length=50)
    query = models.TextField()
