from django import template

register = template.Library()


@register.filter
def get_pool_value(pool_dict, key):
    """Get the key from the passed dict"""
    return pool_dict[key]
