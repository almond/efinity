from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from marvin.utils.fields import CurrencyField
from marvin.store.shop.models import Order, OrderItem
from marvin.addons.genealogy.models import Tree, AccessLevel


class CompensationLevel(models.Model):
    name = models.CharField(max_length=30)
    
    def __unicode__(self):
        return self.name


class Plan(models.Model):
    class Meta:
        verbose_name = _('compensation plan')

    tree = models.ForeignKey(Tree)
    name = models.CharField(max_length=50)
    depth_level = models.IntegerField(help_text=_("The depth of downline to pay out to"))

    def __unicode__(self):
        return self.name


class Commission(models.Model):
    class Meta:
        verbose_name = _('commission')
        verbose_name_plural = _('commissions')

    def __unicode__(self):
        return 'Commission to %s for %s' % ( str( self.user ), str( self.amount ) )

    COMMISSION_TYPES = (
        (1, 'Compensation Payout'),
        (2, 'Compensation Plan Payout'),
        (8, 'Commission Fixture by Admin'),
        (9, 'Compensation Withdrawal by User'),
    )

    type = models.IntegerField(choices=COMMISSION_TYPES)
    # piggy = models.ForeignKey(PiggyBank, verbose_name="Comission Bank", related_name='commissions')
    order_item = models.ForeignKey(OrderItem)
    plan = models.ForeignKey(Plan, blank=True, related_name='commissions')
    user = models.ForeignKey(User, related_name='commissions')
    amount = models.DecimalField(decimal_places=4, max_digits=18, help_text=_("The value of the commission, this can be a negative number to negate a balance"))
    comment = models.CharField(max_length=255, blank=True, null=True)
    gen_level = models.IntegerField(blank=True, null=True)
    comp_level = models.ForeignKey(CompensationLevel, blank=True, null=True)
    points = models.DecimalField(verbose_name="Points generated", 
                                 max_digits=18, decimal_places=4, help_text=_("Points generated from this order item"))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    
class ValueManager(models.Manager):
    def try_get(self, access_level, depth_level):
        try:
            return self.get(plan=plan, access_level=access_level, depth_level=depth_level)
        except self.DoesNotExist:
            pass

        try:
            return self.get(plan=plan, access_level=None, depth_level=depth)
        except self.DoesNotExist:
            pass

        try:
            return self.get(plan=plan, access_level=access_level, depth_level=None)
        except self.DoesNotExist:
            pass

        try:
            return self.get(plan=plan)
        except self.DoesNotExist:
            pass

        return None


class Value(models.Model):
    class Meta:
        verbose_name = _('compensation plan value')
        unique_together = (
            ('plan', 'compensation_level', )
            )

    def __unicode__(self):
        return '%s - %s' % ( str( self.plan ), str( self.compensation_level ) )

    objects = ValueManager()

    RETURN_TYPES = (
        ('static', _('Always give the amount provided (Flate rate)')),
        ('percent', _('Give a percentage of the qualifying amount')),
    )

    plan = models.ForeignKey(Plan, related_name = 'values')

    compensation_level = models.ForeignKey(CompensationLevel, verbose_name='Compensation Level',
           blank=True, null=True)

    return_value = models.DecimalField(decimal_places=2, max_digits=15, help_text=_('This can be a percentage or a number'))
    return_type = models.CharField(max_length=30, choices=RETURN_TYPES, help_text=_("Select the method of calculation."))


Order.add_to_class('distributable_profit', CurrencyField(_("Line distributable profit"), max_digits=18, decimal_places=10, blank=True, null=True))
OrderItem.add_to_class('distributable_profit', CurrencyField(_("Line distributable profit"), max_digits=18, decimal_places=10, blank=True, null=True))


# Start listening on Order Save
import listeners
listeners.start_default_listening()