from django.contrib import admin
from models import *

# class PinCompensationLevelInline(admin.TabularInline):
#     model = PinCompensationLevel
# 
# class CompensationLevelAdmin(admin.ModelAdmin):
#     inlines = [ PinCompensationLevelInline, ]

class ValueInline(admin.TabularInline):
    model = Value
    extra = 3

class PlanAdmin(admin.ModelAdmin):
    inlines = [ValueInline, ]
    
    
class CommissionAdmin(admin.ModelAdmin):
    list_display = ['id','user', 'order_item', 'order_id', 'amount', 'gen_level', 'comp_level', 'plan',]
    
    def order_id(self, form):
        url = '/admin/shop/order/%s/' % form.order_item.order.id
        return '<a href="%s">%s</a>' % (url, form.order_item.order.id)
    order_id.allow_tags=True

admin.site.register(Plan, PlanAdmin)
admin.site.register(CompensationLevel)
admin.site.register(Commission, CommissionAdmin)
