from decimal import *

from marvin.store.shop.signals import order_success
from marvin.addons.compensation.models import Commission

def payout_compensation(sender, order=None, **kwargs):
    from marvin.addons.compensation.utils import process_commission
    for item in order.orderitem_set.all():
        product = item.product
        
        # if this is not a product, say, an event
        # do not payout comp here, do it later
        if not product.is_product:
            continue
        
        # If this is a subscription product, do not pay out compensation.
        if 'SubscriptionProduct' in product.get_subtypes():
            continue
        
        # next, if this product has dp
        if not product.compensation_plan or not product.distributable_profit:
            continue

        process_commission(item)
        
        
        # recurse through the users to the depth specified above. static for now.
        # gen_list = {}
        # for x in range(depth):
        #     if user:
        #         # If a user is at the regular pin level, do nothing
        #         user_compensation_level = user.profile_object.pinlevel.compensation_level
        #         if user_compensation_level:                
        #             gen_credits = CREDITS_PER_GENERATION[x][1]
        #             gen_pctg = Decimal()
        #             gen_pctg = Decimal(gen_credits) / Decimal(TOTAL_CREDITS)
        #         
        #             total_level_amount = dp_level_amount[user_compensation_level]
        #         
        #             # Set decimal precision down to 2 points
        #             getcontext().prec = 2
        #             user_compensation = total_level_amount * gen_pctg
        #             # Set decimal precision back to default for future calculations
        #             getcontext().prec = 28
        #         
        #             # save the commission
        #             commission = Commission()
        #             commission.order_item = item
        #             commission.type = 1
        #             commission.plan = product.compensation_plan
        #             commission.user = user
        #             commission.amount = user_compensation
        #             commission.save()
        #         
        #         user = user.upline
        #     else:
        #         # No user, we are at the top of the upline and no need to go through the
        #         # Rest of the loop
        #         break
            
            
            

        # while should_continue:
        #     # make sure user exists
        #     if not user:
        #         should_continue = False
        #         break
        # 
        #     access_level = user.compensation_group
        #     comp_value = Value.objects.try_get(access_level=access_level, depth_level=depth)
        #     
        #     # wtf? no comp I guess
        #     if not comp_value:
        #         continue
        # 
        #     # go deeper
        #     depth = depth + 1
        #     user = user.upline

    # now we have a total dp, now we
    # need to divide this out
    # 
    # dp_30pct = dp * 0.30
    # dp_70pct = dp * 0.70
    # 
    # # now we need to go through the upline to
    # # payout compensation for people's downline
    # 
    # user = order.user
    # depth = 0
    # 
    # while should_continue is True:
    #     # get user's pin-level
    #     user.comp_group
    # 
    #     # payout compensation
    #     a
    # 
    #     # continue to go to this upline
    #     depth = depth + 1
    # 
    #     # now go to user's upline
    #     user = user.upline
    #     if not user:
    #         should_continue = False
    #         break

def start_default_listening():
    order_success.connect(payout_compensation)