def IF(var, if_true, if_false):
    if var:
        return if_true
    else:
        return if_false
    
CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION = [
    100,        # Gen 0
    90,         #     1
    80,         # Gen 2
    70,         #     3
    60,         #     4
    50,         # Gen 5
    40,         #     6
    30,         #     7
    20,         #     8
    10,         #     9
    5           # Gen 10
]

CREDITS_PER_100_IN_EVENT_SALES_BY_GENERATION = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION

SandboxB21 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[0]
SandboxB22 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[1]
SandboxB23 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[2]
SandboxB24 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[3]
SandboxB25 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[4]
SandboxB26 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[5]
SandboxB27 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[6]
SandboxB28 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[7]
SandboxB29 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[8]
SandboxB30 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[9]
SandboxB31 = CREDITS_PER_100_IN_TOOL_SALES_BY_GENERATION[10]

BonusPoolSplitsTools = {
    # needs to add up to one hundred percent (100%)
    'Platinum': 50,
    'Emerald': 30,
    'Diamond': 20,
}

BonusPoolSplitsEvents = {
    'Platinum': 0,
    'Emerald': 50,
    'Diamond': 50,
}

SanboxB11 = BonusPoolSplitsTools['Platinum']
SanboxB12 = BonusPoolSplitsTools['Emerald']
SanboxB13 = BonusPoolSplitsTools['Diamond']

wkshtB3 = SandboxB21
wkshtB6 = 0 # Total Dist Profit for Tools
wkshtB7 = 0 # Total Dist Profit for Events

wkshtB11 = SanboxB11 # 50%
wkshtB12 = 35862 # wkshtB11 * wkshtB6
wkshtB17 = 267698
