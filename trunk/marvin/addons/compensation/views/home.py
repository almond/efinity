from datetime import date, datetime
from django.contrib.auth.decorators import login_required
from django.db import connection
from django.db.models import Sum
from django.contrib.contenttypes.models import ContentType
from marvin.addons.compensation.utils import calculate_comp_by_month_year, \
    get_month_start_end
from marvin.utils.app import *
from marvin.addons.compensation.models import Commission
from decimal import Decimal
# from marvin.addons.compensation.models import PiggyBank, PiggyBankAccount

def spanning_months(start, end):
    assert start <= end
    current = start.year * 12 + start.month - 1
    end = end.year * 12 + end.month - 1
    while current <= end:
        yield date(current // 12, current % 12 + 1, 1)
        current += 1

class Month(object):
    dt = None
    content_type = ContentType.objects.get(app_label="planning", model="convention")

    def __init__(self, user, dt):
        self.user = user
        self.dt = dt
        self.tools = Commission.objects.filter(user=user, 
            created__year=dt.year, 
            created__month=dt.month).exclude(order_item__content_type=self.content_type).distinct().aggregate(total=Sum("amount"))["total"]
        self.events = Commission.objects.filter(order_item__content_type=self.content_type,
            created__year=dt.year, created__month=dt.month, user=user).distinct().aggregate(total=Sum("amount"))["total"]

        # Check for nonetypes?
        if not self.tools:
            self.tools = Decimal("0")
        if not self.events:
            self.events = Decimal("0")
        self.total = self.tools + self.events

    @property
    def title(self):
        return self.dt.strftime('%Y-%m')


class DetailMonth(object):
    def __init__(self, commission):
        self.order_id = commission["order_item__order__id"]
        self.commission = commission["total"]
        self.created = commission["created"]
        

@login_required
def home(request):
    
    months = []
    for month in spanning_months( request.user.date_joined, datetime.now() ):
        months.append(Month(request.user, month))

    piggybankaccounts = []

    # for piggy in PiggyBank.objects.all():
    #     piggybankaccounts.append(
    #         PiggyBankAccount(
    #             piggy = piggy,
    #             user = request.user,
    #         )
    #     )

    #commissions = Commission.objects.filter(user = request.user)

    return render(request, 'compensation/home.html', {
        # 'piggybankaccounts': piggybankaccounts,
        'months': months,
        })

# @login_required
# def month_detail(request, year, month):
#     from django.core.paginator import Paginator, InvalidPage, EmptyPage
    
#     year = int(year)
#     month = int(month)
    
#     user = request.user
#     month_start, month_end = get_month_start_end(year, month)

#     """Need to see if it can be achieved by the default django ORM without raw sql"""
#     cursor = connection.cursor() #@UndefinedVariable
#     sql = """SELECT
#               `shop_order`.id as order_id,
#                sum(compensation_commission.amount) as commission,
#                compensation_commission.created as created
#              FROM
#               `compensation_commission`,
#               `shop_orderitem`,
#               `shop_order`
#              where
#               compensation_commission.order_item_id=shop_orderitem.id and 
#               shop_orderitem.order_id=shop_order.id and 
#               shop_orderitem.time_stamp >= '%s' and
#               shop_orderitem.time_stamp < '%s' and
#               compensation_commission.user_id = %d
#               group by `shop_order`.id""" % (str(month_start), str(month_end), user.id)
    
#     cursor.execute(sql)
#     desc = cursor.description
#     all_commissions=[
#         dict(zip([col[0] for col in desc], row))
#         for row in cursor.fetchall()
#     ]
#     paginator = Paginator(all_commissions, 50)
    
#     try:
#         page = int(request.GET.get('page', '1'))
#     except ValueError:
#         page = 1
    
#     try:
#         month_commissions = paginator.page(page)
#     except (EmptyPage, InvalidPage):
#         month_commissions = paginator.page(paginator.num_pages)
    
#     return render(request, 'compensation/month_detail.html', {
#         'month_commissions': month_commissions,
#         })

@login_required
def month_detail(request, year, month):
    from django.core.paginator import Paginator, InvalidPage, EmptyPage
    year = int(year)
    month = int(month)
    
    # commissions = Commission.objects.filter(user=request.user, 
    #     created__year=year, created__month=month).distinct().values("order_item__order__id", "created").annotate(total=Sum("amount"))

    commissions = Commission.objects.values("order_item__order__id", "created").annotate(total=Sum("amount")).filter(user=request.user, 
        created__year=year, 
        created__month=month).distinct()

    all_commissions = list()
    for c in commissions:
        all_commissions.append(DetailMonth(c))
    
    paginator = Paginator(all_commissions, 50)
    
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    
    try:
        month_commissions = paginator.page(page)
    except (EmptyPage, InvalidPage):
        month_commissions = paginator.page(paginator.num_pages)

    return render(request, 'compensation/month_detail.html', {
        'month_commissions': month_commissions,
        })
