from decimal import *

from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.contrib.contenttypes.models import ContentType
from django.db.models import Sum

from marvin.utils.app import *
from marvin.addons.compensation.models import Commission
from marvin.addons.planning.models import Convention, Ticket
from marvin.addons.support.models import EventExpensesCategory, EventExpenses
from marvin.store.shop.models import Cart, OrderItem, OrderPayment
from marvin.addons.compensation.utils import process_commission

def process_event_commissions(request):
    if request.POST:
        event_id = request.POST.get('event_id')
        con = Convention.objects.get(pk=event_id)

        ct = ContentType.objects.get(app_label='planning', model='convention')

        order_items = OrderItem.objects.filter(content_type=ct,
            product_id=con.pk, completed=False, order__payments__isnull=False).exclude(order__status='Cancelled').distinct()
        ticket_count = 0
        ticket_total = 0

        order_tracker = []
        for oi in order_items:
            if oi.order.balance == 0  and oi.product:
                if not oi.order.pk in order_tracker:
                    ticket_total += oi.order.total
                    order_tracker.append(oi.order.pk)
                ticket_count = ticket_count+oi.quantity

        exp_cats = EventExpensesCategory.objects.filter(convention=con)
        exps = EventExpenses.objects.filter(category__in=exp_cats)
        total_expenses = 0
        for e in exps:
            total_expenses += e.amount

        pph = (ticket_total - total_expenses) / ticket_count
        # use float formatting to round the pph and then convert back to Decimal
        pph = "%.2f" % pph
        pph = Decimal(pph)
        Commission.objects.filter(order_item=oi, type=1).delete()


        items_to_process = len(order_items)
        print "Items to process %d" % items_to_process

        for oi in order_items:
            if oi.order.balance == 0:
                print "%d items left" % items_to_process
                print "ORDER ID: %d, ITEM ID: %d, IBO: %s" % (oi.order.pk, oi.pk, oi.order.user.username)
                items_to_process -= 1
                process_commission(oi, passed_dp=pph * oi.quantity)

        # Now check everything's fine
        commissions = Commission.objects.filter(order_item__in=order_items)
        commission_total = commissions.aggregate(total=Sum("amount"))["total"]
        total_give_away = ticket_total - total_expenses

        if total_give_away > Decimal("0.00"):
            if commission_total > total_give_away:
                print "Adjusting more than"
                try:
                    difference = commission_total - total_give_away
                    remainder = difference / len(commissions)
                    for com in commissions:
                        com.amount = com.amount - remainder
                        com.save()
                except TypeError:
                    pass
                # difference = commission_total - total_give_away
                # remainder = difference / len(commissions)
                # for com in commissions:
                #     com.amount = com.amount - remainder
                #     com.save()
            elif commission_total < total_give_away:
                print "Adjusting less than"
                try:
                    difference = total_give_away - commission_total
                    remainder = difference / len(commissions)
                    for com in commissions:
                        com.amount = com.amount + remainder
                        com.save()
                except TypeError:
                    pass
                # difference = total_give_away - commission_total
                # remainder = difference / len(commissions)
                # for com in commissions:
                #     com.amount = com.amount + remainder
                #     com.save()


        print "Processing finished!! Rendering report that takes ages for some reason"

        con.compensation_paid = True
        con.save()

        return render(request, 'compensation/process_event_commissions.html', {
        })
    else:
        raise Http404('Event not found')
