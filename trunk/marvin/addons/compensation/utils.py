# from marvin.addons.compensation import *
from marvin.addons.compensation.models import Commission, Plan
from decimal import *
import datetime

def get_month_start_end(year, month):
    month_start = datetime.datetime(year, month, 1, 0, 0, 0, 0)
    one_microsecond = datetime.timedelta(0, 0, 1)

    if month + 1 > 12:
        month = 1
        year = year + 1
    else:
        month = month + 1

    month_end = datetime.datetime(year, month, 1, 0, 0, 0, 0)
    month_end = month_end - one_microsecond

    return (month_start, month_end)


def calculate_comp_by_month_year(user, month, year):
    (month_start, month_end) = get_month_start_end(year, month)

    month_commissions = Commission.objects.filter(user=user, created__gte=month_start, created__lte=month_end)

    total_tools_payment = 0
    total_events_payment = 0

    tool_plan = Plan.objects.get(name__icontains='tool sales')
    event_plan = Plan.objects.get(name__icontains='event sales')
    for mc in month_commissions:
        if mc.plan == tool_plan:
            total_tools_payment += mc.amount
        elif mc.plan == event_plan:
            total_events_payment = mc.amount

    total_monthly_payment = total_tools_payment + total_events_payment

    total_monthly_payment = total_tools_payment + total_events_payment
    return (total_tools_payment, total_events_payment, total_monthly_payment)


# Should this be configurable in the database as well? Leave static here for now. Zenos 2011-10-06
CREDITS_PER_GENERATION = (
    (0, 200),
    (1, 50),
    (2, 21),
    (3, 15),
    (4, 9),
    (5, 3),
)

TOTAL_CREDITS = 298

# This is related to the DR EF site thing. Let's see if it works

def process_commission(item, passed_dp=None, commission_date=None):
    # Added new flag (event_profit) to pass the amount that's supposed to be matched.. If it isn't
    # The remainder should be added divided evenly between every
    # compensated user. Maths are hard

    product = item.product
    # now get the comp plan for this product
    try:
        plan = product.compensation_plan
    except:
        return

    # No plan for this product, we cannot continue
    if not plan:
        return

    # dp is passed in if this is an event. Otherwise we grab it from the product.
    if passed_dp:
        dp = passed_dp
    else:
        dp = product.distributable_profit
        if dp and dp != 0:
            dp = product.distributable_profit * item.quantity
        else:
            # if no DP, no need to continue, and no one gets paid!
            return

    # Grab the compensation levels from the compensation plan.
    level_values = plan.values.all()

    # Get the total commission amount for each level stored in the database
    dp_level_amount = {}
    for lv in level_values:
        if lv.return_type == 'percent':
            dp_level_amount[lv.compensation_level] = dp * (lv.return_value / 100)
        elif lv.return_type == 'static':
            dp_level_amount[lv.compensation_level] = lv.return_value

    root_user = item.order.user
    depth = plan.depth_level

    compensation_level_heirarchy = {
        "platinum": 0,
        "emerald": 1,
        "diamond": 2,
    }

    # In the future, we need to wrap this in transaction management, because
    # We could potentionally fail on the 5th save of a commission, and then
    # Have 4 commissions that should be rolled back.

    comp_list = {}
    # Go throught the values this products's compensation plan has and create a list
    # of people that should get compensation
    for pv in plan.values.all():
        user = root_user
        comp_list[pv.compensation_level] = {}
        comp_level = comp_list[pv.compensation_level]
        comp_level["total_payout"] = dp_level_amount[pv.compensation_level]
        comp_level["total_credits"] = 0
        comp_level["gens"] = {}

        gen_level = 0
        while gen_level < 6 and user:
            # User 0 should only get compensation if no other people are in the pool.
            if user.aid == 0:
                if gen_level == 0:
                    # give user 0 commission.
                    comp_level["gens"][gen_level] = {}
                    comp_level["gens"][gen_level]["user"] = user
                    comp_level["gens"][gen_level]["credits"] = CREDITS_PER_GENERATION[gen_level][1]
                    comp_level["total_credits"] = comp_level["total_credits"] + CREDITS_PER_GENERATION[gen_level][1]
                else:
                    gen_level = 6
                    continue
                # Check to see if user has compensation plan
            user_compensation_level = user.profile_object.pinlevel.compensation_level
            if user_compensation_level:
                # Check if the user can be put into this pool
                user_cl = compensation_level_heirarchy[user_compensation_level.name.lower()]
                plan_cl = compensation_level_heirarchy[pv.compensation_level.name.lower()]
                if user_cl >= plan_cl:
                    comp_level["gens"][gen_level] = {}
                    comp_level["gens"][gen_level]["user"] = user
                    comp_level["gens"][gen_level]["credits"] = CREDITS_PER_GENERATION[gen_level][1]
                    comp_level["total_credits"] = comp_level["total_credits"] + CREDITS_PER_GENERATION[gen_level][1]
                    gen_level = gen_level + 1
            user = user.upline

    #
    # Now that we have all the info for generations, calculate commissions:
    for complevel_object, value in comp_list.iteritems():
        comp_level = value
        total_credits = comp_level["total_credits"]
        payout = comp_level["total_payout"]
        for key, gen in comp_level["gens"].iteritems():
            user_credits = gen["credits"]
            user = gen["user"]

            # use float formatting to round the compensation properly and then convert back to Decimal
            points_total = Decimal(user_credits) / Decimal(total_credits)
            user_compensation = payout * points_total
            user_compensation = "%.2f" % user_compensation
            user_compensation = Decimal(user_compensation)

            commission = Commission()
            commission.order_item = item
            commission.type = 1
            commission.plan = product.compensation_plan
            commission.user = user

            debug_counter = 0

            if user_compensation <= Decimal("0.00"):
                debug_counter += 1
                commission.amount = Decimal("0.00")
                commission.points = Decimal("0.00")
                print "It actually gets here %d" % debug_counter
            else:
                commission.amount = user_compensation
                commission.points = user_credits

            commission.gen_level = key
            commission.comp_level = complevel_object
            commission.save()
            if commission_date:
                commission.created = datetime.date(int(commission_date[0]), int(commission_date[1]),
                                                   int(commission_date[2]))
                commission.save()
