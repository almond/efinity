from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.addons.compensation.views',
    (r'^$', 'home.home', {}, 'comp_home'),
    url(r'^month-detail/(?P<year>[\d]+)/(?P<month>[\d]+)', 'home.month_detail', {}, 'comp_month_detail'),
    url(r'^process-event-commissions/', 'payout.process_event_commissions', {}, 'process_event_commissions'),
)
