from django.db import models

class Note(models.Model):

    user = models.ForeignKey( User, related_name = 'notes' )
    created_by = models.ForeignKey( User, related_name = 'notes_created' )

    note = models.TextField()

    created = models.DateTimeField( auto_now=true )
