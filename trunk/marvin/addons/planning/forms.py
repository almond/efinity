from django import forms
from django.contrib.auth.models import User
from marvin.addons.planning.models import Attending, Convention


class ConventionSearchForm(forms.Form):
    ticket_number = forms.CharField(required=False, max_length=20, label="Ticket Number")
    ibo_number = forms.IntegerField(required=False, label="IBO Number")
    user_name = forms.CharField(required=False, max_length=300, label="User Name")
    

class AttendingSearchForm(forms.Form):
    CHOICES = []
    for event in Convention.objects.all():
        CHOICES.append((event.id, event))
        
    event = forms.ChoiceField(choices=CHOICES)
    number = forms.CharField(max_length=20, label="Enter IBO number or Ticket Number")
    
    
class AttendingForm(forms.ModelForm):
	class Meta:
		model=Attending
		exclude = ('num_checked_in',)
	# Override a couple widgets/labels on the form
	user = forms.ModelChoiceField(User.objects.all(), widget=forms.HiddenInput() )
	event = forms.ModelChoiceField(Convention.objects.all(), widget=forms.HiddenInput() )
	num_checking_in = forms.IntegerField(label="Number of Ticket Holders currently checking in")
	
	"""
	def clean(self):
		cleaned_data = self.cleaned_data
		customer = cleaned_data.get('user')
		event = cleaned_data.get('event')
		num_checking_in = cleaned_data.get('num_checking_in') or 0

		attending = Attending.objects.get(event=event, user=customer)
				
		# total up the numbers of tickets purchased.  These could have been on different orders
		event_orders = customer.conventions(convention_id=event.id)
		if event_orders:
			num_tickets = 0
			event = event_orders[0].product
			for order in event_orders:
				num_tickets += order.quantity
				
			spots_left = num_tickets - attending.num_checked_in
		
		if num_checking_in + attending.num_checked_in > num_tickets:
			raise forms.ValidationError("%s only has %s remaining tickets.  You tried to check in %s people " % (customer.get_full_name(), spots_left, num_checking_in) )
			
		return cleaned_data
	"""