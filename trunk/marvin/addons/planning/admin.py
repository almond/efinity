from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from marvin.store.products.admin import *
from models import *
from django.conf.urls.defaults import *


#added echung 9/5/11, per docs at http://www.theotherblog.com/Articles/2009/06/02/extending-the-django-admin-interface/
class ButtonableModelAdmin(admin.ModelAdmin):
    """
    A subclass of this admin will let you add buttons (like history) in the
    change view of an entry.
    
    ex.
    class FooAdmin(ButtonableModelAdmin):
       ...
    
       def bar(self, obj):
          obj.bar()
       bar.short_description='Example button'
    
       buttons = [ bar ]
    
    you can then put the following in your admin/change_form.html template:
    
       {% block object-tools %}
       {% if change %}{% if not is_popup %}
       <ul class="object-tools">
       {% for button in buttons %}
          <li><a href="{{ button.func_name }}/">{{ button.short_description }}</a></li>
       {% endfor %}
       <li><a href="history/" class="historylink">History</a></li>
       {% if has_absolute_url %}<li><a href="../../../r/{{ content_type_id }}/{{ object_id }}/" class="viewsitelink">View on site</a></li>{% endif%}
       </ul>
       {% endif %}{% endif %}
       {% endblock %}
    
    """
    buttons=[]

    def change_view(self, request, object_id, extra_context={}):
        extra_context['buttons']=self.buttons
        return super(ButtonableModelAdmin, self).change_view(request, object_id, extra_context)

    def __call__(self, request, url):
        if url is not None:
            import re
            res=re.match('(.*/)?(?P<id>\d+)/(?P<command>.*)', url)
            if res:
                if res.group('command') in [b.func_name for b in self.buttons]:
                    obj = self.model._default_manager.get(pk=res.group('id'))
                    getattr(self, res.group('command'))(obj)
                    return HttpResponseRedirect(request.META['HTTP_REFERER'])

        return super(ButtonableModelAdmin, self).__call__(request, url)

class CountryAdmin( ButtonableModelAdmin ):

    def my_button(self, obj):
        "do something here"
        url = "/admin" + "/".join(str( obj._meta ).split(".")) + "/" + str(obj.id) + "/"
        return HttpResponseRedirect( url )
    my_button.url = "planning/attending/add/"    #puts this on the end of the admin URL.
    my_button.short_description='Check-in User'
    buttons = [ my_button,]  


class Price_Inline(admin.TabularInline):
    model = Price
    extra = 2


class ConventionImage_Inline(admin.TabularInline):
    model = ConventionImage


class ConventionAdmin(admin.ModelAdmin):
    def make_active(self, request, queryset):
        rows_updated = queryset.update(active=True)
        if rows_updated == 1:
            message_bit = _("1 event was")
        else:
            message_bit = _("%s events were" % rows_updated)
        self.message_user(request, _("%s successfully marked as active") % message_bit)
        return HttpResponseRedirect('')
    make_active.short_description = _("Mark selected events as active")

    def make_inactive(self, request, queryset):
        rows_updated = queryset.update(active=False)
        if rows_updated == 1:
            message_bit = _("1 event was")
        else:
            message_bit = _("%s events were" % rows_updated)
        self.message_user(request, _("%s successfully marked as inactive") % message_bit)
        return HttpResponseRedirect('')
    make_inactive.short_description = _("Mark selected events as inactive")

    def make_featured(self, request, queryset):
        rows_updated = queryset.update(featured=True)
        if rows_updated == 1:
            message_bit = _("1 event was")
        else:
            message_bit = _("%s events were" % rows_updated)
        self.message_user(request, _("%s successfully marked as featured") % message_bit)
        return HttpResponseRedirect('')
    make_featured.short_description = _("Mark selected events as featured")

    def make_unfeatured(self, request, queryset):
        rows_updated = queryset.update(featured=False)
        if rows_updated == 1:
            message_bit = _("1 event was")
        else:
            message_bit = _("%s events were" % rows_updated)
        self.message_user(request, _("%s successfully marked as not featured") % message_bit)
        return HttpResponseRedirect('')
    make_unfeatured.short_description = _("Mark selected events as not featured")
    

    list_display = ('name', 'unit_price', 'items_in_stock', 'active', 'featured', 'checkin_url', 'nearest_occurance_start')
    list_filter = ('date_added','active','featured')
    actions = ('make_active', 'make_inactive', 'make_featured', 'make_unfeatured')
    fieldsets = (
    (None, {'fields': ('site', 'name', 'slug', 'sku', 'speaker', 'location', 'type', 'language', 'address', 'description', 'short_description',  'start', 'end', 'repeating', 'repeats', 'repeat_ends', 'deadline',
            'active', 'featured', 'can_book', 'items_in_stock', 'max_tickets_per_user', 'total_sold', 'ordering',
            'registration_application', 'compensation_plan',
            )}), (_('Meta Data'), {'fields': ('meta',), 'classes': ('collapse',)}),
            #(_('Compensation'), {'fields':('compensation_plan','distributable_profit'),'classes':('collapse',)}),
        )
    search_fields = ['slug', 'sku', 'name']
    inlines = [Price_Inline, ConventionImage_Inline]

    filter_horizontal = ()#'category',)
    
    
    def checkin_url(self, form):
        url = '/admin/planning/ticket/checkin/%s' % form.pk
        return '<a href="%s">%s</a>' % (url, 'Checkin')
    checkin_url.allow_tags=True


class TicketAdmin(admin.ModelAdmin):
    list_display = ('event','user', 'user_full_name', 'ticket_number', 'created')
    
    list_filter = ('event', 'user', 'user__username', 'seating_chart_section')
    readonly_fields = ['user', 'event', 'orderitem']
    search_fields = ['ticket_number', 'user__username', 'user__first_name', 'user__last_name', 'event__name']
    def user_full_name(self, obj):
        return ("%s" % obj.user.get_full_name())
    user_full_name.short_description = 'Name'
    
    # Overrride the default admin URLs for this model
    def get_urls(self):
        urls = super(TicketAdmin, self).get_urls()
        my_urls = patterns('marvin.addons.support.views.planning_admin_views',
            (r'^checkin/(?P<convention_id>\d+)/$', 'checkin_search', {}, 'checkin_search'),
            (r'^checkin/(?P<convention_id>\d+)/(?P<user_aid>\d+)/$', 'checkin_user', {}, 'checkin_user'),
            (r'^checkin/success/$', 'success', {}, 'checkin_successful'),
        )
        return my_urls + urls

class AttendingAdmin(admin.ModelAdmin):
    
    list_display = ('event','user')
    list_filter = ('event',)
    fieldsets = (
                (None, {'fields': ('event','user')}),
                )
                
    # Overrride the default admin URLs for this model
    def get_urls(self):
        urls = super(AttendingAdmin, self).get_urls()
        my_urls = patterns('marvin.addons.support.views.planning_admin_views',
            (r'^$', 'home', {}, 'checkin_home'),
            (r'^checkin/$', 'checkin', {}, 'support_checkin'),
            (r'^checkin/success/$', 'success', {}, 'checkin_successful'),
        )
        return my_urls + urls
                 

class SeatingChartSectionAdmin(admin.StackedInline):
    model = SeatingChartSection
    template = "planning/chart-section-inline.html"
    
class SeatingChartAdmin(admin.ModelAdmin):
    inlines = [SeatingChartSectionAdmin,]

        
admin.site.register( Convention, ConventionAdmin )
# admin.site.register( Attending, AttendingAdmin )
admin.site.register(Ticket, TicketAdmin)
admin.site.register( SeatingChart, SeatingChartAdmin)
admin.site.register(SeatingColor)
