from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.addons.planning.views',
    (r'^$', 'home', {}, 'planning_home'),
    (r'^list/$', 'event_list', {}, 'event_list'),
    (r'^(?P<event_id>\d+)/$', 'details', {}, 'planning_details'),
    (r'^search/$', 'search_events', {}, 'event_search'),
)

