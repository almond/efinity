import csv, datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseForbidden
from django.template.defaultfilters import slugify
from django.db.models.loading import get_model

from marvin.addons.planning.models import Convention
from marvin.store.products.util import paginate
from django.utils import simplejson

#~ from marvin.utils.search import perform_search

def _get_checked(request):
    # Python2.4
    checked = {
        'convent':'',
        'seminar':'',
        'meeting':'',
        'special':'',
        'english':'',
        'spanish':'',
    }
    if 'Conventions'   in request.GET.getlist('type')    :checked['convent'] ='checked="checked"'
    if 'Seminars'      in request.GET.getlist('type')    :checked['seminar'] ='checked="checked"'
    if 'Open Meetings' in request.GET.getlist('type')    :checked['meeting'] ='checked="checked"'
    if 'Special Event' in request.GET.getlist('type')    :checked['special'] ='checked="checked"'
    if 'English'       in request.GET.getlist('language'):checked['english'] ='checked="checked"'
    if 'Spanish'       in request.GET.getlist('language'):checked['spanish'] ='checked="checked"'

    return checked

def date_range(start, end):
    start=datetime.datetime(start.year, start.month, start.day, 0, 0, 0)
    r = (end+datetime.timedelta(days=1)-start).days
    return [start+datetime.timedelta(days=i) for i in range(r)]

def event_list(request):
    if not request.GET.get('start', None) or not request.GET.get('end', None):
        return HttpResponse('[]')
    start=datetime.datetime.fromtimestamp(float(request.GET.get('start')))
    end=datetime.datetime.fromtimestamp(float(request.GET.get('end')))

    events=Convention.objects.filter(Q(start__gte=start) | Q(start__lte=end), active=True, repeating=False)
    e=[]
    for event in events:
        starts=str(event.nearest_occurance_start)
        ends=''
        if event.nearest_occurance_end is not None:
            ends=str(event.nearest_occurance_end)
        e.append({'id':event.id, 'title':event.name, 'start':starts, 'end':ends, 'allDay':False, 'url':'/user/events/%d/'%(event.id)})

    events=Convention.objects.filter(Q(repeat_ends__gte=start) | Q(repeat_ends=None), active=True, repeating=True)
    dates=date_range(start, end)
    for event in events:
        for date in dates:
            if datetime.datetime(date.year, date.month, date.day, 0, 0, 0) < datetime.datetime(event.start.year, event.start.month, event.start.day, 0, 0, 0):
                continue

            if event.repeat_ends and datetime.datetime(date.year, date.month, date.day, 0, 0, 0) > datetime.datetime(event.repeat_ends.year, event.repeat_ends.month, event.repeat_ends.day, 0, 0, 0):
                continue


            tmp_starts=''
            tmp_ends=''
            show=False
            if event.repeats=='d':
                show=True
            elif event.repeats=='w':
                if date.weekday()==event.start.weekday():
                    show=True
            elif event.repeats=='m':
                if date.day==event.start.day:
                    show=True
            elif event.repeats=='y':
                if date.day==event.start.day and  date.month==event.start.month:
                    show=True

            if show:
                tmp_starts=datetime.datetime(date.year, date.month, date.day, event.start.hour, event.start.minute, event.start.second)
                if event.end and event.end!='':
                    delta=event.end-event.start
                    tmp_ends=tmp_starts+delta
                e.append({'id':event.id, 'title':'%s (Repeating)'%(event.name), 'start':str(tmp_starts), 'end':str(tmp_ends), 'allDay':False, 'url':'/user/events/%d/'%(event.id)})


    return HttpResponse(simplejson.dumps(e))

@login_required
def home(request):
    events = Convention.objects.filter(active=True)
    featured = Convention.objects.filter(featured=True)
    return render_to_response('planning/home.html', {
        'events': events,
        'searchable': events,
        'featured_list': paginate(featured, 1, 5),
    }, context_instance=RequestContext(request))


def daterange(start_date, end_date):
    for n in range((end_date - start_date).days):
        yield start_date + datetime.timedelta(n)


def validate_date(date_str):
    #YYYY-MM-DD
    import time
    try:
        time.strptime(date_str, '%Y-%m-%d')
        return True
    except:
        return False


@login_required
def search_events(request):

    invalid_date=False

    # Type query
    type_query = Q()
    if 'Open Meetings' in request.GET.getlist('type'): type_query.add(Q(type=1), 'OR')
    if 'Seminars'      in request.GET.getlist('type'): type_query.add(Q(type=2), 'OR')
    if 'Conventions'   in request.GET.getlist('type'): type_query.add(Q(type=3), 'OR')
    if 'Special Event' in request.GET.getlist('type'): type_query.add(Q(type=4), 'OR')

    # Language query
    lang_query = Q()
    if 'English' in request.GET.getlist('language'): lang_query.add(Q(language=1), 'OR')
    if 'Spanish' in request.GET.getlist('language'): lang_query.add(Q(language=2), 'OR')

    # Title query
    name_query = {}
    if request.GET.get('name'): name_query = {'name__icontains': request.GET.get('name')}
    if request.GET.get('q'):    name_query = {'name__icontains': request.GET.get('q')}

    # Date query
    date_query = Q()
    if request.GET.get('datefrom') and not request.GET.get('dateto'):
        if validate_date(request.GET.get('datefrom')):
            date_query.add(  ((Q(start__gte=request.GET.get('datefrom')) & Q(repeating=False)) | (Q(repeating=True) & Q(repeat_ends__gte=request.GET.get('datefrom')) )), 'AND')
        else:
            invalid_date=True

    elif request.GET.get('dateto') and not request.GET.get('datefrom'):
        if validate_date(request.GET.get('dateto')):
            date_query.add((Q(start__lte='%s 23:59:59'%(request.GET.get('dateto')))), 'AND')
        else:
            invalid_date=True

    elif request.GET.get('dateto') and request.GET.get('datefrom'):
        if validate_date(request.GET.get('dateto')) and validate_date(request.GET.get('datefrom')):
            date_query.add(
                ( Q(start__gte=request.GET.get('datefrom')) & Q(start__lte='%s 23:59:59'%(request.GET.get('dateto'))) & Q(repeating=False) ),
                'OR'
                )

            start_date=request.GET.get('datefrom').split('-')
            start_date=datetime.datetime(int(start_date[0]), int(start_date[1]), int(start_date[2]))

            end_date=request.GET.get('dateto').split('-')
            end_date=datetime.datetime(int(end_date[0]), int(end_date[1]), int(end_date[2]))

            dates=[]
            dates.append(start_date)
            for r in range((end_date - start_date).days):
                dates.append(start_date+datetime.timedelta(days=r+1))

            #core python datetime, sunday = 6, monday = 0, tuesday=1, wd=2, th=3, fr=4, sat=5
            #django orm, sunday = 1, monday = 2, tu=3, wed=4, thur=5, fri=6, sat=7
            core_python_to_django_orm_weekday={6:1, 0:2, 1:3, 2:4, 3:5, 4:6, 5:7}

            for d in dates:
                weekday=d.weekday()
                weekday=core_python_to_django_orm_weekday[weekday]
                date_query.add(
                    Q(repeating=True) &
                    Q(repeat_ends__gte=request.GET.get('datefrom')) &
                    (
                        (Q(repeats='d')) |
                        (Q(repeats='w') & Q(start__week_day=weekday) ) |
                        (Q(repeats='m') & Q(start__day=d.day)) |
                        (Q(repeats='y') & Q(start__day=d.day) & Q(start__month=d.month))
                        ),
                    'OR'
                    )
        else:
            invalid_date=True

    # Location query
    city_query = {}
    if request.GET.get('location'): city_query = {'location__icontains': request.GET.get('location')}

    search = Convention.objects.filter(active=True).filter(type_query).filter(lang_query).filter(**name_query).filter(**city_query).filter(date_query)

    return render_to_response('planning/results.html', {
        'events':  Convention.objects.filter(active=True).distinct(),
        'results': search.distinct(),
        'checked':_get_checked(request),
        'request': request.GET,
        'invalid_date':invalid_date,
    }, context_instance=RequestContext(request))

@login_required
def details(request, event_id):
    event = Convention.objects.get(pk=event_id)
    events = Convention.objects.filter(active=True)

    return render_to_response('planning/details.html', {
        'event': event,
        'events': events,
        'searchable': events,
    }, context_instance=RequestContext(request))


def export(qs, fields=None):
    model = qs.model
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s.csv' % slugify(model.__name__)
    writer = csv.writer(response)
    # Write headers to CSV file
    if fields:
        headers = fields
    else:
        headers = []
        for field in model._meta.fields:
            headers.append(field.name)
    writer.writerow(headers)
    # Write data to CSV file
    for obj in qs.iterator():
        row = []
        for field in headers:
            if field in headers:
                val = getattr(obj, field)
                if callable(val):
                    val = val()
                if isinstance(val, unicode):
                    row.append(val.encode("utf8"))
                else:
                    row.append(val)
        writer.writerow(row)
    # Return CSV file to browser as download
    return response

def admin_list_export(request, model_name, app_label, queryset=None, fields=None, list_display=True):
    """
    Put the following line in your urls.py BEFORE your admin include
    (r'^admin/(?P<app_label>[\d\w]+)/(?P<model_name>[\d\w]+)/csv/', 'util.csv_view.admin_list_export'),
    """
    if not request.user.is_staff:
        return HttpResponseForbidden()
    if not queryset:
        model = get_model(app_label, model_name)
        queryset = model.objects.all()
        filters = dict()
        for key, value in request.GET.items():
            if key not in ('ot', 'o'):
                filters[str(key)] = str(value)

        if len(filters):
            # This whole thing is not the right way to do it. Anyways
            # I'll be adding my fixing here
            if model_name == "ticket":
                filters["event__name__icontains"] = filters["q"]
                del filters["q"]
            queryset = queryset.filter(**filters)
    if not fields:
        try:
            if list_display and len(queryset.model._meta.admin.list_display) > 1:
                fields = queryset.model._meta.admin.list_display
            else:
                fields = None
        except AttributeError:
            fields = ["event", "user", "user_full_name", "ticket_number"]

    return export(queryset, fields)

    """
    Create your own change_list.html for your admin view and put something like this in it:
    {% block object-tools %}
    <ul class="object-tools">
        <li><a href="csv/{%if request.GET%}?{{request.GET.urlencode}}{%endif%}" class="addlink">Export to CSV</a></li>
    {% if has_add_permission %}
        <li><a href="add/{% if is_popup %}?_popup=1{% endif %}" class="addlink">{% blocktrans with cl.opts.verbose_name|escape as name %}Add {{ name }}{% endblocktrans %}</a></li>
    {% endif %}
    </ul>
    {% endblock %}
    """
