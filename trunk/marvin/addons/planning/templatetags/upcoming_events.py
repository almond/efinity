from marvin.addons.planning.models import Convention
from django.template import Library, Node

register = Library()
     
class UpcomingEventsNode(Node):
    def render(self, context):
        context['upcoming_events_list'] = Convention.objects.all()[:10]
        return ''

@register.tag(name="get_upcoming_events")
def get_upcoming_events(parser, token):
    return UpcomingEventsNode()
