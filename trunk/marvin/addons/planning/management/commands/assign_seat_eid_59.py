import sys
from marvin import unicodecsv
import ftplib
import codecs
import cStringIO
from datetime import date
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from marvin.store.shop.models import Order
from django.db.models import Q
from marvin.addons.planning.models import Convention
from django.conf import settings
import datetime
import re
from django.conf import settings
from marvin.addons.planning.models import Ticket, SeatingChartSection
            
class Command(BaseCommand):
    help='Assign seats to the tickets'

    def handle(self, *args, **options):
        eid=59
        tickets=Ticket.objects.filter(event=eid)
        ticket_id=[]
        for ticket in tickets:
            if ticket.orderitem:
                order=ticket.orderitem.order
                payments=order.payments.all()
                if len(payments)>0:
                    if payments[0].payment=='COD':
                        ticket_id.append(ticket.id)
                    
        Ticket.objects.filter(id__in=ticket_id).update(seating_chart_section=SeatingChartSection.objects.get(color__name='Gold', chart__event=eid))

        seating_color=[{'color':'Red', 'count':400}, {'color':'Blue', 'count':400}, {'color':'Green', 'count':800}]

        seating_color_objects={}
        for value in seating_color:
            seating_color_objects[value['color']]=SeatingChartSection.objects.get(color__name=value['color'], chart__event=eid)

        used=0
        for value in seating_color:
            t=Ticket.objects.values_list('id', flat=True).filter(event=eid).exclude(id__in=ticket_id).order_by('created')[used:(used+value['count'])]
            Ticket.objects.filter(pk__in=list(t)).update(seating_chart_section=seating_color_objects[value['color']])
            used=used+value['count']
