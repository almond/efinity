from django.core.management.base import BaseCommand

from marvin.addons.planning.models import Ticket, SeatingChartSection


class Command(BaseCommand):
    help='Assign seats to the tickets'

    def handle(self, *args, **options):
        eid=args[0]
        seating_color=[{'color':'Gold', 'count':350}, {'color':'Red', 'count':200}, {'color':'Blue', 'count':250}]
        
        seating_color_objects={}
        for value in seating_color:
            seating_color_objects[value['color']]=SeatingChartSection.objects.get(color__name=value['color'], chart__event=eid)

        used=0
        for value in seating_color:
            t=Ticket.objects.values_list('id', flat=True).filter(event=eid).order_by('created')[used:(used+value['count'])]
            Ticket.objects.filter(pk__in=list(t)).update(seating_chart_section=seating_color_objects[value['color']])
            used=used+value['count']
