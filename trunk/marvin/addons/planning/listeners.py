from marvin.store.shop.signals import order_success

from marvin.addons.planning.models import Ticket, Convention, SeatingChart
from marvin.addons.genealogy.models import AccessLevel
from marvin.addons.planning.utils import set_default_seating_for_ticket

import re
import random

def issue_tickets(sender, order=None, **kwargs):
    if order.balance == 0:
        for item in order.orderitem_set.all():
            product = item.product
            
            # Check to make sure that this is an Event.
            if type(product) == Convention:
                qty = item.quantity
                for x in range(qty):
                    # If we already have tickets for this event, get the prepend from an
                    # Existing ticket, else make the first ticket a acronym from the event
                    event_tickets = Ticket.objects.filter(event=product)
                    if len(event_tickets):
                        one_ticket = event_tickets[0]
                        ticket_number_prepend = one_ticket.ticket_number[:3]
                    else:
                        names = product.name.split(' ')
                        ticket_number_prepend = []
                        for name in names:
                            if name != '':
                                letter = name[0]
                                if re.match('[a-zA-z]', letter):
                                    ticket_number_prepend.append(letter.upper())
                        while len(ticket_number_prepend) < 3:
                            letter = random.choice('ABCDEFGHIJKLMNOPQRSTUFWXYZ')
                            ticket_number_prepend.append(letter)
                        
                        ticket_number_prepend = ''.join(ticket_number_prepend)
                    
                    
                    ticket_number_middle = '%05d' % product.id
                    ticket_number_postpend = '%06d' % len(event_tickets)
    
                    ticket = Ticket()
                    ticket.user = order.user
                    ticket.event = product
                    ticket.ticket_number = '%s%s%s' % (ticket_number_prepend, ticket_number_middle, ticket_number_postpend)
                    ticket.orderitem = item
    
                    # Set the seating for the ticket
                    set_default_seating_for_ticket(ticket)
                    ticket.save()


def start_default_listening():
    order_success.connect(issue_tickets)
