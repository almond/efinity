
from django.contrib.auth.models import User
from django.db import models
from django.core import urlresolvers
from marvin.store.shop.models import OrderItem

from marvin.base import ImageBase, Imageable, Purchasable
from marvin.store.products.models import PriceBase
import random
import re, datetime
from l10n.util import lookup_translation

EVENT_TYPE_CHOICES = (
    (1, 'Open Meeting'),
    (2, 'Business Building Seminar'),
    (3, 'Convention'),
    (4, 'Special Event'),
    (5, 'Previews'),
)

LANGUAGE_CHOICES = (
    (1, 'English'),
    (2, 'Spanish'),
)

REPEAT_INTERVALS = (
    ('d', 'Daily'),
    ('w', 'Weekly'),
    ('m', 'Monthly'),
    ('y', 'Yearly')
)

class EventBase(Purchasable, Imageable):
    class Meta:
        abstract = True

    def __unicode__(self):
        return '[%s] %s' %( self.sku, self.name )

    def get_absolute_url(self):
        return urlresolvers.reverse('planning_details',
            kwargs={'event_id': self.pk})        

    _sub_types = []
    is_product = False
    is_subscription = False
    is_shippable = False
    is_downloadable = False
    taxClass = None
    taxable = False

    #sku = models.CharField( max_length = 50, blank = True, verbose_name = 'Event Code' )
    #ALTER TABLE `efinity_skinny`.`planning_convention` ADD COLUMN `speaker` VARCHAR(255) NULL  AFTER `repeat_ends` ;
    #ALTER TABLE `efinity_skinny`.`planning_event` ADD COLUMN `speaker` VARCHAR(255)
    speaker = models.CharField(max_length=255, blank=True, help_text = 'Speaker at the event')

    location = models.CharField(max_length=255, blank=True, help_text = 'City and/or convention center name. Example: San Jose Garden Hotel')
    address = models.TextField(blank=True, help_text='Full address of event, will provide directions and maps based off this' )

    registration_application = models.FileField( upload_to = 'event_docs', null = True, blank = True )
    photo = models.ImageField( upload_to = 'event_images', null = True, blank = True )

    start = models.DateTimeField(verbose_name = 'Date and Time event will Start', blank=True, null=True)
    end = models.DateTimeField(verbose_name = 'Date and Time will End', blank=True, null=True)



    #ALTER TABLE `planning_convention` ADD COLUMN `repeating` TINYINT(1)  NULL DEFAULT 0   ;
    #ALTER TABLE `planning_convention` ADD COLUMN `repeats` VARCHAR(1) NULL DEFAULT NULL ;
    #ALTER TABLE `planning_convention` ADD COLUMN `repeat_ends` DATETIME NULL DEFAULT NULL  ;
    
    #ALTER TABLE `planning_event` ADD COLUMN `repeating` TINYINT(1)  NULL DEFAULT 0   ;
    #ALTER TABLE `planning_event` ADD COLUMN `repeats` VARCHAR(1) NULL DEFAULT NULL ;
    #ALTER TABLE `planning_event` ADD COLUMN `repeat_ends` DATETIME NULL DEFAULT NULL  ;

    repeating = models.BooleanField(help_text='Is it a repeating event?', default=False)
    repeats = models.CharField(help_text='When should it repeat?', max_length=1, choices=REPEAT_INTERVALS, default=None)
    repeat_ends = models.DateTimeField(verbose_name = 'Date when repeating will End', blank=True, null=True, default=None)

    deadline = models.DateTimeField(verbose_name = 'Registration Deadline (for online registrations)', blank=True, null=True)

    created = models.DateTimeField( auto_now_add = True )
    updated = models.DateTimeField( auto_now = True )
    
    ###added echung9/5/11. remember to add int column to table planning_convention
    ###ALTER TABLE `efinity_skinny`.`planning_convention` ADD COLUMN `type` INT NULL DEFAULT NULL  AFTER `can_book` , ADD COLUMN `language` INT NULL DEFAULT NULL  AFTER `type` ;
    # ALTER TABLE `planning_event` ADD COLUMN `type` INT NULL DEFAULT NULL , ADD COLUMN `language` INT NULL DEFAULT NULL  AFTER `type` ;
    type = models.IntegerField(verbose_name = 'Event Type', blank=True, null=True, choices=EVENT_TYPE_CHOICES) 
    language = models.IntegerField(verbose_name = 'Language', blank=True, null=True, choices=LANGUAGE_CHOICES) 


class Event(EventBase):
    '''
    An event is a regular event, where tickets are 
    not required to be purchased
    '''
    pass


class Convention(EventBase):
    '''
    Convention is an event in which the user must
    pay to attend by purchasing a ticket.
    '''
    class Meta:
        verbose_name='meeting'
        verbose_name_plural='meetings'

    #max_tickets = models.IntegerField( verbose_name = 'Number of tickets available', help_text = 'Max amount of tickets available' )
    max_tickets_per_user = models.IntegerField( default=0, verbose_name='Max tickets per person', help_text = 'The maximum number of tickets a single user may purchase. If set to 0 then there is no max.' )

    can_book = models.BooleanField(help_text='Event is open for booking, users are allowed to purchase tickets.')
    
    compensation_paid = models.BooleanField()

    def translated_name(self):
        return self.name

    def getJSON(self):
        return {
            'pk': self.pk,
            # 'name': self.translated_name(),
            'name': self.name,
            'slug': self.slug,
            'unit_price': float(str(self.unit_price)),
            'sku': self.sku,
            'url': self.get_absolute_url(),
        }

    def _get_banners(self):
        return ConventionImage.objects.filter(convention=self)

    banners = property(_get_banners)
    
    def _get_nearest_occurance_start(self):
        if not self.repeating:
            return self.start 
        else:
            if self.repeat_ends and self.start <= self.repeat_ends:
                next_date=self.start
                prev_date=self.start
                while next_date < datetime.datetime.now():
                    prev_date=next_date
                    if self.repeats=='d':
                        next_date=next_date + datetime.timedelta(days=1)
                    elif self.repeats=='w':
                        next_date=next_date + datetime.timedelta(days=7)
                    elif self.repeats=='m':
                        if next_date.month==12:
                            month=1
                            year=next_date.year+1
                        else:
                            month=next_date.month+1
                            year=next_date.year
                        try:
                            tmp_next_date=datetime.datetime(year, month, next_date.day, next_date.hour, next_date.minute, next_date.second)
                        except:
                            month=month+1
                            if month==13:
                                month=1
                                year=year+1
                            tmp_next_date=datetime.datetime(year, month, next_date.day, next_date.hour, next_date.minute, next_date.second)
                        next_date=tmp_next_date
                    elif self.repeats=='y':
                        next_date=datetime.datetime(next_date.year+1, next_date.month, next_date.day, next_date.hour, next_date.minute, next_date.second)

                    if next_date > self.repeat_ends:
                        return prev_date
                return next_date
            else:
                return self.start
    
    nearest_occurance_start=property(_get_nearest_occurance_start)
    
    @property
    def delay_billing(self):
        """
        If this Product has any subtypes associated with 
        it that are subscriptions, then consider it subscription based.
        """
        for prod_type in self.get_subtypes():
            subtype = getattr(self, prod_type.lower())
            if hasattr(subtype, 'delay_billing'):
                return subtype.delay_billing
        return False
        
    def _get_nearest_occurance_end(self):
        if not self.repeating:
            return self.end
        else:
            start=self.nearest_occurance_start
            if self.end=='' or self.end is None:
                next_end=''                                                                                                                                                 
            else:                                                                                                                                                           
                delta=self.end-self.start                                                                                                                                   
                next_end=start+delta
            return next_end

    nearest_occurance_end=property(_get_nearest_occurance_end)

class ConventionImage(ImageBase):
    convention = models.ForeignKey(Convention, related_name='images')


class Price(PriceBase):
    event = models.ForeignKey(Convention)

    class Meta:
        unique_together = (("event", "quantity", "expires"),)


class Attending(models.Model):
    '''Holds information of users who are attending'''
    
    event = models.ForeignKey( Convention, related_name = 'guests' )
    user = models.ForeignKey( User, related_name = 'events_attending' )
    
    created = models.DateTimeField( auto_now_add = True )
    updated = models.DateTimeField( auto_now = True )
    
    # Added by Trevor 2011-08-10
    # ALTER TABLE `planning_attending` ADD COLUMN `num_checked_in` integer NOT NULL DEFAULT 0;
    num_checked_in = models.IntegerField(default=0)
    
    
    class Meta:
        verbose_name = "Check-in"
        verbose_name_plural = "Check-ins"


class SeatingChart(models.Model):
    event = models.OneToOneField(Convention)
    num_tickets = models.IntegerField("# Tickets", help_text="Total number of tickets available for this event")
    
    def __unicode__(self):
        return 'Seating chart: %s' % self.event


class SeatingColor(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name

class SeatingChartSection(models.Model):
    chart = models.ForeignKey(SeatingChart, related_name='sections')
    num_seats = models.IntegerField("# Seats", help_text="Number of seats in this section")
    color = models.ForeignKey(SeatingColor)
    order = models.IntegerField(default=0, help_text="Sets the heirarchy of seating sections for this event.")
    

    def __unicode__(self):
        return 'eid #%s: %s'%(self.chart.event.id, self.color)


    class Meta:
        unique_together = ("chart", "color")


class Ticket(models.Model):
    '''Holds information about tickets to a Convention '''
    event = models.ForeignKey(Convention, related_name = 'tickets' )
    user = models.ForeignKey(User, related_name = 'event_tickets' )
    ticket_number = models.CharField(max_length=20)
    name_on_ticket = models.CharField(max_length=300, blank=True, null=True)
    IBO_number = models.IntegerField(blank=True, null=True)
    checked_in = models.BooleanField(blank=True)
    seating_chart_section = models.ForeignKey(SeatingChartSection, blank=True, null=True)
    
    #ALTER TABLE `efinity_skinny`.`planning_ticket` ADD COLUMN `orderitem_id` INT(11) NULL ;
    orderitem=models.ForeignKey(OrderItem, blank=True, null=True)

    created = models.DateTimeField( auto_now_add = True )
    updated = models.DateTimeField( auto_now = True )
    
    def __unicode__(self):
        return self.ticket_number
    
    def user_full_name(self):
        return self.user.get_full_name()
    
    def save(self, **kwargs):
        if not self.pk:
            # Create the ticket number
            # First create the first three digits, they are derived from the first three 
            # characters of the event name after spaces, or random if there is not enough
            # to grab.
            current_tickets = Ticket.objects.filter(event=self.event)
            if current_tickets:
                one_ticket = current_tickets[0]
                ticket_number_prepend = one_ticket.ticket_number[0:3]
            else:
                names = self.event.name.split(' ')
                names = names[:3]
                letter_re = r'[a-zA-Z]'
                ticket_number_prepend = []
                for name in names:
                    if name != '':
                        if re.match(letter_re, name[0]):
                            ticket_number_prepend.append(name[0])
                if len(ticket_number_prepend) < 3:
                    while len(ticket_number_prepend) < 3:
                        letter = random.choice('abcdefghijklmnopqrstuvwxyz')
                        ticket_number_prepend.append(letter)
            
                ticket_number_prepend = ''.join(ticket_number_prepend)
                ticket_number_prepend = str.upper(str(ticket_number_prepend))
            
            ticket_number_eventnum = '%05d' % self.event.id
            
            current_num_tickets = len(current_tickets)
            ticket_number_postpend = '%06d' % current_num_tickets
                
            self.ticket_number = '%s%s%s' % (ticket_number_prepend, ticket_number_eventnum, ticket_number_postpend)

        super(Ticket, self).save(**kwargs)



# Start listening on Order Save to issue tickets
import listeners
listeners.start_default_listening()
