from marvin.addons.planning.models import Ticket, Convention, SeatingChart
from marvin.addons.genealogy.models import AccessLevel
from django.db.models import Max

def set_ticket_section(ticket, section):
    # Grab the seating chart and sections, if they do not exist, don't do anything
    try:
        seating_chart = ticket.event.seatingchart
        sections = seating_chart.sections.all().order_by('order').order_by('id')
    except:
        return ticket
    section_tickets = Ticket.objects.filter(event=ticket.event).filter(seating_chart_section=section)
    if section.num_seats >= len(section_tickets):
        ticket.seating_chart_section = section
        ticket.save()
        return True, ticket
    else:
        # if that section is full, try to push someone out
        for x in range(len(sections)-1):
            if sections[x] == section:
                break
        moved, ticket = push_out_non_checked_in(sections, x, ticket)
        return moved, Ticket

def push_out_non_checked_in(sections, x, ticket):
    try:
        max_available_ticket_id = Ticket.objects.filter(event=ticket.event).filter(seating_chart_section=section).filter(checked_in=False).annotate(Max('id'))
        max_available_ticket_id = max_available_ticket_id["id__max"]
        ticket_to_move = Ticket.objects.get(id=max_available_ticket_id)
    except:
        # Could not get the ticket to move, meaning there was none to move
        return False, ticket
    
    for y in range(len(sections[x:])-1):
        section = sections[y]
        section_tickets = Ticket.objects.filter(event=ticket.event).filter(seating_chart_section=section)
        if section.num_seats >= len(section_tickets):
            ticket_to_move.seating_chart_section = section
            ticket_to_move.save()
            break
            
    if not ticket_to_move.seating_chart_section:
        # We did not find a seat in the other sections, put them in the lowest section, even though it is full
        section = sections[len(sections)-1]
        ticket_to_move.seating_chart_section = section
        ticket_to_move.save()
    
    
    ticket.section = sections[x]
    ticket.save()
    return True, ticket

def set_default_seating_for_ticket(ticket):
    # Grab the seating chart and sections, if they do not exist, don't do anything
    try:
        seating_chart = ticket.event.seatingchart
        sections = seating_chart.sections.all().order_by('order').order_by('id')
    except:
        return ticket
    
    user_pinlevel = ticket.user.profile.pinlevel
    regular_pinlevel = AccessLevel.objects.get(group__name__iexact='regular')
    
    # If ths user is at a regular pinlevel, then look for seats in the platinum section last
    if user_pinlevel == regular_pinlevel:
        sections = list(sections)
        platinum_section = sections.pop(0)
        sections.append(platinum_section)
        
    for x in range(len(sections)-1):
        section = sections[x]
        section_tickets = Ticket.objects.filter(event=ticket.event).filter(seating_chart_section=section)
        if section.num_seats >= len(section_tickets):
            ticket.seating_chart_section = section
            break
        else:
            # The section is full, push out user if platinum, if possible
            if user_pinlevel != regular_pinlevel:
                moved, ticket = push_out_non_checked_in(sections, x, ticket)
                if moved:
                    break
                
    if not ticket.seating_chart_section:
        # We did not find a seat in the other sections, put them in the lowest section, even though it is full
        section = sections[len(sections)-1]
        ticket.seating_chart_section = section

    ticket.save()
    return ticket