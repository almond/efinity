from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import striptags
from django.utils import simplejson
from marvin.utils.app import *
from marvin.utils.decorators import staff_only as login_required



def ajax_render(data):
    return HttpResponse(
        simplejson.dumps(data, ensure_ascii=False),
        mimetype='application/javascript'
    )

def form_errors(form, is_formset=False):
    errors = {}

    if is_formset:
        for err_lst in form.errors:
            for e in err_lst.iteritems():
                html = '<ul class="errorlist"><li>%s - %s</li></ul>'
                value = unicode(html % (e[0], striptags(e[1])))
                errors.update({ e[0]: value})
    else:
        for e in form.errors.iteritems():
            html = '<ul class="errorlist"><li>%s - %s</li></ul>'
            value = unicode(html % (form[e[0]].label, striptags(e[1])))
            errors.update({ e[0]: value})
    return errors
