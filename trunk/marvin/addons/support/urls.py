from django.utils.translation import ugettext_lazy as _
from django.conf.urls.defaults import *
from marvin.addons.support.forms import *

urlpatterns = patterns('marvin.addons.support.views',

    (r'^$', 'home', {}, 'support_home'),
    #(r'^old/$', index_v2, {}, 'support_home_old'),
    #(r'^old/$', 'home', {}, 'support_home_old'),
    (r'^list/$', 'list', {}, 'support_list'),
    (r'^broadcast/$', 'broadcast', {}, 'broadcast'),
    (r'^contact-us/$', 'contact_us', {}, 'contact_us'),
    (r'^profits/reset/(?P<convention_id>[\d_]+)/$', 'profits_reset', {}, 'profits_reset'),
    (r'^profits/$', 'profits', {}, 'profits'),
    (r'^expense_report/$', 'expense_report', {}, 'expense_report'),
    (r'^delete_address/$', 'delete_address', {}, 'delete_address'),
    (r'^seating_report/$', 'seating_report', {}, 'seating_report'),
    (r'^downline_attendance_report/$', 'downline_attendance_report', {}, 'downline_attendance_report'),
    (r'^non_checked_in/$', 'non_checked_in', {}, 'non_checked_in'),
    (r'^search/$', 'search', {}, 'support_search'),

    (r'^register_ibo/$', 'register_ibo', {}, 'support_register'),
    (r'^register_user_data/(\d+)/$', 'register_user_data', {}, 'support_user_data'),
    (r'^register_user_subscription/(\d+)/$', 'register_user_subscription', {}, 'support_user_subscription'),

    (r'^profile/(?P<user_id>[\d_]+)/$', 'profile.view', {}, 'support_profile'),
    (r'^profile/(?P<user_id>[\d_]+)/update/$', 'profile.update', {}, 'support_update_profile'),

    (r'^position/update/$', 'profile.update_position', {}, 'support_update_position'),

    (r'^profile/(?P<user_id>[\d_]+)/update/phones/$', 'profile.update_inline', {
        'formset': PhonesFormset,
        'generated_comment': 'Updated user phones on record.',
        'success_msg': _('Successfully updated phone records!'),
        }, 'support_update_phones'),

    (r'^profile/(?P<user_id>[\d_]+)/update/addresses/$', 'profile.update_inline', {
        'formset': AddressFormset,
        'generated_comment': 'Updated user addresses on record.',
        'success_msg': _('Successfully updated address records!'),
        }, 'support_update_addresses'),

    (r'^profile/(?P<user_id>[\d_]+)/update/contacts/$', 'profile.update_inline', {
        'formset': ContactFormset,
        'generated_comment': 'Updated user contacts on record.',
        'success_msg': _('Successfully updated contact records!'),
        }, 'support_update_contacts'),

    (r'^profile/(?P<user_id>[\d_]+)/submit/notes/$', 'profile.submit', {
        'form': CustomerNoteForm,
        'generated_comment': 'Added customer comment.',
        'success_msg': _('Successfully added customer comment!'),
        }, 'support_submit_customer_notes'),

    (r'^profile/(?P<user_id>[\d_]+)/upline/$', 'check_upline', {}, 'support_check_upline'),
    (r'^profile/(?P<user_id>[\d_]+)/downline/$', 'check_downline', {}, 'support_check_downline'),

    (r'^virtual/(?P<user_id>[\d_]+)/$', 'virtual', {}, 'support_go_virtual'),
    (r'^virtual-order/(?P<user_id>[\d_]+)/$', 'virtual_order', {}, 'support_virtual_order'),
    (r'^virtual-ticket-order/(?P<user_id>[\d_]+)/$', 'virtual_ticket_order', {}, 'support_virtual_ticket_order'),
    (r'^re-login-admin-user/(?P<admin_user_id>[\d_]+)/$', 're_login_admin_user', {}, 're_login_admin_user'),


    (r'^ajax/$', 'ajax', {}, 'support_ajax'),
    (r'^ajax/dashboard/$', 'ajax_dashboard', {}, 'support_ajax_dashboard'),
    (r'^ajax/search/$', 'ajax_search', {}, 'support_ajax_search'),
    (r'^ajax/search/products/$', 'ajax_product_search', {}, 'support_ajax_product_search'),
    (r'^ajax/search/events/$', 'ajax_event_search', {}, 'support_ajax_event_search'),
    (r'^ajax/cart/add/$', 'ajax_cart_add', {}, 'support_ajax_add_to_cart'),
    (r'^ajax/cart/event/add/$', 'ajax_cart_event_add', {}, 'support_ajax_event_add_to_cart'),
    (r'^ajax/update/address/$', 'update_address.ajax', {}, 'support_ajax_update_addresses'),

)

