from django import forms
from django.contrib.auth.models import User
from django.forms import Textarea
from django.forms.models import inlineformset_factory
from marvin.addons.planning.models import Convention
from marvin.core.accounts.models import Profile
from marvin.core.contacts.models import Contact, PhoneNumber, AddressBook
from marvin.addons.genealogy.models import Position

from models import CustomerNote
import datetime



class CustomerNoteForm(forms.ModelForm):
    class Meta:
        model = CustomerNote
        #exclude = ('user', 'created_by', )
        widgets = {
            'user': forms.HiddenInput(),
            'created_by': forms.HiddenInput(),
        }


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

class ProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

        if instance and instance.id:
            self.fields['w9_date'].widget.attrs['readonly'] = True
            self.fields['agreement_date'].widget.attrs['readonly'] = True
            # self.fields['compensation_level'].widget.attrs['readonly'] = True

    def save(self, force_insert=False, force_update=False, commit=True):
        instance = super(ProfileForm, self).save(commit=False)

        if self.cleaned_data["can_download"]:
            instance.download_expiration_date = datetime.datetime.now() + datetime.timedelta(days=31)
        else:
            instance.download_expiration_date = None

        if commit:
            instance.save()

        return instance

    class Meta:
        model = Profile
        exclude = ('user', 'personal_photo', 'gender', 'download_expiration_date', 'subscription_type')


class PositionForm(forms.ModelForm):
    """ Be able to edit position information for said user """
    class Meta:
        model = Position
        widgets = {
            'tree': forms.HiddenInput,
            'user': forms.HiddenInput,
            'upline': forms.HiddenInput,
            'auto_update': forms.HiddenInput,
        }


#CustomerNoteFormset = inlineformset_factory(User, CustomerNote, extra=1, exclude=('created_by',))
PhonesFormset = inlineformset_factory(User, PhoneNumber, extra=1,
    exclude=('privacy_setting', 'is_primary',))
AddressFormset = inlineformset_factory(User, AddressBook, can_delete=False,
    extra=1, exclude=('description',))# 'is_default_shipping', 'is_default_billing', ))
ContactFormset = inlineformset_factory(User, Contact, can_delete=False,
    extra=1, exclude=('is_primary',))# 'is_default_shipping', 'is_default_billing', ))
