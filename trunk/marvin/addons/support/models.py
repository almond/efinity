from django.db import models
from django.contrib.auth.models import User
from marvin.addons.planning.models import Convention, Ticket

class CustomerNote(models.Model):
    def __unicode__(self):
        return self.note

    user = models.ForeignKey( User, related_name = 'notes' )
    created_by = models.ForeignKey( User, related_name = 'notes_created' )

    note = models.TextField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class EventExpensesCategory(models.Model):
    convention=models.ForeignKey(Convention)
    label=models.CharField(max_length=255, blank=True, null=True)

class EventExpenses(models.Model):
    category=models.ForeignKey(EventExpensesCategory)
    label=models.CharField(max_length=255, blank=True, null=True)
    amount = models.DecimalField(decimal_places=4, max_digits=18)

