from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db.models import Count

from marvin.utils.decorators import staff_only as login_required
from marvin.utils.app import render
from marvin.addons.support.views import search_users, search_checkins
from marvin.addons.planning.forms import AttendingSearchForm, AttendingForm
from marvin.addons.planning.forms import ConventionSearchForm
from marvin.addons.planning.models import Convention, Attending, Ticket, SeatingColor, SeatingChartSection

from marvin.addons.planning.utils import set_ticket_section
from django.db.models import Q

@login_required
def checkin_search(request, convention_id):
    '''
    This view searches for tickets, if one (or a set) is found, it returns
    to the ticket checkin page, otherwise, it returns a 'no tickets found' page
    '''
    event = Convention.objects.get(id=convention_id)
    tickets_checked_in = Ticket.objects.filter(event=event).filter(checked_in=True)
    tickets_checked_in = len(tickets_checked_in)
    search_form = ConventionSearchForm()
    user_results = None
    user_results_tmp=[]

    if request.GET:
        is_get = True
        search_form = ConventionSearchForm(request.GET)
        if search_form.is_valid():
            data = search_form.cleaned_data
            ticket_number = data['ticket_number']
            ibo_number = data['ibo_number']
            user_name = data['user_name']
            
            if ticket_number:
                if len(ticket_number)>=6:
                    tickets = Ticket.objects.filter(ticket_number__iexact=ticket_number)
                else:
                    tickets = Ticket.objects.filter(ticket_number__iendswith=ticket_number)
                
                if len(tickets) > 0:
                    first_ticket = tickets[0]
                    user = first_ticket.user
                    
                    return HttpResponseRedirect('/admin/planning/ticket/checkin/%s/%s' % (event.id, user.aid))
                    
            if ibo_number:
                try:
                    user = User.objects.get(username=ibo_number)
                except User.DoesNotExist:
                    user = None
                
                if user:
                    return HttpResponseRedirect('/admin/planning/ticket/checkin/%s/%s' % (event.id, user.aid))

            if user_name:
                names=user_name.split(' ')
                query=None
                for name in names:
                    if query:
                        query = Q(first_name__icontains=name) | Q(last_name__icontains=name) | query
                    else:
                        query = Q(first_name__icontains=name) | Q(last_name__icontains=name)
                user_results = User.objects.filter(query)

                for u in user_results:
                    canceled_tickets=Ticket.objects.values_list('id', flat=True).filter(~Q(orderitem=None),user=u,event=convention_id,orderitem__order__status='Cancelled')
                    u.ticket_count=Ticket.objects.filter(user=u, event=convention_id).exclude(id__in=list(canceled_tickets)).count()
                    user_results_tmp.append(u)

    else:
        is_get = False
    
    context = {
        'event':event,
        'search_form':search_form,
        'is_get': is_get,
        'tickets_checked_in':tickets_checked_in,
        'user_results': user_results_tmp,
    }
    return render(request, 'support/ticket/search.html', context)

@login_required
def checkin_user(request, convention_id, user_aid):
    event = Convention.objects.get(id=convention_id)
    purchaser = User.get_by_aid(user_aid)

    """
    getting cancelled tickets
    """
    canceled_tickets = Ticket.objects.values_list('id', flat=True).filter(~Q(orderitem=None), user=purchaser, event=event, orderitem__order__status='Cancelled')
    tickets = Ticket.objects.filter(user=purchaser, event=event).exclude(id__in=list(canceled_tickets))
    num_checked_in = len(tickets.filter(checked_in=True))
    
    try:
        seating_chart = event.seatingchart
    except:
        seating_chart = None
    
    # This variable tracks how many tickets were checked in
    post_tickets_checked_in = 0
    post_ticktes_checked_in_removed = 0
    sections_updated = 0
    name_updated = 0
    ibo_num_updated = 0
    
    if request.POST:
        for ticket in tickets:
            # Check in or remove check in
            dirty = False
            
            checked = request.POST.get(ticket.ticket_number, None)
            if checked:
                if not ticket.checked_in:
                    ticket.checked_in = True
                    dirty = True
                    post_tickets_checked_in += 1
                    num_checked_in += 1
            else:
                if ticket.checked_in:
                    ticket.checked_in = False
                    dirty = True
                    post_ticktes_checked_in_removed += 1
                    num_checked_in -= 1
            
            ticket_section = request.POST.get('%s%s' % (ticket.ticket_number, '_section'), None)
            if ticket_section:
                section = SeatingChartSection.objects.get(id=ticket_section)
                if ticket.seating_chart_section != section:
                    saved, ticket = set_ticket_section(ticket, section)
                    sections_updated += 1
            
            ticket_name = request.POST.get('%s%s' % (ticket.ticket_number, '_name'), None)
            if ticket_name:
                if ticket.name_on_ticket != ticket_name:
                    ticket.name_on_ticket = ticket_name
                    name_updated += 1
                    dirty = True
            
            ticket_ibo = request.POST.get('%s%s' % (ticket.ticket_number, '_ibo_number'), None)
            if ticket_ibo:
                try:
                    ticket_ibo = int(ticket_ibo)
                except:
                    ticket_ibo = None
                if ticket.IBO_number != ticket_ibo:
                    ticket.IBO_number = ticket_ibo
                    ibo_num_updated += 1
                    dirty = True
            
            if dirty:
                ticket.save()
        return HttpResponseRedirect('/admin/planning/ticket/checkin/%s/'%(convention_id))
    context = {
        'event': event,
        'purchaser': purchaser,
        'tickets' : tickets,
        'num_checked_in': num_checked_in,
        'post_tickets_checked_in': post_tickets_checked_in,
        'post_ticktes_checked_in_removed': post_ticktes_checked_in_removed,
        'seating_chart': seating_chart,
        'sections_updated':sections_updated,
        'name_updated': name_updated,
        'ibo_num_updated': ibo_num_updated
    }
    return render(request, 'support/ticket/checkin.html', context)
    
@login_required
def home(request):
    params = {}
    form = AttendingSearchForm()
    params['form'] = form
    
    if request.GET: # Actually searched for something
    	form = AttendingSearchForm(request.GET)
    	params.update({'form': form})
    	if form.is_valid():
    		event = int(form.cleaned_data.get('event'))
    		number = form.cleaned_data.get('number')
    		
    		try:
    			customer = User.objects.get(username=number)
    			event_orders = customer.conventions(convention_id=event)
    			
    			# total up the numbers of tickets purchased.  These could have been on different orders
    			if event_orders:
    				quantity = 0
    				event = event_orders[0].product
    				for order in event_orders:
    					quantity += order.quantity
    					
    				# Check how many people have already checked in
    				try:
    					attending = Attending.objects.get(event=event, user=customer)
    				except Attending.DoesNotExist: # If there isn't a checkin for this event/user combination, create one
    					attending = Attending(event=event, user=customer)
    					attending.save()
    				
    			
    			params.update({
    				'event': event,
    				'num_tickets': quantity,
    				'num_checked_in': attending.num_checked_in,
    				'searched_user': customer,
    				'check_in_form': AttendingForm(initial={'event': event, 'user': customer})
    				
    			})
    		except User.DoesNotExist:
    			pass # TODO - Clean this?  The template displays a 'Didn't find IBO with that number error.  Is that enough?
    		
    		
    
    return render(request, 'support/attendings_home.html', params)
    
    
    
@login_required
def checkin(request):
    form = AttendingForm(request.POST or None)
    
    if form.is_valid():
        attending = Attending.objects.get(user=form.cleaned_data['user'], event=form.cleaned_data['event'])
        attending.num_checked_in += form.cleaned_data['num_checking_in']
        attending.save()
        
        return HttpResponseRedirect(reverse('admin:checkin_successful'))
    
    if form.is_bound:
    	searched_user = User.objects.get(id=form['user'].value() )
    	event = Convention.objects.get(id=form['event'].value() )
    	attending = Attending.objects.get(event=event, user=searched_user)
    	
    	
    	# total up the numbers of tickets purchased.  These could have been on different orders
    	event_orders = searched_user.conventions(convention_id=event)
    	quantity = 0
    	if event_orders:
    		event = event_orders[0].product
    		for order in event_orders:
    			quantity += order.quantity
    	
    context = {
    	'check_in_form': form, 
    	'searched_user': searched_user,
    	'event': event,
    	'num_checked_in': attending.num_checked_in,
    	'num_tickets': quantity
    }
    return render(request, 'support/attendings_checkin_form.html', context)
    
    
@login_required
def success(request):
	return render(request, 'support/attendings_checkin_success.html', {})
