from marvin.addons.support.utils import *
from marvin.core.contacts.forms import AddressBookForm
from marvin.core.contacts.models import AddressBook

@login_required
def ajax(request):
    user = get_object_or_404(User, pk=request.REQUEST.get('user_id'))
    address_id = request.REQUEST.get('address_id')

    try:
        address = AddressBook.objects.get(user=user, pk=address_id)
    except AddressBook.DoesNotExist:
        address = None

    if request.method == 'POST':
        form = AddressBookForm(data=request.POST, instance=address)
        if form.is_valid():
            return ajax_render({ 'success': True })

        else:
            errors = {}
            for e in form.errors.iteritems():
                d.update({
                    e[0]: unicode(e[1])
                })

            return ajax_render({ 'errors': errors })

    else:
        form = AddressBookForm(instance=address)
    
    return render(request, 'support/ajax/update_address.html', {'form': form})
