from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType
from django.core.mail import get_connection, EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.db.models import Sum
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template import defaultfilters
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
from marvin.addons.compensation.models import Commission
from marvin.addons.genealogy.models import Position, Tree
from marvin.addons.genealogy.utils import get_upline_ibo
from marvin.addons.planning.models import Attending, Convention, Ticket,\
    SeatingChartSection
from marvin.addons.support.models import CustomerNote, EventExpensesCategory,\
    EventExpenses
from marvin.core.accounts.forms import RegisterStep1Form, RegisterStep2Form,\
    RegisterStep3Form
from marvin.core.accounts.models import Profile, Activity
from marvin.core.contacts.models import AddressBook
from marvin.store.products.models import Product
from marvin.store.products.modules.subscription.models import\
    SubscriptionProduct
from marvin.store.shop.models import Cart, OrderItem, Order
from marvin.store.shop.signals import satchmo_cart_details_query,\
    satchmo_cart_add_complete
from marvin.utils.app import *
from marvin.utils.decorators import staff_only as login_required
from marvin.utils.search import perform_search
from decimal import Decimal
import csv
import datetime
import hashlib
import logging

logger = logging.getLogger(__name__)

def validateEmail(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


@csrf_exempt
def contact_us(request):
    error = False
    show_apology = False
    name = request.POST.get('name', None)
    email = request.POST.get('email', None)
    description = request.POST.get('description', None)
    if request.method == 'POST' and name and email and description and validateEmail(email):
        if "mailer" in settings.INSTALLED_APPS:
            from mailer import send_mail
        else:
            from django.core.mail import send_mail

            send_mail('Efinity Contact Us Form', ("%s \n\n -By %s") % (description, name), email,
                ['jason@myefinity.com', ])

        show_apology = True

    else:
        error = True
    return render(request, 'support/contact-us.html',
        {'error': error, 'name': name, 'show_apology': show_apology, 'email': email, 'description': description})


def delete_address(request):
    if request.method == 'POST':
        try:
            id = int(request.POST.get('id', -1))
            address = AddressBook.objects.filter(id__in=[id, ])
            if len(address) == 1 and request.user.is_authenticated() and (
                address[0].user == request.user or request.user.is_staff):
                address[0].delete()
                return HttpResponse(id)
        except:
            pass
    return HttpResponse('false')


def generate_upline():
    pos = Position.objects.all().order_by('user__id')
    genes = {}
    for p in pos:
        if p.upline:
            upline = p.upline.id
        else:
            upline = 0

        if upline in genes:
            upline = str(genes[upline]) + ',' + str(upline)
        else:
            upline = str(upline)
        genes[p.user.id] = upline
    return genes


@login_required
def expense_report(request):
    eid = request.GET.get('event', None)
    if eid:
        try:
            eid = int(eid)
        except:
            eid = None

    con = None
    user_comm = {}
    ticket_count = 0
    ticket_total = 0
    total_expenses = 0
    total_given_away = 0
    pph = 0
    if eid:
        try:
            con = Convention.objects.get(pk=eid, compensation_paid=True)
        except:
            con = None

        if con:

            test = False
            print "Order ID: %d" % con.pk
            ct = ContentType.objects.get(app_label='planning', model='convention')
            # order_items=OrderItem.objects.filter(content_type=ct, product_id=con.pk, completed=False, order__payments__isnull=False)
            order_items = OrderItem.objects.filter(content_type=ct,
                                                   product_id=con.pk, completed=False,
                                                   order__payments__isnull=False).exclude(order__status='Cancelled').distinct()

            print "len of orderitem dict: %d" % len(order_items)

            orders = Order.objects.filter(orderitem__in=order_items).exclude(status="Cancelled")

            print "Len of orders dict: %d" % len(orders)

            for o in orders:
                if o.balance == 0:
                    ticket_total += o.total
                    for oi in o.orderitem_set.all():
                        ticket_count += oi.quantity

            print "First ticket_total %f" % ticket_total
            print "First ticket_count %f" % ticket_count

            commissions = Commission.objects.filter(order_item__in=order_items,
                                                    type=1)

            total_given_away = commissions.aggregate(total=Sum("amount"))["total"]
            commissions = commissions.values("user").annotate(total=Sum("amount"))

            print "len of commissions dict: %d" % len(commissions)

            for com in commissions:
                com["user"] = User.objects.get(pk=com["user"])
                user_comm[com["user"].username] = dict()
                user_comm[com["user"].username]["total"] = com["total"]
                user_comm[com["user"].username]["eligible"] = "Yes"

                if not com["user"].profile.received_efinity_agreement and not com["user"].profile.received_w9:
                    user_comm[com["user"].username]['eligible'] = 'No, needs Agreement and W9'
                elif not com["user"].profile.received_w9:
                    user_comm[com["user"].username]['eligible'] = 'No, needs W9'
                elif not com["user"].profile.received_efinity_agreement:
                    user_comm[com["user"].username]['eligible'] = 'No, needs W9'

                user_comm[com["user"].username]['id'] = com["user"].pk
                user_comm[com["user"].username]["name"] = "%s %s" % (com["user"].first_name, com["user"].last_name)

            exp_cats = EventExpensesCategory.objects.filter(convention=con)
            exps = EventExpenses.objects.filter(category__in=exp_cats)
            for e in exps:
                total_expenses += e.amount

            pph = (ticket_total - total_expenses) / ticket_count

    # Just Jerry-rig this.. have really no idea why
    # this event is failing
    # if con:
    #     if con.pk == 784:
    #         for k, v in user_comm.items():
    #             user_comm[k]["total"] += Decimal(str(39.59))

    events = Convention.objects.filter(compensation_paid=True)

    if request.GET.get('export', None) == 'csv':
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=commission-given-%s.csv' % (
            defaultfilters.slugify(con.name))

        writer = csv.writer(response)
        writer.writerow(['Name of Event', con.name])
        writer.writerow(['Date of Event', con.start])
        writer.writerow(['Location', con.location])
        writer.writerow(['Address', con.address])
        writer.writerow(['Coordinator', 'eFinity Events'])
        writer.writerow(['Profit Per Head', pph])
        writer.writerow(['IBO Number', 'Name', 'Commission Amount', 'Eligible'])
        for ibo, data in user_comm.items():
            writer.writerow([ibo, data['name'], data['total'], data['eligible']])
        return response

    print "PPH %f" % pph
    print "Ticket count %d" % ticket_count
    print "Ticket total %f" % ticket_total
    print "Total expenses %f" % total_expenses
    print "Amount to give away %f" % (ticket_total - total_expenses)

    return render(request, 'support/expense_report.html',
        {'title': 'Event Payout Report', 'events': events, 'eid': eid,
            'con': con, 'pph': pph, 'user_comm': user_comm,
            "total_given_away": total_given_away, "ticket_count":ticket_count})


@login_required
def downline_attendance_report(request):
    eid = request.GET.get('event', None)
    ibo = request.GET.get('ibo', None)
    if eid:
        try:
            eid = int(eid)
        except:
            eid = None

    if ibo:
        try:
            user = User.objects.get(username=ibo)
            users = get_downline(user)
            tickets = Ticket.objects.filter(user__in=users, event=eid)
        except User.DoesNotExist:
            users = []
            tickets = None

    events = Convention.objects.filter()
    return render(request, 'support/downline_attendance_report.html',
        {'title': 'Downline Attendance Report', 'events': events, 'eid': eid, 'ibo': ibo, 'tickets': tickets})


@login_required
def seating_report(request):
    eid = request.GET.get('event', None)
    if eid:
        try:
            eid = int(eid)
        except:
            eid = None

    if eid:
        seating_section = SeatingChartSection.objects.filter(chart__event=eid).order_by('color__name', )
        seating = []
        total = 0
        for s in seating_section:
            used = len(Ticket.objects.filter(seating_chart_section=s, checked_in=True))
            seating.append(
                {'seating_chart_section__color__name': s.color.name, 'seating_chart_section__num_seats': s.num_seats,
                 'seating_chart_section__count': used, 'available': (s.num_seats - used)})
            total = total + used
    else:
        seating = None
        total = None

    events = Convention.objects.filter()
    return render(request, 'support/sitting_report.html',
        {'title': 'Seating Report', 'events': events, 'eid': eid, 'seating': seating, 'total': total})


@login_required
def non_checked_in(request):
    eid = request.GET.get('event', None)
    if eid:
        try:
            eid = int(eid)
        except:
            eid = None

    if eid:
        users = Ticket.objects.values('user__username', 'user__first_name', 'user__last_name').filter(event=eid,
            checked_in=False).distinct('user')

    else:
        users = None

    events = Convention.objects.filter()
    return render(request, 'support/non_checked_in.html',
        {'title': 'Non Checkedin Ticket Report', 'events': events, 'eid': eid, 'users': users})


def get_recursive_downline(user, downline=[]):
    children = Position.objects.filter(upline=user)
    for c in children:
        downline.append(c)
        downline += get_recursive_downline(c, downline)
    return downline


@login_required
def profits_reset(request, convention_id):
    c = Convention.objects.get(pk=convention_id)
    ct = ContentType.objects.get(app_label='planning', model='convention')
    ois = OrderItem.objects.filter(content_type=ct, product_id=c.pk)

    comms = Commission.objects.filter(order_item__in=ois)

    for comm in comms:
        comm.delete()

    c.compensation_paid = False
    c.save()

    return HttpResponseRedirect(reverse("profits"))


@login_required
def profits(request):
    if request.method == 'POST':
        if request.POST.get('submit_event', None) == 'Select' and request.POST.get('convention', None) is not None:
            selected_event = request.POST.get('convention')
            post = True

            con = Convention.objects.get(pk=selected_event)

            ct = ContentType.objects.get(app_label='planning', model='convention')
            #oitem=OrderItem.objects.filter(content_type=ct, product_id=con.pk, completed=False, order__payments__isnull=False, order__status='Shipped').distinct()
            oitem = OrderItem.objects.filter(content_type=ct, product_id=con.pk,
                completed=False, order__payments__isnull=False).distinct()
            ticket_count = 0
            ticket_total = 0
            cancelled_ticket_count = 0
            cancelled_ticket_total = 0

            print "OITEM: %d" % len(oitem)
            orders = Order.objects.filter(orderitem__in=oitem)
            print "ORDERS: %d" % len(orders)
            for o in orders:
                if o.balance == 0:
                    if o.status == "Cancelled":
                        for p in o.orderitem_set.all():
                            cancelled_ticket_count += p.quantity
                        cancelled_ticket_total += o.total
                    for p in o.orderitem_set.all():
                        ticket_count += p.quantity
                    ticket_total += o.total

            tickets = {'count': ticket_count, 'total': ticket_total}
            cancelled_tickets = {"count": cancelled_ticket_count, "total": cancelled_ticket_total}
            adjusted_tickets = {"count": tickets["count"] - cancelled_tickets["count"],
                                "total": tickets["total"] - cancelled_tickets["total"]}

            exp_cat = EventExpensesCategory.objects.filter(convention=con)
            exps = []
            exp_cat_new = []
            for ecat in exp_cat:
                exp_cat_new.append({'label': ecat.label})
                exps.append(EventExpenses.objects.filter(category=ecat))

            default_labels = ['eFinity Events', 'Hotel / Convention / F&B', 'Speaker / VIP Payments / Travel',
                              'Printing']
            for l in default_labels:
                exists = False
                for e in exp_cat_new:
                    if e['label'] == l:
                        exists = True
                if not exists:
                    exp_cat_new.append({'label': l})
                    exps.append(EventExpenses.objects.filter(category__id=-1))

            exp_cat = exp_cat_new
        else:
            expenses = request.POST.get('expenses')
            expenses = simplejson.loads(expenses)

            con = Convention.objects.get(pk=request.POST.get('convention'))
            eec = EventExpensesCategory.objects.filter(convention=con)
            for e in eec:
                delete = True
                for exp_heads in expenses:
                    if exp_heads['name'] == e.label:
                        delete = False

                if delete:
                    EventExpenses.objects.filter(category=e).delete()
                    e.delete()

            for exp_heads in expenses:
                try:
                    ec, created = EventExpensesCategory.objects.get_or_create(label=exp_heads['name'], convention=con)
                except:
                    created = False
                    ec = EventExpensesCategory.objects.filter(label=exp_heads["name"], convention=con)
                """
                exp=EventExpenses.objects.filter(category=ec)
                delete=True
                for e in exp:
                    for head in exp_heads['costs']:
                        if head == e.label:
                            delete=False
                    if delete:
                        e.delete()
                """
                EventExpenses.objects.filter(category=ec).delete()
                for head in exp_heads['costs']:
                    try:
                        EventExpenses.objects.get_or_create(label=head, category=ec, amount=exp_heads['costs'][head])
                    except EventExpenses.MultipleObjectsReturned:
                        # Do something about type check and what not
                        event_expenses = EventExpenses.objects.filter(label=head, category=ec,
                            amount=exp_heads['costs'][head])
                        event_expenses.delete()

                        # Now create the one you want
                        new_event_expense = EventExpenses(label=head, category=ec, amount=exp_heads['costs'][head])
                        new_event_expense.save()

            post = False
            selected_event = None
            tickets = {'count': 0}
            exp_cat = None
            exps = None
            cancelled_tickets = {"count": 0}
            adjusted_tickets = {"count": 0}
    else:
        selected_event = None
        tickets = {'count': 0}
        post = False
        exp_cat = None
        exps = None
        con = None
        cancelled_tickets = {"count": 0}
        adjusted_tickets = {"count": 0}

    conventions = Convention.objects.all().order_by('name')
    return render(request, 'support/profits.html',
        {'title': 'Expense Report', 'exps': exps, 'exp_cat': exp_cat, 'post': post, 'conventions': conventions,
         'selected_event': selected_event, 'tickets': tickets, 'con': con, "cancelled_tickets": cancelled_tickets,
         "adjusted_tickets": adjusted_tickets})

"""
def get_subscriptions(user):
    subscriptions = {}
    for order in user.orders.all().order_by('-time_stamp'):
        for orderitem in order.orderitem_set.all():
            if orderitem.completed:
                product = orderitem.product
                if hasattr(product, 'is_subscription') and product.is_subscription:
                    if orderitem.expire_date > datetime.date.today():
                        subscriptions[orderitem.id] = {"product": product,  "quantity": orderitem.quantity, "orderitem":orderitem}
    return subscriptions
"""

def get_users_for_subscription(product):
    try:
        s = SubscriptionProduct.objects.get(product=product)
        return set(OrderItem.objects.values_list('order__user', flat=True).filter(product_id=product,
            expire_date__gte=datetime.datetime.today()))
    except:
        return []


def get_downline(user, dl=[]):
    downline = Position.objects.filter(upline=user)
    for d in downline:
        if d.user_id not in dl:
            try:
                u = User.objects.get(pk=d.user_id)
                dl.append(d.user_id)
                dl = get_downline(u, dl)
            except:
                pass
    return dl


@login_required
def broadcast(request):
    not_send_to = []
    send_to = []
    if request.method == 'POST':
        search_type = request.POST.get('search_type', None)
        st = request.POST.get('st', None)
        ibo = request.POST.get('ibo', None)
        los = request.POST.get('los', None)
        pin_equality = request.POST.get('pin_equality', None)
        pin_level = request.POST.get('pin_level', None)
        subject = request.POST.get('subject', None)
        email = request.POST.get('email', None)
        if search_type and search_type == 'st' and st:
            users = get_users_for_subscription(product=st)
        elif search_type and search_type == 'los' and los:
            try:
                users = get_downline(User.objects.get(pk=los))
            except:
                users = []
        elif search_type and search_type == 'ibo' and ibo:
            ibo = ibo.split(',')
            users = []
            for i in ibo:
                users.append(i.strip())
        else:
            users = []

        send_to = []
        for u in users:
            if pin_level and pin_level != '-1':
                try:
                    if pin_equality == '1':
                        usr = User.objects.get(id=u, profile_object__pinlevel__eq=pin_level)
                    elif pin_equality == '2':
                        usr = User.objects.get(id=u, profile_object__pinlevel__lte=pin_level)
                    else:
                        usr = User.objects.get(id=u, profile_object__pinlevel__gte=pin_level)
                    send_to.append(usr)
                except:
                    pass
            else:
                try:
                    if search_type and search_type == 'ibo' and ibo:
                        us = User.objects.get(username=u)
                    else:
                        us = User.objects.get(pk=u)

                    if us not in send_to:
                        send_to.append(us)
                except:
                    if u not in not_send_to:
                        not_send_to.append(u)

        connection = get_connection()
        for us in send_to:
            if us.email:
                msg = EmailMultiAlternatives(subject, '', settings.DEFAULT_FROM_EMAIL, [us.email],
                    connection=connection)
                msg.attach_alternative(email, "text/html")
                msg.send()

    subscriptions = SubscriptionProduct.objects.values('product__id', 'product__name').all()
    groups = Group.objects.all()
    leaders = User.objects.filter(profile_object__pinlevel__gt=1)
    return render(request, 'support/boradcast.html',
        {'title': 'Broadcast', 'groups': groups, 'leaders': leaders, 'subscriptions': subscriptions,
         'not_send_to': not_send_to, 'send_to': send_to})


def search_users(q, limit=20):
    results = []

    # first we look to match if this is an IBO number
    if is_int(q):
        for profile in Profile.objects.filter(ibo_number=q):
            results.append(profile.user)

    # user users
    qs = User.objects.all()
    qs = perform_search(qs,
        ['username', 'first_name', 'last_name', 'email'],
        q)

    for user in qs.distinct():
        results.append(user)

    return results


def search_orders(q, limit=20):
    results = []

    # first we look to match if this is an IBO number
    if is_int(q):
        for order in Order.objects.filter(pk=q):
            results.append(order)

    # user users
    qs = Order.objects.all()
    qs = perform_search(qs, ['pk', ], q)

    for order in qs.distinct():
        results.append(order)

    return results


def search_checkins(q, limit=20):
    results = []

    # first we look to match if this is an IBO number
    if is_int(q):
        for checkin in Attending.objects.filter(pk=q):
            results.append(checkin)

    # user users
    qs = Attending.objects.all()
    qs = perform_search(qs, ['pk', ], q)

    for checkin in qs.distinct():
        results.append(checkin)

    return results


@login_required
def home_old(request):
    params = {}
    params['title'] = 'Customer Support Dashboard'
    return render(request, 'support/home.html', params)


@login_required
def home(request):
    params = {}
    params['title'] = 'Dashboard'
    return render(request, 'support/home_v2.html', params)


@login_required
def list(request):
    return render(request, 'support/list.html', {'title': 'Support'})


# MRVG: Modifications made by me

@login_required
def register_ibo(request, template_name='support/support_registration.html'):
    """
    First check that the IBO is not in use already
    """
    if request.method == 'POST':
        form = RegisterStep1Form(request.POST)

        if form.is_valid():
            user = form.save(send_email=False)
            return redirect(reverse('support_user_data', args=[user.pk]))
    else:
        form = RegisterStep1Form()

    return render(request, template_name, {'form': form})


@login_required
def register_user_data(request, user_id, template_name='support/support_registration.html', form=RegisterStep2Form):
    """
    Now.. after creating the user and verifyin the IBO preload basic user information
    """
    user = get_object_or_404(User, pk=user_id)

    if request.method == 'POST':
        form = form(request.POST)

        if form.is_valid():
            form.save(user)
            return redirect(reverse('support_user_subscription', args=[user.pk]))
    else:
        form = form()

    return render(request, template_name, {
        'form': form,
        'member': user,
    })


@login_required
def register_user_subscription(request, user_id, template_name='support/support_registration.html',
                               form=RegisterStep3Form):
    """
    Finally register a subscription for the user. MAke this optional I think.. not sure this is necessary
    """
    user = get_object_or_404(User, pk=user_id)
    if request.method == 'POST':
        form = form(request.POST)
        if form.is_valid():
            form.save(user)
            return redirect(reverse('support_profile', args=[user.pk]))
    else:
        form = form()

    return render(request, template_name, {
        'form': form,
        'member': user,
    })

# MRVG: End modifications

@login_required
def OLD_profile(request, user_id):
    params = {}
    params['user_query'] = user_id
    member = get_object_or_404(User, pk=int(user_id))
    params['member'] = member
    params['title'] = 'Customer Profile - %s %s' % ( member.get_full_name(), member.profile.ibo_number )

    if request.method == 'POST':
        if request.POST.get('note'):
            CustomerNote(
                user=member,
                created_by=request.user,
                note=request.POST.get('note'),
            ).save()

            messages.success(request, 'Saved a customer note.')

    elif request.method == 'GET':
        if request.GET.get('note_id_to_delete'):
            try:
                note = CustomerNote.objects.get(pk=request.GET.get('note_id_to_delete'))
                note.delete()
            except:
                pass

            messages.success(request, 'Just deleted a customer note.')

    return render(request, 'support/view_profile.html', params)


@login_required
def virtual(request, user_id):
    # Set the session value of the current admin user
    passed_admin_user_id = request.user.id
    params = {}

    member = get_object_or_404(User, pk=int(user_id))

    hash = hashlib.md5()
    hash.update("%s:%s:%s" % ('user', user_id, settings.ADMIN_HASH_SECRET))
    request_hash = request.GET.get("hash", "")
    if request_hash != hash.hexdigest().upper():
        raise Exception("invalid hash value")

    member = authenticate(username=member.username, password=settings.ADMIN_HASH_SECRET)
    login(request, member)

    request.session["admin_user_id"] = passed_admin_user_id
    request.session["django_language"] = member.profile.language
    return redirect('/')


@login_required
def virtual_order(request, user_id):
    # Set the session value of the current admin user
    passed_admin_user_id = request.user.id

    # First we have to login the user
    params = {}
    member = get_object_or_404(User, pk=int(user_id))
    hash = hashlib.md5()
    hash.update("%s:%s:%s" % ('user', user_id, settings.ADMIN_HASH_SECRET))
    request_hash = request.POST.get("hash", "")
    if request_hash != hash.hexdigest().upper():
        raise Exception("invalid hash value")

    member = authenticate(username=member.username, password=settings.ADMIN_HASH_SECRET)
    login(request, member)

    # Now that the user is logged in, add the order items to their cart.
    for x in range(int(request.POST.get("num_added", 0))):
        formdata = request.POST
        remove = formdata.get("remove%_%s % x", None)
        if not remove:
            productslug = formdata["productname_%s" % x]
            quantity = int(formdata["quantity_%s" % x])
            product = Product.objects.get(slug=productslug)
            cart = Cart.objects.from_request(request, create=True)
            details = []
            formdata = request.POST
            satchmo_cart_details_query.send(
                cart,
                product=product,
                quantity=quantity,
                details=details,
                request=request,
                form=formdata
            )
            added_item = cart.add_item('products', 'product', product.pk, number_added=quantity, details=details)
            satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item,
                product=product, request=request, form=formdata)

    # Set session variables for logging back in later
    # admin_hash = hashlib.md5()
    # admin_hash.update("%s:%s:%s" % ('user', passed_admin_user_id, settings.ADMIN_HASH_SECRET))

    request.session["admin_user_id"] = passed_admin_user_id
    # request.session["admin_hash"] = admin_hash

    return HttpResponseRedirect(reverse("checkout"))


@login_required
def virtual_ticket_order(request, user_id):
    # Set the session value of the current admin user
    passed_admin_user_id = request.user.id

    # First we have to login the user
    params = {}
    member = get_object_or_404(User, pk=int(user_id))
    hash = hashlib.md5()
    hash.update("%s:%s:%s" % ('user', user_id, settings.ADMIN_HASH_SECRET))
    request_hash = request.POST.get("hash", "")
    if request_hash != hash.hexdigest().upper():
        raise Exception("invalid hash value")

    member = authenticate(username=member.username, password=settings.ADMIN_HASH_SECRET)
    login(request, member)

    # Now that the user is logged in, add the order items to their cart.
    for x in range(int(request.POST.get(u"num_events_added", 0))):
        formdata = request.POST
        remove = formdata.get("remove%_%s % x", None)
        if not remove:
            productslug = formdata["productname_%s" % x]
            quantity = int(formdata["quantity_%s" % x])
            convention = Convention.objects.get(slug=productslug)
            cart = Cart.objects.from_request(request, create=True)
            details = []
            formdata = request.POST
            # satchmo_cart_details_query.send(
            #     cart,
            #     product=product,
            #     quantity=quantity,
            #     details=details,
            #     request=request,
            #     form=formdata
            # )
            added_item = cart.add_item('planning', 'convention', convention.pk, number_added=quantity, details=details)
            # added_item = cart.add_item('products', 'product', product.pk, number_added=quantity, details=details)
            # satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item,
            #     product=product, request=request, form=formdata)

    # Set session variables for logging back in later
    # admin_hash = hashlib.md5()
    # admin_hash.update("%s:%s:%s" % ('user', passed_admin_user_id, settings.ADMIN_HASH_SECRET))

    request.session["admin_user_id"] = passed_admin_user_id
    # request.session["admin_hash"] = admin_hash

    return HttpResponseRedirect(reverse("checkout"))


def re_login_admin_user(request, admin_user_id):
    follow = request.GET.get('follow', None)
    admin_user_id = request.session.get("admin_user_id", None)
    if admin_user_id:
        member = get_object_or_404(User, pk=int(admin_user_id))
        member = authenticate(username=member.username, password=settings.ADMIN_HASH_SECRET)
        login(request, member)
        if follow:
            return HttpResponseRedirect(follow)
        else:
            return HttpResponseRedirect(reverse("support_home"))
    else:
        return HttpResponseRedirect('/')


@login_required
def search(request, search_argument='q'):
    params = {}
    params['title'] = 'Customer Support Dashboard, Search Results'

    search_query = request.GET.get(search_argument, None)
    if search_query:
        params['search_query'] = search_query
        params['search'] = search_users(search_query)
        params['search_orders'] = search_orders(search_query)

    return render(request, 'support/search_results.html', params)


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


@login_required
def ajax(request):
    result = {}
    act = request.POST.get('action')

    if act == 'update':
        user_id = request.POST.get('user_id')
        user = User.objects.get(pk=user_id)
        value = request.POST.get('value')
        key = request.POST.get('key').split('.')

        if key[0] == 'user':
            if key[1] == 'password':
                user.set_password(value)
            else:
                setattr(user, key[1], value)

            user.save()
            generated_comment = 'Updated user %s' % key[1]

        elif key[0] == 'profile':
            profile = user.profile
            setattr(profile, key[1], value)
            profile.save()

            generated_comment = 'Updated user\'s profile field %s' % key[1]

        result = {
            'user_id': user_id,
            'key': key, #'.'.join(key),
            'value': value,
        }

        Activity(
            user=user,
            performed_by=request.user,
            flag=Activity.CHANGE,
            generated_comment=generated_comment,
            comment=request.POST.get('comment'),
        ).save()

    return HttpResponse(simplejson.dumps(result))


@login_required
def ajax_search(request):
    q = request.GET.get('q')
    limit = int(request.GET.get('limit', 20))
    results = []
    #results = {}

    # first we look to match if this is an IBO number
    if is_int(q):
        for profile in Profile.objects.filter(ibo_number=q):
            results.append(profile.getJSON())
            #results[ profile.ibo_number ] = profile.get_ambiguous_title

    # user users
    qs = User.objects.all()
    qs = perform_search(qs,
        ['username', 'first_name', 'last_name', 'email'],
        q)

    for user in qs.distinct():
        results.append(user.profile.getJSON())
        #results[ user.profile.ibo_number ] = user.profile.get_ambiguous_title

    return HttpResponse(simplejson.dumps(results))


@login_required
def ajax_dashboard(request):
    """
    Updates the dashboard position user the user profile section.

    """
    # TODO: make it work
    results = {'changed': True}
    return HttpResponse(simplejson.dumps(results))


@login_required
def ajax_product_search(request):
    from marvin.store.products.models import Product

    q = request.REQUEST.get('q')
    results = []

    qs = Product.objects.all()
    qs = perform_search(qs, ['name', ], q)

    for product in qs.distinct():
        results.append({
            'name': product.name,
            'sku': product.sku,
            'id': product.id,
            'unit_price': product.unit_price_display,
        })

    return HttpResponse(simplejson.dumps(results))


@login_required
def ajax_event_search(request):
    from marvin.addons.planning.models import Convention

    q = request.REQUEST.get('q')
    results = []

    qs = Convention.objects.all()
    qs = perform_search(qs, ['name'], q)

    for convention in qs.distinct():
        results.append({
            'name': convention.name,
            'sku': convention.sku,
            'id': convention.id,
            'unit_price': convention.unit_price_display,
        })

    return HttpResponse(simplejson.dumps(results))


@login_required
def ajax_cart_event_add(request):
    from marvin.addons.planning.models import Convention

    pk = request.REQUEST.get('convention')
    convention = get_object_or_404(Convention, pk=pk)

    results = {
        'convention': convention.getJSON(),
    }

    return HttpResponse(simplejson.dumps(results))


@login_required
def ajax_cart_add(request):
    from marvin.store.products.models import Product

    pk = request.REQUEST.get('product')
    product = get_object_or_404(Product, pk=pk)

    results = {
        'product': product.getJSON(),
    }

    return HttpResponse(simplejson.dumps(results))


@login_required
def check_upline(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return redirect("support_profile", user_id=user_id)

    upline_ibo = get_upline_ibo(int(user.username))
    if upline_ibo:
        user.upline_ibo = upline_ibo
        user.save()
        try:
            position = Position.objects.get(user=user)
        except Position.DoesNotExist:
            position = Position(user=user, auto_update=True)
        position.tree = Tree.objects.get(id=1)
        position.upline = user.get_by_aid(user.upline_ibo)
        position.save()
        logger.debug("Downline check successful for ibo %s" % upline_ibo)
    else:
        logger.debug("Upline check failed for user id %s" % str(user_id))

    return redirect("support_profile", user_id=user_id)


@login_required
def check_downline(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return redirect("support_profile", user_id=user_id)

    for p in user.downline.all():
        upline_ibo = get_upline_ibo(int(p.user.username))
        if upline_ibo:
            new_user = User.objects.get(username=int(upline_ibo))
            new_user.upline_ibo = upline_ibo
            new_user.save()
            try:
                position = Position.objects.get(user=new_user)
            except Position.DoesNotExist:
                position = Position(user=new_user, auto_update=True)
            position.tree = Tree.objects.get(id=1)
            position.upline = new_user.get_by_aid(new_user.upline_ibo)
            position.save()
            logger.debug("Downline check successful for ibo %s" % upline_ibo)
        else:
            logger.debug("Downline check failed for user id %s" % str(user_id))

    return redirect("support_profile", user_id=user_id)
