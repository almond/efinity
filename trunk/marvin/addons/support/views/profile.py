from django.http import HttpResponseRedirect
from django.views.decorators.http import require_GET, require_POST
from marvin.addons.support.forms import *
from marvin.addons.support.models import *
from marvin.addons.support.utils import *
from marvin.core.accounts.models import Activity
from marvin.store.payments.forms import CreditPayShipForm
from marvin.addons.genealogy.models import Position
from marvin.utils.dbsettings.functions import config_get_group
from marvin.utils.view import *


@staff_only
def view(request, user_id, template='support/view_profile.html'):
    user = get_object_or_404(User, pk=user_id)

    if request.method=='POST':
        new_note=request.POST.get('new_note', None)
        if new_note:
            CustomerNote(created_by=request.user, user=user, note=new_note).save()
            return HttpResponseRedirect('/user/support/profile/%s/'%(user_id))
    else:
        if 'delete_note' in request.GET:
            CustomerNote.objects.filter(pk=request.GET.get('delete_note'), created_by=request.user).delete()
            return HttpResponseRedirect('/user/support/profile/%s/'%(user_id))

    NewNote = CustomerNote(created_by=request.user, user=user)
    user_form = UserForm(instance=user)
    profile_form = ProfileForm(instance=user.profile)
    phones_form = PhonesFormset(instance=user)
    addresses_form = AddressFormset(instance=user)
    contacts_form = ContactFormset(instance=user)
    notes_form = CustomerNoteForm(instance=NewNote)
    position_form = PositionForm(instance=user.position.all()[0])

    notes=CustomerNote.objects.filter(user=user)
    # TODO
    # pay_module = config_get_group('PAYMENT_AUTHORIZENET')
    # cc_form = CreditPayShipForm(request, pay_module)

    return render(request, template, {
        'notes':notes,
        'member': user,
        'title': t('Customer Profile'),
        'user_form': user_form,
        'profile_form': profile_form,
        'phones_form': phones_form,
        'addresses_form': addresses_form,
        'contacts_form': contacts_form,
        'notes_form': notes_form,
        'position_form': position_form
    })


@staff_only
@require_POST
def update(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    user_form = UserForm(data=request.POST, instance=user)
    profile_form = ProfileForm(data=request.POST, instance=user.profile)

    if user_form.is_valid() and profile_form.is_valid():
        user_form.save()
        profile_form.save()
        user.activity_change('Update user profile.', request.POST.get('comment'), request.user)
        return ajax_render({'messages': [ unicode(t('Success!')), ]})
    else:
        errors = {}
        errors.update(form_errors(user_form))
        errors.update(form_errors(profile_form))
        return ajax_render({'errors': errors})

@staff_only
@require_POST
def update_position(request):
    user = User.objects.get(pk=int(request.POST["user"]))
    position_form = PositionForm(data=request.POST, instance=user.position.all()[0])
    if position_form.is_valid():
        # Get the correct position and try to update it
        pos = user.position.all()[0]
        pos.auto_update_team = position_form.cleaned_data["auto_update_team"]
        pos.save()
        return ajax_render({"messages": [ unicode(t("Success!")), ]})
    else:
        errors = {"auto_update_team": position_form.errors }
        return ajax_render({"errors": errors})

@staff_only
@require_POST
def update_inline(request, user_id, formset, generated_comment, success_msg = t('Success!')):
    user = get_object_or_404(User, pk=user_id)
    # import pdb; pdb.set_trace()
    form = formset(request.POST, request.FILES, instance=user)

    if form.is_valid():
        form.save()
        user.activity_change(generated_comment, request.POST.get('comment'), request.user)
        return ajax_render({ 'messages': [ unicode(success_msg), ] })
    else:
        return ajax_render({ 'errors': form_errors(form, is_formset=True) })


@staff_only
@require_POST
def submit(request, user_id, form, generated_comment, success_msg = t('Success!')):
    user = get_object_or_404(User, pk=user_id)
    form = form(request.POST, request.FILES)

    if form.is_valid():
        form.save()
        user.activity_change(generated_comment, request.POST.get('comment'), request.user)
        return ajax_render({ 'messages': [ unicode(success_msg), ] })
    else:
        return ajax_render({ 'errors': form_errors(form) })
