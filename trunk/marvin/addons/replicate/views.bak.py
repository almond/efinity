from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm as AuthUserCreationForm
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from django.utils import simplejson


from django.utils.http import int_to_base36
from django.template import loader
from django.core.mail import send_mail

from utils.app import *
from accounts.models import Profile, Group
from accounts.forms import UserCreationForm, CustomUserFieldsForm
from replicate.models import Page

def default(request):
	if request.user.is_authenticated():
		return redirect(reverse('dashboard.views.home'))

	return render(request, 'public/index.html', {
		'login': AuthenticationForm(),
		'register': UserCreationForm(),
		})


@verify_username
def homepage(request, sponsor):
	can_edit = False
	if sponsor == request.user and request.GET.get('edit'):
		can_edit = True

	return render(request, 'public/replicate/index.html', {
		'sponsor': sponsor,
		'sponsor_profile': sponsor.get_profile(),
		'editable': can_edit,
		'tag_line': sponsor.content_section('tag_line', can_edit),
		'splash': sponsor.content_section('splash', can_edit),
		'splash2': sponsor.content_section('splash2', can_edit),
	})


@verify_username
def register(request, sponsor):
	if request.method == 'POST':
		form = UserCreationForm( data = request.POST.copy() )
		if form.is_valid():
			(user, profile) = form.save()
			profile.sponsor = sponsor
			profile.save()

			return redirect('/%s/register_success' % sponsor.username)
	else:
		form = UserCreationForm()

	return render(request, 'public/replicate/register.html', {
		'sponsor': sponsor,
		'sponsor_profile': sponsor.get_profile(),
		'username': sponsor.username,
		'login': AuthenticationForm(),
		#'register': CustomUserFieldsForm(),
		'standard': form,
		#'groups': Group.objects.all().filter(listed = True),
		})


@verify_username
def page(request, sponsor, slug):
	try:
		return render(request, 'public/replicate/%s.html' % slug, {
			'sponsor': sponsor,
			'sponsor_profile': sponsor.get_profile(),

			'tag_line': sponsor.content_section('tag_line'),
			'splash': sponsor.content_section('splash'),
		})
	except:
		raise Http404
