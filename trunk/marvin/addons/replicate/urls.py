from django.conf.urls.defaults import *

urlpatterns = patterns('marvin.addons.replicate.views',

    (r'^(?P<sponsorUsername>[^/]+)/$', 'home', {}, 'replicate_index'),
	(r'^(?P<sponsorUsername>[^/]+)/(?P<page>\w+)/$', 'page', {}, 'replicate_page'),
    (r'^(?P<sponsorUsername>[^/]+)/(?P<page>[^/]+)/(?P<accessKey>[^/]+)/$', 'private', {}, 'replicate_private'),

)
