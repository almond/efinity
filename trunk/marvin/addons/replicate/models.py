from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from marvin.utils import uuid

class ReplicatedSite(models.Model):
    '''
    Stores information for replicated websites
    '''

    ACCESS_BY_OPTIONS = (
        ('username', _('Username')),
        ('userid', _('User ID')),
        ('rand', _('Random String')),
    )

    def __unicode__(self):
        return self.name

    name = models.CharField(max_length=255)
    site = models.ForeignKey(Site, help_text='Select the domain you want this to be replicated with. If using the same domain for replication enter a slug below to have multi-site replication support.')
    slug = models.SlugField(blank=True, help_text='Enter a slug if you are using the same domain for multiple replicated sites.')

    access_by = models.CharField(max_length=20, choices=ACCESS_BY_OPTIONS, help_text=_("Select how you would like to allow access to this replicated site, using one of these within the URL. Example: If username, the replicated site might be at http://example.com/username"))

    description = models.TextField(blank=True, help_text='Optional. Description of what this website is used for.')

    is_public = models.BooleanField(_('public'), default=True, help_text='If this is a public site then it will be accessible by anonymous guests. If not then this is only visible by invite urls and invite emails.' )
    is_active = models.BooleanField(_('active'), default=True, help_text='If active then replication will work.')


class PromoInvite(models.Model):

    def __unicode__(self):
        #return '%s invited by %s' % ( str( self.name ), str( self.sponsor ) )
        return '%s invited' % ( str( self.name ) )

    rand = models.CharField(max_length=32, primary_key=True, unique=True)

    replicated_site = models.ForeignKey( ReplicatedSite, blank=True, null=True )
    user = models.ForeignKey(User, related_name='invites')

    name = models.CharField(max_length=100)
    email = models.EmailField()

    visited = models.DateTimeField( blank = True, null = True )
    created = models.DateTimeField( auto_now_add = True )
    updated = models.DateTimeField( auto_now = True )
    
    view_count = models.IntegerField( default = 0 ) 

    def save(self):
        if not self.rand:
            self.rand = str(uuid.uuid4()).replace('-', '')
        super(PromoInvite, self).save()

    @property
    def url(self):
        if self.replicated_site:
            return 'http://%s/r/%s/%s/%s' % ( self.replicated_site.site.domain, self.user.username, self.replicated_site.slug, self.rand )

        else:
            return 'http://www.myefinity.com/r/%s/promo/%s' % ( self.user.username, self.rand )
