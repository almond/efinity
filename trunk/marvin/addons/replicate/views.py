from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect
from marvin.addons.mediaupload.models import Promo, PromoUser
from marvin.addons.replicate.models import PromoInvite
from marvin.core.contacts.models import Contact
from marvin.utils.app import *
import datetime

#from marvin.addons.replicate.models import Page

def default(request, *args, **kwargs):
    if request.user.is_authenticated() and request.user.is_staff:
        return redirect(reverse('marvin.addons.support.views.home'))

    elif request.user.is_authenticated():
        return redirect(reverse('marvin.core.dashboard.views.index'))
    
    return render(request, 'public/index.html', {
        #'login': AuthenticationForm(),
        #'register': UserCreationForm(),
    })

@verify_username
def home(request, sponsor):
    can_edit = False
    if sponsor == request.user and request.GET.get('edit'):
        can_edit = True

    return render(request, 'replicate/index.html', {
        'sponsor': sponsor,
        #'sponsor_profile': sponsor.get_profile(),
        'editable': can_edit,
        'tag_line': sponsor.content_section('tag_line', can_edit),
        'splash': sponsor.content_section('splash', can_edit),
        'splash2': sponsor.content_section('splash2', can_edit),
    })

@verify_username
def page(request, sponsor, page):
    pass

@verify_username
def private(request, sponsor, page, accessKey):
    editable = False
    
    if request.user == sponsor:
        if request.GET.get('edit'):
            editable = True

    # if the user isn't the user who has the promo page,
    # then we are displaying the promo page - see if they
    # have access rights
    else:

        try:
            twodays=datetime.datetime.now()-datetime.timedelta(hours=47, minutes=59, seconds=59)
            invite = PromoInvite.objects.get(user = sponsor, rand = accessKey, created__gte=twodays, view_count__lte=4)
            invite.view_count += 1
            invite.save()
        except PromoInvite.DoesNotExist:
            raise Http404 #This link doesnot exists or has expired message


    """
    try:
        user_data = Contact.objects.get(user=sponsor.id)
    except Contact.DoesNotExist:
        user_data = None
    """

    user_data = Contact.objects.filter(user=sponsor.id)
    if len(user_data)>0:
        user_data=user_data[0]
    else:
        user_data=None


    # Return all media that the user can select
    #user_medias = Media.objects.filter(mediauser__user__exact=request.user)
    user_medias = PromoUser.objects.filter(user=sponsor)
    medias = Promo.objects.filter(is_published = True, is_featured = True)

    return render(request, 'private/promo.html', {
        'editable': editable,
        'tag_line': sponsor.content_section('tag_line', editable),
        'about_me': sponsor.content_section('about_me', editable),
        'user_data': user_data,
        'user_medias': user_medias,
        'medias': medias,
    })
