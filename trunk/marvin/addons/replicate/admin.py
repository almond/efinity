from django.contrib import admin
from models import *

class ReplicatedSiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'site', 'slug', 'is_public', 'is_active', )
    ordering = ('name',)
    search_fields = ('name',)
    list_filter = ('is_public', 'is_active')

admin.site.register( ReplicatedSite, ReplicatedSiteAdmin )

