from django import forms

from models import Message

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        exclude = ('performed_by', )


class SupportMessageForm(forms.Model):
    class Meta:
        model = Message
        a
