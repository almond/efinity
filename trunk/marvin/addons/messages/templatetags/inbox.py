from django.template import Library, Node

class InboxOutput(Node):
    def render(self, context):
        try:
            user = context['user']
            count = user.received_messages.filter(read_at__isnull=True, recipient_deleted_at__isnull=True).count()
        except (KeyError, AttributeError):
            count = ''
        return "%s" % (count)        
        
def do_print_inbox_count(parser, token):
    """
    A templatetag to show the unread-count for a logged in user.
    Prints the number of unread messages in the user's inbox.
    Usage::
        {% load inbox %}
        {% inbox_count %}
     
    """
    return InboxOutput()

class UpcomingEventsNode(Node):
    def render(self, context):
        try:
            user = context['user']
            context['inbox_messages'] = user.received_messages.all()
        except:
            pass
        return ''

def get_inbox_messages(parser, token):
    return UpcomingEventsNode()


register = Library()     
register.tag('inbox_count', do_print_inbox_count)
register.tag('get_inbox_messages', get_inbox_messages)
