import os
import logging
import decimal

##################################################
# This will enable media and static files to be
# served through django and not through the
# web server. This is slow and not recommended.
if os.getenv("LOGNAME") == "axolote" or os.getenv("LOGNAME") == "machin":
    LOCAL_DEV = True
    DEBUG = True
else:
    LOCAL_DEV = False
    DEBUG = False

# Amway API credentials
API_USER_ID = "USJQ1101"
API_PASSWORD = "jU37PoMs5H"
API_URL = "https://naservices.amway.com/Services/LOAValidation/LOAValidation.svc?WSDL"

TEMPLATE_DEBUG = DEBUG

if not LOCAL_DEV:
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s %(message)s',
        filename=os.path.join(os.path.dirname(__file__), "..", "..", "logs", "app.log"),
        filemode='w',
    )
else:
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s %(message)s',
        filename=os.path.join(os.path.dirname(__file__), "..", "logs", "local_dev.log"),
        filemode='w',
    )

DIRNAME = os.path.dirname(__file__)
WEBSITE = "myefinity.com"
EXCHANGE_AMOUNT = decimal.Decimal("1.0")

# Which instance is this? eFinity or DR?
if "efinityrd.com" in DIRNAME:
    WEBSITE = "efinityrd.com"
    # Now we define the commissions magic number
    EXCHANGE_AMOUNT = decimal.Decimal("40.0")
elif "myefinity.com" in DIRNAME:
    WEBSITE = "myefinity.com"
    # Now we define the commissions magic number
    EXCHANGE_AMOUNT = decimal.Decimal("1.0")
elif "efinity_stage" in DIRNAME:
    WEBSITE = "staging"
    EXCHANGE_AMOUNT = decimal.Decimal("1.0")
    DEBUG = True


ADMINS = (
    #('Almond Admin', 'admin@almondev.com'),
    ('Mario R. Vallejo', 'mario.r.vallejo@gmail.com'),
)

MANAGERS = ADMINS

# I don't feel like writing a router
if os.getenv("LOGNAME") == "axolote":
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'myefinity',
            'USER': 'root',
            'PASSWORD': '',
        },
    }

elif WEBSITE == "efinityrd.com":
    DATABASES = {
        'default': {
            "ENGINE": 'django.db.backends.mysql',
            "NAME": 'efinityrd',
            "USER": "efinityrd",
            "PASSWORD": "550604721",
        },
    }

elif WEBSITE == "staging":
    DATABASES = {
        'default': {
            "ENGINE": 'django.db.backends.mysql',
            "NAME": 'efinity_stage',
            "USER": "myefinity",
            "PASSWORD": "550604721",
        },
    }

elif WEBSITE == "myefinity.com":
    DATABASES = {
        'default': {
            "ENGINE": 'django.db.backends.mysql',
            "HOST": "",
            "NAME": 'myefinity',
            "USER": "myefinity",
            "PASSWORD": "550604721",
        },
    }
    #DATABASES = {
        #'default': {
            #"ENGINE": 'django.db.backends.mysql',
            #"HOST": "75.126.69.220",
            #"NAME": 'efinity_skinny',
            #"USER": "root",
            #"PASSWORD": "sqlt00Rw0rd",
        #},
    #}

FILE_UPLOAD_TEMP_DIR = os.path.join(DIRNAME, 'tmp')

TIME_ZONE = 'America/New_York'

if WEBSITE == "efinityrd.com":
    LANGUAGE_CODE = 'en-mx'
elif WEBSITE == "myefinity.com":
    LANGUAGE_CODE = "en-us"
SITE_ID = 1
USE_I18N = True
USE_L10N = True

MEDIA_ROOT = os.path.join(DIRNAME, '..', '..', 'media')
STATIC_ROOT = os.path.join(DIRNAME, '..', '..', 'static')

MEDIA_URL = 'https://s3.amazonaws.com/efinity_files/'
STATIC_URL = '/static/'

VENDOR_URL = '/static/vendor/'
# ADMIN_MEDIA_PREFIX = 'http://efinity_files.almondev.org/static/admin/'
SECRET_KEY = 'tn&tycvl45xe4@ap!zw6hj6z*@+k_#t^yzy0fkr9ru4jh*9p6='

#AUTH_PROFILE_MODULE = 'accounts.Profile'

gettext = lambda s: s

##################################################
# Cross Market Settings
LANGUAGES = (
    ('en', gettext('English')),
    ('es', gettext('Spanish')),
)

USE_AID = True
AID_TITLE = gettext('IBO Number')
DECIMAL_SEPARATOR = "."
USE_L10N = False


##################################################
# Session settings
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

PAGINATOR_PER_PAGE = 24
PAGINATOR_PAGE_RANGE = 3

##################################################
# Cache settings
CACHE_PREFIX = "efinity"
CACHE_TIMEOUT = 180

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'cache_table_efinity',
    }
}

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    )

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.doc.XViewMiddleware',

    #'marvin.middleware.domains.DomainMiddleware',
    'marvin.middleware.secure.SSLRedirect',
    'marvin.middleware.threaded.ThreadLocalMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    'django.core.context_processors.request',
    "django.contrib.messages.context_processors.messages",
    "marvin.core.accounts.context_processors.settings_variables",
    )

ADMIN_HASH_SECRET = '123qweasdzxc123qweasdzxc123qweasdzxc'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'marvin.addons.support.authenticator.LoginAsUserBackend',
    )

ROOT_URLCONF = 'marvin.urls'

TEMPLATE_DIRS = (
    os.path.join(DIRNAME, 'templates'),
    )

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    )

STATICFILES_DIRS = (
    os.path.join(DIRNAME, 'static'),
    )

INSTALLED_APPS = (
    ### django apps
    'django.contrib.admin',
    #'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.comments',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.messages',
    'django.contrib.sessions',
    #'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.webdesign',
    'django.contrib.humanize',
    'django.contrib.staticfiles',

    ### third party apps
    # 'efinity',
    # 'payible',
    # 'south',
    'l10n',

    ### utils
    #'threaded_multihost',
    'marvin.utils',  # Enables utils template tags
    'marvin.utils.keyedcache',  # Helps with caching content
    'marvin.utils.dbsettings',  # Handles admin settings stored in database
    'marvin.utils.thumbnail',  # Handles creating and displaying thumbnails
    'marvin.trans',  # Enables model translations
    'marvin.app_plugins',

    ### almond
    'marvin.almond.customers',

    ### cross market basics
    'marvin.core.accounts',
    'marvin.core.contacts',
    'marvin.core.cms',

    'marvin.core.notifications',  # Enables notifications and email alerts
    'marvin.addons.messages', # Enables user-to-user messaging

    ### cross market store
    'marvin.store.shop', # Store and Cart features
    'marvin.store.products', # Product management
    'marvin.store.products.modules.configurable', # Enables advance config
    'marvin.store.products.modules.downloadable', # Enables digital products
    'marvin.store.products.modules.subscription', # Enables subscriptions
    #    'marvin.store.products.modules.membership',     # Enables memberships
    'marvin.store.shipping', # Shipping handleing
    'marvin.store.shipping.modules.ups',
    'marvin.store.tax', # Taxes
    'marvin.store.payments', # integrates with the payible payment processor
    'marvin.store.dist',

    ### cross market prayment processor
    # 'marvin.store.payments.modules.dummy',
    'marvin.store.payments.modules.authorizenet',
    'marvin.store.payments.modules.authorizenetconventions',
    'marvin.store.payments.modules.cod',
    'marvin.store.payments.modules.cheque',

    ### cross market affiliate addons
    'marvin.addons.replicate', # Add replicated sites addon
    'marvin.core.dashboard',
    'marvin.addons.genealogy', # Add genealogy structure and views
    'marvin.addons.compensation',
    'marvin.addons.planning',
    'marvin.addons.support', # Adds support module
    'marvin.addons.mediaupload',
    'marvin.addons.reports', # Adds reporting

    ## Enable Tax Modules
    'marvin.store.tax.modules.no', # No Tax
    'marvin.store.tax.modules.area', # Area Tax (per-state)
    'marvin.store.tax.modules.percent', # Percntile Tax
    'marvin.store.tax.modules.us_sst', # US, Streamlined Sales Tax

    ## Enable Shipper Modules
    'marvin.store.shipping.modules.per',
    'marvin.store.shipping.modules.usps',
    'marvin.store.shipping.modules.flat',

    # Stuff needed for the new subcription migrations
    'marvin.cep_migration',

    # WYSIWYG for admin text fields
    'django_wysiwyg',

    # S3
    'boto',
    'storages',
    )

if LOCAL_DEV:
    INSTALLED_APPS += ("gunicorn",)

if DEBUG:
    INTERNAL_IPS = ('127.0.0.1', '0.0.0.0', '192.168.15.46')

    INSTALLED_APPS += (
        'django_extensions',
        'debug_toolbar',
    )

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }

# S3 settings and stuff
DEFAULT_FILE_STORAGE = "storages.backends.s3boto.S3BotoStorage"
AWS_ACCESS_KEY_ID = "AKIAJHLJGMB3UM5MTIDA"
AWS_SECRET_ACCESS_KEY = "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"
AWS_STORAGE_BUCKET_NAME = "efinity_files"
AWS_QUERYSTRING_AUTH = False

DEFAULT_FROM_EMAIL = 'IBOSupport@myefinity.com'
SERVER_EMAIL = 'IBOSupport@myefinity.com'

if "efinitystage" in DIRNAME or DEBUG:
    NOTIFICATION_QUEUE_ALL = False

try:
    from settings_local import *

    DEBUG = True
    TEMPLATE_DEBUG = DEBUG
    MEDIA_URL = '/media/'
    STATIC_URL = '/static/'
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = os.path.join(DIRNAME, "..", "..", "email")  # change this to a proper location
    #should pickup the local email host
except ImportError:
    #EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    #EMAIL_HOST_USER = 'smtp@myefinity.com'
    #EMAIL_HOST = 'mailserver.efinitymail.com'
    #EMAIL_HOST_PASSWORD = '0utB0unD1'
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = os.path.join(DIRNAME, "email_tests") # change thisto a proper location

#Configure logging
# LOGDIR = os.path.abspath(os.path.dirname(__file__))
# LOGFILE = "efinity_satchmo.log"
# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
#                     datefmt='%a, %d %b %Y %H:%M:%S',
#                     filename = '%s/%s' % (DIRNAME,LOGFILE),
#                     filemode = 'w')

#fileLog = logging.FileHandler(os.path.join(DIRNAME, LOGFILE), 'w')
#fileLog.setLevel(logging.DEBUG)
# add the handler to the root logger
#logging.getLogger('').addHandler(fileLog)
# logging.getLogger('keyedcache').setLevel(logging.DEBUG)
# else:
#     logging.getLogger('keyedcache').setLevel(logging.INFO)
logging.info("Satchmo Started")
