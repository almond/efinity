var cm = cm || {};

cm.support = {};

cm.support.search = function() {

	// real-time searching
	new Meio.Autocomplete.Select('search-field', cm.url.support_ajax_search, {
		valueField: $$('#value-field'),
		valueFilter: function(data) {
			return data.identifier;
		},
		urlOptions: {
			queryVarName: 'q',
			max: 20
		},
		requestOptions: {
			/*formatResponse: function(jsonResponse){
                console.log(jsonResponse);
                return jsonResponse;
            },*/
			noCache: true
		},
		filter: {
			type: 'contains',
			path: 'value'
		}
	});

	$$('#searchForm').addEvent('submit', function(event) {
		if (event) event.stop();
		return false;
	});
};

cm.update_user = function(key, value, comment, onSuccess) {
	return cm.ajax.post(cm.url.support_ajax, {
		'action': 'update',
		'user_id': cm.member_id,
		'key': key,
		'value': value,
		'comment': comment
	},
	onSuccess);
};

cm.support.updatable = function() {
    //  This is broken.
    //     var field, input, value, edit, index;
    // $$('td.updatable').each(function(el) {
    //  field = el.getElement('.field').setStyle('display', 'none');
    //  if (el.getElement('.field select') != null) {
    //      index = el.getElement('.field select').selectedIndex;
    //      input = el.getElement('.field select')
    //      value = input[index].innerText;
    //  } else {
    //      input = el.getElement('.field input');
    //          value = input.value;
    //  }       
    //  edit = new Element('div', {
    //      'text': value
    //  });
    //  if (input.get('type') == 'checkbox') {
    //      edit.set('text', input.get('checked') ? 'true' : 'false');
    //  }
    //  el.adopt(edit);
    // 
    //  /*input.addEvent('keyup', function(event){
    //             form.submit();
    //         });*/
    // 
    //  edit.addEvent('click', function(event) {
    //      if (el.retrieve('editing') == true) {
    //          return false;
    //      }
    //      el.store('editing', true);
    // 
    //      edit.setStyle('display', 'none');
    //      field.setStyle('display', 'block');
    //      input.fireEvent('focus').focus();
    //  });
    // 
    //  el.set('title', 'Click to modify');
    // });
};



function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 8;
	var randomstring = '';
	for (var i = 0; i < string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}
	return randomstring;
}

cm.support.password = function() {
	$('#GeneratePassword').click(function() {
		var pass = randomString();
		$('#PasswordResetForm input[type="password"]').val(pass);
		$('.GeneratePasswordDisplayContainer').show();
		$('#GeneratePasswordDisplay').text(pass);
		human_msg.display_msg('The generated password is: ' + pass);
	});

	$('#PasswordResetForm').submit(function() {
		var p1 = $('#password1').val(),
			p2 = $('#password2').val();
		if (p1.length < 6) {
			human_msg.display_msg('The new password is too short.');
		} else if (p1 != p2) {
			human_msg.display_msg('The new passwords do not match.');
		} else {
			comment = prompt('Why do you want to to reset this user\'s password? Hitting OK will reset the password.');
			if (comment) {
				cm.update_user('user.password', p1, comment, function() {
					human_msg.display_msg('Updated customer\'s password.');
				});
			}
		}

		return false;
	});
};

cm.collapsible = function() {
	$$('.collapsible').each(function(el) {

		var ev = function(event) {
			if (event) event.stop();
			var e = el.getElement('.inner');
			console.log(e);
			$(e).toggle('slide');
		};

		el.adopt(
		new Element('div').addClass('collapsible-toggle').set('html', '&mdash;').set('title', 'Toggle Visibility').addEvent('click', ev));
	});
};

cm.support.cart = function() {
	$$('#productSearch').addEvent('keyup', function(event) {
		if (event.key == 'enter') {
			var request = this.retrieve('request');
			if (request && request.running) {
				return false;
			}

			request = cm.ajax.post(cm.url.support_ajax_product_search, {
				'q': this.get('value')
			},
			function(json) {
				$each(json, function(product) {
					$$('#produtSearchResults').adopt(
					new Element('tr').adopt([
					new Element('td').adopt(
					new Element('a', {
						href: product.url,
						target: '_blank',
						title: 'View ' + product.name + ' in store',
						text: product.name
					})), new Element('td').set('text', product.unit_price), new Element('td').adopt(
					new Element('a', {
						href: '#',
						title: 'Add ' + product.name + ' to cart',
						text: 'Add to Cart'
					}).addEvent('click', cm.support.add_to_cart.pass(product)))]));
				});
			});

			this.store('request', request);
		}
	});
};

cm.support.event_search = function() {
    $$('#eventSearch').addEvent('keyup', function(event) {
        if (event.key == 'enter') {
            var request = this.retrieve('request');
            if (request && request.running) {
                return false;
            }

            request = cm.ajax.post(cm.url.support_ajax_event_search, {
                'q': this.get('value')
            },
            function(json) {
                $each(json, function(product) {
                    $$('#eventSearchResults').adopt(
                    new Element('tr').adopt([
                    new Element('td').adopt(
                    new Element('a', {
                        href: product.url,
                        target: '_blank',
                        title: 'View ' + product.name + ' in store',
                        text: product.name
                    })), new Element('td').set('text', product.unit_price), new Element('td').adopt(
                    new Element('a', {
                        href: '#',
                        title: 'Add ' + product.name + ' to cart',
                        text: 'Add to Cart'
                    }).addEvent('click', cm.support.add_event_to_cart.pass(product)))]));
                });
            });

            this.store('request', request);
        }
    });
};

cm.support.num_events_added = 0;
cm.support.add_event_to_cart = function(pk) {
    pk = pk.id || pk; // in case we pass a product object
    cm.ajax.post(cm.url.support_ajax_event_add_to_cart, {
        'convention': pk
    },
    function(json) {
        var html, innerHTML, upCount;
        html =  '<tr class="product-row">';
        html += '<td class="product">'+ json.convention.name;
        html += '<input type="hidden" value="' + json.convention.slug + '" class="productname" id="id_productname" name="productname' + '_' + cm.support.num_events_added + '"></td>';
        html += '<td class="quantity"><input type="text" class="'+ json.convention.slug + '" id="id_quantity" name="quantity' + '_' + cm.support.num_events_added + '" value="1"></td>';
        html += '<td class="remove-item"><input type="checkbox" id="id_remove'+ '_' + cm.support.num_events_added + '" name="remove"></td>';
        html += '</tr>';

        // Remove the empty row.
        if($('#no_items_in_event_cart').length > 0) {
            $('#event_cart').html(html);
            cm.support.num_events_added++;
            $("#num_events_added").val(cm.support.num_events_added);
        } else {
            // Add the new row or update the count, 
            // if add to cart was clicked more than once.
            if ($('.product:contains('+ json.convention.name +')').length == 0) {
                 //innerHTML = $('#event_cart').html() + html;
                 // update the number of rows
		$('#event_cart').append(html);
                 cm.support.num_events_added++;
                 $("#num_events_added").val(cm.support.num_events_added);
            } else {
                upCount = parseInt($('.'+json.convention.slug).attr('value'), 10) + 1;
                $('.'+json.convention.sku).attr('value', upCount);
            }
            //$('#event_cart').html(innerHTML);
        }
        
	});
	return false;
};

// Remove items from cart when the checkbox for that row is clicked.
$('.remove-item #id_remove').live('click', function(){
    $(this).parent().parent().remove();
});

cm.support.num_added = 0;
cm.support.add_to_cart = function(pk) {
	pk = pk.id || pk; // in case we pass a product object
	cm.ajax.post(cm.url.support_ajax_add_to_cart, {
		'product': pk
	},
	function(json) {
        var html, innerHTML, upCount;
        html =  '<tr class="product-row">';
        html += '<td class="product">'+ json.product.name;
        html += '<input type="hidden" value="' + json.product.slug + '" class="productname" id="id_productname" name="productname' + '_' + cm.support.num_added + '"></td>';
        html += '<td class="quantity"><input type="text" class="'+ json.product.slug + '" id="id_quantity" name="quantity' + '_' + cm.support.num_added + '" value="1"></td>';
        html += '<td class="remove-item"><input type="checkbox" id="id_remove'+ '_' + cm.support.num_added + '" name="remove"></td>';
        html += '</tr>';

        // Remove the empty row.
        if($('#no_items_in_cart').length > 0) {
            $('#cart').html(html);
            cm.support.num_added++;
            $("#num_added").val(cm.support.num_added);
        } else {
            // Add the new row or update the count, 
            // if add to cart was clicked more than once.
            if ($('.product:contains('+ json.product.name +')').length == 0) {
                 //innerHTML = $('#cart').html() + html;
                 // update the number of rows
		$('#cart').append(html);
                 cm.support.num_added++;
                 $("#num_added").val(cm.support.num_added);
            } else {
                upCount = parseInt($('.'+json.product.slug).attr('value'), 10) + 1;
                $('.'+json.product.sku).attr('value', upCount);
            }
        }
        
	});
	return false;
};

cm.support.ajax = function() {
	$$('.ajaxForm').each(function(form) {
		form.getElements('.save').addEvent('click', function(event) {
			event.stop();
			form.fireEvent('submit');
		});

		form.addEvent('submit', function(event) {
			if (event) event.stop();

			// Comment Support
			//var comment = prompt('Please comment on what you changed.');
			var comment = 'comment';
			var c = form.getElement('textarea[name="comment"]') || new Element('textarea', {
				'name': 'comment'
			}).inject(form).setStyle('display', 'none');
			c.set('value', comment);

			cm.ajax.post(form.get('action'), form, function(json) {
				if (console && console.log) console.log(json);

				if (json.messages) {
					$each(json.messages, function(msg) {
						human_msg.display_msg(msg);
					});
				}

				if (json.errors) {
					$each(json.errors, function(msg, index) {
						human_msg.display_msg(msg);
					});
				}

			});
			return false;
		});
	});
};

window.addEvent('domready', function() {
	//cm.support.search();
	//cm.support.updatable();
	cm.support.password();
	cm.support.cart();
	cm.support.event_search();
	cm.support.ajax();
	//cm.collapsible();
});
