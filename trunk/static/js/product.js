var crossmarket = crossmarket || {};

crossmarket.slider = function() {
    var first_a;

    if ( $$('.slide').length <= 1 ) {
        return false;
    }

    $$('.slide').each(function(el, i){
        el.setStyle('display', 'none');
        var a;

        new Element('li').adopt(
            a = new Element('a').set('text', '•')
        ).inject( $$('ul.slideNav')[0] );

        a.addEvent('click', function(event){
            if ( event ) event.stop();
            $$('.slide').setStyle('display', 'none');

            $$('ul.slideNav a').removeClass('active');
            this.addClass('active');

            el.setStyle('display', 'block');
            el.getElement('img').position({relativeTo:el});
        });

        if ( i <= 0 ) {
            first_a = a;
        }
    });

    window.addEvent('load', function(){
        first_a.fireEvent('click');
    });
}

window.addEvent('domready', function(){
    crossmarket.slider();
});
