#! /bin/zsh
source `which virtualenvwrapper.sh`
set -e
LOGFILE=/srv/www/stage.myefinity.com/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=1

USER=nobody
GROUP=nobody
workon stage.myefinity.com
cd /srv/www/stage.myefinity.com/app
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn_django -b 0.0.0.0:8001 -w $NUM_WORKERS -t 1200 --user=$USER --group=$GROUP --log-level=debug --log-file=$LOGFILE 2>>$LOGFILE
