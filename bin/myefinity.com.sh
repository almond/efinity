#! /bin/zsh
set -e
LOGFILE=/srv/www/myefinity.com/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=4

USER=almondev
GROUP=almondev
source /home/almondev/.virtualenvs/myefinity.com/bin/activate
cd /srv/www/myefinity.com/app
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn_django -b 0.0.0.0:8001 -w $NUM_WORKERS -t 1200 --user=$USER --group=$GROUP --log-level=debug --log-file=$LOGFILE 2>>$LOGFILE
