#! /bin/zsh
set -e
LOGFILE=/srv/www/efinityrd.com/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=1

USER=nobody
GROUP=nobody
workon efinityrd.com
cd /srv/www/efinityrd.com/app
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn_django -b 0.0.0.0:8002 -w $NUM_WORKERS -t 1200 --user=$USER --group=$GROUP --log-level=debug --log-file=$LOGFILE 2>>$LOGFILE
